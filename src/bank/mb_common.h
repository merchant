/*
  This file is part of Taler
  Copyright (C) 2015-2023 Taler Systems SA

  Taler is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Taler is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Taler; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file bank/mb_common.h
 * @brief Common functions for the bank API
 * @author Christian Grothoff
 */
#ifndef MB_COMMON_H
#define MB_COMMON_H

#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_bank_lib.h"
#include <taler/taler_json_lib.h>


/**
 * Set authentication data in @a easy from @a auth.
 *
 * @param easy curl handle to setup for authentication
 * @param auth authentication data to use
 * @return #GNUNET_OK on success
 */
enum GNUNET_GenericReturnValue
TALER_MERCHANT_BANK_setup_auth_ (
  CURL *easy,
  const struct TALER_MERCHANT_BANK_AuthenticationData *auth);


#endif
