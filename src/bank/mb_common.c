/*
  This file is part of Taler
  Copyright (C) 2015-2023 Taler Systems SA

  Taler is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Taler is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Taler; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file bank/mb_common.c
 * @brief Common functions for the bank API
 * @author Christian Grothoff
 */
#include "platform.h"
#include "mb_common.h"


enum GNUNET_GenericReturnValue
TALER_MERCHANT_BANK_setup_auth_ (
  CURL *easy,
  const struct TALER_MERCHANT_BANK_AuthenticationData *auth)
{
  enum GNUNET_GenericReturnValue ret;

  ret = GNUNET_OK;
  switch (auth->method)
  {
  case TALER_MERCHANT_BANK_AUTH_NONE:
    return GNUNET_OK;
  case TALER_MERCHANT_BANK_AUTH_BASIC:
    {
      char *up;

      GNUNET_asprintf (&up,
                       "%s:%s",
                       auth->details.basic.username,
                       auth->details.basic.password);
      if ( (CURLE_OK !=
            curl_easy_setopt (easy,
                              CURLOPT_HTTPAUTH,
                              CURLAUTH_BASIC)) ||
           (CURLE_OK !=
            curl_easy_setopt (easy,
                              CURLOPT_USERPWD,
                              up)) )
        ret = GNUNET_SYSERR;
      GNUNET_free (up);
      break;
    }
  case TALER_MERCHANT_BANK_AUTH_BEARER:
    {
      if ( (CURLE_OK !=
            curl_easy_setopt (easy,
                              CURLOPT_HTTPAUTH,
                              CURLAUTH_BEARER)) ||
           (CURLE_OK !=
            curl_easy_setopt (easy,
                              CURLOPT_XOAUTH2_BEARER,
                              auth->details.bearer.token)) )
        ret = GNUNET_SYSERR;
      break;
    }
  }
  return ret;
}


/* end of mb_common.c */
