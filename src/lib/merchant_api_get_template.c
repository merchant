/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_template.c
 * @brief Implementation of the GET /templates/$ID request of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /templates/$ID operation.
 */
struct TALER_MERCHANT_TemplateGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TemplateGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /templates/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_TemplateGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_template_finished (void *cls,
                              long response_code,
                              const void *response)
{
  struct TALER_MERCHANT_TemplateGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_TemplateGetResponse tgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  tgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /templates/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const json_t *contract;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("template_description",
                                 &tgr.details.ok.template_description),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_string ("otp_id",
                                   &tgr.details.ok.otp_id),
          NULL),
        GNUNET_JSON_spec_object_const ("template_contract",
                                       &contract),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK ==
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        tgr.details.ok.template_contract = contract;
        tgh->cb (tgh->cb_cls,
                 &tgr);
        TALER_MERCHANT_template_get_cancel (tgh);
        return;
      }
      tgr.hr.http_status = 0;
      tgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) tgr.hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &tgr);
  TALER_MERCHANT_template_get_cancel (tgh);
}


struct TALER_MERCHANT_TemplateGetHandle *
TALER_MERCHANT_template_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *template_id,
  TALER_MERCHANT_TemplateGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_TemplateGetHandle *tgh;
  CURL *eh;

  tgh = GNUNET_new (struct TALER_MERCHANT_TemplateGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/templates/%s",
                     template_id);
    tgh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_template_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_template_get_cancel (
  struct TALER_MERCHANT_TemplateGetHandle *tgh)
{
  if (NULL != tgh->job)
    GNUNET_CURL_job_cancel (tgh->job);
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}
