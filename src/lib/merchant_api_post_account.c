/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_account.c
 * @brief Implementation of the POST /account request
 *        of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include "merchant_api_common.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a POST /private/accounts operation.
 */
struct TALER_MERCHANT_AccountsPostHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_AccountsPostCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP POST /account request.
 *
 * @param cls the `struct TALER_MERCHANT_AccountPostHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_account_finished (void *cls,
                              long response_code,
                              const void *response)
{
  struct TALER_MERCHANT_AccountsPostHandle *aph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_AccountsPostResponse apr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  aph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "POST /accounts completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    apr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    {
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("h_wire",
                                     &apr.details.ok.h_wire),
        GNUNET_JSON_spec_fixed_auto ("salt",
                                     &apr.details.ok.salt),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        apr.hr.http_status = 0;
        apr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
    }
    break;
  case MHD_HTTP_BAD_REQUEST:
    GNUNET_break_op (0);
    apr.hr.ec = TALER_JSON_get_error_code (json);
    apr.hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_FORBIDDEN:
    apr.hr.ec = TALER_JSON_get_error_code (json);
    apr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_NOT_FOUND:
    apr.hr.ec = TALER_JSON_get_error_code (json);
    apr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_CONFLICT:
    apr.hr.ec = TALER_JSON_get_error_code (json);
    apr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    apr.hr.ec = TALER_JSON_get_error_code (json);
    apr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &apr.hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) apr.hr.ec);
    GNUNET_break_op (0);
    break;
  }
  aph->cb (aph->cb_cls,
           &apr);
  TALER_MERCHANT_accounts_post_cancel (aph);
}


struct TALER_MERCHANT_AccountsPostHandle *
TALER_MERCHANT_accounts_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  struct TALER_FullPayto payto_uri,
  const char *credit_facade_url,
  const json_t *credit_facade_credentials,
  TALER_MERCHANT_AccountsPostCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_AccountsPostHandle *aph;
  json_t *req_obj;

  req_obj = GNUNET_JSON_PACK (
    TALER_JSON_pack_full_payto (
      "payto_uri",
      payto_uri),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string (
        "credit_facade_url",
        credit_facade_url)),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_object_incref (
        "credit_facade_credentials",
        (json_t *) credit_facade_credentials))
    );
  aph = GNUNET_new (struct TALER_MERCHANT_AccountsPostHandle);
  aph->ctx = ctx;
  aph->cb = cb;
  aph->cb_cls = cb_cls;
  aph->url = TALER_url_join (backend_url,
                             "private/accounts",
                             NULL);
  if (NULL == aph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (aph);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (aph->url);
    GNUNET_assert (GNUNET_OK ==
                   TALER_curl_easy_post (&aph->post_ctx,
                                         eh,
                                         req_obj));
    json_decref (req_obj);
    aph->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     aph->post_ctx.headers,
                                     &handle_post_account_finished,
                                     aph);
    GNUNET_assert (NULL != aph->job);
  }
  return aph;
}


void
TALER_MERCHANT_accounts_post_cancel (
  struct TALER_MERCHANT_AccountsPostHandle *aph)
{
  if (NULL != aph->job)
  {
    GNUNET_CURL_job_cancel (aph->job);
    aph->job = NULL;
  }
  TALER_curl_easy_post_finished (&aph->post_ctx);
  GNUNET_free (aph->url);
  GNUNET_free (aph);
}


/* end of merchant_api_post_account.c */
