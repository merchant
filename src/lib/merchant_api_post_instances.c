/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_instances.c
 * @brief Implementation of the POST /instances request
 *        of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include "merchant_api_common.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>
#include <taler/taler_kyclogic_lib.h>


/**
 * Handle for a POST /instances/$ID operation.
 */
struct TALER_MERCHANT_InstancesPostHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_InstancesPostCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP POST /instances request.
 *
 * @param cls the `struct TALER_MERCHANT_InstancesPostHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_instances_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_InstancesPostHandle *iph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  iph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "POST /management/instances completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_FORBIDDEN:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we tried to abort the payment
     * after it was successful. We should pass the JSON reply to the
     * application */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    GNUNET_break_op (0);
    break;
  }
  iph->cb (iph->cb_cls,
           &hr);
  TALER_MERCHANT_instances_post_cancel (iph);
}


struct TALER_MERCHANT_InstancesPostHandle *
TALER_MERCHANT_instances_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  const char *name,
  const json_t *address,
  const json_t *jurisdiction,
  bool use_stefan,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  const char *auth_token,
  TALER_MERCHANT_InstancesPostCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_InstancesPostHandle *iph;
  json_t *req_obj;
  json_t *auth_obj;

  if (NULL != auth_token)
  {
    if (0 != strncasecmp (RFC_8959_PREFIX,
                          auth_token,
                          strlen (RFC_8959_PREFIX)))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Authentication token must start with `%s'\n",
                  RFC_8959_PREFIX);
      return NULL;
    }
    auth_obj = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("method",
                               "token"),
      GNUNET_JSON_pack_string ("token",
                               auth_token));
  }
  else
  {
    auth_obj = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_string ("method",
                               "external"));
  }
  if (NULL == auth_obj)
  {
    GNUNET_break (0);
    return NULL;
  }
  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_string ("id",
                             instance_id),
    GNUNET_JSON_pack_string ("name",
                             name),
    GNUNET_JSON_pack_object_incref ("address",
                                    (json_t *) address),
    GNUNET_JSON_pack_object_incref ("jurisdiction",
                                    (json_t *) jurisdiction),
    GNUNET_JSON_pack_bool ("use_stefan",
                           use_stefan),
    GNUNET_JSON_pack_time_rel ("default_wire_transfer_delay",
                               default_wire_transfer_delay),
    GNUNET_JSON_pack_time_rel ("default_pay_delay",
                               default_pay_delay),
    GNUNET_JSON_pack_object_steal ("auth",
                                   auth_obj));
  iph = GNUNET_new (struct TALER_MERCHANT_InstancesPostHandle);
  iph->ctx = ctx;
  iph->cb = cb;
  iph->cb_cls = cb_cls;
  iph->url = TALER_url_join (backend_url,
                             "management/instances",
                             NULL);
  if (NULL == iph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (iph);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (iph->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&iph->post_ctx,
                              eh,
                              req_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (req_obj);
      GNUNET_free (iph);
      return NULL;
    }
    json_decref (req_obj);
    iph->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     iph->post_ctx.headers,
                                     &handle_post_instances_finished,
                                     iph);
  }
  return iph;
}


void
TALER_MERCHANT_instances_post_cancel (
  struct TALER_MERCHANT_InstancesPostHandle *iph)
{
  if (NULL != iph->job)
  {
    GNUNET_CURL_job_cancel (iph->job);
    iph->job = NULL;
  }
  TALER_curl_easy_post_finished (&iph->post_ctx);
  GNUNET_free (iph->url);
  GNUNET_free (iph);
}


/* end of merchant_api_post_instances.c */
