/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_account.c
 * @brief Implementation of the GET /accounts/$ID request of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /accounts/$ID operation.
 */
struct TALER_MERCHANT_AccountGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_AccountGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /accounts/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_AccountGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_account_finished (void *cls,
                             long response_code,
                             const void *response)
{
  struct TALER_MERCHANT_AccountGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_AccountGetResponse tgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  tgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /accounts/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_fixed_auto ("salt",
                                     &tgr.details.ok.ad.salt),
        GNUNET_JSON_spec_mark_optional (
          TALER_JSON_spec_web_url ("credit_facade_url",
                                   &tgr.details.ok.ad.credit_facade_url),
          NULL),
        TALER_JSON_spec_full_payto_uri ("payto_uri",
                                        &tgr.details.ok.ad.payto_uri),
        GNUNET_JSON_spec_fixed_auto ("h_wire",
                                     &tgr.details.ok.ad.h_wire),
        GNUNET_JSON_spec_bool ("active",
                               &tgr.details.ok.ad.active),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK ==
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        tgh->cb (tgh->cb_cls,
                 &tgr);
        TALER_MERCHANT_account_get_cancel (tgh);
        return;
      }
      tgr.hr.http_status = 0;
      tgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) tgr.hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &tgr);
  TALER_MERCHANT_account_get_cancel (tgh);
}


struct TALER_MERCHANT_AccountGetHandle *
TALER_MERCHANT_account_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *instance_id,
  const struct TALER_MerchantWireHashP *h_wire,
  TALER_MERCHANT_AccountGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_AccountGetHandle *tgh;
  CURL *eh;

  tgh = GNUNET_new (struct TALER_MERCHANT_AccountGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;
  {
    char w_str[sizeof (*h_wire) * 2];
    char *path;
    char *end;

    end = GNUNET_STRINGS_data_to_string (h_wire,
                                         sizeof (*h_wire),
                                         w_str,
                                         sizeof (w_str));
    *end = '\0';
    GNUNET_asprintf (&path,
                     "private/accounts/%s",
                     w_str);
    tgh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_account_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_account_get_cancel (
  struct TALER_MERCHANT_AccountGetHandle *tgh)
{
  if (NULL != tgh->job)
    GNUNET_CURL_job_cancel (tgh->job);
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}
