/*
  This file is part of TALER
  Copyright (C) 2020-2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_delete_order.c
 * @brief Implementation of the DELETE /orders/$ORDER_ID request of the merchant's HTTP API
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>

/**
 * Handle for a DELETE /orders/$ID operation.
 */
struct TALER_MERCHANT_OrderDeleteHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_OrderDeleteCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;
};


/**
 * Function called when we're done processing the
 * HTTP DELETE /orders/$ORDER_ID request.
 *
 * @param cls the `struct TALER_MERCHANT_OrderDeleteHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_delete_order_finished (void *cls,
                              long response_code,
                              const void *response)
{
  struct TALER_MERCHANT_OrderDeleteHandle *odh = cls;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = NULL,
  };

  odh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /orders/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (response);
    hr.hint = TALER_JSON_get_error_hint (response);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u\n",
                (unsigned int) response_code);
    break;
  }
  odh->cb (odh->cb_cls,
           &hr);
  TALER_MERCHANT_order_delete_cancel (odh);
}


struct TALER_MERCHANT_OrderDeleteHandle *
TALER_MERCHANT_order_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *order_id,
  bool force,
  TALER_MERCHANT_OrderDeleteCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_OrderDeleteHandle *odh;

  odh = GNUNET_new (struct TALER_MERCHANT_OrderDeleteHandle);
  odh->ctx = ctx;
  odh->cb = cb;
  odh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/orders/%s%s",
                     order_id,
                     force
                     ? "?force=yes"
                     : "");

    odh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == odh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request url.\n");
    GNUNET_free (odh);
    return NULL;
  }

  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (odh->url);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_DELETE));
    odh->job = GNUNET_CURL_job_add (ctx,
                                    eh,
                                    &handle_delete_order_finished,
                                    odh);
  }
  return odh;
}


void
TALER_MERCHANT_order_delete_cancel (
  struct TALER_MERCHANT_OrderDeleteHandle *odh)
{
  if (NULL != odh->job)
    GNUNET_CURL_job_cancel (odh->job);
  GNUNET_free (odh->url);
  GNUNET_free (odh);
}
