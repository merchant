/*
  This file is part of TALER
  Copyright (C) 2022-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_using_templates.c
 * @brief Implementation of the POST /using_templates request
 *        of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_common.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a POST /templates/$ID operation.
 */
struct TALER_MERCHANT_UsingTemplatesPostHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_PostOrdersCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;
};

/**
 * Function called when we're done processing the
 * HTTP POST /using-templates request.
 *
 * @param cls the `struct TALER_MERCHANT_UsingTemplatesPostHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_using_templates_finished (void *cls,
                                      long response_code,
                                      const void *response)
{
  struct TALER_MERCHANT_UsingTemplatesPostHandle *utph = cls;
  const json_t *json = response;

  utph->job = NULL;
  TALER_MERCHANT_handle_order_creation_response_ (utph->cb,
                                                  utph->cb_cls,
                                                  response_code,
                                                  json);
  TALER_MERCHANT_using_templates_post_cancel (utph);
}


struct TALER_MERCHANT_UsingTemplatesPostHandle *
TALER_MERCHANT_using_templates_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *template_id,
  const char *summary,
  const struct TALER_Amount *amount,
  TALER_MERCHANT_PostOrdersCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_UsingTemplatesPostHandle *utph;
  json_t *req_obj;

  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("summary",
                               summary)),
    GNUNET_JSON_pack_allow_null (
      TALER_JSON_pack_amount ("amount",
                              amount)));
  utph = GNUNET_new (struct TALER_MERCHANT_UsingTemplatesPostHandle);
  utph->ctx = ctx;
  utph->cb = cb;
  utph->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "templates/%s",
                     template_id);
    utph->url = TALER_url_join (backend_url,
                                path,
                                NULL);
    GNUNET_free (path);
  }
  if (NULL == utph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (utph);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (utph->url);
    GNUNET_assert (GNUNET_OK ==
                   TALER_curl_easy_post (&utph->post_ctx,
                                         eh,
                                         req_obj));
    json_decref (req_obj);
    utph->job = GNUNET_CURL_job_add2 (ctx,
                                      eh,
                                      utph->post_ctx.headers,
                                      &handle_post_using_templates_finished,
                                      utph);
    GNUNET_assert (NULL != utph->job);
  }
  return utph;
}


void
TALER_MERCHANT_using_templates_post_cancel (
  struct TALER_MERCHANT_UsingTemplatesPostHandle *utph)
{
  if (NULL != utph->job)
  {
    GNUNET_CURL_job_cancel (utph->job);
    utph->job = NULL;
  }
  TALER_curl_easy_post_finished (&utph->post_ctx);
  GNUNET_free (utph->url);
  GNUNET_free (utph);
}


/* end of merchant_api_post_using_templates.c */
