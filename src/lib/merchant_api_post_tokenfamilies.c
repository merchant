/*
  This file is part of TALER
  Copyright (C) 2020-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_tokenfamilies.c
 * @brief Implementation of the POST /tokenfamilies request
 *        of the merchant's HTTP API
 * @author Christian Blättler
 */
#include "platform.h"
#include <curl/curl.h>
#include <gnunet/gnunet_json_lib.h>
#include <gnunet/gnunet_time_lib.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include "merchant_api_common.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a POST /tokenfamilies operation.
 */
struct TALER_MERCHANT_TokenFamiliesPostHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TokenFamiliesPostCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};

/**
 * Function called when we're done processing the
 * HTTP POST /tokenfamilies request.
 *
 * @param cls the `struct TALER_MERCHANT_TokenFamiliesPostHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_post_token_families_finished (void *cls,
                                     long response_code,
                                     const void *response)
{
  struct TALER_MERCHANT_TokenFamiliesPostHandle *handle = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  handle->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "POST /tokenfamilies completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_FORBIDDEN:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we tried to abort the payment
     * after it was successful. We should pass the JSON reply to the
     * application */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    GNUNET_break_op (0);
    break;
  }
  handle->cb (handle->cb_cls,
              &hr);
  TALER_MERCHANT_token_families_post_cancel (handle);
}


struct TALER_MERCHANT_TokenFamiliesPostHandle *
TALER_MERCHANT_token_families_post (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *slug,
  const char *name,
  const char *description,
  const json_t *description_i18n,
  const json_t *extra_data,
  struct GNUNET_TIME_Timestamp valid_after,
  struct GNUNET_TIME_Timestamp valid_before,
  struct GNUNET_TIME_Relative duration,
  struct GNUNET_TIME_Relative validity_granularity,
  struct GNUNET_TIME_Relative start_offset,
  const char *kind,
  TALER_MERCHANT_TokenFamiliesPostCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_TokenFamiliesPostHandle *handle;
  json_t *req_obj;

  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_string ("slug",
                             slug),
    GNUNET_JSON_pack_string ("name",
                             name),
    GNUNET_JSON_pack_string ("description",
                             description),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_object_incref ("description_i18n",
                                      (json_t *) description_i18n)),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_object_incref ("extra_data",
                                      (json_t *) extra_data)),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_timestamp ("valid_after",
                                  valid_after)),
    GNUNET_JSON_pack_timestamp ("valid_before",
                                valid_before),
    GNUNET_JSON_pack_time_rel ("duration",
                               duration),
    GNUNET_JSON_pack_time_rel ("validity_granularity",
                               validity_granularity),
    GNUNET_JSON_pack_time_rel ("start_offset",
                               start_offset),
    GNUNET_JSON_pack_string ("kind",
                             kind));
  handle = GNUNET_new (struct TALER_MERCHANT_TokenFamiliesPostHandle);
  handle->ctx = ctx;
  handle->cb = cb;
  handle->cb_cls = cb_cls;
  handle->url = TALER_url_join (backend_url,
                                "private/tokenfamilies",
                                NULL);
  if (NULL == handle->url)
  {

    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (handle);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (handle->url);
    GNUNET_assert (GNUNET_OK ==
                   TALER_curl_easy_post (&handle->post_ctx,
                                         eh,
                                         req_obj));
    json_decref (req_obj);
    handle->job = GNUNET_CURL_job_add2 (ctx,
                                        eh,
                                        handle->post_ctx.headers,
                                        &handle_post_token_families_finished,
                                        handle);
    GNUNET_assert (NULL != handle->job);
  }
  return handle;
}


void
TALER_MERCHANT_token_families_post_cancel (
  struct TALER_MERCHANT_TokenFamiliesPostHandle *pph)
{
  if (NULL != pph->job)
  {
    GNUNET_CURL_job_cancel (pph->job);
    pph->job = NULL;
  }
  TALER_curl_easy_post_finished (&pph->post_ctx);
  GNUNET_free (pph->url);
  GNUNET_free (pph);
}
