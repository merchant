/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_webhook.c
 * @brief Implementation of the GET /webhooks/$ID request of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /webhooks/$ID operation.
 */
struct TALER_MERCHANT_WebhookGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_WebhookGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /webhooks/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_WebhookGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_webhook_finished (void *cls,
                             long response_code,
                             const void *response)
{
  struct TALER_MERCHANT_WebhookGetHandle *wgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  wgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /webhooks/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const char *event_type;
      const char *url;
      const char *http_method;
      const char *header_template;
      const char *body_template;
      bool rst_ok = true;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("event_type",
                                 &event_type),
        TALER_JSON_spec_web_url ("url",
                                 &url),
        GNUNET_JSON_spec_string ("http_method",
                                 &http_method),
        GNUNET_JSON_spec_string ("header_template",
                                 &header_template),
        GNUNET_JSON_spec_string ("body_template",
                                 &body_template),
        GNUNET_JSON_spec_end ()
      };


      if ( (rst_ok) &&
           (GNUNET_OK ==
            GNUNET_JSON_parse (json,
                               spec,
                               NULL, NULL)) )
      {
        wgh->cb (wgh->cb_cls,
                 &hr,
                 event_type,
                 url,
                 http_method,
                 header_template,
                 body_template);
        GNUNET_JSON_parse_free (spec);
        TALER_MERCHANT_webhook_get_cancel (wgh);
        return;
      }
      hr.http_status = 0;
      hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      GNUNET_JSON_parse_free (spec);
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  wgh->cb (wgh->cb_cls,
           &hr,
           NULL,
           NULL,
           NULL,
           NULL,
           NULL);
  TALER_MERCHANT_webhook_get_cancel (wgh);
}


struct TALER_MERCHANT_WebhookGetHandle *
TALER_MERCHANT_webhook_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *webhook_id,
  TALER_MERCHANT_WebhookGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_WebhookGetHandle *wgh;
  CURL *eh;

  wgh = GNUNET_new (struct TALER_MERCHANT_WebhookGetHandle);
  wgh->ctx = ctx;
  wgh->cb = cb;
  wgh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/webhooks/%s",
                     webhook_id);
    wgh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == wgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (wgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              wgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (wgh->url);
  wgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_webhook_finished,
                                  wgh);
  return wgh;
}


void
TALER_MERCHANT_webhook_get_cancel (
  struct TALER_MERCHANT_WebhookGetHandle *wgh)
{
  if (NULL != wgh->job)
    GNUNET_CURL_job_cancel (wgh->job);
  GNUNET_free (wgh->url);
  GNUNET_free (wgh);
}
