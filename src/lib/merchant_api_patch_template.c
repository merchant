/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_patch_template.c
 * @brief Implementation of the PATCH /templates/$ID request
 *        of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_common.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a PATCH /templates/$ID operation.
 */
struct TALER_MERCHANT_TemplatePatchHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TemplatePatchCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP PATCH /templates/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_TemplatePatchHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_patch_template_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_TemplatePatchHandle *tph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  tph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "PATCH /templates/$ID completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_break_op (0);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_FORBIDDEN:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we tried to abort the payment
     * after it was successful. We should pass the JSON reply to the
     * application */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    GNUNET_break_op (0);
    break;
  }
  tph->cb (tph->cb_cls,
           &hr);
  TALER_MERCHANT_template_patch_cancel (tph);
}


struct TALER_MERCHANT_TemplatePatchHandle *
TALER_MERCHANT_template_patch (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *template_id,
  const char *template_description,
  const char *otp_id,
  json_t *template_contract,
  TALER_MERCHANT_TemplatePatchCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_TemplatePatchHandle *tph;
  json_t *req_obj;

  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_string ("template_description",
                             template_description),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("otp_id",
                               otp_id)),
    GNUNET_JSON_pack_object_incref ("template_contract",
                                    (json_t *) template_contract));
  tph = GNUNET_new (struct TALER_MERCHANT_TemplatePatchHandle);
  tph->ctx = ctx;
  tph->cb = cb;
  tph->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/templates/%s",
                     template_id);
    tph->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == tph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (tph);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (tph->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&tph->post_ctx,
                              eh,
                              req_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (req_obj);
      GNUNET_free (tph);
      return NULL;
    }
    json_decref (req_obj);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_PATCH));
    tph->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     tph->post_ctx.headers,
                                     &handle_patch_template_finished,
                                     tph);
  }
  return tph;
}


void
TALER_MERCHANT_template_patch_cancel (
  struct TALER_MERCHANT_TemplatePatchHandle *tph)
{
  if (NULL != tph->job)
  {
    GNUNET_CURL_job_cancel (tph->job);
    tph->job = NULL;
  }
  TALER_curl_easy_post_finished (&tph->post_ctx);
  GNUNET_free (tph->url);
  GNUNET_free (tph);
}


/* end of merchant_api_patch_template.c */
