/*
  This file is part of TALER
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_instances.c
 * @brief Implementation of the GET /instances request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Maximum number of instances permitted.
 */
#define MAX_INSTANCES 1024

/**
 * Handle for a GET /instances operation.
 */
struct TALER_MERCHANT_InstancesGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_InstancesGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Parse instance information from @a ia.
 *
 * @param json overall reply body
 * @param ia JSON array (or NULL!) with instance data
 * @param igh operation handle
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
parse_instances (const json_t *json,
                 const json_t *ia,
                 struct TALER_MERCHANT_InstancesGetHandle *igh)
{
  unsigned int iis_len = (unsigned int) json_array_size (ia);

  if ( (json_array_size (ia) != (size_t)  iis_len) ||
       (iis_len > MAX_INSTANCES) )
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  {
    struct TALER_MERCHANT_InstanceInformation iis[GNUNET_NZL (iis_len)];
    size_t index;
    json_t *value;
    struct TALER_MERCHANT_InstancesGetResponse igr = {
      .hr.http_status = MHD_HTTP_OK,
      .hr.reply = json,
      .details.ok.iis_length = iis_len,
      .details.ok.iis = iis
    };

    json_array_foreach (ia, index, value) {
      struct TALER_MERCHANT_InstanceInformation *ii = &iis[index];
      const char *uts;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("name",
                                 &ii->name),
        GNUNET_JSON_spec_string ("user_type",
                                 &uts),
        GNUNET_JSON_spec_string ("id",
                                 &ii->id),
        GNUNET_JSON_spec_fixed_auto ("merchant_pub",
                                     &ii->merchant_pub),
        GNUNET_JSON_spec_array_const ("payment_targets",
                                      &ii->payment_targets),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (value,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
      for (size_t i = 0; i<json_array_size (ii->payment_targets); i++)
      {
        if  (! json_is_string (json_array_get (ii->payment_targets,
                                               i)))
        {
          GNUNET_break_op (0);
          return GNUNET_SYSERR;
        }
      }
    } /* for all instances */
    igh->cb (igh->cb_cls,
             &igr);
    igh->cb = NULL; /* just to be sure */
  }
  return GNUNET_OK;
}


/**
 * Function called when we're done processing the
 * HTTP /instances request.
 *
 * @param cls the `struct TALER_MERCHANT_InstancesGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_instances_finished (void *cls,
                           long response_code,
                           const void *response)
{
  struct TALER_MERCHANT_InstancesGetHandle *igh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_InstancesGetResponse igr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  igh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /instances response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const json_t *instances;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_array_const ("instances",
                                      &instances),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        igr.hr.http_status = 0;
        igr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      if (GNUNET_OK ==
          parse_instances (json,
                           instances,
                           igh))
      {
        TALER_MERCHANT_instances_get_cancel (igh);
        return;
      }
      igr.hr.http_status = 0;
      igr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    igr.hr.ec = TALER_JSON_get_error_code (json);
    igr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    igr.hr.ec = TALER_JSON_get_error_code (json);
    igr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) igr.hr.ec);
    break;
  }
  igh->cb (igh->cb_cls,
           &igr);
  TALER_MERCHANT_instances_get_cancel (igh);
}


struct TALER_MERCHANT_InstancesGetHandle *
TALER_MERCHANT_instances_get (struct GNUNET_CURL_Context *ctx,
                              const char *backend_url,
                              TALER_MERCHANT_InstancesGetCallback instances_cb,
                              void *instances_cb_cls)
{
  struct TALER_MERCHANT_InstancesGetHandle *igh;
  CURL *eh;

  igh = GNUNET_new (struct TALER_MERCHANT_InstancesGetHandle);
  igh->ctx = ctx;
  igh->cb = instances_cb;
  igh->cb_cls = instances_cb_cls;
  igh->url = TALER_url_join (backend_url,
                             "management/instances",
                             NULL);
  if (NULL == igh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (igh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              igh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (igh->url);
  igh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_instances_finished,
                                  igh);
  return igh;
}


void
TALER_MERCHANT_instances_get_cancel (
  struct TALER_MERCHANT_InstancesGetHandle *igh)
{
  if (NULL != igh->job)
    GNUNET_CURL_job_cancel (igh->job);
  GNUNET_free (igh->url);
  GNUNET_free (igh);
}
