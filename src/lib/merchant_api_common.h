/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_common.h
 * @brief Implementation of common logic for libtalermerchant
 * @author Christian Grothoff
 * @author Priscilla Huang
 */
#ifndef MERCHANT_API_COMMON_H
#define MERCHANT_API_COMMON_H
#include "taler_merchant_service.h"


/**
 * Function called when we're done processing a
 * HTTP POST request to create an order.
 *
 * @param cb callback
 * @param cb_cls closure for @a cb
 * @param response_code HTTP response code, 0 on error
 * @param json response body, NULL if not JSON
 */
void
TALER_MERCHANT_handle_order_creation_response_ (
  TALER_MERCHANT_PostOrdersCallback cb,
  void *cb_cls,
  long response_code,
  const json_t *json);


/**
 * Take a @a response from the merchant API that (presumably) contains
 * error details and setup the corresponding @a hr structure.  Internally
 * used to convert merchant's responses in to @a hr.
 *
 * @param response if NULL we will report #TALER_EC_GENERIC_INVALID_RESPONSE in `ec`
 * @param http_status http status to use
 * @param[out] hr response object to initialize, fields will
 *        only be valid as long as @a response is valid as well
 */
void
TALER_MERCHANT_parse_error_details_ (const json_t *response,
                                     unsigned int http_status,
                                     struct TALER_MERCHANT_HttpResponse *hr);


#endif
