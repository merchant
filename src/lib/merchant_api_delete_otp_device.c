/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_delete_otp_device.c
 * @brief Implementation of the DELETE /otp-devices/$ID request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a DELETE /otp-devices/$ID operation.
 */
struct TALER_MERCHANT_OtpDeviceDeleteHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_OtpDeviceDeleteCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /otp-devices/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_OtpDeviceDeleteHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_delete_otp_device_finished (void *cls,
                                   long response_code,
                                   const void *response)
{
  struct TALER_MERCHANT_OtpDeviceDeleteHandle *tdh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  tdh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got DELETE /otp-devices/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  tdh->cb (tdh->cb_cls,
           &hr);
  TALER_MERCHANT_otp_device_delete_cancel (tdh);
}


struct TALER_MERCHANT_OtpDeviceDeleteHandle *
TALER_MERCHANT_otp_device_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *otp_device_id,
  TALER_MERCHANT_OtpDeviceDeleteCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_OtpDeviceDeleteHandle *tdh;

  tdh = GNUNET_new (struct TALER_MERCHANT_OtpDeviceDeleteHandle);
  tdh->ctx = ctx;
  tdh->cb = cb;
  tdh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/otp-devices/%s",
                     otp_device_id);
    tdh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == tdh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tdh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tdh->url);
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (tdh->url);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_DELETE));
    tdh->job = GNUNET_CURL_job_add (ctx,
                                    eh,
                                    &handle_delete_otp_device_finished,
                                    tdh);
  }
  return tdh;
}


void
TALER_MERCHANT_otp_device_delete_cancel (
  struct TALER_MERCHANT_OtpDeviceDeleteHandle *tdh)
{
  if (NULL != tdh->job)
    GNUNET_CURL_job_cancel (tdh->job);
  GNUNET_free (tdh->url);
  GNUNET_free (tdh);
}
