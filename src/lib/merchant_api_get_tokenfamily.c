/*
  This file is part of TALER
  Copyright (C) 2023-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_tokenfamily.c
 * @brief Implementation of the GET /tokenfamily/$ID request of the merchant's HTTP API
 * @author Christian Blättler
 */
#include "platform.h"
#include <curl/curl.h>
#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_json_lib.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /tokenfamilies/$SLUG operation.
 */
struct TALER_MERCHANT_TokenFamilyGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_TokenFamilyGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /tokenfamilies/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_TokenFamilyGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_token_family_finished (void *cls,
                                  long response_code,
                                  const void *response)
{
  struct TALER_MERCHANT_TokenFamilyGetHandle *handle = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_TokenFamilyGetResponse res = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  handle->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /tokenfamilies/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      // Parse token family response
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("slug",
                                 &res.details.ok.slug),
        GNUNET_JSON_spec_string ("name",
                                 &res.details.ok.name),
        GNUNET_JSON_spec_string ("description",
                                 &res.details.ok.description),
        GNUNET_JSON_spec_object_const ("description_i18n",
                                       &res.details.ok.description_i18n),
        GNUNET_JSON_spec_object_const ("extra_data",
                                       &res.details.ok.extra_data),
        GNUNET_JSON_spec_timestamp ("valid_after",
                                    &res.details.ok.valid_after),
        GNUNET_JSON_spec_timestamp ("valid_before",
                                    &res.details.ok.valid_before),
        GNUNET_JSON_spec_relative_time ("duation",
                                        &res.details.ok.duration),
        GNUNET_JSON_spec_relative_time ("validity_granularity",
                                        &res.details.ok.validity_granularity),
        GNUNET_JSON_spec_relative_time ("start_offset",
                                        &res.details.ok.start_offset),
        GNUNET_JSON_spec_string ("kind",
                                 &res.details.ok.kind),
        GNUNET_JSON_spec_uint64 ("issued",
                                 &res.details.ok.issued),
        GNUNET_JSON_spec_uint64 ("used",
                                 &res.details.ok.used),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK ==
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        handle->cb (handle->cb_cls,
                    &res);
        GNUNET_JSON_parse_free (spec);
        TALER_MERCHANT_token_family_get_cancel (handle);
        return;
      }
      res.hr.http_status = 0;
      res.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    res.hr.ec = TALER_JSON_get_error_code (json);
    res.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    res.hr.ec = TALER_JSON_get_error_code (json);
    res.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    res.hr.ec = TALER_JSON_get_error_code (json);
    res.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) res.hr.ec);
    break;
  }
}


struct TALER_MERCHANT_TokenFamilyGetHandle *
TALER_MERCHANT_token_family_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *token_family_slug,
  TALER_MERCHANT_TokenFamilyGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_TokenFamilyGetHandle *handle;
  CURL *eh;

  handle = GNUNET_new (struct TALER_MERCHANT_TokenFamilyGetHandle);
  handle->ctx = ctx;
  handle->cb = cb;
  handle->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/tokenfamilies/%s",
                     token_family_slug);
    handle->url = TALER_url_join (backend_url,
                                  path,
                                  NULL);
    GNUNET_free (path);
  }
  if (NULL == handle->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (handle);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              handle->url);
  eh = TALER_MERCHANT_curl_easy_get_ (handle->url);
  handle->job = GNUNET_CURL_job_add (ctx,
                                     eh,
                                     &handle_get_token_family_finished,
                                     handle);
  return handle;
}


void
TALER_MERCHANT_token_family_get_cancel (
  struct TALER_MERCHANT_TokenFamilyGetHandle *handle)
{
  if (NULL != handle->job)
    GNUNET_CURL_job_cancel (handle->job);
  GNUNET_free (handle->url);
  GNUNET_free (handle);
}
