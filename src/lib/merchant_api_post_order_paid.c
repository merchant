/*
  This file is part of TALER
  Copyright (C) 2020-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_order_paid.c
 * @brief Implementation of the POST /order/$ID/paid request
 *        of the merchant's HTTP API
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include "merchant_api_common.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_exchange_service.h>
#include <taler/taler_curl_lib.h>


/**
 * @brief Handle to a POST /orders/$ID/paid operation at a merchant.
 */
struct TALER_MERCHANT_OrderPaidHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_OrderPaidCallback paid_cb;

  /**
   * Closure for @a paid_cb.
   */
  void *paid_cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;
};


/**
 * Function called when we're done processing the
 * HTTP /paid request.
 *
 * @param cls the `struct TALER_MERCHANT_OrderPaidHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_paid_finished (void *cls,
                      long response_code,
                      const void *response)
{
  struct TALER_MERCHANT_OrderPaidHandle *oph = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_OrderPaidResponse opr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  oph->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "/paid completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    opr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    {
      bool refunded;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_bool ("refunded",
                               &refunded),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (opr.hr.reply,
                             spec,
                             NULL,
                             NULL))
      {
        GNUNET_break_op (0);
        opr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      break;
    }
    break;
  case MHD_HTTP_BAD_REQUEST:
    opr.hr.ec = TALER_JSON_get_error_code (json);
    opr.hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us
     * or the merchant is buggy (or API version conflict);
     * just pass JSON reply to the application */
    break;
  case MHD_HTTP_FORBIDDEN:
    opr.hr.ec = TALER_JSON_get_error_code (json);
    opr.hr.hint = TALER_JSON_get_error_hint (json);
    /* The signature provided was invalid */
    break;
  case MHD_HTTP_NOT_FOUND:
    opr.hr.ec = TALER_JSON_get_error_code (json);
    opr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
       happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_CONFLICT:
    opr.hr.ec = TALER_JSON_get_error_code (json);
    opr.hr.hint = TALER_JSON_get_error_hint (json);
    /* The hashed contract terms don't match with the order_id. */
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    opr.hr.ec = TALER_JSON_get_error_code (json);
    opr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  case MHD_HTTP_SERVICE_UNAVAILABLE:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &opr.hr);
    /* Exchange couldn't respond properly; the retry is
       left to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &opr.hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) opr.hr.ec);
    GNUNET_break_op (0);
    break;
  }
  oph->paid_cb (oph->paid_cb_cls,
                &opr);
  TALER_MERCHANT_order_paid_cancel (oph);
}


struct TALER_MERCHANT_OrderPaidHandle *
TALER_MERCHANT_order_paid (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  const char *session_id,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  const struct GNUNET_HashCode *wallet_data_hash,
  const struct TALER_MerchantSignatureP *merchant_sig,
  TALER_MERCHANT_OrderPaidCallback paid_cb,
  void *paid_cb_cls)
{
  struct TALER_MERCHANT_OrderPaidHandle *oph;
  json_t *req_obj;

  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_auto ("sig",
                                merchant_sig),
    GNUNET_JSON_pack_data_auto ("h_contract",
                                h_contract_terms),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_data_auto ("wallet_data_hash",
                                  wallet_data_hash)),
    GNUNET_JSON_pack_string ("session_id",
                             session_id));
  oph = GNUNET_new (struct TALER_MERCHANT_OrderPaidHandle);
  oph->ctx = ctx;
  oph->paid_cb = paid_cb;
  oph->paid_cb_cls = paid_cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "orders/%s/paid",
                     order_id);
    oph->url = TALER_url_join (merchant_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == oph->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (oph);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (oph->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&oph->post_ctx,
                              eh,
                              req_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (req_obj);
      GNUNET_free (oph);
      return NULL;
    }
    json_decref (req_obj);
    oph->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     oph->post_ctx.headers,
                                     &handle_paid_finished,
                                     oph);
  }
  return oph;
}


void
TALER_MERCHANT_order_paid_cancel (
  struct TALER_MERCHANT_OrderPaidHandle *oph)
{
  if (NULL != oph->job)
  {
    GNUNET_CURL_job_cancel (oph->job);
    oph->job = NULL;
  }
  TALER_curl_easy_post_finished (&oph->post_ctx);
  GNUNET_free (oph->url);
  GNUNET_free (oph);
}


/* end of merchant_api_post_order_paid.c */
