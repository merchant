/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_otp_device.c
 * @brief Implementation of the GET /otp-devices/$ID request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /otp-devices/$ID operation.
 */
struct TALER_MERCHANT_OtpDeviceGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_OtpDeviceGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /otp-devices/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_OtpDeviceGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_otp_device_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_OtpDeviceGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_OtpDeviceGetResponse tgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  tgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /otp-devices/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      uint32_t alg32;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("otp_device_description",
                                 &tgr.details.ok.otp_device_description),
        GNUNET_JSON_spec_uint32 ("otp_algorithm",
                                 &alg32),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_uint64 ("otp_ctr",
                                   &tgr.details.ok.otp_ctr),
          NULL),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_uint64 ("otp_timestamp",
                                   &tgr.details.ok.otp_timestamp_s),
          NULL),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_string ("otp_code",
                                   &tgr.details.ok.otp_code),
          NULL),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK ==
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        tgr.details.ok.otp_alg =
          (enum TALER_MerchantConfirmationAlgorithm) alg32;
        tgh->cb (tgh->cb_cls,
                 &tgr);
        TALER_MERCHANT_otp_device_get_cancel (tgh);
        return;
      }
      tgr.hr.http_status = 0;
      tgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) tgr.hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &tgr);
  TALER_MERCHANT_otp_device_get_cancel (tgh);
}


struct TALER_MERCHANT_OtpDeviceGetHandle *
TALER_MERCHANT_otp_device_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *otp_device_id,
  TALER_MERCHANT_OtpDeviceGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_OtpDeviceGetHandle *tgh;
  CURL *eh;

  tgh = GNUNET_new (struct TALER_MERCHANT_OtpDeviceGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/otp-devices/%s",
                     otp_device_id);
    tgh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_otp_device_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_otp_device_get_cancel (
  struct TALER_MERCHANT_OtpDeviceGetHandle *tgh)
{
  if (NULL != tgh->job)
    GNUNET_CURL_job_cancel (tgh->job);
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}
