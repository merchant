/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_webhooks.c
 * @brief Implementation of the GET /webhooks request of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Maximum number of webhooks we return.
 */
#define MAX_WEBHOOKS 1024

/**
 * Handle for a GET /webhooks operation.
 */
struct TALER_MERCHANT_WebhooksGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_WebhooksGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Parse webhook information from @a ia.
 *
 * @param ia JSON array (or NULL!) with webhook data
 * @param[in] wgr partially filled webhook response
 * @param wgh operation handle
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
parse_webhooks (const json_t *ia,
                struct TALER_MERCHANT_WebhooksGetResponse *wgr,
                struct TALER_MERCHANT_WebhooksGetHandle *wgh)
{
  unsigned int whook_len = (unsigned int) json_array_size (ia);

  if ( (json_array_size (ia) != (size_t)  whook_len) ||
       (whook_len > MAX_WEBHOOKS) )
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  {
    struct TALER_MERCHANT_WebhookEntry whook[GNUNET_NZL (whook_len)];
    size_t index;
    json_t *value;

    json_array_foreach (ia, index, value) {
      struct TALER_MERCHANT_WebhookEntry *ie = &whook[index];
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string ("webhook_id",
                                 &ie->webhook_id),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (value,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
    }
    wgr->details.ok.webhooks_length = whook_len;
    wgr->details.ok.webhooks = whook;
    wgh->cb (wgh->cb_cls,
             wgr);
    wgh->cb = NULL; /* just to be sure */
  }
  return GNUNET_OK;
}


/**
 * Function called when we're done processing the
 * HTTP /webhooks request.
 *
 * @param cls the `struct TALER_MERCHANT_WebhooksGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_webhooks_finished (void *cls,
                              long response_code,
                              const void *response)
{
  struct TALER_MERCHANT_WebhooksGetHandle *wgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_WebhooksGetResponse wgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  wgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /webhooks response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const json_t *webhooks;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_array_const ("webhooks",
                                      &webhooks),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        wgr.hr.http_status = 0;
        wgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      if (GNUNET_OK ==
          parse_webhooks (webhooks,
                          &wgr,
                          wgh))
      {
        TALER_MERCHANT_webhooks_get_cancel (wgh);
        return;
      }
      wgr.hr.http_status = 0;
      wgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    wgr.hr.ec = TALER_JSON_get_error_code (json);
    wgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  default:
    /* unexpected response code */
    wgr.hr.ec = TALER_JSON_get_error_code (json);
    wgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) wgr.hr.ec);
    break;
  }
  wgh->cb (wgh->cb_cls,
           &wgr);
  TALER_MERCHANT_webhooks_get_cancel (wgh);
}


struct TALER_MERCHANT_WebhooksGetHandle *
TALER_MERCHANT_webhooks_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  TALER_MERCHANT_WebhooksGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_WebhooksGetHandle *wgh;
  CURL *eh;

  wgh = GNUNET_new (struct TALER_MERCHANT_WebhooksGetHandle);
  wgh->ctx = ctx;
  wgh->cb = cb;
  wgh->cb_cls = cb_cls;
  wgh->url = TALER_url_join (backend_url,
                             "private/webhooks",
                             NULL);
  if (NULL == wgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (wgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              wgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (wgh->url);
  wgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_webhooks_finished,
                                  wgh);
  return wgh;
}


void
TALER_MERCHANT_webhooks_get_cancel (
  struct TALER_MERCHANT_WebhooksGetHandle *wgh)
{
  if (NULL != wgh->job)
    GNUNET_CURL_job_cancel (wgh->job);
  GNUNET_free (wgh->url);
  GNUNET_free (wgh);
}
