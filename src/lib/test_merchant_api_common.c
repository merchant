/*
  This file is part of TALER
  (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file util/test_merchant_api_common.c
 * @brief Unit test for test_merchant_api_common.c
 * @author Florian Dold
 */
#include "platform.h"
#include "taler_merchant_service.h"


int
main (int argc,
      const char *const argv[])
{
  struct TALER_MERCHANT_PayUriData pd;
  struct TALER_MERCHANT_RefundUriData rd;

  (void) argc;
  (void) argv;
  GNUNET_log_setup ("test_merchant_api_common",
                    "WARNING",
                    NULL);
  /* test pay parser */
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_pay_uri (
                   "http://pay/localhost/my-order",
                   &pd));
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler://pay/localhost/my-order",
                   &pd));
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler://refund/localhost/my-order/",
                   &pd));
  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler://PAY/localhost/my-order/",
                   &pd));
  GNUNET_assert (0 == strcmp (pd.merchant_host,
                              "localhost"));
  GNUNET_assert (NULL == pd.merchant_prefix_path);
  GNUNET_assert (0 == strcmp (pd.order_id,
                              "my-order"));
  GNUNET_assert (NULL == pd.session_id);
  GNUNET_assert (NULL == pd.claim_token);
  GNUNET_assert (NULL == pd.ssid);
  GNUNET_assert (! pd.use_http);
  TALER_MERCHANT_parse_pay_uri_free (&pd);

  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler://pay/localhost/my-order/#myssid",
                   &pd));
  GNUNET_assert (0 == strcmp (pd.merchant_host,
                              "localhost"));
  GNUNET_assert (0 == strcmp (pd.order_id,
                              "my-order"));
  GNUNET_assert (NULL == pd.session_id);
  GNUNET_assert (NULL == pd.claim_token);
  GNUNET_assert (0 == strcmp (pd.ssid,
                              "myssid"));
  GNUNET_assert (! pd.use_http);
  TALER_MERCHANT_parse_pay_uri_free (&pd);

  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler+http://pay/localhost/instance/foo/my-order/my-session#myssid",
                   &pd));
  GNUNET_assert (0 == strcmp (pd.merchant_host,
                              "localhost"));
  GNUNET_assert (0 == strcmp (pd.merchant_prefix_path,
                              "instance/foo"));
  GNUNET_assert (0 == strcmp (pd.order_id,
                              "my-order"));
  GNUNET_assert (0 == strcmp (pd.session_id,
                              "my-session"));
  GNUNET_assert (NULL == pd.claim_token);
  GNUNET_assert (0 == strcmp (pd.ssid,
                              "myssid"));
  GNUNET_assert (pd.use_http);
  TALER_MERCHANT_parse_pay_uri_free (&pd);

  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_pay_uri (
                   "taler+http://pay/localhost/my-order/my-session?c=3QV8F3W34Y1FX6R8P2JZNMQ2JQ#myssid",
                   &pd));
  GNUNET_assert (0 == strcmp (pd.merchant_host,
                              "localhost"));
  GNUNET_assert (NULL == pd.merchant_prefix_path);
  GNUNET_assert (0 == strcmp (pd.order_id,
                              "my-order"));
  GNUNET_assert (0 == strcmp (pd.session_id,
                              "my-session"));
  GNUNET_assert (NULL != pd.claim_token);
  GNUNET_assert (0 == strcmp (pd.ssid,
                              "myssid"));
  GNUNET_assert (pd.use_http);
  TALER_MERCHANT_parse_pay_uri_free (&pd);


  /* test refund parser */
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_refund_uri (
                   "http://refund/localhost/my-order/",
                   &rd));
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_refund_uri (
                   "taler://refund/localhost/my-order",
                   &rd));
  GNUNET_assert (GNUNET_SYSERR ==
                 TALER_MERCHANT_parse_refund_uri (
                   "taler://pay/localhost/my-order/",
                   &rd));

  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_refund_uri (
                   "taler://refund/localhost/my-order/#myssid",
                   &rd));
  GNUNET_assert (0 == strcmp (rd.merchant_host,
                              "localhost"));
  GNUNET_assert (NULL == pd.merchant_prefix_path);
  GNUNET_assert (0 == strcmp (rd.order_id,
                              "my-order"));
  GNUNET_assert (0 == strcmp (rd.ssid,
                              "myssid"));
  GNUNET_assert (! rd.use_http);
  TALER_MERCHANT_parse_refund_uri_free (&rd);

  GNUNET_assert (GNUNET_OK ==
                 TALER_MERCHANT_parse_refund_uri (
                   "taler+http://REFUND/localhost/instance/foo/my-order/",
                   &rd));
  GNUNET_assert (0 == strcmp (rd.merchant_host,
                              "localhost"));
  GNUNET_assert (0 == strcmp ("instance/foo",
                              rd.merchant_prefix_path));
  GNUNET_assert (0 == strcmp (rd.order_id,
                              "my-order"));
  GNUNET_assert (NULL == rd.ssid);
  GNUNET_assert (rd.use_http);
  TALER_MERCHANT_parse_refund_uri_free (&rd);

  return 0;
}


/* end of test_merchant_api_common.c */
