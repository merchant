/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_delete_template.c
 * @brief Implementation of the DELETE /webhooks/$ID request of the merchant's HTTP API
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a DELETE /webhooks/$ID operation.
 */
struct TALER_MERCHANT_WebhookDeleteHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_WebhookDeleteCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /webhooks/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_WebhookDeleteHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_delete_webhook_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_WebhookDeleteHandle *wdh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  wdh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /webhooks/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    break;
  }
  wdh->cb (wdh->cb_cls,
           &hr);
  TALER_MERCHANT_webhook_delete_cancel (wdh);
}


struct TALER_MERCHANT_WebhookDeleteHandle *
TALER_MERCHANT_webhook_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *webhook_id,
  TALER_MERCHANT_WebhookDeleteCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_WebhookDeleteHandle *wdh;

  wdh = GNUNET_new (struct TALER_MERCHANT_WebhookDeleteHandle);
  wdh->ctx = ctx;
  wdh->cb = cb;
  wdh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/webhooks/%s",
                     webhook_id);
    wdh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == wdh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (wdh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              wdh->url);
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (wdh->url);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_DELETE));
    wdh->job = GNUNET_CURL_job_add (ctx,
                                    eh,
                                    &handle_delete_webhook_finished,
                                    wdh);
  }
  return wdh;
}


void
TALER_MERCHANT_webhook_delete_cancel (
  struct TALER_MERCHANT_WebhookDeleteHandle *wdh)
{
  if (NULL != wdh->job)
    GNUNET_CURL_job_cancel (wdh->job);
  GNUNET_free (wdh->url);
  GNUNET_free (wdh);
}
