/*
  This file is part of TALER
  Copyright (C) 2020, 2021, 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_patch_order_forget.c
 * @brief Implementation of the PATCH /orders/$ID/forget request
 *        of the merchant's HTTP API
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_common.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>


/**
 * Handle for a PATCH /orders/$ORDER_ID/forget operation.
 */
struct TALER_MERCHANT_OrderForgetHandle
{

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_ForgetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

};


/**
 * Function called when we're done processing the
 * HTTP PATCH /orders/$ORDER_ID/forget request.
 *
 * @param cls the `struct TALER_MERCHANT_OrderForgetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_forget_finished (void *cls,
                        long response_code,
                        const void *response)
{
  struct TALER_MERCHANT_OrderForgetHandle *ofh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_HttpResponse hr = {
    .http_status = (unsigned int) response_code,
    .reply = json
  };

  ofh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "PATCH /orders/$ORDER_ID/forget completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    /* fields were NOW forgotten */
    break;
  case MHD_HTTP_NO_CONTENT:
    /* fields were already forgotten before */
    break;
  case MHD_HTTP_BAD_REQUEST:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_UNAUTHORIZED:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_CONFLICT:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    hr.ec = TALER_JSON_get_error_code (json);
    hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  default:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &hr);
    /* unexpected response code */
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) hr.ec);
    GNUNET_break_op (0);
    break;
  }
  ofh->cb (ofh->cb_cls,
           &hr);
  TALER_MERCHANT_order_forget_cancel (ofh);
}


struct TALER_MERCHANT_OrderForgetHandle *
TALER_MERCHANT_order_forget (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  unsigned int fields_length,
  const char *fields[static fields_length],
  TALER_MERCHANT_ForgetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_OrderForgetHandle *ofh;
  json_t *req_fields;
  json_t *req_obj;

  req_fields = json_array ();
  if (NULL == req_fields)
  {
    GNUNET_break (0);
    return NULL;
  }
  for (unsigned int i = 0; i<fields_length; i++)
  {
    if (0 !=
        json_array_append_new (req_fields,
                               json_string (fields[i])))
    {
      GNUNET_break (0);
      json_decref (req_fields);
      return NULL;
    }
  }
  req_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("fields",
                                  req_fields));
  ofh = GNUNET_new (struct TALER_MERCHANT_OrderForgetHandle);
  ofh->ctx = ctx;
  ofh->cb = cb;
  ofh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/orders/%s/forget",
                     order_id);
    ofh->url = TALER_url_join (merchant_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == ofh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (req_obj);
    GNUNET_free (ofh);
    return NULL;
  }
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (ofh->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&ofh->post_ctx,
                              eh,
                              req_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (req_obj);
      GNUNET_free (ofh);
      return NULL;
    }
    json_decref (req_obj);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_PATCH));
    ofh->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     ofh->post_ctx.headers,
                                     &handle_forget_finished,
                                     ofh);
  }
  return ofh;
}


void
TALER_MERCHANT_order_forget_cancel (
  struct TALER_MERCHANT_OrderForgetHandle *ofh)
{
  if (NULL != ofh->job)
  {
    GNUNET_CURL_job_cancel (ofh->job);
    ofh->job = NULL;
  }
  TALER_curl_easy_post_finished (&ofh->post_ctx);
  GNUNET_free (ofh->url);
  GNUNET_free (ofh);
}


/* end of merchant_api_patch_order_forget.c */
