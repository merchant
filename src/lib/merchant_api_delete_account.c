/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_delete_account.c
 * @brief Implementation of the DELETE /private/account/$H_WIRE request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a DELETE /accounts/$ID operation.
 */
struct TALER_MERCHANT_AccountDeleteHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_AccountDeleteCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP DELETE /accounts/$H_WIRE request.
 *
 * @param cls the `struct TALER_MERCHANT_AccountDeleteHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_delete_account_finished (void *cls,
                                long response_code,
                                const void *response)
{
  struct TALER_MERCHANT_AccountDeleteHandle *adh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_AccountDeleteResponse adr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  adh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /accounts/$H_WIRE response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    adr.hr.ec = TALER_JSON_get_error_code (json);
    adr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    /* unexpected response code */
    adr.hr.ec = TALER_JSON_get_error_code (json);
    adr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d for DELETE /account/ID\n",
                (unsigned int) response_code,
                (int) adr.hr.ec);
    break;
  }
  adh->cb (adh->cb_cls,
           &adr);
  TALER_MERCHANT_account_delete_cancel (adh);
}


struct TALER_MERCHANT_AccountDeleteHandle *
TALER_MERCHANT_account_delete (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const struct TALER_MerchantWireHashP *h_wire,
  TALER_MERCHANT_AccountDeleteCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_AccountDeleteHandle *adh;

  adh = GNUNET_new (struct TALER_MERCHANT_AccountDeleteHandle);
  adh->ctx = ctx;
  adh->cb = cb;
  adh->cb_cls = cb_cls;
  {
    char h_wire_str[sizeof (*h_wire) * 2];
    char *path;
    char *end;

    end = GNUNET_STRINGS_data_to_string (h_wire,
                                         sizeof (*h_wire),
                                         h_wire_str,
                                         sizeof (h_wire_str));
    *end = '\0';
    GNUNET_asprintf (&path,
                     "private/account/%s",
                     h_wire_str);
    adh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == adh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (adh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              adh->url);
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (adh->url);
    GNUNET_assert (CURLE_OK ==
                   curl_easy_setopt (eh,
                                     CURLOPT_CUSTOMREQUEST,
                                     MHD_HTTP_METHOD_DELETE));
    adh->job = GNUNET_CURL_job_add (ctx,
                                    eh,
                                    &handle_delete_account_finished,
                                    adh);
  }
  return adh;
}


void
TALER_MERCHANT_account_delete_cancel (
  struct TALER_MERCHANT_AccountDeleteHandle *adh)
{
  if (NULL != adh->job)
    GNUNET_CURL_job_cancel (adh->job);
  GNUNET_free (adh->url);
  GNUNET_free (adh);
}
