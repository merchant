/*
  This file is part of TALER
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation; either version 2.1,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with TALER; see the file COPYING.LGPL.
  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_post_order_abort.c
 * @brief Implementation of the POST /orders/$ID/abort request
 *        of the merchant's HTTP API
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include "merchant_api_common.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_exchange_service.h>
#include <taler/taler_curl_lib.h>


/**
 * Maximum number of refunds we return.
 */
#define MAX_REFUNDS 1024


/**
 * @brief An abort Handle
 */
struct TALER_MERCHANT_OrderAbortHandle
{
  /**
   * Hash of the contract.
   */
  struct TALER_PrivateContractHashP h_contract_terms;

  /**
   * Public key of the merchant.
   */
  struct TALER_MerchantPublicKeyP merchant_pub;

  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_AbortCallback abort_cb;

  /**
   * Closure for @a abort_cb.
   */
  void *abort_cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * The coins we are aborting on.
   */
  struct TALER_MERCHANT_AbortCoin *coins;

  /**
   * Number of @e coins we are paying with.
   */
  unsigned int num_coins;

};


/**
 * Check that the response for an abort is well-formed,
 * and call the application callback with the result if it is
 * OK. Otherwise returns #GNUNET_SYSERR.
 *
 * @param oah handle to operation that created the reply
 * @param[in] ar abort response, partially initialized
 * @param json the reply to parse
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
check_abort_refund (struct TALER_MERCHANT_OrderAbortHandle *oah,
                    struct TALER_MERCHANT_AbortResponse *ar,
                    const json_t *json)
{
  const json_t *refunds;
  unsigned int num_refunds;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_array_const ("refunds",
                                  &refunds),
    GNUNET_JSON_spec_end ()
  };

  if (GNUNET_OK !=
      GNUNET_JSON_parse (json,
                         spec,
                         NULL, NULL))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  num_refunds = (unsigned int) json_array_size (refunds);
  if ( (json_array_size (refunds) != (size_t)  num_refunds) ||
       (num_refunds > MAX_REFUNDS) )
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }

  {
    struct TALER_MERCHANT_AbortedCoin res[GNUNET_NZL (num_refunds)];

    for (unsigned int i = 0; i<num_refunds; i++)
    {
      json_t *refund = json_array_get (refunds, i);
      uint32_t exchange_status;
      struct GNUNET_JSON_Specification spec_es[] = {
        GNUNET_JSON_spec_uint32 ("exchange_status",
                                 &exchange_status),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (refund,
                             spec_es,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
      if (MHD_HTTP_OK == exchange_status)
      {
        struct GNUNET_JSON_Specification spec_detail[] = {
          GNUNET_JSON_spec_fixed_auto ("exchange_sig",
                                       &res[i].exchange_sig),
          GNUNET_JSON_spec_fixed_auto ("exchange_pub",
                                       &res[i].exchange_pub),
          GNUNET_JSON_spec_end ()
        };

        if (GNUNET_OK !=
            GNUNET_JSON_parse (refund,
                               spec_detail,
                               NULL, NULL))
        {
          GNUNET_break_op (0);
          return GNUNET_SYSERR;
        }

        if (GNUNET_OK !=
            TALER_exchange_online_refund_confirmation_verify (
              &oah->h_contract_terms,
              &oah->coins[i].coin_pub,
              &oah->merchant_pub,
              0,                                                   /* transaction id */
              &oah->coins[i].amount_with_fee,
              &res[i].exchange_pub,
              &res[i].exchange_sig))
        {
          GNUNET_break_op (0);
          return GNUNET_SYSERR;
        }
      }
    }
    ar->details.ok.merchant_pub = &oah->merchant_pub;
    ar->details.ok.num_aborts = num_refunds;
    ar->details.ok.aborts = res;
    oah->abort_cb (oah->abort_cb_cls,
                   ar);
    oah->abort_cb = NULL;
  }
  return GNUNET_OK;
}


/**
 * Function called when we're done processing the
 * abort request.
 *
 * @param cls the `struct TALER_MERCHANT_OrderAbortHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_abort_finished (void *cls,
                       long response_code,
                       const void *response)
{
  struct TALER_MERCHANT_OrderAbortHandle *oah = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_AbortResponse ar = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  oah->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "/pay completed with response code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case 0:
    ar.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_OK:
    if (GNUNET_OK ==
        check_abort_refund (oah,
                            &ar,
                            json))
    {
      TALER_MERCHANT_order_abort_cancel (oah);
      return;
    }
    ar.hr.http_status = 0;
    ar.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
    break;
  case MHD_HTTP_BAD_REQUEST:
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    /* This should never happen, either us or the
       merchant is buggy (or API version conflict); just
       pass JSON reply to the application */
    break;
  case MHD_HTTP_FORBIDDEN:
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_NOT_FOUND:
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, this should never
 happen, we should pass the JSON reply to the
       application */
    break;
  case MHD_HTTP_REQUEST_TIMEOUT:
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says one of
       the signatures is invalid; as we checked them,
       this should never happen, we should pass the JSON
       reply to the application */
    break;
  case MHD_HTTP_PRECONDITION_FAILED:
    /* Our *payment* already succeeded fully. */
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  case MHD_HTTP_INTERNAL_SERVER_ERROR:
    ar.hr.ec = TALER_JSON_get_error_code (json);
    ar.hr.hint = TALER_JSON_get_error_hint (json);
    /* Server had an internal issue; we should retry,
       but this API leaves this to the application */
    break;
  case MHD_HTTP_BAD_GATEWAY:
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &ar.hr);
    /* Nothing really to verify, the merchant is blaming the exchange.
       We should pass the JSON reply to the application */
    break;
  default:
    /* unexpected response code */
    TALER_MERCHANT_parse_error_details_ (json,
                                         response_code,
                                         &ar.hr);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) ar.hr.ec);
    GNUNET_break_op (0);
    break;
  }
  oah->abort_cb (oah->abort_cb_cls,
                 &ar);
  TALER_MERCHANT_order_abort_cancel (oah);
}


struct TALER_MERCHANT_OrderAbortHandle *
TALER_MERCHANT_order_abort (
  struct GNUNET_CURL_Context *ctx,
  const char *merchant_url,
  const char *order_id,
  const struct TALER_MerchantPublicKeyP *merchant_pub,
  const struct TALER_PrivateContractHashP *h_contract,
  unsigned int num_coins,
  const struct TALER_MERCHANT_AbortCoin coins[static num_coins],
  TALER_MERCHANT_AbortCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_OrderAbortHandle *oah;
  json_t *abort_obj;
  json_t *j_coins;

  j_coins = json_array ();
  if (NULL == j_coins)
  {
    GNUNET_break (0);
    return NULL;
  }
  for (unsigned int i = 0; i<num_coins; i++)
  {
    const struct TALER_MERCHANT_AbortCoin *ac = &coins[i];
    json_t *j_coin;

    /* create JSON for this coin */
    j_coin = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("coin_pub",
                                  &ac->coin_pub),
      /* FIXME: no longer needed since **v18**, remove eventually! */
      TALER_JSON_pack_amount ("contribution",
                              &ac->amount_with_fee),
      GNUNET_JSON_pack_string ("exchange_url",
                               ac->exchange_url));
    if (0 !=
        json_array_append_new (j_coins,
                               j_coin))
    {
      GNUNET_break (0);
      json_decref (j_coins);
      return NULL;
    }
  }
  abort_obj = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("coins",
                                  j_coins),
    GNUNET_JSON_pack_data_auto ("h_contract",
                                h_contract));
  oah = GNUNET_new (struct TALER_MERCHANT_OrderAbortHandle);
  oah->h_contract_terms = *h_contract;
  oah->merchant_pub = *merchant_pub;
  oah->ctx = ctx;
  oah->abort_cb = cb;
  oah->abort_cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "orders/%s/abort",
                     order_id);
    oah->url = TALER_url_join (merchant_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == oah->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    json_decref (abort_obj);
    GNUNET_free (oah);
    return NULL;
  }
  oah->num_coins = num_coins;
  oah->coins = GNUNET_new_array (num_coins,
                                 struct TALER_MERCHANT_AbortCoin);
  GNUNET_memcpy (oah->coins,
                 coins,
                 num_coins * sizeof (struct TALER_MERCHANT_AbortCoin));
  {
    CURL *eh;

    eh = TALER_MERCHANT_curl_easy_get_ (oah->url);
    if (GNUNET_OK !=
        TALER_curl_easy_post (&oah->post_ctx,
                              eh,
                              abort_obj))
    {
      GNUNET_break (0);
      curl_easy_cleanup (eh);
      json_decref (abort_obj);
      GNUNET_free (oah);
      return NULL;
    }
    json_decref (abort_obj);
    oah->job = GNUNET_CURL_job_add2 (ctx,
                                     eh,
                                     oah->post_ctx.headers,
                                     &handle_abort_finished,
                                     oah);
  }
  return oah;
}


void
TALER_MERCHANT_order_abort_cancel (
  struct TALER_MERCHANT_OrderAbortHandle *oah)
{
  if (NULL != oah->job)
  {
    GNUNET_CURL_job_cancel (oah->job);
    oah->job = NULL;
  }
  TALER_curl_easy_post_finished (&oah->post_ctx);
  GNUNET_free (oah->coins);
  GNUNET_free (oah->url);
  GNUNET_free (oah);
}


/* end of merchant_api_post_order_abort.c */
