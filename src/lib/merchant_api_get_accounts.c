/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_accounts.c
 * @brief Implementation of the GET /accounts request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>

/**
 * Maximum number of accounts permitted.
 */
#define MAX_ACCOUNTS 1024

/**
 * Handle for a GET /accounts operation.
 */
struct TALER_MERCHANT_AccountsGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_AccountsGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Parse account information from @a ia.
 *
 * @param ia JSON array (or NULL!) with account data
 * @param[in] tgr partially filled response
 * @param tgh operation handle
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
parse_accounts (const json_t *ia,
                struct TALER_MERCHANT_AccountsGetResponse *tgr,
                struct TALER_MERCHANT_AccountsGetHandle *tgh)
{
  unsigned int tmpl_len = (unsigned int) json_array_size (ia);

  if ( (json_array_size (ia) != (size_t)  tmpl_len) ||
       (tmpl_len > MAX_ACCOUNTS) )
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  {
    struct TALER_MERCHANT_AccountEntry tmpl[GNUNET_NZL (tmpl_len)];
    size_t index;
    json_t *value;

    json_array_foreach (ia, index, value) {
      struct TALER_MERCHANT_AccountEntry *ie = &tmpl[index];
      struct GNUNET_JSON_Specification spec[] = {
        TALER_JSON_spec_full_payto_uri ("payto_uri",
                                        &ie->payto_uri),
        GNUNET_JSON_spec_fixed_auto ("h_wire",
                                     &ie->h_wire),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (value,
                             spec,
                             NULL, NULL))
      {
        GNUNET_break_op (0);
        return GNUNET_SYSERR;
      }
    }
    tgr->details.ok.accounts_length = tmpl_len;
    tgr->details.ok.accounts = tmpl;
    tgh->cb (tgh->cb_cls,
             tgr);
    tgh->cb = NULL; /* just to be sure */
  }
  return GNUNET_OK;
}


/**
 * Function called when we're done processing the
 * HTTP /accounts request.
 *
 * @param cls the `struct TALER_MERCHANT_AccountsGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_accounts_finished (void *cls,
                              long response_code,
                              const void *response)
{
  struct TALER_MERCHANT_AccountsGetHandle *tgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_AccountsGetResponse tgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  tgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /accounts response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      const json_t *accounts;
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_array_const ("accounts",
                                      &accounts),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK !=
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        tgr.hr.http_status = 0;
        tgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
        break;
      }
      if (GNUNET_OK ==
          parse_accounts (accounts,
                          &tgr,
                          tgh))
      {
        TALER_MERCHANT_accounts_get_cancel (tgh);
        return;
      }
      tgr.hr.http_status = 0;
      tgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  default:
    /* unexpected response code */
    tgr.hr.ec = TALER_JSON_get_error_code (json);
    tgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) tgr.hr.ec);
    break;
  }
  tgh->cb (tgh->cb_cls,
           &tgr);
  TALER_MERCHANT_accounts_get_cancel (tgh);
}


struct TALER_MERCHANT_AccountsGetHandle *
TALER_MERCHANT_accounts_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  TALER_MERCHANT_AccountsGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_AccountsGetHandle *tgh;
  CURL *eh;

  tgh = GNUNET_new (struct TALER_MERCHANT_AccountsGetHandle);
  tgh->ctx = ctx;
  tgh->cb = cb;
  tgh->cb_cls = cb_cls;
  tgh->url = TALER_url_join (backend_url,
                             "private/accounts",
                             NULL);
  if (NULL == tgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (tgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              tgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (tgh->url);
  tgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_accounts_finished,
                                  tgh);
  return tgh;
}


void
TALER_MERCHANT_accounts_get_cancel (
  struct TALER_MERCHANT_AccountsGetHandle *tgh)
{
  if (NULL != tgh->job)
    GNUNET_CURL_job_cancel (tgh->job);
  GNUNET_free (tgh->url);
  GNUNET_free (tgh);
}
