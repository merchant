/*
  This file is part of TALER
  Copyright (C) 2014-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 2.1, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License along with
  TALER; see the file COPYING.LGPL.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file merchant_api_get_product.c
 * @brief Implementation of the GET /product/$ID request of the merchant's HTTP API
 * @author Christian Grothoff
 */
#include "platform.h"
#include <curl/curl.h>
#include <jansson.h>
#include <microhttpd.h> /* just for HTTP status codes */
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include "taler_merchant_service.h"
#include "merchant_api_curl_defaults.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_signatures.h>


/**
 * Handle for a GET /products/$ID operation.
 */
struct TALER_MERCHANT_ProductGetHandle
{
  /**
   * The url for this request.
   */
  char *url;

  /**
   * Handle for the request.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  TALER_MERCHANT_ProductGetCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Function called when we're done processing the
 * HTTP GET /products/$ID request.
 *
 * @param cls the `struct TALER_MERCHANT_ProductGetHandle`
 * @param response_code HTTP response code, 0 on error
 * @param response response body, NULL if not in JSON
 */
static void
handle_get_product_finished (void *cls,
                             long response_code,
                             const void *response)
{
  struct TALER_MERCHANT_ProductGetHandle *pgh = cls;
  const json_t *json = response;
  struct TALER_MERCHANT_ProductGetResponse pgr = {
    .hr.http_status = (unsigned int) response_code,
    .hr.reply = json
  };

  pgh->job = NULL;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Got /products/$ID response with status code %u\n",
              (unsigned int) response_code);
  switch (response_code)
  {
  case MHD_HTTP_OK:
    {
      struct GNUNET_JSON_Specification spec[] = {
        GNUNET_JSON_spec_string (
          "description",
          &pgr.details.ok.description),
        GNUNET_JSON_spec_object_const (
          "description_i18n",
          &pgr.details.ok.description_i18n),
        GNUNET_JSON_spec_string (
          "unit",
          &pgr.details.ok.unit),
        TALER_JSON_spec_amount_any (
          "price",
          &pgr.details.ok.price),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_string (
            "image",
            &pgr.details.ok.image),
          NULL),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_array_const (
            "taxes",
            &pgr.details.ok.taxes),
          NULL),
        GNUNET_JSON_spec_int64 (
          "total_stock",
          &pgr.details.ok.total_stock),
        GNUNET_JSON_spec_uint64 (
          "total_sold",
          &pgr.details.ok.total_sold),
        GNUNET_JSON_spec_uint64 (
          "total_lost",
          &pgr.details.ok.total_lost),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_object_const (
            "address",
            &pgr.details.ok.location),
          NULL),
        GNUNET_JSON_spec_mark_optional (
          GNUNET_JSON_spec_timestamp (
            "next_restock",
            &pgr.details.ok.next_restock),
          NULL),
        GNUNET_JSON_spec_end ()
      };

      if (GNUNET_OK ==
          GNUNET_JSON_parse (json,
                             spec,
                             NULL, NULL))
      {
        pgh->cb (pgh->cb_cls,
                 &pgr);
        GNUNET_JSON_parse_free (spec);
        TALER_MERCHANT_product_get_cancel (pgh);
        return;
      }
      pgr.hr.http_status = 0;
      pgr.hr.ec = TALER_EC_GENERIC_INVALID_RESPONSE;
      break;
    }
  case MHD_HTTP_UNAUTHORIZED:
    pgr.hr.ec = TALER_JSON_get_error_code (json);
    pgr.hr.hint = TALER_JSON_get_error_hint (json);
    /* Nothing really to verify, merchant says we need to authenticate. */
    break;
  case MHD_HTTP_NOT_FOUND:
    pgr.hr.ec = TALER_JSON_get_error_code (json);
    pgr.hr.hint = TALER_JSON_get_error_hint (json);
    break;
  default:
    /* unexpected response code */
    pgr.hr.ec = TALER_JSON_get_error_code (json);
    pgr.hr.hint = TALER_JSON_get_error_hint (json);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d\n",
                (unsigned int) response_code,
                (int) pgr.hr.ec);
    break;
  }
  pgh->cb (pgh->cb_cls,
           &pgr);
  TALER_MERCHANT_product_get_cancel (pgh);
}


struct TALER_MERCHANT_ProductGetHandle *
TALER_MERCHANT_product_get (
  struct GNUNET_CURL_Context *ctx,
  const char *backend_url,
  const char *product_id,
  TALER_MERCHANT_ProductGetCallback cb,
  void *cb_cls)
{
  struct TALER_MERCHANT_ProductGetHandle *pgh;
  CURL *eh;

  pgh = GNUNET_new (struct TALER_MERCHANT_ProductGetHandle);
  pgh->ctx = ctx;
  pgh->cb = cb;
  pgh->cb_cls = cb_cls;
  {
    char *path;

    GNUNET_asprintf (&path,
                     "private/products/%s",
                     product_id);
    pgh->url = TALER_url_join (backend_url,
                               path,
                               NULL);
    GNUNET_free (path);
  }
  if (NULL == pgh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (pgh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Requesting URL '%s'\n",
              pgh->url);
  eh = TALER_MERCHANT_curl_easy_get_ (pgh->url);
  pgh->job = GNUNET_CURL_job_add (ctx,
                                  eh,
                                  &handle_get_product_finished,
                                  pgh);
  return pgh;
}


void
TALER_MERCHANT_product_get_cancel (
  struct TALER_MERCHANT_ProductGetHandle *pgh)
{
  if (NULL != pgh->job)
    GNUNET_CURL_job_cancel (pgh->job);
  GNUNET_free (pgh->url);
  GNUNET_free (pgh);
}
