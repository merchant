/*
  This file is part of TALER
  (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-delete-orders-ID.c
 * @brief implement DELETE /orders/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-delete-orders-ID.h"
#include <stdint.h>
#include <taler/taler_json_lib.h>


/**
 * Handle a DELETE "/orders/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_delete_orders_ID (const struct TMH_RequestHandler *rh,
                              struct MHD_Connection *connection,
                              struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  enum GNUNET_DB_QueryStatus qs;
  const char *force_s;
  bool force;

  (void) rh;
  force_s = MHD_lookup_connection_value (connection,
                                         MHD_GET_ARGUMENT_KIND,
                                         "force");
  if (NULL == force_s)
    force_s = "no";
  force = (0 == strcasecmp (force_s,
                            "yes"));

  GNUNET_assert (NULL != mi);
  qs = TMH_db->delete_order (TMH_db->cls,
                             mi->settings.id,
                             hc->infix,
                             force);
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
    qs = TMH_db->delete_contract_terms (TMH_db->cls,
                                        mi->settings.id,
                                        hc->infix,
                                        TMH_legal_expiration);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_COMMIT_FAILED,
                                       NULL);
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       NULL);
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    {
      struct TALER_MerchantPostDataHashP unused;
      uint64_t order_serial;
      bool paid = false;
      bool wired = false;
      bool matches = false;
      int16_t choice_index;

      qs = TMH_db->lookup_order (TMH_db->cls,
                                 mi->settings.id,
                                 hc->infix,
                                 NULL,
                                 &unused,
                                 NULL);
      if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
      {
        qs = TMH_db->lookup_contract_terms3 (TMH_db->cls,
                                             mi->settings.id,
                                             hc->infix,
                                             NULL,
                                             NULL,
                                             &order_serial,
                                             &paid,
                                             &wired,
                                             &matches,
                                             NULL,
                                             &choice_index);
      }
      if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_NOT_FOUND,
                                           TALER_EC_MERCHANT_GENERIC_ORDER_UNKNOWN,
                                           hc->infix);
      if (paid)
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_CONFLICT,
                                           TALER_EC_MERCHANT_PRIVATE_DELETE_ORDERS_ALREADY_PAID,
                                           hc->infix);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_CONFLICT,
                                         TALER_EC_MERCHANT_PRIVATE_DELETE_ORDERS_AWAITING_PAYMENT,
                                         hc->infix);
    }
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    return TALER_MHD_reply_static (connection,
                                   MHD_HTTP_NO_CONTENT,
                                   NULL,
                                   NULL,
                                   0);
  }
  GNUNET_assert (0);
  return MHD_NO;
}


/* end of taler-merchant-httpd_private-delete-orders-ID.c */
