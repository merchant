/*
  This file is part of TALER
  (C) 2023, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-token-families-SLUG.c
 * @brief implementing PATCH /tokenfamilies/$SLUG request handling
 * @author Christian Blättler
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-token-families-SLUG.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


/**
 * Handle a PATCH "/tokenfamilies/$slug" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_patch_token_family_SLUG (const struct TMH_RequestHandler *rh,
                                     struct MHD_Connection *connection,
                                     struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *slug = hc->infix;
  struct TALER_MERCHANTDB_TokenFamilyDetails details = {0};
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("name",
                             (const char **) &details.name),
    GNUNET_JSON_spec_string ("description",
                             (const char **) &details.description),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("description_i18n",
                             &details.description_i18n),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("extra_data",
                             &details.extra_data),
      NULL),
    GNUNET_JSON_spec_timestamp ("valid_after",
                                &details.valid_after),
    GNUNET_JSON_spec_timestamp ("valid_before",
                                &details.valid_before),
    GNUNET_JSON_spec_end ()
  };

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != slug);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }

  /* Ensure that valid_after is before valid_before */
  if (GNUNET_TIME_timestamp_cmp (details.valid_after,
                                 >=,
                                 details.valid_before))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "valid_after >= valid_before");
  }

  if (NULL == details.description_i18n)
  {
    details.description_i18n = json_object ();
    GNUNET_assert (NULL != details.description_i18n);
  }
  if (! TALER_JSON_check_i18n (details.description_i18n))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "description_i18n");
  }

  {
    enum GNUNET_DB_QueryStatus qs;
    MHD_RESULT ret = MHD_NO;

    qs = TMH_db->update_token_family (TMH_db->cls,
                                      mi->settings.id,
                                      slug,
                                      &details);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        NULL);
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_NOT_FOUND,
                                        TALER_EC_MERCHANT_PATCH_TOKEN_FAMILY_NOT_FOUND,
                                        slug);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-token-families-SLUG.c */
