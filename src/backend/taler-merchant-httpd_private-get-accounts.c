/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-accounts.c
 * @brief implement GET /accounts
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-accounts.h"
#include <taler/taler_json_lib.h>


/**
 * Add account details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param merchant_priv private key of the merchant instance
 * @param ad details about the account
 */
static void
add_account (void *cls,
             const struct TALER_MerchantPrivateKeyP *merchant_priv,
             const struct TALER_MERCHANTDB_AccountDetails *ad)
{
  json_t *pa = cls;

  (void) merchant_priv;
  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_bool ("active",
                                            ad->active),
                     TALER_JSON_pack_full_payto ("payto_uri",
                                                 ad->payto_uri),
                     GNUNET_JSON_pack_data_auto ("h_wire",
                                                 &ad->h_wire))));
}


MHD_RESULT
TMH_private_get_accounts (const struct TMH_RequestHandler *rh,
                          struct MHD_Connection *connection,
                          struct TMH_HandlerContext *hc)
{
  json_t *pa;
  enum GNUNET_DB_QueryStatus qs;

  pa = json_array ();
  GNUNET_assert (NULL != pa);
  qs = TMH_db->select_accounts (TMH_db->cls,
                                hc->instance->settings.id,
                                &add_account,
                                pa);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (pa);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (connection,
                                    MHD_HTTP_OK,
                                    GNUNET_JSON_pack_array_steal ("accounts",
                                                                  pa));
}


/* end of taler-merchant-httpd_private-get-accounts.c */
