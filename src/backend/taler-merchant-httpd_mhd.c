/*
  This file is part of TALER
  Copyright (C) 2014-2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_mhd.c
 * @brief helpers for MHD interaction; these are TALER_EXCHANGE_handler_ functions
 *        that generate simple MHD replies that do not require any real operations
 *        to be performed (error handling, static pages, etc.)
 * @author Florian Dold
 * @author Benedikt Mueller
 * @author Christian Grothoff
 */
#include "platform.h"
#include <jansson.h>
#include "taler-merchant-httpd_mhd.h"


MHD_RESULT
TMH_MHD_handler_static_response (const struct TMH_RequestHandler *rh,
                                 struct MHD_Connection *connection,
                                 struct TMH_HandlerContext *hc)
{
  (void) hc;
  return TALER_MHD_reply_static (connection,
                                 rh->response_code,
                                 rh->mime_type,
                                 rh->data,
                                 rh->data_size);
}


MHD_RESULT
TMH_MHD_handler_agpl_redirect (const struct TMH_RequestHandler *rh,
                               struct MHD_Connection *connection,
                               struct TMH_HandlerContext *hc)
{
  (void) rh;
  (void) hc;
  return TALER_MHD_reply_agpl (connection,
                               "http://www.git.taler.net/?p=merchant.git");
}


bool
TMH_MHD_test_html_desired (struct MHD_Connection *connection)
{
  const char *accept;
  double json_q;
  double html_q;

  accept = MHD_lookup_connection_value (connection,
                                        MHD_HEADER_KIND,
                                        MHD_HTTP_HEADER_ACCEPT);
  if (NULL == accept)
    return false; /* nothing selected, we read this as not HTML */
  html_q = TALER_pattern_matches (accept,
                                  "text/html");
  json_q = TALER_pattern_matches (accept,
                                  "application/json");
  if (html_q > json_q)
    return true;
  return false;
}


/* end of taler-exchange-httpd_mhd.c */
