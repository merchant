/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-templates-ID.c
 * @brief implement GET /templates/$ID
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-templates-ID.h"
#include <taler/taler_json_lib.h>


MHD_RESULT
TMH_private_get_templates_ID (
  const struct TMH_RequestHandler *rh,
  struct MHD_Connection *connection,
  struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_TemplateDetails tp = { 0 };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  qs = TMH_db->lookup_template (TMH_db->cls,
                                mi->settings.id,
                                hc->infix,
                                &tp);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      TALER_EC_GENERIC_DB_FETCH_FAILED,
      "lookup_template");
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_NOT_FOUND,
      TALER_EC_MERCHANT_GENERIC_TEMPLATE_UNKNOWN,
      hc->infix);
  }
  {
    MHD_RESULT ret;

    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_object_incref ("editable_defaults",
                                        tp.editable_defaults)),
      GNUNET_JSON_pack_string ("template_description",
                               tp.template_description),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("otp_id",
                                 tp.otp_id)),
      GNUNET_JSON_pack_object_incref ("template_contract",
                                      tp.template_contract));
    TALER_MERCHANTDB_template_details_free (&tp);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-templates-ID.c */
