/*
  This file is part of TALER
  (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-accounts-ID.c
 * @brief implementing PATCH /accounts/$ID request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-accounts-ID.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * PATCH configuration of an existing instance, given its configuration.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_patch_accounts_ID (const struct TMH_RequestHandler *rh,
                                struct MHD_Connection *connection,
                                struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *h_wire_s = hc->infix;
  enum GNUNET_DB_QueryStatus qs;
  const json_t *cfc;
  const char *cfu;
  struct TALER_MerchantWireHashP h_wire;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_mark_optional (
      TALER_JSON_spec_web_url ("credit_facade_url",
                               &cfu),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_object_const ("credit_facade_credentials",
                                     &cfc),
      NULL),
    GNUNET_JSON_spec_end ()
  };

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != h_wire_s);
  if (GNUNET_OK !=
      GNUNET_STRINGS_string_to_data (h_wire_s,
                                     strlen (h_wire_s),
                                     &h_wire,
                                     sizeof (h_wire)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_MERCHANT_GENERIC_H_WIRE_MALFORMED,
                                       h_wire_s);
  }
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }
  
  qs = TMH_db->update_account (TMH_db->cls,
                               mi->settings.id,
                               &h_wire,
                               cfu,
                               cfc);
  {
    MHD_RESULT ret = MHD_NO;

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        "update_account");
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_NOT_FOUND,
                                         TALER_EC_MERCHANT_GENERIC_ACCOUNT_UNKNOWN,
                                         h_wire_s);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-accounts-ID.c */
