/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-webhooks-ID.c
 * @brief implement GET /webhooks/$ID
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-webhooks-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/webhooks/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_webhooks_ID (const struct TMH_RequestHandler *rh,
                             struct MHD_Connection *connection,
                             struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_WebhookDetails wb = { 0 };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  qs = TMH_db->lookup_webhook (TMH_db->cls,
                               mi->settings.id,
                               hc->infix,
                               &wb);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "lookup_webhook");
  }
  if (0 == qs)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_WEBHOOK_UNKNOWN,
                                       hc->infix);
  }
  {
    MHD_RESULT ret;

    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("event_type",
                               wb.event_type),
      GNUNET_JSON_pack_string ("url",
                               wb.url),
      GNUNET_JSON_pack_string ("http_method",
                               wb.http_method),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("header_template",
                                 wb.header_template)),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("body_template",
                                 wb.body_template)));
    GNUNET_free (wb.event_type);
    GNUNET_free (wb.url);
    GNUNET_free (wb.http_method);
    GNUNET_free (wb.header_template);
    GNUNET_free (wb.body_template);

    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-webhooks-ID.c */
