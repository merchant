/*
  This file is part of TALER
  (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-post-instances.c
 * @brief implementing POST /instances request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-post-instances.h"
#include "taler-merchant-httpd_helper.h"
#include "taler_merchant_bank_lib.h"
#include <taler/taler_dbevents.h>
#include <taler/taler_json_lib.h>
#include <regex.h>

/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


/**
 * Generate an instance, given its configuration.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_post_instances (const struct TMH_RequestHandler *rh,
                            struct MHD_Connection *connection,
                            struct TMH_HandlerContext *hc)
{
  struct TALER_MERCHANTDB_InstanceSettings is = { 0 };
  struct TALER_MERCHANTDB_InstanceAuthSettings ias;
  const char *auth_token = NULL;
  const char *uts = "business";
  struct TMH_WireMethod *wm_head = NULL;
  struct TMH_WireMethod *wm_tail = NULL;
  const json_t *jauth;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("id",
                             (const char **) &is.id),
    GNUNET_JSON_spec_string ("name",
                             (const char **) &is.name),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("user_type",
                               &uts),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("email",
                               (const char **) &is.email),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("website",
                               (const char **) &is.website),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("logo",
                               (const char **) &is.logo),
      NULL),
    GNUNET_JSON_spec_object_const ("auth",
                                   &jauth),
    GNUNET_JSON_spec_json ("address",
                           &is.address),
    GNUNET_JSON_spec_json ("jurisdiction",
                           &is.jurisdiction),
    GNUNET_JSON_spec_bool ("use_stefan",
                           &is.use_stefan),
    GNUNET_JSON_spec_relative_time ("default_wire_transfer_delay",
                                    &is.default_wire_transfer_delay),
    GNUNET_JSON_spec_relative_time ("default_pay_delay",
                                    &is.default_pay_delay),
    GNUNET_JSON_spec_end ()
  };

  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }
  {
    enum GNUNET_GenericReturnValue ret;

    ret = TMH_check_auth_config (connection,
                                 jauth,
                                 &auth_token);
    if (GNUNET_OK != ret)
    {
      GNUNET_JSON_parse_free (spec);
      return (GNUNET_NO == ret) ? MHD_YES : MHD_NO;
    }
  }

  /* check 'id' well-formed */
  {
    static bool once;
    static regex_t reg;
    bool id_wellformed = true;

    if (! once)
    {
      once = true;
      GNUNET_assert (0 ==
                     regcomp (&reg,
                              "^[A-Za-z0-9][A-Za-z0-9_.@-]+$",
                              REG_EXTENDED));
    }

    if (0 != regexec (&reg,
                      is.id,
                      0, NULL, 0))
      id_wellformed = false;
    if (! id_wellformed)
    {
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "id");
    }
  }

  if (! TMH_location_object_valid (is.address))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "address");
  }

  if (! TMH_location_object_valid (is.jurisdiction))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "jurisdiction");
  }

  if ( (NULL != is.logo) &&
       (! TMH_image_data_url_valid (is.logo)) )
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "logo");
  }

  {
    /* Test if an instance of this id is known */
    struct TMH_MerchantInstance *mi;

    mi = TMH_lookup_instance (is.id);
    if (NULL != mi)
    {
      if (mi->deleted)
      {
        GNUNET_JSON_parse_free (spec);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_CONFLICT,
                                           TALER_EC_MERCHANT_PRIVATE_POST_INSTANCES_PURGE_REQUIRED,
                                           is.id);
      }
      /* Check for idempotency */
      if ( (0 == strcmp (mi->settings.id,
                         is.id)) &&
           (0 == strcmp (mi->settings.name,
                         is.name)) &&
           ((mi->settings.email == is.email) ||
            (NULL != is.email && NULL != mi->settings.email &&
             0 == strcmp (mi->settings.email,
                          is.email))) &&
           ((mi->settings.website == is.website) ||
            (NULL != is.website && NULL != mi->settings.website &&
             0 == strcmp (mi->settings.website,
                          is.website))) &&
           ((mi->settings.logo == is.logo) ||
            (NULL != is.logo && NULL != mi->settings.logo &&
             0 == strcmp (mi->settings.logo,
                          is.logo))) &&
           ( ( (NULL != auth_token) &&
               (GNUNET_OK ==
                TMH_check_auth (auth_token,
                                &mi->auth.auth_salt,
                                &mi->auth.auth_hash)) ) ||
             ( (NULL == auth_token) &&
               (GNUNET_YES ==
                GNUNET_is_zero (&mi->auth.auth_hash))) ) &&
           (1 == json_equal (mi->settings.address,
                             is.address)) &&
           (1 == json_equal (mi->settings.jurisdiction,
                             is.jurisdiction)) &&
           (mi->settings.use_stefan == is.use_stefan) &&
           (GNUNET_TIME_relative_cmp (mi->settings.default_wire_transfer_delay,
                                      ==,
                                      is.default_wire_transfer_delay)) &&
           (GNUNET_TIME_relative_cmp (mi->settings.default_pay_delay,
                                      ==,
                                      is.default_pay_delay)) )
      {
        GNUNET_JSON_parse_free (spec);
        return TALER_MHD_reply_static (connection,
                                       MHD_HTTP_NO_CONTENT,
                                       NULL,
                                       NULL,
                                       0);
      }
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_CONFLICT,
                                         TALER_EC_MERCHANT_PRIVATE_POST_INSTANCES_ALREADY_EXISTS,
                                         is.id);
    }
  }

  /* handle authentication token setup */
  if (NULL == auth_token)
  {
    memset (&ias.auth_salt,
            0,
            sizeof (ias.auth_salt));
    memset (&ias.auth_hash,
            0,
            sizeof (ias.auth_hash));
  }
  else
  {
    /* Sets 'auth_salt' and 'auth_hash' */
    TMH_compute_auth (auth_token,
                      &ias.auth_salt,
                      &ias.auth_hash);
  }

  /* create in-memory data structure */
  {
    struct TMH_MerchantInstance *mi;
    enum GNUNET_DB_QueryStatus qs;

    mi = GNUNET_new (struct TMH_MerchantInstance);
    mi->wm_head = wm_head;
    mi->wm_tail = wm_tail;
    mi->settings = is;
    mi->settings.address = json_incref (mi->settings.address);
    mi->settings.jurisdiction = json_incref (mi->settings.jurisdiction);
    mi->settings.id = GNUNET_strdup (is.id);
    mi->settings.name = GNUNET_strdup (is.name);
    if (NULL != is.email)
      mi->settings.email = GNUNET_strdup (is.email);
    if (NULL != is.website)
      mi->settings.website = GNUNET_strdup (is.website);
    if (NULL != is.logo)
      mi->settings.logo = GNUNET_strdup (is.logo);
    mi->auth = ias;
    GNUNET_CRYPTO_eddsa_key_create (&mi->merchant_priv.eddsa_priv);
    GNUNET_CRYPTO_eddsa_key_get_public (&mi->merchant_priv.eddsa_priv,
                                        &mi->merchant_pub.eddsa_pub);

    for (unsigned int i = 0; i<MAX_RETRIES; i++)
    {
      if (GNUNET_OK !=
          TMH_db->start (TMH_db->cls,
                         "post /instances"))
      {
        mi->rc = 1;
        TMH_instance_decref (mi);
        GNUNET_JSON_parse_free (spec);
        return TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_START_FAILED,
                                           NULL);
      }
      qs = TMH_db->insert_instance (TMH_db->cls,
                                    &mi->merchant_pub,
                                    &mi->merchant_priv,
                                    &mi->settings,
                                    &mi->auth);
      switch (qs)
      {
      case GNUNET_DB_STATUS_HARD_ERROR:
        {
          MHD_RESULT ret;

          TMH_db->rollback (TMH_db->cls);
          GNUNET_break (0);
          ret = TALER_MHD_reply_with_error (connection,
                                            MHD_HTTP_INTERNAL_SERVER_ERROR,
                                            TALER_EC_GENERIC_DB_STORE_FAILED,
                                            is.id);
          mi->rc = 1;
          TMH_instance_decref (mi);
          GNUNET_JSON_parse_free (spec);
          return ret;
        }
      case GNUNET_DB_STATUS_SOFT_ERROR:
        goto retry;
      case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
        {
          MHD_RESULT ret;

          TMH_db->rollback (TMH_db->cls);
          GNUNET_break (0);
          ret = TALER_MHD_reply_with_error (connection,
                                            MHD_HTTP_CONFLICT,
                                            TALER_EC_MERCHANT_PRIVATE_POST_INSTANCES_ALREADY_EXISTS,
                                            is.id);
          mi->rc = 1;
          TMH_instance_decref (mi);
          GNUNET_JSON_parse_free (spec);
          return ret;
        }
      case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
        /* handled below */
        break;
      }
      qs = TMH_db->commit (TMH_db->cls);
      if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
        qs = GNUNET_DB_STATUS_SUCCESS_ONE_RESULT;
retry:
      if (GNUNET_DB_STATUS_SOFT_ERROR != qs)
        break; /* success! -- or hard failure */
    } /* for .. MAX_RETRIES */
    if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT != qs)
    {
      mi->rc = 1;
      TMH_instance_decref (mi);
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_COMMIT_FAILED,
                                         NULL);
    }
    /* Finally, also update our running process */
    GNUNET_assert (GNUNET_OK ==
                   TMH_add_instance (mi));
    TMH_reload_instances (mi->settings.id);
  }
  if (0 == strcmp (is.id,
                   "default"))
  {
    GNUNET_free (TMH_default_auth); /* clear it if the default instance was
                                       created */
  }

  GNUNET_JSON_parse_free (spec);
  return TALER_MHD_reply_static (connection,
                                 MHD_HTTP_NO_CONTENT,
                                 NULL,
                                 NULL,
                                 0);
}


/* end of taler-merchant-httpd_private-post-instances.c */
