/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-post-otp-devices.c
 * @brief implementing POST /otp-devices request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-post-otp-devices.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


/**
 * Check if the two otp-devices are identical.
 *
 * @param t1 device to compare
 * @param t2 other device to compare
 * @return true if they are 'equal', false if not or of payto_uris is not an array
 */
static bool
otp_devices_equal (const struct TALER_MERCHANTDB_OtpDeviceDetails *t1,
                   const struct TALER_MERCHANTDB_OtpDeviceDetails *t2)
{
  return ( (0 == strcmp (t1->otp_description,
                         t2->otp_description)) &&
           (0 == strcmp (t1->otp_key,
                         t2->otp_key) ) &&
           (t1->otp_ctr == t2->otp_ctr) &&
           (t1->otp_algorithm == t2->otp_algorithm) );
}


MHD_RESULT
TMH_private_post_otp_devices (const struct TMH_RequestHandler *rh,
                              struct MHD_Connection *connection,
                              struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_OtpDeviceDetails tp = { 0 };
  const char *device_id;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("otp_device_id",
                             &device_id),
    GNUNET_JSON_spec_string ("otp_device_description",
                             (const char **) &tp.otp_description),
    TALER_JSON_spec_otp_type ("otp_algorithm",
                              &tp.otp_algorithm),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_uint64 ("otp_ctr",
                               &tp.otp_ctr),
      NULL),
    TALER_JSON_spec_otp_key ("otp_key",
                             (const char **) &tp.otp_key),
    GNUNET_JSON_spec_end ()
  };

  GNUNET_assert (NULL != mi);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
    {
      GNUNET_break_op (0);
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
    }
  }

  /* finally, interact with DB until no serialization error */
  for (unsigned int i = 0; i<MAX_RETRIES; i++)
  {
    /* Test if a OTP device of this id is known */
    struct TALER_MERCHANTDB_OtpDeviceDetails etp;

    if (GNUNET_OK !=
        TMH_db->start (TMH_db->cls,
                       "/post otp-devices"))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_START_FAILED,
                                         NULL);
    }
    qs = TMH_db->select_otp (TMH_db->cls,
                             mi->settings.id,
                             device_id,
                             &etp);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      /* Clean up and fail hard */
      GNUNET_break (0);
      TMH_db->rollback (TMH_db->cls);
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SOFT_ERROR:
      /* restart transaction */
      goto retry;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      /* Good, we can proceed! */
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      /* idempotency check: is etp == tp? */
      {
        bool eq;

        eq = otp_devices_equal (&tp,
                                &etp);
        GNUNET_free (etp.otp_description);
        GNUNET_free (etp.otp_key);
        TMH_db->rollback (TMH_db->cls);
        GNUNET_JSON_parse_free (spec);
        return eq
          ? TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0)
          : TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_CONFLICT,
                                        TALER_EC_MERCHANT_PRIVATE_POST_OTP_DEVICES_CONFLICT_OTP_DEVICE_EXISTS,
                                        device_id);
      }
    } /* end switch (qs) */

    qs = TMH_db->insert_otp (TMH_db->cls,
                             mi->settings.id,
                             device_id,
                             &tp);
    if (GNUNET_DB_STATUS_HARD_ERROR == qs)
    {
      TMH_db->rollback (TMH_db->cls);
      break;
    }
    if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT == qs)
    {
      qs = TMH_db->commit (TMH_db->cls);
      if (GNUNET_DB_STATUS_SOFT_ERROR != qs)
        break;
    }
retry:
    GNUNET_assert (GNUNET_DB_STATUS_SOFT_ERROR == qs);
    TMH_db->rollback (TMH_db->cls);
  } /* for RETRIES loop */
  GNUNET_JSON_parse_free (spec);
  if (qs < 0)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      (GNUNET_DB_STATUS_SOFT_ERROR == qs)
      ? TALER_EC_GENERIC_DB_SOFT_FAILURE
      : TALER_EC_GENERIC_DB_COMMIT_FAILED,
      NULL);
  }
  return TALER_MHD_reply_static (connection,
                                 MHD_HTTP_NO_CONTENT,
                                 NULL,
                                 NULL,
                                 0);
}


/* end of taler-merchant-httpd_private-post-otp-devices.c */
