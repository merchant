/*
  This file is part of TALER
  (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-accounts-ID.c
 * @brief implement GET /accounts/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-accounts-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/accounts/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_accounts_ID (const struct TMH_RequestHandler *rh,
                             struct MHD_Connection *connection,
                             struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *h_wire_s = hc->infix;
  struct TALER_MerchantWireHashP h_wire;
  struct TALER_MERCHANTDB_AccountDetails tp = { 0 };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != h_wire_s);
  if (GNUNET_OK !=
      GNUNET_STRINGS_string_to_data (h_wire_s,
                                     strlen (h_wire_s),
                                     &h_wire,
                                     sizeof (h_wire)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_MERCHANT_GENERIC_H_WIRE_MALFORMED,
                                       h_wire_s);
  }
  qs = TMH_db->select_account (TMH_db->cls,
                               mi->settings.id,
                               &h_wire,
                               &tp);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "lookup_account");
  }
  if (0 == qs)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_ACCOUNT_UNKNOWN,
                                       hc->infix);
  }
  {
    MHD_RESULT ret;

    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_bool ("active",
                             tp.active),
      TALER_JSON_pack_full_payto ("payto_uri",
                                  tp.payto_uri),
      GNUNET_JSON_pack_data_auto ("h_wire",
                                  &tp.h_wire),
      GNUNET_JSON_pack_data_auto ("salt",
                                  &tp.salt),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("credit_facade_url",
                                 tp.credit_facade_url)));
    /* We do not return the credentials, as they may
       be sensitive */
    json_decref (tp.credit_facade_credentials);
    GNUNET_free (tp.payto_uri.full_payto);
    GNUNET_free (tp.credit_facade_url);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-accounts-ID.c */
