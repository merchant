/*
  This file is part of TALER
  (C) 2023, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-token-families-SLUG.c
 * @brief implement GET /tokenfamilies/$SLUG/
 * @author Christian Blättler
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-token-families-SLUG.h"
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/tokenfamilies/$SLUG" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_tokenfamilies_SLUG (const struct TMH_RequestHandler *rh,
                                    struct MHD_Connection *connection,
                                    struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_TokenFamilyDetails details = { 0 };
  enum GNUNET_DB_QueryStatus status;

  GNUNET_assert (NULL != mi);
  status = TMH_db->lookup_token_family (TMH_db->cls,
                                        mi->settings.id,
                                        hc->infix,
                                        &details);
  if (0 > status)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "lookup_token_family");
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == status)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_TOKEN_FAMILY_UNKNOWN,
                                       hc->infix);
  }
  {
    char *kind = NULL;
    MHD_RESULT result;

    if (TALER_MERCHANTDB_TFK_Subscription == details.kind)
    {
      kind = GNUNET_strdup ("subscription");
    }
    else if (TALER_MERCHANTDB_TFK_Discount == details.kind)
    {
      kind = GNUNET_strdup ("discount");
    }
    else
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                         "invalid_token_family_kind");
    }

    result = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("slug",
                               details.slug),
      GNUNET_JSON_pack_string ("name",
                               details.name),
      GNUNET_JSON_pack_string ("description",
                               details.description),
      GNUNET_JSON_pack_object_steal ("description_i18n",
                                     details.description_i18n),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_object_steal ("extra_data",
                                       details.extra_data)),
      GNUNET_JSON_pack_timestamp ("valid_after",
                                  details.valid_after),
      GNUNET_JSON_pack_timestamp ("valid_before",
                                  details.valid_before),
      GNUNET_JSON_pack_time_rel ("duration",
                                 details.duration),
      GNUNET_JSON_pack_time_rel ("validity_granularity",
                                 details.validity_granularity),
      GNUNET_JSON_pack_time_rel ("start_offset",
                                 details.start_offset),
      GNUNET_JSON_pack_string ("kind",
                               kind),
      GNUNET_JSON_pack_int64 ("issued",
                              details.issued),
      GNUNET_JSON_pack_int64 ("used",
                              details.used)
      );

    GNUNET_free (details.name);
    GNUNET_free (details.description);
    GNUNET_free (details.cipher_spec);
    GNUNET_free (kind);
    return result;
  }
}


/* end of taler-merchant-httpd_private-get-token-families-SLUG.c */
