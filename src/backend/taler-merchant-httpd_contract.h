/*
  This file is part of TALER
  (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_contract.h
 * @brief shared logic for contract terms handling
 * @author Christian Blättler
 */
#ifndef TALER_MERCHANT_HTTPD_CONTRACT_H
#define TALER_MERCHANT_HTTPD_CONTRACT_H

#include "taler-merchant-httpd.h"
#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_time_lib.h>
#include "taler_merchant_util.h"
#include <jansson.h>


/**
 * Find matching token family in @a families based on @a slug. Then use
 * @a valid_after to find the matching public key within it.
 *
 * @param slug slug of the token family
 * @param valid_after end time of the validity period of the key to find
 * @param families array of token families to search in
 * @param families_len length of the @a families array
 * @param[out] family found family, set to NULL to only check for existence
 * @param[out] key found key, set to NULL to only check for existence
 * @return #GNUNET_OK on success #GNUNET_NO if no key was found
 */
// FIXME: awkward API, implementation duplicates code...
enum GNUNET_GenericReturnValue
TMH_find_token_family_key (
  const char *slug,
  struct GNUNET_TIME_Timestamp valid_after,
  const struct TALER_MERCHANT_ContractTokenFamily *families,
  unsigned int families_len,
  struct TALER_MERCHANT_ContractTokenFamily *family,
  struct TALER_MERCHANT_ContractTokenFamilyKey *key);

#endif
