/*
  This file is part of TALER
  (C) 2019, 2020, 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-products-ID.c
 * @brief implement GET /products/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-products-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/products/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_products_ID (
  const struct TMH_RequestHandler *rh,
  struct MHD_Connection *connection,
  struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_ProductDetails pd = { 0 };
  enum GNUNET_DB_QueryStatus qs;
  size_t num_categories = 0;
  uint64_t *categories = NULL;
  json_t *jcategories;

  GNUNET_assert (NULL != mi);
  qs = TMH_db->lookup_product (TMH_db->cls,
                               mi->settings.id,
                               hc->infix,
                               &pd,
                               &num_categories,
                               &categories);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "lookup_product");
  }
  if (0 == qs)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_PRODUCT_UNKNOWN,
                                       hc->infix);
  }
  jcategories = json_array ();
  GNUNET_assert (NULL != jcategories);
  for (size_t i = 0; i<num_categories; i++)
  {
    GNUNET_assert (0 ==
                   json_array_append_new (jcategories,
                                          json_integer (categories[i])));
  }
  GNUNET_free (categories);

  {
    MHD_RESULT ret;

    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("description",
                               pd.description),
      GNUNET_JSON_pack_object_steal ("description_i18n",
                                     pd.description_i18n),
      GNUNET_JSON_pack_string ("unit",
                               pd.unit),
      GNUNET_JSON_pack_array_steal ("categories",
                                    jcategories),
      TALER_JSON_pack_amount ("price",
                              &pd.price),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("image",
                                 pd.image)),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_array_steal ("taxes",
                                      pd.taxes)),
      GNUNET_JSON_pack_int64 ("total_stock",
                              (INT64_MAX == pd.total_stock)
                              ? -1LL
                              : pd.total_stock),
      GNUNET_JSON_pack_uint64 ("total_sold",
                               pd.total_sold),
      GNUNET_JSON_pack_uint64 ("total_lost",
                               pd.total_lost),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_object_steal ("address",
                                       pd.address)),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_timestamp ("next_restock",
                                    (pd.next_restock))),
      GNUNET_JSON_pack_uint64 ("minimum_age",
                               pd.minimum_age));
    GNUNET_free (pd.description);
    GNUNET_free (pd.image);
    GNUNET_free (pd.unit);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-products-ID.c */
