/*
  This file is part of GNU Taler
  (C) 2023 Taler Systems SA

  GNU Taler is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  GNU Taler is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-post-instances-ID-token.c
 * @brief implementing DELETE /instances/$ID/token request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-delete-instances-ID-token.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


MHD_RESULT
TMH_private_delete_instances_ID_token (const struct TMH_RequestHandler *rh,
                                       struct MHD_Connection *connection,
                                       struct TMH_HandlerContext *hc)
{
  const char *bearer = "Bearer ";
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *tok;
  struct TALER_MERCHANTDB_LoginTokenP btoken;
  enum GNUNET_DB_QueryStatus qs;

  tok = MHD_lookup_connection_value (connection,
                                     MHD_HEADER_KIND,
                                     MHD_HTTP_HEADER_AUTHORIZATION);
  /* This was presumably checked before... */
  if (0 !=
      strncmp (tok,
               bearer,
               strlen (bearer)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_ec (connection,
                                    TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                    "login token (in 'Authorization' header)");
  }
  tok += strlen (bearer);
  while (' ' == *tok)
    tok++;
  if (0 != strncasecmp (tok,
                        RFC_8959_PREFIX,
                        strlen (RFC_8959_PREFIX)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_ec (connection,
                                    TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                    "login token (in 'Authorization' header)");
  }
  tok += strlen (RFC_8959_PREFIX);

  if (GNUNET_OK !=
      GNUNET_STRINGS_string_to_data (tok,
                                     strlen (tok),
                                     &btoken,
                                     sizeof (btoken)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_ec (connection,
                                    TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                    "login token (in 'Authorization' header)");
  }
  qs = TMH_db->delete_login_token (TMH_db->cls,
                                   mi->settings.id,
                                   &btoken);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_ec (connection,
                                    TALER_EC_GENERIC_DB_STORE_FAILED,
                                    "delete_login_token");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    /* No 404, as the login token must have existed
       when we got the request as it was accepted as
       valid. So we can only get here due to concurrent
       modification, and then the client should still
       simply see the success. Hence, fall-through */
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    return TALER_MHD_reply_static (connection,
                                   MHD_HTTP_NO_CONTENT,
                                   NULL,
                                   NULL,
                                   0);
  }
  GNUNET_break (0);
  return MHD_NO;
}


/* end of taler-merchant-httpd_private-delete-instances-ID-login.c */
