/*
  This file is part of TALER
  (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-post-transfers.c
 * @brief implement API for registering wire transfers
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <jansson.h>
#include <taler/taler_signatures.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_dbevents.h>
#include "taler-merchant-httpd_exchanges.h"
#include "taler-merchant-httpd_helper.h"
#include "taler-merchant-httpd_private-post-transfers.h"


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


MHD_RESULT
TMH_private_post_transfers (const struct TMH_RequestHandler *rh,
                            struct MHD_Connection *connection,
                            struct TMH_HandlerContext *hc)
{
  struct TALER_FullPayto payto_uri;
  const char *exchange_url;
  struct TALER_WireTransferIdentifierRawP wtid;
  struct TALER_Amount amount;
  struct GNUNET_JSON_Specification spec[] = {
    TALER_JSON_spec_amount_any ("credit_amount",
                                &amount),
    GNUNET_JSON_spec_fixed_auto ("wtid",
                                 &wtid),
    TALER_JSON_spec_full_payto_uri ("payto_uri",
                                    &payto_uri),
    TALER_JSON_spec_web_url ("exchange_url",
                             &exchange_url),
    GNUNET_JSON_spec_end ()
  };
  enum GNUNET_GenericReturnValue res;
  enum GNUNET_DB_QueryStatus qs;

  res = TALER_MHD_parse_json_data (connection,
                                   hc->request_body,
                                   spec);
  if (GNUNET_OK != res)
    return (GNUNET_NO == res)
      ? MHD_YES
      : MHD_NO;
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "New inbound wire transfer over %s to %s from %s\n",
              TALER_amount2s (&amount),
              payto_uri.full_payto,
              exchange_url);

  /* Check if transfer data is in database, if not, add it. */
  for (unsigned int retry = 0; retry<MAX_RETRIES; retry++)
  {
    TMH_db->preflight (TMH_db->cls);
    if (GNUNET_OK !=
        TMH_db->start (TMH_db->cls,
                       "post-transfers"))
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_START_FAILED,
                                         "transfer");
    }
    qs = TMH_db->insert_transfer (TMH_db->cls,
                                  hc->instance->settings.id,
                                  exchange_url,
                                  &wtid,
                                  &amount,
                                  payto_uri,
                                  true /* confirmed! */);
    if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
      qs = TMH_db->set_transfer_status_to_confirmed (TMH_db->cls,
                                                     hc->instance->settings.id,
                                                     exchange_url,
                                                     &wtid,
                                                     &amount);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      TMH_db->rollback (TMH_db->cls);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_STORE_FAILED,
                                         "insert_transfer");
    case GNUNET_DB_STATUS_SOFT_ERROR:
      TMH_db->rollback (TMH_db->cls);
      continue;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      /* Could not set to confirmed, must differ by amount! */
      TMH_db->rollback (TMH_db->cls);
      return TALER_MHD_reply_with_error (
        connection,
        MHD_HTTP_CONFLICT,
        TALER_EC_MERCHANT_PRIVATE_POST_TRANSFERS_CONFLICTING_SUBMISSION,
        NULL);
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }
    {
      struct GNUNET_DB_EventHeaderP es = {
        .size = htons (sizeof (es)),
        .type = htons (TALER_DBEVENT_MERCHANT_WIRE_TRANSFER_CONFIRMED)
      };

      TMH_db->event_notify (TMH_db->cls,
                            &es,
                            NULL,
                            0);
    }
    qs = TMH_db->commit (TMH_db->cls);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      TMH_db->rollback (TMH_db->cls);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_COMMIT_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SOFT_ERROR:
      TMH_db->rollback (TMH_db->cls);
      continue;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                  "post-transfer committed successfully\n");
      break;
    }
  }
  return TALER_MHD_reply_static (connection,
                                 MHD_HTTP_NO_CONTENT,
                                 NULL,
                                 NULL,
                                 0);
}


/* end of taler-merchant-httpd_private-post-transfers.c */
