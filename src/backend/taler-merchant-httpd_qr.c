/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_qr.c
 * @brief logic to create QR codes in HTML
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <qrencode.h>
#include <taler-merchant-httpd_qr.h>

/**
 * Create the HTML for a QR code for a URI.
 *
 * @param uri input string to encode
 * @return NULL on error, encoded URI otherwise
 */
char *
TMH_create_qrcode (const char *uri)
{
  QRinput *qri;
  QRcode *qrc;
  struct GNUNET_Buffer buf = { 0 };

  qri = QRinput_new2 (0,
                      QR_ECLEVEL_M);
  if (NULL == qri)
  {
    GNUNET_log_strerror (GNUNET_ERROR_TYPE_WARNING,
                         "QRinput_new2");
    return NULL;
  }
  /* first try encoding as uppercase-only alpha-numerical
     QR code (much smaller encoding); if that fails, also
     try using binary encoding */
  if ( (0 !=
        QRinput_append (qri,
                        QR_MODE_AN,
                        strlen (uri),
                        (unsigned char *) uri)) &&
       (0 !=
        QRinput_append (qri,
                        QR_MODE_8,
                        strlen (uri),
                        (unsigned char *) uri)) )
  {
    GNUNET_log_strerror (GNUNET_ERROR_TYPE_WARNING,
                         "QRinput_append");
    QRinput_free (qri);
    return NULL;
  }
  qrc = QRcode_encodeInput (qri);
  if (NULL == qrc)
  {
    GNUNET_log_strerror (GNUNET_ERROR_TYPE_WARNING,
                         "QRcode_encodeInput");
    QRinput_free (qri);
    return NULL;
  }
  QRinput_free (qri);
  GNUNET_buffer_write_fstr (&buf,
                            "<svg width='100mm' height='100mm' viewBox='0 0 %u %u' "
                            "version='1.1' xmlns='http://www.w3.org/2000/svg' "
                            "style='shape-rendering: crispedges;'>\n",
                            qrc->width,
                            qrc->width);
  for (unsigned int y = 0; y<(unsigned int) qrc->width; y++)
  {
    for (unsigned int x = 0; x<(unsigned int) qrc->width; x++)
    {
      unsigned int off = x + y * (unsigned int) qrc->width;
      if (0 == (qrc->data[off] & 1))
        continue;
      GNUNET_buffer_write_fstr (&buf,
                                " <rect x=%u y=%u width=1 height=1 />\n",
                                x,
                                y);
    }
  }
  GNUNET_buffer_write_str (&buf,
                           "</svg>");
  QRcode_free (qrc);
  return GNUNET_buffer_reap_str (&buf);
}
