/*
  This file is part of TALER
  (C) 2022-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-categories-ID.c
 * @brief implement GET /private/categories/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-categories-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/private/categories/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_categories_ID (
  const struct TMH_RequestHandler *rh,
  struct MHD_Connection *connection,
  struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  enum GNUNET_DB_QueryStatus qs;
  unsigned long long cnum;
  char dummy;
  struct TALER_MERCHANTDB_CategoryDetails cd;
  size_t num_products = 0;
  char *products = NULL;

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != hc->infix);
  if (1 != sscanf (hc->infix,
                   "%llu%c",
                   &cnum,
                   &dummy))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_BAD_REQUEST,
      TALER_EC_GENERIC_PARAMETER_MALFORMED,
      "category_id must be a number");
  }

  qs = TMH_db->select_category (TMH_db->cls,
                                mi->settings.id,
                                cnum,
                                &cd,
                                &num_products,
                                &products);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "select_category");
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_CATEGORY_UNKNOWN,
                                       hc->infix);
  }
  {
    MHD_RESULT ret;
    json_t *jproducts;
    const char *pos = products;

    jproducts = json_array ();
    GNUNET_assert (NULL != jproducts);
    for (unsigned int i = 0; i<num_products; i++)
    {
      const char *product_id = pos;
      json_t *jprod;

      pos = pos + strlen (product_id) + 1;
      jprod = GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("product_id",
                                 product_id));
      GNUNET_assert (0 ==
                     json_array_append_new (jproducts,
                                            jprod));
    }
    GNUNET_free (products);
    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("name",
                               cd.category_name),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_object_incref ("name_i18n",
                                        cd.category_name_i18n)),
      GNUNET_JSON_pack_array_steal ("products",
                                    jproducts));
    TALER_MERCHANTDB_category_details_free (&cd);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-categories-ID.c */
