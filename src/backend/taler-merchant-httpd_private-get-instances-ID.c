/*
  This file is part of TALER
  (C) 2019-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-instances-ID.c
 * @brief implement GET /instances/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-instances-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/instances/$ID" request.
 *
 * @param mi instance to return information about
 * @param connection the MHD connection to handle
 * @return MHD result code
 */
static MHD_RESULT
get_instances_ID (struct TMH_MerchantInstance *mi,
                  struct MHD_Connection *connection)
{
  json_t *ja;
  json_t *auth;

  GNUNET_assert (NULL != mi);
  ja = json_array ();
  GNUNET_assert (NULL != ja);
  for (struct TMH_WireMethod *wm = mi->wm_head;
       NULL != wm;
       wm = wm->next)
  {
    GNUNET_assert (
      0 ==
      json_array_append_new (
        ja,
        GNUNET_JSON_PACK (
          TALER_JSON_pack_full_payto (
            "payto_uri",
            wm->payto_uri),
          GNUNET_JSON_pack_allow_null (
            GNUNET_JSON_pack_string (
              "credit_facade_url",
              wm->credit_facade_url)),
          GNUNET_JSON_pack_data_auto ("h_wire",
                                      &wm->h_wire),
          GNUNET_JSON_pack_data_auto (
            "salt",
            &wm->wire_salt),
          GNUNET_JSON_pack_bool ("active",
                                 wm->active))));
  }

  auth = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_string ("method",
                             GNUNET_is_zero (&mi->auth.auth_hash)
                                                    ? "external"
                                                    : "token"));
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_array_steal ("accounts",
                                  ja),
    GNUNET_JSON_pack_string ("name",
                             mi->settings.name),
    GNUNET_JSON_pack_string (
      "user_type",
      "business"),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("website",
                               mi->settings.website)),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("email",
                               mi->settings.email)),
    GNUNET_JSON_pack_allow_null (
      GNUNET_JSON_pack_string ("logo",
                               mi->settings.logo)),
    GNUNET_JSON_pack_data_auto ("merchant_pub",
                                &mi->merchant_pub),
    GNUNET_JSON_pack_object_incref ("address",
                                    mi->settings.address),
    GNUNET_JSON_pack_object_incref ("jurisdiction",
                                    mi->settings.jurisdiction),
    GNUNET_JSON_pack_bool ("use_stefan",
                           mi->settings.use_stefan),
    GNUNET_JSON_pack_time_rel ("default_wire_transfer_delay",
                               mi->settings.default_wire_transfer_delay),
    GNUNET_JSON_pack_time_rel ("default_pay_delay",
                               mi->settings.default_pay_delay),
    GNUNET_JSON_pack_object_steal ("auth",
                                   auth));
}


MHD_RESULT
TMH_private_get_instances_ID (const struct TMH_RequestHandler *rh,
                              struct MHD_Connection *connection,
                              struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;

  return get_instances_ID (mi,
                           connection);
}


MHD_RESULT
TMH_private_get_instances_default_ID (const struct TMH_RequestHandler *rh,
                                      struct MHD_Connection *connection,
                                      struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi;

  mi = TMH_lookup_instance (hc->infix);
  if ( (NULL == mi) ||
       (mi->deleted) )
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_INSTANCE_UNKNOWN,
                                       hc->infix);
  }
  return get_instances_ID (mi,
                           connection);
}


/* end of taler-merchant-httpd_private-get-instances-ID.c */
