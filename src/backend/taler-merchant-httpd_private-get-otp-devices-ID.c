/*
  This file is part of TALER
  (C) 2022-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-otp-devices-ID.c
 * @brief implement GET /otp-devices/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-otp-devices-ID.h"
#include <taler/taler_json_lib.h>


/**
 * Handle a GET "/otp-devices/$ID" request.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_get_otp_devices_ID (const struct TMH_RequestHandler *rh,
                                struct MHD_Connection *connection,
                                struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_OtpDeviceDetails tp = { 0 };
  enum GNUNET_DB_QueryStatus qs;
  uint64_t faketime_s
    = GNUNET_TIME_timestamp_to_s (GNUNET_TIME_timestamp_get ());
  struct GNUNET_TIME_Timestamp my_time;
  struct TALER_Amount price;

  TALER_MHD_parse_request_number (connection,
                                  "faketime",
                                  &faketime_s);
  memset (&price,
          0,
          sizeof (price));
  TALER_MHD_parse_request_amount (connection,
                                  "price",
                                  &price);
  my_time = GNUNET_TIME_timestamp_from_s (faketime_s);
  GNUNET_assert (NULL != mi);
  qs = TMH_db->select_otp (TMH_db->cls,
                           mi->settings.id,
                           hc->infix,
                           &tp);
  if (0 > qs)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       "select_otp");
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_OTP_DEVICE_UNKNOWN,
                                       hc->infix);
  }
  {
    MHD_RESULT ret;
    char *pos_confirmation;

    pos_confirmation = (NULL == tp.otp_key)
      ? NULL
      : TALER_build_pos_confirmation (tp.otp_key,
                                      tp.otp_algorithm,
                                      &price,
                                      my_time);
    /* Note: we deliberately (by design) do not return the otp_key */
    ret = TALER_MHD_REPLY_JSON_PACK (
      connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_string ("device_description",
                               tp.otp_description),
      GNUNET_JSON_pack_allow_null (
        GNUNET_JSON_pack_string ("otp_code",
                                 pos_confirmation)),
      GNUNET_JSON_pack_uint64 ("otp_timestamp",
                               faketime_s),
      GNUNET_JSON_pack_uint64 ("otp_algorithm",
                               tp.otp_algorithm),
      GNUNET_JSON_pack_uint64 ("otp_ctr",
                               tp.otp_ctr));
    GNUNET_free (pos_confirmation);
    GNUNET_free (tp.otp_description);
    GNUNET_free (tp.otp_key);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-get-otp-devices-ID.c */
