/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_post-using-templates.h
 * @brief headers for POST /using-templates handler
 * @author Priscilla Huang
 */
#ifndef TALER_MERCHANT_HTTPD_POST_USING_TEMPLATES_H
#define TALER_MERCHANT_HTTPD_POST_USING_TEMPLATES_H

#include "taler-merchant-httpd.h"

/**
 * Generate a template that customer can use it. Returns an MHD_RESULT.
 *
 * @param rh details about this request handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_post_using_templates_ID (const struct TMH_RequestHandler *rh,
                             struct MHD_Connection *connection,
                             struct TMH_HandlerContext *hc);


#endif
