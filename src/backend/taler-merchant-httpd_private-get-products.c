/*
  This file is part of TALER
  (C) 2019, 2020, 2021, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-products.c
 * @brief implement GET /products
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-products.h"


/**
 * Add product details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param product_serial serial (row) number of the product in the database
 * @param product_id ID of the product
 */
static void
add_product (void *cls,
             uint64_t product_serial,
             const char *product_id)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_uint64 ("product_serial",
                                              product_serial),
                     GNUNET_JSON_pack_string ("product_id",
                                              product_id))));
}


MHD_RESULT
TMH_private_get_products (const struct TMH_RequestHandler *rh,
                          struct MHD_Connection *connection,
                          struct TMH_HandlerContext *hc)
{
  json_t *pa;
  enum GNUNET_DB_QueryStatus qs;
  int64_t limit;
  uint64_t offset;

  limit = 20; /* default */
  TALER_MHD_parse_request_snumber (connection,
                                   "limit",
                                   &limit);
  if (limit < 0)
    offset = INT64_MAX;
  else
    offset = 0;
  TALER_MHD_parse_request_number (connection,
                                  "offset",
                                  &offset);
  pa = json_array ();
  GNUNET_assert (NULL != pa);
  qs = TMH_db->lookup_products (TMH_db->cls,
                                hc->instance->settings.id,
                                offset,
                                limit,
                                &add_product,
                                pa);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (pa);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (connection,
                                    MHD_HTTP_OK,
                                    GNUNET_JSON_pack_array_steal ("products",
                                                                  pa));
}


/* end of taler-merchant-httpd_private-get-products.c */
