/*
  This file is part of TALER
  (C) 2019, 2020, 2021, 2023, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_config.c
 * @brief implement API for querying configuration data of the backend
 * @author Florian Dold
 */
#include "platform.h"
#include <jansson.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler-merchant-httpd.h"
#include "taler-merchant-httpd_config.h"
#include "taler-merchant-httpd_mhd.h"
#include "taler-merchant-httpd_exchanges.h"


/**
 * Taler protocol version in the format CURRENT:REVISION:AGE
 * as used by GNU libtool.  See
 * https://www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html
 *
 * Please be very careful when updating and follow
 * https://www.gnu.org/software/libtool/manual/html_node/Updating-version-info.html#Updating-version-info
 * precisely.  Note that this version has NOTHING to do with the
 * release version, and the format is NOT the same that semantic
 * versioning uses either.
 *
 * When changing this version, you likely want to also update
 * #MERCHANT_PROTOCOL_CURRENT and #MERCHANT_PROTOCOL_AGE in
 * merchant_api_config.c!
 */
#define MERCHANT_PROTOCOL_VERSION "18:0:15"


/**
 * Callback on an exchange known to us. Does not warrant
 * that the "keys" information is actually available for
 * @a exchange.
 *
 * @param cls closure with `json_t *` array to expand
 * @param url base URL of the exchange
 * @param exchange internal handle for the exchange
 */
static void
add_exchange (void *cls,
              const char *url,
              const struct TMH_Exchange *exchange)
{
  json_t *xa = cls;
  json_t *xi;

  xi = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_auto ("master_pub",
                                TMH_EXCHANGES_get_master_pub (exchange)),
    GNUNET_JSON_pack_string ("currency",
                             TMH_EXCHANGES_get_currency (exchange)),
    GNUNET_JSON_pack_string ("base_url",
                             url));
  GNUNET_assert (NULL != xi);
  GNUNET_assert (0 ==
                 json_array_append_new (xa,
                                        xi));
}


MHD_RESULT
MH_handler_config (const struct TMH_RequestHandler *rh,
                   struct MHD_Connection *connection,
                   struct TMH_HandlerContext *hc)
{
  static struct MHD_Response *response;
  static struct GNUNET_TIME_Absolute a;

  (void) rh;
  (void) hc;
  if ( (GNUNET_TIME_absolute_is_past (a)) &&
       (NULL != response) )
  {
    MHD_destroy_response (response);
    response = NULL;
  }
  if (NULL == response)
  {
    json_t *specs = json_object ();
    json_t *exchanges = json_array ();
    struct GNUNET_TIME_Timestamp km;
    char dat[128];

    GNUNET_assert (NULL != specs);
    GNUNET_assert (NULL != exchanges);
    a = GNUNET_TIME_relative_to_absolute (GNUNET_TIME_UNIT_DAYS);
    /* Round up to next full day to ensure the expiration
       time does not become a fingerprint! */
    a = GNUNET_TIME_absolute_round_down (a,
                                         GNUNET_TIME_UNIT_DAYS);
    a = GNUNET_TIME_absolute_add (a,
                                  GNUNET_TIME_UNIT_DAYS);
    /* => /config response stays at most 48h in caches! */
    km = GNUNET_TIME_absolute_to_timestamp (a);
    TALER_MHD_get_date_string (km.abs_time,
                               dat);
    TMH_exchange_get_trusted (&add_exchange,
                              exchanges);
    for (unsigned int i = 0; i<TMH_num_cspecs; i++)
    {
      const struct TALER_CurrencySpecification *cspec = &TMH_cspecs[i];

      if (TMH_test_exchange_configured_for_currency (cspec->currency))
        GNUNET_assert (0 ==
                       json_object_set_new (specs,
                                            cspec->currency,
                                            TALER_CONFIG_currency_specs_to_json
                                            (
                                              cspec)));
    }
    response = TALER_MHD_MAKE_JSON_PACK (
      GNUNET_JSON_pack_string ("currency",
                               TMH_currency),
      GNUNET_JSON_pack_object_steal ("currencies",
                                     specs),
      GNUNET_JSON_pack_array_steal ("exchanges",
                                    exchanges),
      GNUNET_JSON_pack_string (
        "implementation",
        "urn:net:taler:specs:taler-merchant:c-reference"),
      GNUNET_JSON_pack_string ("name",
                               "taler-merchant"),
      GNUNET_JSON_pack_string ("version",
                               MERCHANT_PROTOCOL_VERSION));
    GNUNET_break (MHD_YES ==
                  MHD_add_response_header (response,
                                           MHD_HTTP_HEADER_EXPIRES,
                                           dat));
    GNUNET_break (MHD_YES ==
                  MHD_add_response_header (response,
                                           MHD_HTTP_HEADER_CACHE_CONTROL,
                                           "public,max-age=21600")); /* 6h */
  }
  return MHD_queue_response (connection,
                             MHD_HTTP_OK,
                             response);
}


/* end of taler-merchant-httpd_config.c */
