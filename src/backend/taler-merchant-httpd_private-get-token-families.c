/*
  This file is part of TALER
  (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-token-families.c
 * @brief implement GET /tokenfamilies
 * @author Christian Blättler
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-token-families.h"



/**
 * Add token family details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param slug slug of the token family
 * @param name name of the token family
 * @param valid_after start time of the token family's validity period
 * @param valid_before end time of the token family's validity period
 * @param kind kind of the token family
 */
static void
add_token_family (void *cls,
                  const char *slug,
                  const char *name,
                  struct GNUNET_TIME_Timestamp valid_after,
                  struct GNUNET_TIME_Timestamp valid_before,
                  const char *kind)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                  json_array_append_new (
                    pa,
                    GNUNET_JSON_PACK (
                      GNUNET_JSON_pack_string ("slug", slug),
                      GNUNET_JSON_pack_string ("name", name),
                      GNUNET_JSON_pack_timestamp ("valid_after", valid_after),
                      GNUNET_JSON_pack_timestamp ("valid_before", valid_before),
                      GNUNET_JSON_pack_string ("kind", kind))));
}


MHD_RESULT
TMH_private_get_tokenfamilies (const struct TMH_RequestHandler *rh,
                               struct MHD_Connection *connection,
                               struct TMH_HandlerContext *hc)
{
  json_t *families;
  enum GNUNET_DB_QueryStatus qs;

  families = json_array ();
  GNUNET_assert (NULL != families);
  qs = TMH_db->lookup_token_families (TMH_db->cls,
                                      hc->instance->settings.id,
                                      &add_token_family,
                                      families);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (families);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (connection,
                                    MHD_HTTP_OK,
                                    GNUNET_JSON_pack_array_steal ("token_families",
                                                                  families));
}


/* end of taler-merchant-httpd_private-get-token-families.c */
