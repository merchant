/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_spa.h
 * @brief logic to preload and serve static files
 * @author Christian Grothoff
 */
#ifndef TALER_MERCHANT_HTTPD_SPA_H
#define TALER_MERCHANT_HTTPD_SPA_H

#include <microhttpd.h>
#include "taler-merchant-httpd.h"


/**
 * Return our single-page-app user interface (see contrib/spa/).
 *
 * @param rh request handler
 * @param connection the connection we act upon
 * @param hc handler context
 * @return #MHD_YES on success (reply queued), #MHD_NO on error (close connection)
 */
MHD_RESULT
TMH_return_spa (const struct TMH_RequestHandler *rh,
                struct MHD_Connection *connection,
                struct TMH_HandlerContext *hc);


/**
 * Preload and compress SPA files.
 *
 * @return #GNUNET_OK on success
 */
enum GNUNET_GenericReturnValue
TMH_spa_init (void);


#endif
