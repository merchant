/*
  This file is part of TALER
  (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-post-categories.c
 * @brief implementing POST /private/categories request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-post-categories.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


MHD_RESULT
TMH_private_post_categories (const struct TMH_RequestHandler *rh,
                             struct MHD_Connection *connection,
                             struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *category_name;
  const json_t *category_name_i18n;
  uint64_t category_id;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("name",
                             &category_name),
    GNUNET_JSON_spec_object_const ("name_i18n",
                                   &category_name_i18n),
    GNUNET_JSON_spec_end ()
  };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
    {
      GNUNET_break_op (0);
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
    }
  }

  /* finally, interact with DB until no serialization error */
  for (unsigned int i = 0; i<MAX_RETRIES; i++)
  {
    json_t *xcategory_name_i18n;

    if (GNUNET_OK !=
        TMH_db->start (TMH_db->cls,
                       "POST /categories"))
    {
      GNUNET_break (0);
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_START_FAILED,
                                         NULL);
    }
    qs = TMH_db->select_category_by_name (TMH_db->cls,
                                          mi->settings.id,
                                          category_name,
                                          &xcategory_name_i18n,
                                          &category_id);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      /* Clean up and fail hard */
      GNUNET_break (0);
      TMH_db->rollback (TMH_db->cls);
      GNUNET_JSON_parse_free (spec);
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SOFT_ERROR:
      /* restart transaction */
      goto retry;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      /* Good, we can proceed! */
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      /* idempotency check: is etp == tp? */
      {
        bool eq;

        eq = (1 == json_equal (xcategory_name_i18n,
                               category_name_i18n));
        json_decref (xcategory_name_i18n);
        TMH_db->rollback (TMH_db->cls);
        GNUNET_JSON_parse_free (spec);
        return eq
          ? TALER_MHD_REPLY_JSON_PACK (connection,
                                       MHD_HTTP_OK,
                                       GNUNET_JSON_pack_uint64 ("category_id",
                                                                category_id))
          : TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_CONFLICT,
                                        TALER_EC_MERCHANT_PRIVATE_POST_CATEGORIES_CONFLICT_CATEGORY_EXISTS,
                                        category_name);
      }
    } /* end switch (qs) */

    qs = TMH_db->insert_category (TMH_db->cls,
                                  mi->settings.id,
                                  category_name,
                                  category_name_i18n,
                                  &category_id);
    if (GNUNET_DB_STATUS_HARD_ERROR == qs)
    {
      TMH_db->rollback (TMH_db->cls);
      break;
    }
    if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT == qs)
    {
      qs = TMH_db->commit (TMH_db->cls);
      if (GNUNET_DB_STATUS_SOFT_ERROR != qs)
        break;
    }
retry:
    GNUNET_assert (GNUNET_DB_STATUS_SOFT_ERROR == qs);
    TMH_db->rollback (TMH_db->cls);
  } /* for RETRIES loop */
  GNUNET_JSON_parse_free (spec);
  if (qs < 0)
  {
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      (GNUNET_DB_STATUS_SOFT_ERROR == qs)
      ? TALER_EC_GENERIC_DB_SOFT_FAILURE
      : TALER_EC_GENERIC_DB_COMMIT_FAILED,
      NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_uint64 ("category_id",
                             category_id));
}


/* end of taler-merchant-httpd_private-post-categories.c */
