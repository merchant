/*
  This file is part of TALER
  (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-categories-ID.c
 * @brief implementing PATCH /categories/$ID request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-categories-ID.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


MHD_RESULT
TMH_private_patch_categories_ID (const struct TMH_RequestHandler *rh,
                                 struct MHD_Connection *connection,
                                 struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  unsigned long long cnum;
  char dummy;
  const char *category_name;
  const json_t *category_name_i18n;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("name",
                             &category_name),
    GNUNET_JSON_spec_object_const ("name_i18n",
                                   &category_name_i18n),
    GNUNET_JSON_spec_end ()
  };
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != hc->infix);
  if (1 != sscanf (hc->infix,
                   "%llu%c",
                   &cnum,
                   &dummy))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "category_id must be a number");
  }

  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }

  qs = TMH_db->update_category (TMH_db->cls,
                                mi->settings.id,
                                cnum,
                                category_name,
                                category_name_i18n);
  {
    MHD_RESULT ret = MHD_NO;

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        "update_category");
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_NOT_FOUND,
                                        TALER_EC_MERCHANT_GENERIC_CATEGORY_UNKNOWN,
                                        category_name);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-categories-ID.c */
