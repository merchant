/*
  This file is part of TALER
  (C) 2017-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-orders-ID.c
 * @brief implementation of GET /private/orders/ID handler
 * @author Florian Dold
 * @author Christian Grothoff
 * @author Bohdan Potuzhnyi
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_util.h>
#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_json_lib.h>
#include "taler_merchant_util.h"
#include "taler-merchant-httpd_helper.h"
#include "taler-merchant-httpd_private-get-orders.h"
#include "taler-merchant-httpd_private-get-orders-ID.h"

/**
 * Data structure we keep for a check payment request.
 */
struct GetOrderRequestContext;


/**
 * Request to an exchange for details about wire transfers
 * in response to a coin's deposit operation.
 */
struct TransferQuery
{

  /**
   * Kept in a DLL.
   */
  struct TransferQuery *next;

  /**
   * Kept in a DLL.
   */
  struct TransferQuery *prev;

  /**
   * Base URL of the exchange.
   */
  char *exchange_url;

  /**
   * Overall request this TQ belongs with.
   */
  struct GetOrderRequestContext *gorc;

  /**
   * Hash of the merchant's bank account the transfer (presumably) went to.
   */
  struct TALER_MerchantWireHashP h_wire;

  /**
   * Value deposited (including deposit fee).
   */
  struct TALER_Amount amount_with_fee;

  /**
   * Deposit fee paid for this coin.
   */
  struct TALER_Amount deposit_fee;

  /**
   * Public key of the coin this is about.
   */
  struct TALER_CoinSpendPublicKeyP coin_pub;

  /**
   * Which deposit operation is this about?
   */
  uint64_t deposit_serial;

};


/**
 * Phases of order processing.
 */
enum GetOrderPhase
{
  /**
   * Initialization.
   */
  GOP_INIT = 0,

  /**
   * Obtain contract terms from database.
   */
  GOP_FETCH_CONTRACT = 1,

  /**
   * Parse the contract terms.
   */
  GOP_PARSE_CONTRACT = 2,

  /**
   * Check if the contract was fully paid.
   */
  GOP_CHECK_PAID = 3,

  /**
   * Check if the wallet may have purchased an equivalent
   * order before and we need to redirect the wallet to
   * an existing paid order.
   */
  GOP_CHECK_REPURCHASE = 4,

  /**
   * Terminate processing of unpaid orders, either by
   * suspending until payment or by returning the
   * unpaid order status.
   */
  GOP_UNPAID_FINISH = 5,

  /**
   * Check if the (paid) order was refunded.
   */
  GOP_CHECK_REFUNDS = 6,

  /**
   * Load all deposits associated with the order.
   */
  GOP_CHECK_DEPOSITS = 7,

  /**
   * Check local records for transfers of funds to
   * the merchant.
   */
  GOP_CHECK_LOCAL_TRANSFERS = 8,

  /**
   * Generate final comprehensive result.
   */
  GOP_REPLY_RESULT = 9,

  /**
   * End with the HTTP status and error code in
   * wire_hc and wire_ec.
   */
  GOP_ERROR = 10,

  /**
   * We are suspended awaiting payment.
   */
  GOP_SUSPENDED_ON_UNPAID = 11,

  /**
   * Processing is done, return #MHD_YES.
   */
  GOP_END_YES = 12,

  /**
   * Processing is done, return #MHD_NO.
   */
  GOP_END_NO = 13

};


/**
 * Data structure we keep for a check payment request.
 */
struct GetOrderRequestContext
{

  /**
   * Processing phase we are in.
   */
  enum GetOrderPhase phase;

  /**
   * Entry in the #resume_timeout_heap for this check payment, if we are
   * suspended.
   */
  struct TMH_SuspendedConnection sc;

  /**
   * Which merchant instance is this for?
   */
  struct TMH_HandlerContext *hc;

  /**
   * session of the client
   */
  const char *session_id;

  /**
   * Kept in a DLL while suspended on exchange.
   */
  struct GetOrderRequestContext *next;

  /**
   * Kept in a DLL while suspended on exchange.
   */
  struct GetOrderRequestContext *prev;

  /**
   * Head of DLL of individual queries for transfer data.
   */
  struct TransferQuery *tq_head;

  /**
   * Tail of DLL of individual queries for transfer data.
   */
  struct TransferQuery *tq_tail;

  /**
   * Timeout task while waiting on exchange.
   */
  struct GNUNET_SCHEDULER_Task *tt;

  /**
   * Database event we are waiting on to be resuming
   * for payment or refunds.
   */
  struct GNUNET_DB_EventHandler *eh;

  /**
   * Database event we are waiting on to be resuming
   * for session capture.
   */
  struct GNUNET_DB_EventHandler *session_eh;

  /**
   * Contract terms of the payment we are checking. NULL when they
   * are not (yet) known.
   */
  json_t *contract_terms_json;

  /**
   * Parsed contract terms, NULL when parsing failed
   */
  struct TALER_MERCHANT_Contract *contract_terms;

  /**
   * Claim token of the order.
   */
  struct TALER_ClaimTokenP claim_token;

  /**
   * Timestamp of the last payment.
   */
  struct GNUNET_TIME_Timestamp last_payment;

  /**
   * Wire details for the payment, to be returned in the reply. NULL
   * if not available.
   */
  json_t *wire_details;

  /**
   * Details about refunds, NULL if there are no refunds.
   */
  json_t *refund_details;

  /**
   * Amount of the order, unset for unpaid v1 orders.
   */
  struct TALER_Amount contract_amount;

  /**
   * Hash over the @e contract_terms.
   */
  struct TALER_PrivateContractHashP h_contract_terms;

  /**
   * Total amount the exchange deposited into our bank account
   * (confirmed or unconfirmed), excluding fees.
   */
  struct TALER_Amount deposits_total;

  /**
   * Total amount in deposit fees we paid for all coins.
   */
  struct TALER_Amount deposit_fees_total;

  /**
   * Total value of the coins that the exchange deposited into our bank
   * account (confirmed or unconfirmed), including deposit fees.
   */
  struct TALER_Amount value_total;

  /**
   * Serial ID of the order.
   */
  uint64_t order_serial;

  /**
   * Index of selected choice from ``choices`` array in the contract_terms.
   * Is -1 for orders without choices.
   */
  int16_t choice_index;

  /**
   * Total refunds granted for this payment. Only initialized
   * if @e refunded is set to true.
   */
  struct TALER_Amount refund_amount;

  /**
   * Exchange HTTP error code encountered while trying to determine wire transfer
   * details. #TALER_EC_NONE for no error encountered.
   */
  unsigned int exchange_hc;

  /**
   * Exchange error code encountered while trying to determine wire transfer
   * details. #TALER_EC_NONE for no error encountered.
   */
  enum TALER_ErrorCode exchange_ec;

  /**
   * Error code encountered while trying to determine wire transfer
   * details. #TALER_EC_NONE for no error encountered.
   */
  enum TALER_ErrorCode wire_ec;

  /**
   * Set to YES if refunded orders should be included when
   * doing repurchase detection.
   */
  enum TALER_EXCHANGE_YesNoAll allow_refunded_for_repurchase;

  /**
   * HTTP status to return with @e wire_ec, 0 if @e wire_ec is #TALER_EC_NONE.
   */
  unsigned int wire_hc;

  /**
   * Did we suspend @a connection and are thus in
   * the #gorc_head DLL (#GNUNET_YES). Set to
   * #GNUNET_NO if we are not suspended, and to
   * #GNUNET_SYSERR if we should close the connection
   * without a response due to shutdown.
   */
  enum GNUNET_GenericReturnValue suspended;

  /**
   * Set to true if this payment has been refunded and
   * @e refund_amount is initialized.
   */
  bool refunded;

  /**
   * True if the order was paid.
   */
  bool paid;

  /**
   * True if the paid session in the database matches
   * our @e session_id.
   */
  bool paid_session_matches;

  /**
   * True if the exchange wired the money to the merchant.
   */
  bool wired;

  /**
   * True if the order remains unclaimed.
   */
  bool order_only;

  /**
   * Set to true if this payment has been refunded and
   * some refunds remain to be picked up by the wallet.
   */
  bool refund_pending;

  /**
   * Set to true if our database (incorrectly) has refunds
   * in a different currency than the currency of the
   * original payment for the order.
   */
  bool refund_currency_mismatch;

  /**
   * Set to true if our database (incorrectly) has deposits
   * in a different currency than the currency of the
   * original payment for the order.
   */
  bool deposit_currency_mismatch;
};


/**
 * Head of list of suspended requests waiting on the exchange.
 */
static struct GetOrderRequestContext *gorc_head;

/**
 * Tail of list of suspended requests waiting on the exchange.
 */
static struct GetOrderRequestContext *gorc_tail;


void
TMH_force_gorc_resume (void)
{
  struct GetOrderRequestContext *gorc;

  while (NULL != (gorc = gorc_head))
  {
    GNUNET_CONTAINER_DLL_remove (gorc_head,
                                 gorc_tail,
                                 gorc);
    GNUNET_assert (GNUNET_YES == gorc->suspended);
    gorc->suspended = GNUNET_SYSERR;
    MHD_resume_connection (gorc->sc.con);
  }
}


/**
 * We have received a trigger from the database
 * that we should (possibly) resume the request.
 *
 * @param cls a `struct GetOrderRequestContext` to resume
 * @param extra string encoding refund amount (or NULL)
 * @param extra_size number of bytes in @a extra
 */
static void
resume_by_event (void *cls,
                 const void *extra,
                 size_t extra_size)
{
  struct GetOrderRequestContext *gorc = cls;

  (void) extra;
  (void) extra_size;
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Resuming request for order %s by trigger\n",
              gorc->hc->infix);
  if (GNUNET_NO == gorc->suspended)
    return; /* duplicate event is possible */
  gorc->suspended = GNUNET_NO;
  gorc->phase = GOP_FETCH_CONTRACT;
  GNUNET_CONTAINER_DLL_remove (gorc_head,
                               gorc_tail,
                               gorc);
  MHD_resume_connection (gorc->sc.con);
  TALER_MHD_daemon_trigger ();   /* we resumed, kick MHD */
}


/**
 * Clean up the session state for a GET /private/order/ID request.
 *
 * @param cls closure, must be a `struct GetOrderRequestContext *`
 */
static void
gorc_cleanup (void *cls)
{
  struct GetOrderRequestContext *gorc = cls;

  if (NULL != gorc->contract_terms_json)
    json_decref (gorc->contract_terms_json);
  if (NULL != gorc->contract_terms)
  {
    TALER_MERCHANT_contract_free (gorc->contract_terms);
    gorc->contract_terms = NULL;
  }
  if (NULL != gorc->wire_details)
    json_decref (gorc->wire_details);
  if (NULL != gorc->refund_details)
    json_decref (gorc->refund_details);
  if (NULL != gorc->tt)
  {
    GNUNET_SCHEDULER_cancel (gorc->tt);
    gorc->tt = NULL;
  }
  if (NULL != gorc->eh)
  {
    TMH_db->event_listen_cancel (gorc->eh);
    gorc->eh = NULL;
  }
  if (NULL != gorc->session_eh)
  {
    TMH_db->event_listen_cancel (gorc->session_eh);
    gorc->session_eh = NULL;
  }
  GNUNET_free (gorc);
}


/**
 * Processing the request @a gorc is finished, set the
 * final return value in phase based on @a mret.
 *
 * @param[in,out] gorc order context to initialize
 * @param mret MHD HTTP response status to return
 */
static void
phase_end (struct GetOrderRequestContext *gorc,
           MHD_RESULT mret)
{
  gorc->phase = (MHD_YES == mret)
    ? GOP_END_YES
    : GOP_END_NO;
}


/**
 * Initialize event callbacks for the order processing.
 *
 * @param[in,out] gorc order context to initialize
 */
static void
phase_init (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  struct TMH_OrderPayEventP pay_eh = {
    .header.size = htons (sizeof (pay_eh)),
    .header.type = htons (TALER_DBEVENT_MERCHANT_ORDER_PAID),
    .merchant_pub = hc->instance->merchant_pub
  };

  if (! GNUNET_TIME_absolute_is_future (gorc->sc.long_poll_timeout))
  {
    gorc->phase++;
    return;
  }

  GNUNET_CRYPTO_hash (hc->infix,
                      strlen (hc->infix),
                      &pay_eh.h_order_id);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Subscribing to payment triggers for %p\n",
              gorc);
  gorc->eh = TMH_db->event_listen (
    TMH_db->cls,
    &pay_eh.header,
    GNUNET_TIME_absolute_get_remaining (gorc->sc.long_poll_timeout),
    &resume_by_event,
    gorc);
  if ( (NULL != gorc->session_id) &&
       (NULL != gorc->contract_terms->fulfillment_url) )
  {
    struct TMH_SessionEventP session_eh = {
      .header.size = htons (sizeof (session_eh)),
      .header.type = htons (TALER_DBEVENT_MERCHANT_SESSION_CAPTURED),
      .merchant_pub = hc->instance->merchant_pub
    };

    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Subscribing to session triggers for %p\n",
                gorc);
    GNUNET_CRYPTO_hash (gorc->session_id,
                        strlen (gorc->session_id),
                        &session_eh.h_session_id);
    GNUNET_CRYPTO_hash (gorc->contract_terms->fulfillment_url,
                        strlen (gorc->contract_terms->fulfillment_url),
                        &session_eh.h_fulfillment_url);
    gorc->session_eh
      = TMH_db->event_listen (
          TMH_db->cls,
          &session_eh.header,
          GNUNET_TIME_absolute_get_remaining (gorc->sc.long_poll_timeout),
          &resume_by_event,
          gorc);
  }
  gorc->phase++;
}


/**
 * Obtain latest contract terms from the database.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_fetch_contract (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  enum GNUNET_DB_QueryStatus qs;

  if (NULL != gorc->contract_terms_json)
  {
    /* Free memory filled with old contract terms before fetching the latest
       ones from the DB.  Note that we cannot simply skip the database
       interaction as the contract terms loaded previously might be from an
       earlier *unclaimed* order state (which we loaded in a previous
       invocation of this function and we are back here due to long polling)
       and thus the contract terms could have changed during claiming. Thus,
       we need to fetch the latest contract terms from the DB again. */
    json_decref (gorc->contract_terms_json);
    gorc->contract_terms_json = NULL;
    gorc->order_only = false;
  }
  TMH_db->preflight (TMH_db->cls);
  qs = TMH_db->lookup_contract_terms3 (TMH_db->cls,
                                       hc->instance->settings.id,
                                       hc->infix,
                                       gorc->session_id,
                                       &gorc->contract_terms_json,
                                       &gorc->order_serial,
                                       &gorc->paid,
                                       &gorc->wired,
                                       &gorc->paid_session_matches,
                                       &gorc->claim_token,
                                       &gorc->choice_index);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "lookup_contract_terms (%s) returned %d\n",
              hc->infix,
              (int) qs);
  if (0 > qs)
  {
    /* single, read-only SQL statements should never cause
       serialization problems */
    GNUNET_break (GNUNET_DB_STATUS_SOFT_ERROR != qs);
    /* Always report on hard error as well to enable diagnostics */
    GNUNET_break (GNUNET_DB_STATUS_HARD_ERROR == qs);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "contract terms"));
    return;
  }
  if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT == qs)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Order %s is %s (%s) according to database\n",
                hc->infix,
                gorc->paid ? "paid" : "unpaid",
                gorc->wired ? "wired" : "unwired");
    gorc->phase++;
    return;
  }
  GNUNET_assert (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs);
  GNUNET_assert (! gorc->paid);
  /* No contract, only order, fetch from orders table */
  gorc->order_only = true;
  {
    struct TALER_MerchantPostDataHashP unused;

    /* We need the order for two cases:  Either when the contract doesn't exist yet,
     * or when the order is claimed but unpaid, and we need the claim token. */
    qs = TMH_db->lookup_order (TMH_db->cls,
                               hc->instance->settings.id,
                               hc->infix,
                               &gorc->claim_token,
                               &unused,
                               &gorc->contract_terms_json);
  }
  if (0 > qs)
  {
    /* single, read-only SQL statements should never cause
       serialization problems */
    GNUNET_break (GNUNET_DB_STATUS_SOFT_ERROR != qs);
    /* Always report on hard error as well to enable diagnostics */
    GNUNET_break (GNUNET_DB_STATUS_HARD_ERROR == qs);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "order"));
    return;
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_NOT_FOUND,
                                           TALER_EC_MERCHANT_GENERIC_ORDER_UNKNOWN,
                                           hc->infix));
    return;
  }
  gorc->phase++;
}


/**
 * Obtain parse contract terms of the order.  Extracts the fulfillment URL,
 * total amount, summary and timestamp from the contract terms!
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_parse_contract (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;

  if (NULL == gorc->contract_terms)
  {
    gorc->contract_terms = TALER_MERCHANT_contract_parse (
      gorc->contract_terms_json,
      true);

    if (NULL == gorc->contract_terms)
    {
      GNUNET_break (0);
      phase_end (gorc,
                 TALER_MHD_reply_with_error (
                   gorc->sc.con,
                   MHD_HTTP_INTERNAL_SERVER_ERROR,
                   TALER_EC_MERCHANT_GENERIC_DB_CONTRACT_CONTENT_INVALID,
                   hc->infix));
      return;
    }
  }

  switch (gorc->contract_terms->version)
  {
  case TALER_MERCHANT_CONTRACT_VERSION_0:
    gorc->contract_amount = gorc->contract_terms->details.v0.brutto;
    break;
  case TALER_MERCHANT_CONTRACT_VERSION_1:
    if (gorc->choice_index >= 0)
    {
      if (gorc->choice_index >=
          gorc->contract_terms->details.v1.choices_len)
      {
        GNUNET_break (0);
        phase_end (gorc,
                   TALER_MHD_reply_with_error (
                     gorc->sc.con, MHD_HTTP_INTERNAL_SERVER_ERROR,
                     TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
                     NULL));
        return;
      }

      gorc->contract_amount =
        gorc->contract_terms->details.v1.choices[gorc->choice_index].amount;
    }
    else
    {
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "choice index %i for order %s is invalid or not yet available",
                  gorc->choice_index,
                  gorc->contract_terms->order_id);
    }
    break;
  default:
    {
      GNUNET_break (0);
      phase_end (gorc,
                 TALER_MHD_reply_with_error (
                   gorc->sc.con,
                   MHD_HTTP_INTERNAL_SERVER_ERROR,
                   TALER_EC_MERCHANT_GET_ORDERS_ID_INVALID_CONTRACT_VERSION,
                   NULL));
      return;
    }
  }

  if (! gorc->order_only && GNUNET_OK !=
      TALER_JSON_contract_hash (gorc->contract_terms_json,
                                &gorc->h_contract_terms))
  {
    {
      GNUNET_break (0);
      phase_end (gorc,
                 TALER_MHD_reply_with_error (gorc->sc.con,
                                             MHD_HTTP_INTERNAL_SERVER_ERROR,
                                             TALER_EC_GENERIC_FAILED_COMPUTE_JSON_HASH,
                                             NULL));
      return;
    }
  }
  GNUNET_assert (NULL != gorc->contract_terms_json);
  GNUNET_assert (NULL != gorc->contract_terms);
  gorc->phase++;
}


/**
 * Check payment status of the order.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_check_paid (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;

  if (gorc->order_only)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Order %s unclaimed, no need to lookup payment status\n",
                hc->infix);
    GNUNET_assert (! gorc->paid);
    GNUNET_assert (! gorc->wired);
    gorc->phase++;
    return;
  }
  if (NULL == gorc->session_id)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "No session ID, do not need to lookup session-ID specific payment status (%s/%s)\n",
                gorc->paid ? "paid" : "unpaid",
                gorc->wired ? "wired" : "unwired");
    gorc->phase++;
    return;
  }
  if (! gorc->paid_session_matches)
  {
    gorc->paid = false;
    gorc->wired = false;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Order %s %s for session %s (%s)\n",
              hc->infix,
              gorc->paid ? "paid" : "unpaid",
              gorc->session_id,
              gorc->wired ? "wired" : "unwired");
  gorc->phase++;
}


/**
 * Check if re-purchase detection applies to the order.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_check_repurchase (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  char *already_paid_order_id = NULL;
  enum GNUNET_DB_QueryStatus qs;
  char *taler_pay_uri;
  char *order_status_url;
  MHD_RESULT ret;

  if ( (gorc->paid) ||
       (NULL == gorc->contract_terms->fulfillment_url) ||
       (NULL == gorc->session_id) )
  {
    /* Repurchase cannot apply */
    gorc->phase++;
    return;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Running re-purchase detection for %s/%s\n",
              gorc->session_id,
              gorc->contract_terms->fulfillment_url);
  qs = TMH_db->lookup_order_by_fulfillment (TMH_db->cls,
                                            hc->instance->settings.id,
                                            gorc->contract_terms->
                                            fulfillment_url,
                                            gorc->session_id,
                                            TALER_EXCHANGE_YNA_NO !=
                                            gorc->allow_refunded_for_repurchase,
                                            &already_paid_order_id);
  if (0 > qs)
  {
    /* single, read-only SQL statements should never cause
       serialization problems, and the entry should exist as per above */
    GNUNET_break (GNUNET_DB_STATUS_HARD_ERROR == qs);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "order by fulfillment"));
    return;
  }
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
                "No already paid order for %s/%s\n",
                gorc->session_id,
                gorc->contract_terms->fulfillment_url);
    gorc->phase++;
    return;
  }

  /* User did pay for this order, but under a different session; ask wallet to
     switch order ID */
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Found already paid order %s\n",
              already_paid_order_id);
  taler_pay_uri = TMH_make_taler_pay_uri (gorc->sc.con,
                                          hc->infix,
                                          gorc->session_id,
                                          hc->instance->settings.id,
                                          &gorc->claim_token);
  order_status_url = TMH_make_order_status_url (gorc->sc.con,
                                                hc->infix,
                                                gorc->session_id,
                                                hc->instance->settings.id,
                                                &gorc->claim_token,
                                                NULL);
  if ( (NULL == taler_pay_uri) ||
       (NULL == order_status_url) )
  {
    GNUNET_break_op (0);
    GNUNET_free (taler_pay_uri);
    GNUNET_free (order_status_url);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_BAD_REQUEST,
                                           TALER_EC_GENERIC_HTTP_HEADERS_MALFORMED,
                                           "host"));
    return;
  }
  ret = TALER_MHD_REPLY_JSON_PACK (
    gorc->sc.con,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_string ("taler_pay_uri",
                             taler_pay_uri),
    GNUNET_JSON_pack_string ("order_status_url",
                             order_status_url),
    GNUNET_JSON_pack_string ("order_status",
                             "unpaid"),
    GNUNET_JSON_pack_string ("already_paid_order_id",
                             already_paid_order_id),
    GNUNET_JSON_pack_string ("already_paid_fulfillment_url",
                             gorc->contract_terms->fulfillment_url),
    /* undefined for unpaid v1 contracts */
    GNUNET_JSON_pack_allow_null (
      TALER_JSON_pack_amount ("total_amount",
                              &gorc->contract_amount)),
    GNUNET_JSON_pack_string ("summary",
                             gorc->contract_terms->summary),
    GNUNET_JSON_pack_timestamp ("creation_time",
                                gorc->contract_terms->timestamp));
  GNUNET_free (taler_pay_uri);
  GNUNET_free (already_paid_order_id);
  phase_end (gorc,
             ret);
}


/**
 * Check if we should suspend until the order is paid.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_unpaid_finish (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  char *taler_pay_uri;
  char *order_status_url;
  MHD_RESULT ret;

  if (gorc->paid)
  {
    gorc->phase++;
    return;
  }
  /* User never paid for this order, suspend waiting
     on payment or return details. */
  if (GNUNET_TIME_absolute_is_future (gorc->sc.long_poll_timeout))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Suspending GET /private/orders/%s\n",
                hc->infix);
    GNUNET_CONTAINER_DLL_insert (gorc_head,
                                 gorc_tail,
                                 gorc);
    gorc->phase = GOP_SUSPENDED_ON_UNPAID;
    gorc->suspended = GNUNET_YES;
    MHD_suspend_connection (gorc->sc.con);
    return;
  }
  order_status_url = TMH_make_order_status_url (gorc->sc.con,
                                                hc->infix,
                                                gorc->session_id,
                                                hc->instance->settings.id,
                                                &gorc->claim_token,
                                                NULL);
  if (! gorc->order_only)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Order %s claimed but not paid yet\n",
                hc->infix);
    phase_end (gorc,
               TALER_MHD_REPLY_JSON_PACK (
                 gorc->sc.con,
                 MHD_HTTP_OK,
                 GNUNET_JSON_pack_string ("order_status_url",
                                          order_status_url),
                 GNUNET_JSON_pack_object_incref ("contract_terms",
                                                 gorc->contract_terms_json),
                 GNUNET_JSON_pack_string ("order_status",
                                          "claimed")));
    return;
  }
  taler_pay_uri = TMH_make_taler_pay_uri (gorc->sc.con,
                                          hc->infix,
                                          gorc->session_id,
                                          hc->instance->settings.id,
                                          &gorc->claim_token);
  ret = TALER_MHD_REPLY_JSON_PACK (
    gorc->sc.con,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_string ("taler_pay_uri",
                             taler_pay_uri),
    GNUNET_JSON_pack_string ("order_status_url",
                             order_status_url),
    GNUNET_JSON_pack_string ("order_status",
                             "unpaid"),
    /* undefined for unpaid v1 contracts */
    GNUNET_JSON_pack_allow_null (
      TALER_JSON_pack_amount ("total_amount",
                              &gorc->contract_amount)),
    GNUNET_JSON_pack_string ("summary",
                             gorc->contract_terms->summary),
    GNUNET_JSON_pack_timestamp ("creation_time",
                                gorc->contract_terms->timestamp));
  GNUNET_free (taler_pay_uri);
  GNUNET_free (order_status_url);
  phase_end (gorc,
             ret);

}


/**
 * Function called with information about a refund.
 * It is responsible for summing up the refund amount.
 *
 * @param cls closure
 * @param refund_serial unique serial number of the refund
 * @param timestamp time of the refund (for grouping of refunds in the wallet UI)
 * @param coin_pub public coin from which the refund comes from
 * @param exchange_url URL of the exchange that issued @a coin_pub
 * @param rtransaction_id identificator of the refund
 * @param reason human-readable explanation of the refund
 * @param refund_amount refund amount which is being taken from @a coin_pub
 * @param pending true if the this refund was not yet processed by the wallet/exchange
 */
static void
process_refunds_cb (
  void *cls,
  uint64_t refund_serial,
  struct GNUNET_TIME_Timestamp timestamp,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const char *exchange_url,
  uint64_t rtransaction_id,
  const char *reason,
  const struct TALER_Amount *refund_amount,
  bool pending)
{
  struct GetOrderRequestContext *gorc = cls;

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Found refund %llu over %s for reason %s\n",
              (unsigned long long) rtransaction_id,
              TALER_amount2s (refund_amount),
              reason);
  GNUNET_assert (
    0 ==
    json_array_append_new (
      gorc->refund_details,
      GNUNET_JSON_PACK (
        TALER_JSON_pack_amount ("amount",
                                refund_amount),
        GNUNET_JSON_pack_bool ("pending",
                               pending),
        GNUNET_JSON_pack_timestamp ("timestamp",
                                    timestamp),
        GNUNET_JSON_pack_string ("reason",
                                 reason))));
  /* For refunded coins, we are not charged deposit fees, so subtract those
     again */
  for (struct TransferQuery *tq = gorc->tq_head;
       NULL != tq;
       tq = tq->next)
  {
    if (0 ==
        GNUNET_memcmp (&tq->coin_pub,
                       coin_pub))
    {
      if (GNUNET_OK !=
          TALER_amount_cmp_currency (
            &gorc->deposit_fees_total,
            &tq->deposit_fee))
      {
        gorc->refund_currency_mismatch = true;
        return;
      }

      GNUNET_assert (
        0 <=
        TALER_amount_subtract (&gorc->deposit_fees_total,
                               &gorc->deposit_fees_total,
                               &tq->deposit_fee));
    }
  }
  if (GNUNET_OK !=
      TALER_amount_cmp_currency (
        &gorc->refund_amount,
        refund_amount))
  {
    gorc->refund_currency_mismatch = true;
    return;
  }
  GNUNET_assert (0 <=
                 TALER_amount_add (&gorc->refund_amount,
                                   &gorc->refund_amount,
                                   refund_amount));
  gorc->refunded = true;
  gorc->refund_pending |= pending;
}


/**
 * Check refund status for the order.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_check_refunds (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (! gorc->order_only);
  GNUNET_assert (gorc->paid);

  /* Accumulate refunds, if any. */
  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (gorc->contract_amount.currency,
                                        &gorc->refund_amount));

  qs = TMH_db->lookup_refunds_detailed (
    TMH_db->cls,
    hc->instance->settings.id,
    &gorc->h_contract_terms,
    &process_refunds_cb,
    gorc);
  if (0 > qs)
  {
    GNUNET_break (0);
    phase_end (gorc,
               TALER_MHD_reply_with_error (
                 gorc->sc.con,
                 MHD_HTTP_INTERNAL_SERVER_ERROR,
                 TALER_EC_GENERIC_DB_FETCH_FAILED,
                 "detailed refunds"));
    return;
  }
  if (gorc->refund_currency_mismatch)
  {
    GNUNET_break (0);
    phase_end (gorc,
               TALER_MHD_reply_with_error (
                 gorc->sc.con,
                 MHD_HTTP_INTERNAL_SERVER_ERROR,
                 TALER_EC_GENERIC_DB_FETCH_FAILED,
                 "refunds in different currency than original order price"));
    return;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Total refunds are %s\n",
              TALER_amount2s (&gorc->refund_amount));
  gorc->phase++;
}


/**
 * Function called with each @a coin_pub that was deposited into the
 * @a h_wire account of the merchant for the @a deposit_serial as part
 * of the payment for the order identified by @a cls.
 *
 * Queries the exchange for the payment status associated with the
 * given coin.
 *
 * @param cls a `struct GetOrderRequestContext`
 * @param deposit_serial identifies the deposit operation
 * @param exchange_url URL of the exchange that issued @a coin_pub
 * @param h_wire hash of the merchant's wire account into which the deposit was made
 * @param deposit_timestamp when was the deposit made
 * @param amount_with_fee amount the exchange will deposit for this coin
 * @param deposit_fee fee the exchange will charge for this coin
 * @param coin_pub public key of the deposited coin
 */
static void
deposit_cb (
  void *cls,
  uint64_t deposit_serial,
  const char *exchange_url,
  const struct TALER_MerchantWireHashP *h_wire,
  struct GNUNET_TIME_Timestamp deposit_timestamp,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_CoinSpendPublicKeyP *coin_pub)
{
  struct GetOrderRequestContext *gorc = cls;
  struct TransferQuery *tq;

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Checking deposit status for coin %s (over %s)\n",
              TALER_B2S (coin_pub),
              TALER_amount2s (amount_with_fee));
  gorc->last_payment
    = GNUNET_TIME_timestamp_max (gorc->last_payment,
                                 deposit_timestamp);
  tq = GNUNET_new (struct TransferQuery);
  tq->gorc = gorc;
  tq->exchange_url = GNUNET_strdup (exchange_url);
  tq->deposit_serial = deposit_serial;
  GNUNET_CONTAINER_DLL_insert (gorc->tq_head,
                               gorc->tq_tail,
                               tq);
  tq->coin_pub = *coin_pub;
  tq->h_wire = *h_wire;
  tq->amount_with_fee = *amount_with_fee;
  tq->deposit_fee = *deposit_fee;
}


/**
 * Check wire transfer status for the order at the exchange.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_check_deposits (struct GetOrderRequestContext *gorc)
{
  GNUNET_assert (! gorc->order_only);
  GNUNET_assert (gorc->paid);

  /* amount must be always valid for paid orders */
  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_is_valid (&gorc->contract_amount));

  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (gorc->contract_amount.currency,
                                        &gorc->deposits_total));
  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (gorc->contract_amount.currency,
                                        &gorc->deposit_fees_total));
  TMH_db->lookup_deposits_by_order (TMH_db->cls,
                                    gorc->order_serial,
                                    &deposit_cb,
                                    gorc);
  gorc->phase++;
}


/**
 * Function called with available wire details, to be added to
 * the response.
 *
 * @param cls a `struct GetOrderRequestContext`
 * @param wtid wire transfer subject of the wire transfer for the coin
 * @param exchange_url base URL of the exchange that made the payment
 * @param execution_time when was the payment made
 * @param deposit_value contribution of the coin to the total wire transfer value
 * @param deposit_fee deposit fee charged by the exchange for the coin
 * @param transfer_confirmed did the merchant confirm that a wire transfer with
 *        @a wtid over the total amount happened?
 */
static void
process_transfer_details (
  void *cls,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp execution_time,
  const struct TALER_Amount *deposit_value,
  const struct TALER_Amount *deposit_fee,
  bool transfer_confirmed)
{
  struct GetOrderRequestContext *gorc = cls;
  json_t *wire_details = gorc->wire_details;
  struct TALER_Amount wired;

  if ( (GNUNET_OK !=
        TALER_amount_cmp_currency (&gorc->deposits_total,
                                   deposit_value)) ||
       (GNUNET_OK !=
        TALER_amount_cmp_currency (&gorc->deposit_fees_total,
                                   deposit_fee)) )
  {
    GNUNET_break (0);
    gorc->deposit_currency_mismatch = true;
    return;
  }

  /* Compute total amount *wired* */
  GNUNET_assert (0 <=
                 TALER_amount_add (&gorc->deposits_total,
                                   &gorc->deposits_total,
                                   deposit_value));
  GNUNET_assert (0 <=
                 TALER_amount_add (&gorc->deposit_fees_total,
                                   &gorc->deposit_fees_total,
                                   deposit_fee));
  GNUNET_assert (0 <= TALER_amount_subtract (&wired,
                                             deposit_value,
                                             deposit_fee));
  GNUNET_assert (0 ==
                 json_array_append_new (
                   wire_details,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_data_auto ("wtid",
                                                 wtid),
                     GNUNET_JSON_pack_string ("exchange_url",
                                              exchange_url),
                     TALER_JSON_pack_amount ("amount",
                                             &wired),
                     GNUNET_JSON_pack_timestamp ("execution_time",
                                                 execution_time),
                     GNUNET_JSON_pack_bool ("confirmed",
                                            transfer_confirmed))));
}


/**
 * Check transfer status in local database.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_check_local_transfers (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (gorc->paid);
  GNUNET_assert (! gorc->order_only);

  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (gorc->contract_amount.currency,
                                        &gorc->deposits_total));
  GNUNET_assert (GNUNET_OK ==
                 TALER_amount_set_zero (gorc->contract_amount.currency,
                                        &gorc->deposit_fees_total));

  qs = TMH_db->lookup_transfer_details_by_order (TMH_db->cls,
                                                 gorc->order_serial,
                                                 &process_transfer_details,
                                                 gorc);
  if (0 > qs)
  {
    GNUNET_break (0);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "transfer details"));
    return;
  }
  if (gorc->deposit_currency_mismatch)
  {
    GNUNET_break (0);
    phase_end (gorc,
               TALER_MHD_reply_with_error (gorc->sc.con,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_FETCH_FAILED,
                                           "deposits in different currency than original order price"));
    return;
  }

  if (! gorc->wired)
  {
    /* we believe(d) the wire transfer did not happen yet, check if maybe
       in light of new evidence it did */
    struct TALER_Amount expect_total;

    if (0 >
        TALER_amount_subtract (&expect_total,
                               &gorc->contract_amount,
                               &gorc->refund_amount))
    {
      GNUNET_break (0);
      phase_end (gorc,
                 TALER_MHD_reply_with_error (
                   gorc->sc.con,
                   MHD_HTTP_INTERNAL_SERVER_ERROR,
                   TALER_EC_MERCHANT_GENERIC_DB_CONTRACT_CONTENT_INVALID,
                   "refund exceeds contract value"));
      return;
    }
    if (0 >
        TALER_amount_subtract (&expect_total,
                               &expect_total,
                               &gorc->deposit_fees_total))
    {
      GNUNET_break (0);
      phase_end (gorc,
                 TALER_MHD_reply_with_error (
                   gorc->sc.con,
                   MHD_HTTP_INTERNAL_SERVER_ERROR,
                   TALER_EC_MERCHANT_GENERIC_DB_CONTRACT_CONTENT_INVALID,
                   "deposit fees exceed total minus refunds"));
      return;
    }
    if (0 >=
        TALER_amount_cmp (&expect_total,
                          &gorc->deposits_total))
    {
      /* expect_total <= gorc->deposits_total: good: we got the wire transfer */
      gorc->wired = true;
      qs = TMH_db->mark_order_wired (TMH_db->cls,
                                     gorc->order_serial);
      GNUNET_break (qs >= 0);   /* just warn if transaction failed */
      TMH_notify_order_change (hc->instance,
                               TMH_OSF_PAID
                               | TMH_OSF_WIRED,
                               gorc->contract_terms->timestamp,
                               gorc->order_serial);
    }
  }
  gorc->phase++;
}


/**
 * Generate final result for the status request.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_reply_result (struct GetOrderRequestContext *gorc)
{
  struct TMH_HandlerContext *hc = gorc->hc;
  MHD_RESULT ret;
  char *order_status_url;

  GNUNET_assert (gorc->paid);
  GNUNET_assert (! gorc->order_only);

  {
    struct TALER_PrivateContractHashP *h_contract = NULL;

    /* In a session-bound payment, allow the browser to check the order
     * status page (e.g. to get a refund).
     *
     * Note that we don't allow this outside of session-based payment, as
     * otherwise this becomes an oracle to convert order_id to h_contract.
     */
    if (NULL != gorc->session_id)
      h_contract = &gorc->h_contract_terms;

    order_status_url =
      TMH_make_order_status_url (gorc->sc.con,
                                 hc->infix,
                                 gorc->session_id,
                                 hc->instance->settings.id,
                                 &gorc->claim_token,
                                 h_contract);
  }
  if (GNUNET_TIME_absolute_is_zero (gorc->last_payment.abs_time))
  {
    GNUNET_break (GNUNET_YES ==
                  TALER_amount_is_zero (&gorc->contract_amount));
    gorc->last_payment = gorc->contract_terms->timestamp;
  }
  ret = TALER_MHD_REPLY_JSON_PACK (
    gorc->sc.con,
    MHD_HTTP_OK,
    // Deprecated in protocol v6.
    GNUNET_JSON_pack_array_steal ("wire_reports",
                                  json_array ()),
    GNUNET_JSON_pack_uint64 ("exchange_code",
                             gorc->exchange_ec),
    GNUNET_JSON_pack_uint64 ("exchange_http_status",
                             gorc->exchange_hc),
    /* legacy: */
    GNUNET_JSON_pack_uint64 ("exchange_ec",
                             gorc->exchange_ec),
    /* legacy: */
    GNUNET_JSON_pack_uint64 ("exchange_hc",
                             gorc->exchange_hc),
    TALER_JSON_pack_amount ("deposit_total",
                            &gorc->deposits_total),
    GNUNET_JSON_pack_object_incref ("contract_terms",
                                    gorc->contract_terms_json),
    GNUNET_JSON_pack_string ("order_status",
                             "paid"),
    GNUNET_JSON_pack_timestamp ("last_payment",
                                gorc->last_payment),
    GNUNET_JSON_pack_bool ("refunded",
                           gorc->refunded),
    GNUNET_JSON_pack_bool ("wired",
                           gorc->wired),
    GNUNET_JSON_pack_bool ("refund_pending",
                           gorc->refund_pending),
    GNUNET_JSON_pack_allow_null (
      TALER_JSON_pack_amount ("refund_amount",
                              &gorc->refund_amount)),
    GNUNET_JSON_pack_array_steal ("wire_details",
                                  gorc->wire_details),
    GNUNET_JSON_pack_array_steal ("refund_details",
                                  gorc->refund_details),
    GNUNET_JSON_pack_string ("order_status_url",
                             order_status_url),
    (gorc->choice_index >= 0)
      ? GNUNET_JSON_pack_int64 ("choice_index",
                                gorc->choice_index)
      : GNUNET_JSON_pack_end_ ());
  GNUNET_free (order_status_url);
  gorc->wire_details = NULL;
  gorc->refund_details = NULL;
  phase_end (gorc,
             ret);
}


/**
 * End with error status in wire_hc and wire_ec.
 *
 * @param[in,out] gorc order context to update
 */
static void
phase_error (struct GetOrderRequestContext *gorc)
{
  GNUNET_assert (TALER_EC_NONE != gorc->wire_ec);
  phase_end (gorc,
             TALER_MHD_reply_with_error (gorc->sc.con,
                                         gorc->wire_hc,
                                         gorc->wire_ec,
                                         NULL));
}


MHD_RESULT
TMH_private_get_orders_ID (
  const struct TMH_RequestHandler *rh,
  struct MHD_Connection *connection,
  struct TMH_HandlerContext *hc)
{
  struct GetOrderRequestContext *gorc = hc->ctx;

  if (NULL == gorc)
  {
    /* First time here, parse request and check order is known */
    GNUNET_assert (NULL != hc->infix);
    gorc = GNUNET_new (struct GetOrderRequestContext);
    hc->cc = &gorc_cleanup;
    hc->ctx = gorc;
    gorc->sc.con = connection;
    gorc->hc = hc;
    gorc->wire_details = json_array ();
    GNUNET_assert (NULL != gorc->wire_details);
    gorc->refund_details = json_array ();
    GNUNET_assert (NULL != gorc->refund_details);
    gorc->session_id = MHD_lookup_connection_value (connection,
                                                    MHD_GET_ARGUMENT_KIND,
                                                    "session_id");
    if (! (TALER_MHD_arg_to_yna (connection,
                                 "allow_refunded_for_repurchase",
                                 TALER_EXCHANGE_YNA_NO,
                                 &gorc->allow_refunded_for_repurchase)) )
      return TALER_MHD_reply_with_error (connection,
                                         MHD_HTTP_BAD_REQUEST,
                                         TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                         "allow_refunded_for_repurchase");
    TALER_MHD_parse_request_timeout (connection,
                                     &gorc->sc.long_poll_timeout);
    GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
                "Starting GET /private/orders/%s processing with timeout %s\n",
                hc->infix,
                GNUNET_STRINGS_absolute_time_to_string (
                  gorc->sc.long_poll_timeout));
  }
  if (GNUNET_SYSERR == gorc->suspended)
    return MHD_NO; /* we are in shutdown */
  while (1)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Processing order %s in phase %d\n",
                hc->infix,
                (int) gorc->phase);
    switch (gorc->phase)
    {
    case GOP_INIT:
      phase_init (gorc);
      break;
    case GOP_FETCH_CONTRACT:
      phase_fetch_contract (gorc);
      break;
    case GOP_PARSE_CONTRACT:
      phase_parse_contract (gorc);
      break;
    case GOP_CHECK_PAID:
      phase_check_paid (gorc);
      break;
    case GOP_CHECK_REPURCHASE:
      phase_check_repurchase (gorc);
      break;
    case GOP_UNPAID_FINISH:
      phase_unpaid_finish (gorc);
      break;
    case GOP_CHECK_REFUNDS:
      phase_check_refunds (gorc);
      break;
    case GOP_CHECK_DEPOSITS:
      phase_check_deposits (gorc);
      break;
    case GOP_CHECK_LOCAL_TRANSFERS:
      phase_check_local_transfers (gorc);
      break;
    case GOP_REPLY_RESULT:
      phase_reply_result (gorc);
      break;
    case GOP_ERROR:
      phase_error (gorc);
      break;
    case GOP_SUSPENDED_ON_UNPAID:
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                  "Suspending order request awaiting payment\n");
      return MHD_YES;
    case GOP_END_YES:
      return MHD_YES;
    case GOP_END_NO:
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "Closing connection, no response generated\n");
      return MHD_NO;
    }
  } /* end first-time per-request initialization */
}
