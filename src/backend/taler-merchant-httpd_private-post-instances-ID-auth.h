/*
  This file is part of GNU Taler
  (C) 2021 Taler Systems SA

  GNU Taler is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  GNU Taler is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-post-instances-ID-auth.h
 * @brief implements POST /instances/$ID/auth request handling
 * @author Christian Grothoff
 * @author Florian Dold
 */
#ifndef TALER_MERCHANT_HTTPD_PRIVATE_POST_INSTANCES_ID_AUTH_H
#define TALER_MERCHANT_HTTPD_PRIVATE_POST_INSTANCES_ID_AUTH_H
#include "taler-merchant-httpd.h"


/**
 * Change the instance's auth settings.
 * This is the handler called using the instance's own authentication.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_post_instances_ID_auth (const struct TMH_RequestHandler *rh,
                                    struct MHD_Connection *connection,
                                    struct TMH_HandlerContext *hc);


/**
 * Change the instance's auth settings.
 * This is the handler called using the default instance's authentication.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_post_instances_default_ID_auth (const struct TMH_RequestHandler *rh,
                                            struct MHD_Connection *connection,
                                            struct TMH_HandlerContext *hc);

#endif
