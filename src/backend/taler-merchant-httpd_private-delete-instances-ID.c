/*
  This file is part of TALER
  (C) 2020-2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-delete-instances-ID.c
 * @brief implement DELETE /instances/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-delete-instances-ID.h"
#include <taler/taler_json_lib.h>
#include <taler/taler_dbevents.h>


/**
 * Handle a DELETE "/instances/$ID" request.
 *
 * @param mi instance to delete
 * @param connection the MHD connection to handle
 * @return MHD result code
 */
static MHD_RESULT
delete_instances_ID (struct TMH_MerchantInstance *mi,
                     struct MHD_Connection *connection)
{
  const char *purge_s;
  bool purge;
  enum GNUNET_DB_QueryStatus qs;

  GNUNET_assert (NULL != mi);
  purge_s = MHD_lookup_connection_value (connection,
                                         MHD_GET_ARGUMENT_KIND,
                                         "purge");
  if (NULL == purge_s)
    purge_s = "no";
  purge = (0 == strcasecmp (purge_s,
                            "yes"));
  if (purge)
    qs = TMH_db->purge_instance (TMH_db->cls,
                                 mi->settings.id);
  else
    qs = TMH_db->delete_instance_private_key (TMH_db->cls,
                                              mi->settings.id);
  {
    struct GNUNET_DB_EventHeaderP es = {
      .size = htons (sizeof (es)),
      .type = htons (TALER_DBEVENT_MERCHANT_ACCOUNTS_CHANGED)
    };

    TMH_db->event_notify (TMH_db->cls,
                          &es,
                          NULL,
                          0);
  }
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_STORE_FAILED,
                                       "delete private key");
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       NULL);
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_INSTANCE_UNKNOWN,
                                       purge
                                       ? "Instance unknown"
                                       : "Private key unknown");
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    TMH_reload_instances (mi->settings.id);
    return TALER_MHD_reply_static (connection,
                                   MHD_HTTP_NO_CONTENT,
                                   NULL,
                                   NULL,
                                   0);
  }
  GNUNET_assert (0);
  return MHD_NO;
}


MHD_RESULT
TMH_private_delete_instances_ID (const struct TMH_RequestHandler *rh,
                                 struct MHD_Connection *connection,
                                 struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;

  (void) rh;
  return delete_instances_ID (mi,
                              connection);
}


MHD_RESULT
TMH_private_delete_instances_default_ID (const struct TMH_RequestHandler *rh,
                                         struct MHD_Connection *connection,
                                         struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi;

  (void) rh;
  mi = TMH_lookup_instance (hc->infix);
  if (NULL == mi)
  {
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_INSTANCE_UNKNOWN,
                                       hc->infix);
  }
  return delete_instances_ID (mi,
                              connection);
}


/* end of taler-merchant-httpd_private-delete-instances-ID.c */
