/*
  This file is part of TALER
  (C) 2020-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-post-products.c
 * @brief implementing POST /products request handling
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-post-products.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


MHD_RESULT
TMH_private_post_products (const struct TMH_RequestHandler *rh,
                           struct MHD_Connection *connection,
                           struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  struct TALER_MERCHANTDB_ProductDetails pd = { 0 };
  const json_t *categories = NULL;
  const char *product_id;
  int64_t total_stock;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("product_id",
                             &product_id),
    GNUNET_JSON_spec_string ("description",
                             (const char **) &pd.description),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("description_i18n",
                             &pd.description_i18n),
      NULL),
    GNUNET_JSON_spec_string ("unit",
                             (const char **) &pd.unit),
    TALER_JSON_spec_amount_any ("price",
                                &pd.price),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("image",
                               (const char **) &pd.image),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("taxes",
                             &pd.taxes),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_array_const ("categories",
                                    &categories),
      NULL),
    GNUNET_JSON_spec_int64 ("total_stock",
                            &total_stock),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("address",
                             &pd.address),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_timestamp ("next_restock",
                                  &pd.next_restock),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_uint32 ("minimum_age",
                               &pd.minimum_age),
      NULL),
    GNUNET_JSON_spec_end ()
  };
  size_t num_cats = 0;
  uint64_t *cats = NULL;
  bool conflict;
  bool no_instance;
  ssize_t no_cat;
  enum GNUNET_DB_QueryStatus qs;
  MHD_RESULT ret;

  GNUNET_assert (NULL != mi);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
    {
      GNUNET_break_op (0);
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
    }
  }
  if (total_stock < -1)
  {
    GNUNET_break_op (0);
    ret = TALER_MHD_reply_with_error (connection,
                                      MHD_HTTP_BAD_REQUEST,
                                      TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                      "total_stock");
    goto cleanup;
  }
  num_cats = json_array_size (categories);
  cats = GNUNET_new_array (num_cats,
                           uint64_t);
  {
    size_t idx;
    json_t *val;

    json_array_foreach (categories, idx, val)
    {
      if (! json_is_integer (val))
      {
        GNUNET_break_op (0);
        ret = TALER_MHD_reply_with_error (connection,
                                          MHD_HTTP_BAD_REQUEST,
                                          TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                          "categories");
        goto cleanup;
      }
      cats[idx] = json_integer_value (val);
    }
  }

  if (-1 == total_stock)
    pd.total_stock = INT64_MAX;
  else
    pd.total_stock = (uint64_t) total_stock;

  if (NULL == pd.address)
    pd.address = json_object ();
  if (NULL == pd.description_i18n)
    pd.description_i18n = json_object ();
  if (NULL == pd.taxes)
    pd.taxes = json_array ();

  /* check taxes is well-formed */
  if (! TMH_taxes_array_valid (pd.taxes))
  {
    GNUNET_break_op (0);
    ret = TALER_MHD_reply_with_error (connection,
                                      MHD_HTTP_BAD_REQUEST,
                                      TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                      "taxes");
    goto cleanup;
  }

  if (! TMH_location_object_valid (pd.address))
  {
    GNUNET_break_op (0);
    ret = TALER_MHD_reply_with_error (connection,
                                      MHD_HTTP_BAD_REQUEST,
                                      TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                      "address");
    goto cleanup;
  }

  if (! TALER_JSON_check_i18n (pd.description_i18n))
  {
    GNUNET_break_op (0);
    ret = TALER_MHD_reply_with_error (connection,
                                      MHD_HTTP_BAD_REQUEST,
                                      TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                      "description_i18n");
    goto cleanup;
  }

  if (NULL == pd.image)
    pd.image = (char *) "";
  if (! TMH_image_data_url_valid (pd.image))
  {
    GNUNET_break_op (0);
    ret = TALER_MHD_reply_with_error (connection,
                                      MHD_HTTP_BAD_REQUEST,
                                      TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                      "image");
    goto cleanup;
  }

  qs = TMH_db->insert_product (TMH_db->cls,
                               mi->settings.id,
                               product_id,
                               &pd,
                               num_cats,
                               cats,
                               &no_instance,
                               &conflict,
                               &no_cat);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
  case GNUNET_DB_STATUS_SOFT_ERROR:
    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      (GNUNET_DB_STATUS_SOFT_ERROR == qs)
      ? TALER_EC_GENERIC_DB_SOFT_FAILURE
      : TALER_EC_GENERIC_DB_COMMIT_FAILED,
      NULL);
    goto cleanup;
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_INTERNAL_SERVER_ERROR,
      TALER_EC_GENERIC_DB_INVARIANT_FAILURE,
      NULL);
    goto cleanup;
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break;
  }
  if (no_instance)
  {
    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_NOT_FOUND,
      TALER_EC_MERCHANT_GENERIC_INSTANCE_UNKNOWN,
      mi->settings.id);
    goto cleanup;
  }
  if (conflict)
  {
    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_CONFLICT,
      TALER_EC_MERCHANT_PRIVATE_POST_PRODUCTS_CONFLICT_PRODUCT_EXISTS,
      product_id);
    goto cleanup;
  }
  if (-1 != no_cat)
  {
    char nocats[24];

    GNUNET_break_op (0);
    TMH_db->rollback (TMH_db->cls);
    GNUNET_snprintf (nocats,
                     sizeof (nocats),
                     "%llu",
                     (unsigned long long) no_cat);
    ret = TALER_MHD_reply_with_error (
      connection,
      MHD_HTTP_NOT_FOUND,
      TALER_EC_MERCHANT_GENERIC_CATEGORY_UNKNOWN,
      nocats);
    goto cleanup;
  }
  ret = TALER_MHD_reply_static (connection,
                                MHD_HTTP_NO_CONTENT,
                                NULL,
                                NULL,
                                0);
cleanup:
  GNUNET_JSON_parse_free (spec);
  GNUNET_free (cats);
  return ret;
}


/* end of taler-merchant-httpd_private-post-products.c */
