/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-webhooks-ID.c
 * @brief implementing PATCH /webhooks/$ID request handling
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-webhooks-ID.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * How often do we retry the simple INSERT database transaction?
 */
#define MAX_RETRIES 3


/**
 * Determine the cause of the PATCH failure in more detail and report.
 *
 * @param connection connection to report on
 * @param instance_id instance we are processing
 * @param webhook_id ID of the webhook to patch
 * @param wb webhook details we failed to set
 */
static MHD_RESULT
determine_cause (struct MHD_Connection *connection,
                 const char *instance_id,
                 const char *webhook_id,
                 const struct TALER_MERCHANTDB_WebhookDetails *wb)
{
  struct TALER_MERCHANTDB_WebhookDetails wpx;
  enum GNUNET_DB_QueryStatus qs;

  qs = TMH_db->lookup_webhook (TMH_db->cls,
                               instance_id,
                               webhook_id,
                               &wpx);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "unexpected serialization problem");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_WEBHOOK_UNKNOWN,
                                       webhook_id);
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break; /* do below */
  }

  {
    enum TALER_ErrorCode ec;

    ec = TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE;
    TALER_MERCHANTDB_webhook_details_free (&wpx);
    GNUNET_break (TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE != ec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_CONFLICT,
                                       ec,
                                       NULL);
  }
}


/**
 * PATCH configuration of an existing instance, given its configuration.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_patch_webhooks_ID (const struct TMH_RequestHandler *rh,
                               struct MHD_Connection *connection,
                               struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *webhook_id = hc->infix;
  struct TALER_MERCHANTDB_WebhookDetails wb = {0};
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("event_type",
                             (const char **) &wb.event_type),
    TALER_JSON_spec_web_url ("url",
                             (const char **) &wb.url),
    GNUNET_JSON_spec_string ("http_method",
                             (const char **) &wb.http_method),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("header_template",
                               (const char **) &wb.header_template),
      NULL),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("body_template",
                               (const char **) &wb.body_template),
      NULL),
    GNUNET_JSON_spec_end ()
  };

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != webhook_id);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }


  qs = TMH_db->update_webhook (TMH_db->cls,
                               mi->settings.id,
                               webhook_id,
                               &wb);
  {
    MHD_RESULT ret = MHD_NO;

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        NULL);
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ret = determine_cause (connection,
                             mi->settings.id,
                             webhook_id,
                             &wb);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-webhooks-ID.c */
