/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-webhooks.c
 * @brief implement GET /webhooks
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-webhooks.h"


/**
 * Add webhook details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param webhook_id ID of the webhook
 * @param event_type what type of event is the hook for
 */
static void
add_webhook (void *cls,
             const char *webhook_id,
             const char *event_type)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_string ("webhook_id",
                                              webhook_id),
                     GNUNET_JSON_pack_string ("event_type",
                                              event_type))));
}


MHD_RESULT
TMH_private_get_webhooks (const struct TMH_RequestHandler *rh,
                          struct MHD_Connection *connection,
                          struct TMH_HandlerContext *hc)
{
  json_t *pa;
  enum GNUNET_DB_QueryStatus qs;

  pa = json_array ();
  GNUNET_assert (NULL != pa);
  qs = TMH_db->lookup_webhooks (TMH_db->cls,
                                hc->instance->settings.id,
                                &add_webhook,
                                pa);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (pa);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (connection,
                                    MHD_HTTP_OK,
                                    GNUNET_JSON_pack_array_steal ("webhooks",
                                                                  pa));
}


/* end of taler-merchant-httpd_private-get-webhooks.c */
