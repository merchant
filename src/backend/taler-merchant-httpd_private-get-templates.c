/*
  This file is part of TALER
  (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-templates.c
 * @brief implement GET /templates
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-templates.h"


/**
 * Add template details to our JSON array.
 *
 * @param cls a `json_t *` JSON array to build
 * @param template_id ID of the template
 * @param template_description human-readable description for the template
 */
static void
add_template (void *cls,
              const char *template_id,
              const char *template_description)
{
  json_t *pa = cls;

  GNUNET_assert (0 ==
                 json_array_append_new (
                   pa,
                   GNUNET_JSON_PACK (
                     GNUNET_JSON_pack_string ("template_id", template_id),
                     GNUNET_JSON_pack_string ("template_description",
                                              template_description))));
}


MHD_RESULT
TMH_private_get_templates (const struct TMH_RequestHandler *rh,
                           struct MHD_Connection *connection,
                           struct TMH_HandlerContext *hc)
{
  json_t *pa;
  enum GNUNET_DB_QueryStatus qs;

  pa = json_array ();
  GNUNET_assert (NULL != pa);
  qs = TMH_db->lookup_templates (TMH_db->cls,
                                 hc->instance->settings.id,
                                 &add_template,
                                 pa);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (pa);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (connection,
                                    MHD_HTTP_OK,
                                    GNUNET_JSON_pack_array_steal ("templates",
                                                                  pa));
}


/* end of taler-merchant-httpd_private-get-templates.c */
