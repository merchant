/*
  This file is part of TALER
  (C) 2022, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/

/**
 * @file taler-merchant-httpd_private-patch-templates-ID.c
 * @brief implementing PATCH /templates/$ID request handling
 * @author Priscilla HUANG
 */
#include "platform.h"
#include "taler-merchant-httpd_private-patch-templates-ID.h"
#include "taler-merchant-httpd_helper.h"
#include <taler/taler_json_lib.h>


/**
 * Determine the cause of the PATCH failure in more detail and report.
 *
 * @param connection connection to report on
 * @param instance_id instance we are processing
 * @param template_id ID of the product to patch
 * @param tp template details we failed to set
 */
static MHD_RESULT
determine_cause (struct MHD_Connection *connection,
                 const char *instance_id,
                 const char *template_id,
                 const struct TALER_MERCHANTDB_TemplateDetails *tp)
{
  struct TALER_MERCHANTDB_TemplateDetails tpx;
  enum GNUNET_DB_QueryStatus qs;

  qs = TMH_db->lookup_template (TMH_db->cls,
                                instance_id,
                                template_id,
                                &tpx);
  switch (qs)
  {
  case GNUNET_DB_STATUS_HARD_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  case GNUNET_DB_STATUS_SOFT_ERROR:
    GNUNET_break (0);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                       "unexpected serialization problem");
  case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_NOT_FOUND,
                                       TALER_EC_MERCHANT_GENERIC_TEMPLATE_UNKNOWN,
                                       template_id);
  case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
    break; /* do below */
  }

  {
    enum TALER_ErrorCode ec;

    ec = TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE;
    TALER_MERCHANTDB_template_details_free (&tpx);
    GNUNET_break (TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE != ec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_CONFLICT,
                                       ec,
                                       NULL);
  }
}


/**
 * PATCH configuration of an existing instance, given its configuration.
 *
 * @param rh context of the handler
 * @param connection the MHD connection to handle
 * @param[in,out] hc context with further information about the request
 * @return MHD result code
 */
MHD_RESULT
TMH_private_patch_templates_ID (const struct TMH_RequestHandler *rh,
                                struct MHD_Connection *connection,
                                struct TMH_HandlerContext *hc)
{
  struct TMH_MerchantInstance *mi = hc->instance;
  const char *template_id = hc->infix;
  struct TALER_MERCHANTDB_TemplateDetails tp = {0};
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_string ("template_description",
                             (const char **) &tp.template_description),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_string ("otp_id",
                               (const char **) &tp.otp_id),
      NULL),
    GNUNET_JSON_spec_json ("template_contract",
                           &tp.template_contract),
    GNUNET_JSON_spec_mark_optional (
      GNUNET_JSON_spec_json ("editable_defaults",
                             &tp.editable_defaults),
      NULL),
    GNUNET_JSON_spec_end ()
  };

  GNUNET_assert (NULL != mi);
  GNUNET_assert (NULL != template_id);
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (connection,
                                     hc->request_body,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_NO == res)
             ? MHD_YES
             : MHD_NO;
  }

  if (! TMH_template_contract_valid (tp.template_contract))
  {
    GNUNET_break_op (0);
    GNUNET_JSON_parse_free (spec);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "template_contract");
  }
  if (NULL != tp.editable_defaults)
  {
    const char *key;
    json_t *val;

    json_object_foreach (tp.editable_defaults, key, val)
    {
      if (NULL !=
          json_object_get (tp.template_contract,
                           key))
      {
        char *msg;
        MHD_RESULT ret;

        GNUNET_break_op (0);
        GNUNET_asprintf (&msg,
                         "editable_defaults::%s conflicts with template_contract",
                         key);
        GNUNET_JSON_parse_free (spec);
        ret = TALER_MHD_reply_with_error (connection,
                                          MHD_HTTP_BAD_REQUEST,
                                          TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                          msg);
        GNUNET_free (msg);
        return ret;
      }
    }
  }

  qs = TMH_db->update_template (TMH_db->cls,
                                mi->settings.id,
                                template_id,
                                &tp);
  {
    MHD_RESULT ret = MHD_NO;

    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_DB_STORE_FAILED,
                                        NULL);
      break;
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      ret = TALER_MHD_reply_with_error (connection,
                                        MHD_HTTP_INTERNAL_SERVER_ERROR,
                                        TALER_EC_GENERIC_INTERNAL_INVARIANT_FAILURE,
                                        "unexpected serialization problem");
      break;
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      ret = determine_cause (connection,
                             mi->settings.id,
                             template_id,
                             &tp);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      ret = TALER_MHD_reply_static (connection,
                                    MHD_HTTP_NO_CONTENT,
                                    NULL,
                                    NULL,
                                    0);
      break;
    }
    GNUNET_JSON_parse_free (spec);
    return ret;
  }
}


/* end of taler-merchant-httpd_private-patch-templates-ID.c */
