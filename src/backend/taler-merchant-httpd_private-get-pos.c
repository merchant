/*
  This file is part of TALER
  (C) 2019, 2020, 2021, 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file taler-merchant-httpd_private-get-pos.c
 * @brief implement GET /private/pos
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler-merchant-httpd_private-get-pos.h"
#include <taler/taler_json_lib.h>

/**
 * Closure for add_product().
 */
struct Context
{
  /**
   * JSON array of products we are building.
   */
  json_t *pa;

  /**
   * JSON array of categories we are building.
   */
  json_t *ca;

};


/**
 * Add category to the @e ca array.
 *
 * @param cls a `struct Context` with JSON arrays to build
 * @param category_id ID of the category
 * @param category_name name of the category
 * @param category_name_i18n translations of the @a category_name
 * @param product_count number of products in the category
 */
static void
add_category (
  void *cls,
  uint64_t category_id,
  const char *category_name,
  const json_t *category_name_i18n,
  uint64_t product_count)
{
  struct Context *ctx = cls;

  (void) product_count;
  GNUNET_assert (
    0 ==
    json_array_append_new (
      ctx->ca,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("id",
                                 category_id),
        GNUNET_JSON_pack_object_incref ("name_i18n",
                                        (json_t *) category_name_i18n),
        GNUNET_JSON_pack_string ("name",
                                 category_name))));
}


/**
 * Add product details to our JSON array.
 *
 * @param cls a `struct Context` with JSON arrays to build
 * @param product_serial row ID of the product
 * @param product_id ID of the product
 * @param pd full product details
 * @param num_categories length of @a categories array
 * @param categories array of categories the
 *   product is in
 */
static void
add_product (void *cls,
             uint64_t product_serial,
             const char *product_id,
             const struct TALER_MERCHANTDB_ProductDetails *pd,
             size_t num_categories,
             const uint64_t *categories)
{
  struct Context *ctx = cls;
  json_t *pa = ctx->pa;
  json_t *cata;

  cata = json_array ();
  GNUNET_assert (NULL != cata);
  for (size_t i = 0; i<num_categories; i++)
    GNUNET_assert (
      0 == json_array_append_new (
        cata,
        json_integer (categories[i])));
  if (0 == num_categories)
  {
    // If there is no category, we return the default category
    GNUNET_assert (
      0 == json_array_append_new (
        cata,
        json_integer (0)));
  }
  GNUNET_assert (
    0 ==
    json_array_append_new (
      pa,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_string ("description",
                                 pd->description),
        GNUNET_JSON_pack_object_incref ("description_i18n",
                                        (json_t *) pd->description_i18n),
        GNUNET_JSON_pack_string ("unit",
                                 pd->unit),
        TALER_JSON_pack_amount ("price",
                                &pd->price),
        GNUNET_JSON_pack_allow_null (
          GNUNET_JSON_pack_string ("image",
                                   pd->image)),
        GNUNET_JSON_pack_array_steal ("categories",
                                      cata),
        GNUNET_JSON_pack_allow_null (
          GNUNET_JSON_pack_array_incref ("taxes",
                                         (json_t *) pd->taxes)),
        (INT64_MAX == pd->total_stock)
        ? GNUNET_JSON_pack_int64 ("total_stock",
                                  pd->total_stock)
        : GNUNET_JSON_pack_allow_null (
          GNUNET_JSON_pack_string ("total_stock",
                                   NULL)),
        GNUNET_JSON_pack_uint64 ("minimum_age",
                                 pd->minimum_age),
        GNUNET_JSON_pack_uint64 ("product_serial",
                                 product_serial),
        GNUNET_JSON_pack_string ("product_id",
                                 product_id))));
}


MHD_RESULT
TMH_private_get_pos (const struct TMH_RequestHandler *rh,
                     struct MHD_Connection *connection,
                     struct TMH_HandlerContext *hc)
{
  struct Context ctx;
  enum GNUNET_DB_QueryStatus qs;

  ctx.pa = json_array ();
  GNUNET_assert (NULL != ctx.pa);
  ctx.ca = json_array ();
  GNUNET_assert (NULL != ctx.ca);
  GNUNET_assert (
    0 == json_array_append_new (
      ctx.ca,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_uint64 ("id",
                                 0),
        GNUNET_JSON_pack_string ("name",
                                 "default"))));
  qs = TMH_db->lookup_categories (TMH_db->cls,
                                  hc->instance->settings.id,
                                  &add_category,
                                  &ctx);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (ctx.pa);
    json_decref (ctx.ca);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  qs = TMH_db->lookup_all_products (TMH_db->cls,
                                    hc->instance->settings.id,
                                    &add_product,
                                    &ctx);
  if (0 > qs)
  {
    GNUNET_break (0);
    json_decref (ctx.pa);
    json_decref (ctx.ca);
    return TALER_MHD_reply_with_error (connection,
                                       MHD_HTTP_INTERNAL_SERVER_ERROR,
                                       TALER_EC_GENERIC_DB_FETCH_FAILED,
                                       NULL);
  }
  return TALER_MHD_REPLY_JSON_PACK (
    connection,
    MHD_HTTP_OK,
    GNUNET_JSON_pack_array_steal ("categories",
                                  ctx.ca),
    GNUNET_JSON_pack_array_steal ("products",
                                  ctx.pa));
}


/* end of taler-merchant-httpd_private-get-pos.c */
