/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_token_family_key.c
 * @brief Implementation of the insert_token_family_key function for Postgres
 * @author Christian Blättler
 */
#include "platform.h"
#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_pq_lib.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_token_family_key.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_token_family_key (
  void *cls,
  const char *merchant_id,
  const char *token_family_slug,
  const struct TALER_TokenIssuePublicKey *pub,
  const struct TALER_TokenIssuePrivateKey *priv,
  struct GNUNET_TIME_Timestamp key_expires,
  struct GNUNET_TIME_Timestamp valid_after,
  struct GNUNET_TIME_Timestamp valid_before)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Timestamp now
    = GNUNET_TIME_timestamp_get ();
  const char *cipher = NULL;
  struct GNUNET_HashCode pub_hash;

  switch (pub->public_key->cipher)
  {
  case GNUNET_CRYPTO_BSA_RSA:
    cipher = "rsa";
    GNUNET_CRYPTO_rsa_public_key_hash (
      pub->public_key->details.rsa_public_key,
      &pub_hash);
    break;
  case GNUNET_CRYPTO_BSA_CS:
    cipher = "cs";
    GNUNET_CRYPTO_hash (
      &pub->public_key->details.cs_public_key,
      sizeof (pub->public_key->details.cs_public_key),
      &pub_hash);
    break;
  case GNUNET_CRYPTO_BSA_INVALID:
    GNUNET_break (0);
    return GNUNET_DB_STATUS_HARD_ERROR;
  }
  GNUNET_assert (pub->public_key->cipher ==
                 priv->private_key->cipher);
  GNUNET_assert (0 ==
                 GNUNET_memcmp (&pub_hash,
                                &pub->public_key->pub_key_hash));
  GNUNET_assert (! GNUNET_TIME_absolute_is_zero (
                   valid_after.abs_time));
  GNUNET_assert (! GNUNET_TIME_absolute_is_zero (
                   valid_before.abs_time));
  PREPARE (pg,
           "token_family_key_insert",
           "INSERT INTO merchant_token_family_keys "
           "(token_family_serial"
           ",pub"
           ",h_pub"
           ",priv"
           ",private_key_created_at"
           ",private_key_deleted_at"
           ",signature_validity_start"
           ",signature_validity_end"
           ",cipher)"
           " SELECT token_family_serial, $2, $3, $4, $5, $6, $7, $8, $9"
           " FROM merchant_token_families"
           " WHERE (slug = $1)"
           "   AND merchant_serial="
           "   (SELECT merchant_serial"
           "      FROM merchant_instances"
           "     WHERE merchant_id=$10)");
  {
    struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string (token_family_slug),
      GNUNET_PQ_query_param_blind_sign_pub (pub->public_key),
      GNUNET_PQ_query_param_auto_from_type (&pub->public_key->pub_key_hash),
      GNUNET_PQ_query_param_blind_sign_priv (priv->private_key),
      GNUNET_PQ_query_param_timestamp (&now),
      GNUNET_PQ_query_param_timestamp (&key_expires),
      GNUNET_PQ_query_param_timestamp (&valid_after),
      GNUNET_PQ_query_param_timestamp (&valid_before),
      GNUNET_PQ_query_param_string (cipher),
      GNUNET_PQ_query_param_string (merchant_id),
      GNUNET_PQ_query_param_end
    };
    enum GNUNET_DB_QueryStatus qs;

    qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "token_family_key_insert",
                                             params);
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Insert into MTFK %s with valid [%llu,%llu] got %d\n",
                token_family_slug,
                (unsigned long long) valid_after.abs_time.abs_value_us,
                (unsigned long long) valid_before.abs_time.abs_value_us,
                (int) qs);
    return qs;
  }
}
