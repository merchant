/*
   This file is part of TALER
   Copyright (C) 2022-2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_template.c
 * @brief Implementation of the update_template function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_template.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_template (void *cls,
                        const char *instance_id,
                        const char *template_id,
                        const struct TALER_MERCHANTDB_TemplateDetails *td)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (template_id),
    GNUNET_PQ_query_param_string (td->template_description),
    (NULL == td->otp_id)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (td->otp_id),
    TALER_PQ_query_param_json (td->template_contract),
    (NULL == td->editable_defaults)
    ? GNUNET_PQ_query_param_null ()
    : TALER_PQ_query_param_json (td->editable_defaults),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_template",
           "WITH mid AS ("
           "  SELECT merchant_serial"
           "    FROM merchant_instances"
           "   WHERE merchant_id=$1)"
           ",otp AS ("
           "   SELECT otp_serial"
           "     FROM merchant_otp_devices"
           "     JOIN mid USING (merchant_serial)"
           "    WHERE otp_id=$4)"
           "UPDATE merchant_template SET"
           " template_description=$3"
           ",otp_device_id="
           "  COALESCE((SELECT otp_serial"
           "            FROM otp), NULL)"
           ",template_contract=$5"
           ",editable_defaults=$6"
           " WHERE merchant_serial="
           "   (SELECT merchant_serial"
           "      FROM mid)"
           "   AND template_id=$2");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_template",
                                             params);
}
