/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_instance.c
 * @brief Implementation of the insert_instance function for Postgres
 * @author Christian Grothoff
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_instance.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_instance (
  void *cls,
  const struct TALER_MerchantPublicKeyP *merchant_pub,
  const struct TALER_MerchantPrivateKeyP *merchant_priv,
  const struct TALER_MERCHANTDB_InstanceSettings *is,
  const struct TALER_MERCHANTDB_InstanceAuthSettings *ias)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (merchant_pub),
    GNUNET_PQ_query_param_auto_from_type (&ias->auth_hash),
    GNUNET_PQ_query_param_auto_from_type (&ias->auth_salt),
    GNUNET_PQ_query_param_string (is->id),
    GNUNET_PQ_query_param_string (is->name),
    TALER_PQ_query_param_json (is->address),
    TALER_PQ_query_param_json (is->jurisdiction),
    GNUNET_PQ_query_param_bool (is->use_stefan),
    GNUNET_PQ_query_param_relative_time (
      &is->default_wire_transfer_delay),
    GNUNET_PQ_query_param_relative_time (&is->default_pay_delay),
    (NULL == is->website)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->website),
    (NULL == is->email)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->email),
    (NULL == is->logo)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->logo),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_QueryParam params_priv[] = {
    GNUNET_PQ_query_param_auto_from_type (merchant_priv),
    GNUNET_PQ_query_param_string (is->id),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "insert_instance",
           "INSERT INTO merchant_instances"
           "(merchant_pub"
           ",auth_hash"
           ",auth_salt"
           ",merchant_id"
           ",merchant_name"
           ",address"
           ",jurisdiction"
           ",use_stefan"
           ",default_wire_transfer_delay"
           ",default_pay_delay"
           ",website"
           ",email"
           ",logo"
           ",user_type)"
           "VALUES"
           "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, 0)");
  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "insert_instance",
                                           params);
  if (qs <= 0)
    return qs;
  PREPARE (pg,
           "insert_keys",
           "INSERT INTO merchant_keys"
           "(merchant_priv"
           ",merchant_serial)"
           " SELECT $1, merchant_serial"
           " FROM merchant_instances"
           " WHERE merchant_id=$2");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_keys",
                                             params_priv);
}
