/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_check_transfer_exists.h
 * @brief implementation of the check_transfer_exists function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_CHECK_TRANSFER_EXISTS_H
#define PG_CHECK_TRANSFER_EXISTS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Check if information about a transfer exists with the
 * backend.  Returns no data, only the query status.
 *
 * @param cls closure
 * @param instance_id instance to delete transfer of
 * @param transfer_serial_id transfer to delete
 * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT
 *           if the transfer record exists
 */
enum GNUNET_DB_QueryStatus
TMH_PG_check_transfer_exists (void *cls,
                              const char *instance_id,
                              uint64_t transfer_serial_id);

#endif
