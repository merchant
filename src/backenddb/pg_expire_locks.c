/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_expire_locks.c
 * @brief Implementation of the expire_locks function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_expire_locks.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_expire_locks (void *cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Absolute now = GNUNET_TIME_absolute_get ();
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_absolute_time (&now),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs1;
  enum GNUNET_DB_QueryStatus qs2;
  enum GNUNET_DB_QueryStatus qs3;

  check_connection (pg);
  PREPARE (pg,
           "unlock_products",
           "DELETE FROM merchant_inventory_locks"
           " WHERE expiration < $1");
  qs1 = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                            "unlock_products",
                                            params);
  if (qs1 < 0)
  {
    GNUNET_break (0);
    return qs1;
  }
  PREPARE (pg,
           "unlock_orders",
           "DELETE FROM merchant_orders"
           " WHERE pay_deadline < $1");
  qs2 = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                            "unlock_orders",
                                            params);
  if (qs2 < 0)
  {
    GNUNET_break (0);
    return qs2;
  }
  PREPARE (pg,
           "unlock_contracts",
           "DELETE FROM merchant_contract_terms"
           " WHERE NOT paid"
           "   AND pay_deadline < $1");
  qs3 = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                            "unlock_contracts",
                                            params);
  if (qs3 < 0)
  {
    GNUNET_break (0);
    return qs3;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Released %d+%d+%d locks\n",
              qs1,
              qs2,
              qs3);
  return qs1 + qs2 + qs3;
}
