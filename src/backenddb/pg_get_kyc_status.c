/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_get_kyc_status.c
 * @brief Implementation of the get_kyc_status function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_get_kyc_status.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_get_kyc_status (
  void *cls,
  struct TALER_FullPayto merchant_account_uri,
  const char *instance_id,
  const char *exchange_url,
  bool *auth_ok,
  struct TALER_AccountAccessTokenP *access_token,
  bool *kyc_ok,
  unsigned int *last_http_status,
  enum TALER_ErrorCode *last_ec,
  struct GNUNET_TIME_Timestamp *last_kyc_check,
  bool *aml_review,
  json_t **jlimits)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (merchant_account_uri.full_payto),
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_end
  };
  uint32_t h32 = 0;
  uint32_t e32 = 0;
  bool token_is_null = true;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_auto_from_type ("access_token",
                                            access_token),
      &token_is_null),
    GNUNET_PQ_result_spec_uint32 ("exchange_http_status",
                                  &h32),
    GNUNET_PQ_result_spec_uint32 ("exchange_ec_code",
                                  &e32),
    GNUNET_PQ_result_spec_bool ("kyc_ok",
                                kyc_ok),
    GNUNET_PQ_result_spec_timestamp ("kyc_timestamp",
                                     last_kyc_check),
    GNUNET_PQ_result_spec_bool ("aml_review",
                                aml_review),
    GNUNET_PQ_result_spec_allow_null (
      TALER_PQ_result_spec_json ("jaccount_limits",
                                 jlimits),
      NULL),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "get_kyc_status",
           "SELECT"
           " mk.access_token"
           ",mk.exchange_http_status"
           ",mk.exchange_ec_code"
           ",mk.kyc_ok"
           ",mk.kyc_timestamp"
           ",mk.aml_review"
           ",mk.jaccount_limits"
           " FROM merchant_kyc mk"
           " WHERE mk.exchange_url=$3"
           "   AND mk.account_serial="
           "   (SELECT account_serial"
           "      FROM merchant_accounts"
           "     WHERE payto_uri=$1"
           "       AND merchant_serial="
           "       (SELECT merchant_serial"
           "          FROM merchant_instances"
           "         WHERE merchant_id=$2));");
  *jlimits = NULL;
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "get_kyc_status",
                                                 params,
                                                 rs);
  *last_ec = (enum TALER_ErrorCode) (int) e32;
  *last_http_status = (unsigned int) h32;
  *auth_ok = ! token_is_null;
  return qs;
}
