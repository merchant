/*
   This file is part of TALER
   Copyright (C) 2022, 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_deposit.c
 * @brief Implementation of the insert_deposit function for Postgres
 * @author Christian Grothoff
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_deposit.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_deposit (
  void *cls,
  uint32_t offset,
  uint64_t deposit_confirmation_serial,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const struct TALER_CoinSpendSignatureP *coin_sig,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_Amount *refund_fee)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&deposit_confirmation_serial),
    GNUNET_PQ_query_param_uint32 (&offset),
    GNUNET_PQ_query_param_auto_from_type (coin_pub),
    GNUNET_PQ_query_param_auto_from_type (coin_sig),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               amount_with_fee),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               deposit_fee),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               refund_fee),
    GNUNET_PQ_query_param_end
  };

  /* no preflight check here, run in transaction by caller! */
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Storing deposit for coin_pub: `%s', amount_with_fee: %s\n",
              TALER_B2S (coin_pub),
              TALER_amount2s (amount_with_fee));
  check_connection (pg);
  PREPARE (pg,
           "insert_deposit",
           "INSERT INTO merchant_deposits"
           "(deposit_confirmation_serial"
           ",coin_offset"
           ",coin_pub"
           ",coin_sig"
           ",amount_with_fee"
           ",deposit_fee"
           ",refund_fee"
           ") VALUES ($1, $2, $3, $4, $5, $6, $7)");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_deposit",
                                             params);
}
