/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_transfer_status.h
 * @brief implementation of the update_transfer_status function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_TRANSFER_STATUS_H
#define PG_UPDATE_TRANSFER_STATUS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Update transfer status.
 *
 * @param cls closure
 * @param exchange_url the exchange that made the transfer
 * @param wtid wire transfer subject
 * @param next_attempt when should we try again (if ever)
 * @param ec current error state of checking the transfer
 * @param failed true if validation has failed for good
 * @param verified true if validation has succeeded for good
 * @return database transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_transfer_status (
  void *cls,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  struct GNUNET_TIME_Absolute next_attempt,
  enum TALER_ErrorCode ec,
  bool failed,
  bool verified);

#endif
