/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_issued_token.c
 * @brief Implementation of the insert_issued_token function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_issued_token.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_issued_token (void *cls,
                            const struct TALER_PrivateContractHashP *
                            h_contract_terms,
                            const struct TALER_TokenIssuePublicKeyHashP *
                            h_issue_pub,
                            const struct TALER_BlindedTokenIssueSignature *
                            blind_sig)
{
  struct PostgresClosure *pg = cls;

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (h_issue_pub),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_blinded_sig (blind_sig->signature),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "issued_token_insert",
           "INSERT INTO merchant_issued_tokens"
           "(token_family_key_serial"
           ",h_contract_terms"
           ",blind_sig)"
           " SELECT token_family_key_serial, $2, $3"
           " FROM merchant_token_families"
           " JOIN merchant_token_family_keys"
           "   USING (token_family_serial)"
           " WHERE h_pub = $1");

  /* FIXME-#9434: Increase issued counter on merchant_token_family table. */
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "issued_token_insert",
                                             params);
}
