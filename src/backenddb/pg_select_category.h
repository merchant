/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_category.h
 * @brief implementation of the select_category function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_CATEGORY_H
#define PG_SELECT_CATEGORY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Lookup details about product category.
 *
 * @param cls closure
 * @param instance_id instance to lookup template for
 * @param category_id category to update
 * @param[out] cd set to the category details on success, can be NULL
 *             (in that case we only want to check if the category exists)
 * @param[out] num_products set to length of @a products array
 * @param[out] products set to buffer with @a num_products 0-terminated strings with the product IDs, caller must free() it.
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_category (void *cls,
                        const char *instance_id,
                        uint64_t category_id,
                        struct TALER_MERCHANTDB_CategoryDetails *cd,
                        size_t *num_products,
                        char **products);

#endif
