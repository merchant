/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_exchange_keys.c
 * @brief Implementation of the select_exchange_keys function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_exchange_keys.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_select_exchange_keys (void *cls,
                             const char *exchange_url,
                             struct TALER_EXCHANGE_Keys **keys)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_end
  };
  json_t *jkeys;
  struct GNUNET_PQ_ResultSpec rs[] = {
    TALER_PQ_result_spec_json ("keys_json",
                               &jkeys),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "select_exchange_keys",
           "SELECT"
           " keys_json"
           " FROM merchant_exchange_keys"
           " WHERE exchange_url=$1;");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "select_exchange_keys",
                                                 params,
                                                 rs);
  if (qs <= 0)
    return qs;
  *keys = TALER_EXCHANGE_keys_from_json (jkeys);
  json_decref (jkeys);
  if (NULL == *keys)
  {
    /* malformed /keys in cache, maybe format change. Just ignore */
    GNUNET_break (0);
    return GNUNET_DB_STATUS_SUCCESS_NO_RESULTS;
  }
  return qs;
}
