/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_mark_contract_paid.h
 * @brief implementation of the mark_contract_paid function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_MARK_CONTRACT_PAID_H
#define PG_MARK_CONTRACT_PAID_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Mark contract as paid and store the current @a session_id
 * for which the contract was paid. Deletes the underlying order
 * and marks the locked stocks of the order as sold.
 *
 * @param cls closure
 * @param instance_id instance to mark contract as paid for
 * @param h_contract_terms hash of the contract that is now paid
 * @param session_id the session that paid the contract
 * @param choice_index the contract v1 choice index to select, -1 for v0
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_mark_contract_paid (void *cls,
                           const char *instance_id,
                           const struct
                           TALER_PrivateContractHashP *h_contract_terms,
                           const char *session_id,
                           int16_t choice_index);

#endif
