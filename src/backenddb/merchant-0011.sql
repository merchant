--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- @file merchant-0011.sql
-- @brief Remove dead deposit_thresholds and related columns and replace with more generic JSON column
-- @author Christian Grothoff

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0011', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_kyc
  DROP COLUMN deposit_thresholds
 ,DROP COLUMN deposit_timeframes
 ,DROP COLUMN deposit_limits_are_soft
 ,DROP COLUMN exchange_kyc_serial
 ,ADD COLUMN jaccount_limits TEXT DEFAULT NULL;

COMMENT ON COLUMN merchant_kyc.jaccount_limits
 IS 'JSON with AccountLimits that apply to this account';

COMMIT;
