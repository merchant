/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_login_token.h
 * @brief implementation of the insert_login_token function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_LOGIN_TOKEN_H
#define PG_INSERT_LOGIN_TOKEN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Insert instance login token into our database.
 *
 * @param cls closure
 * @param id identifier of the instance
 * @param token value of the token
 * @param creation_time the current time
 * @param expiration_time when does the token expire
 * @param validity_scope scope of the token
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_login_token (
  void *cls,
  const char *id,
  const struct TALER_MERCHANTDB_LoginTokenP *token,
  struct GNUNET_TIME_Timestamp creation_time,
  struct GNUNET_TIME_Timestamp expiration_time,
  uint32_t validity_scope);


#endif
