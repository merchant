--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- @file merchant-0007.sql
-- @brief alter length check of public key hash in merchant_token_family_keys
-- @author Christian Blättler

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0007', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_token_family_keys
  DROP CONSTRAINT merchant_token_family_keys_h_pub_check,
  ADD CONSTRAINT h_pub_length_check CHECK (LENGTH(h_pub) = 64);

-- Complete transaction
COMMIT;
