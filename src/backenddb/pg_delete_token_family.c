/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_token_family.c
 * @brief Implementation of the delete_token_family function for Postgres
 * @author Christian Blättler
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_token_family.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_delete_token_family (void *cls,
                            const char *instance_id,
                            const char *token_family_slug)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (token_family_slug),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "delete_token_family",
           "DELETE"
           " FROM merchant_token_families"
           " WHERE merchant_token_families.merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND slug=$2");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_token_family",
                                             params);
}