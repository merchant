/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_pending_deposits.h
 * @brief implementation of the lookup_pending_deposits function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_LOOKUP_PENDING_DEPOSITS_H
#define PG_LOOKUP_PENDING_DEPOSITS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Lookup deposits that are finished and awaiting a wire transfer.
 *
 * @param cls closure
 * @param exchange_url exchange to filter deposits by
 * @param limit maximum number of deposits to return
 * @param allow_future true to allow deposits with wire deadline in the future
 * @param cb function to call with deposit data
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_pending_deposits (
  void *cls,
  const char *exchange_url,
  uint64_t limit,
  bool allow_future,
  TALER_MERCHANTDB_PendingDepositsCallback cb,
  void *cb_cls);


#endif
