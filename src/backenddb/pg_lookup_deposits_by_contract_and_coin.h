/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_deposits_by_contract_and_coin.h
 * @brief implementation of the lookup_deposits_by_contract_and_coin function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_DEPOSITS_BY_CONTRACT_AND_COIN_H
#define PG_LOOKUP_DEPOSITS_BY_CONTRACT_AND_COIN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup information about coin payments by @a h_contract_terms and
 * @a coin_pub.
 *
 * @param cls closure
 * @param instance_id instance to lookup payments for
 * @param h_contract_terms proposal data's hashcode
 * @param coin_pub public key to use for the search
 * @param cb function to call with payment data
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_deposits_by_contract_and_coin (void *cls,
                                             const char *instance_id,
                                             const struct TALER_PrivateContractHashP *h_contract_terms,
                                             const struct TALER_CoinSpendPublicKeyP *coin_pub,
                                             TALER_MERCHANTDB_CoinDepositCallback cb,
                                             void *cb_cls);

#endif
