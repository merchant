/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_refund_proof.h
 * @brief implementation of the insert_refund_proof function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_REFUND_PROOF_H
#define PG_INSERT_REFUND_PROOF_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert refund proof data from the exchange into the database.
 *
 * @param cls closure
 * @param refund_serial serial number of the refund
 * @param exchange_sig signature from exchange that coin was refunded
 * @param exchange_pub signing key that was used for @a exchange_sig
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_refund_proof (void *cls,
                            uint64_t refund_serial,
                            const struct TALER_ExchangeSignatureP *exchange_sig,
                            const struct TALER_ExchangePublicKeyP *exchange_pub);

#endif
