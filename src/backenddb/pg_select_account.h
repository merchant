/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_account.h
 * @brief implementation of the select_account function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_ACCOUNT_H
#define PG_SELECT_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Obtain information about an instance's accounts.
 *
 * @param cls closure
 * @param id identifier of the instance
 * @param h_wire wire hash of the account
 * @param[out] ad account details returned
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_account (void *cls,
                       const char *id,
                       const struct TALER_MerchantWireHashP *h_wire,
                       struct TALER_MERCHANTDB_AccountDetails *ad);

#endif
