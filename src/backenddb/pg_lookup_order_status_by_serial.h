/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order_status_by_serial.h
 * @brief implementation of the lookup_order_status_by_serial function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_ORDER_STATUS_BY_SERIAL_H
#define PG_LOOKUP_ORDER_STATUS_BY_SERIAL_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve contract terms given its @a order_serial
 *
 * @param cls closure
 * @param instance_id instance's identifier
 * @param order_serial serial ID of the order to look up
 * @param[out] order_id set to ID of the order
 * @param[out] h_contract_terms set to the hash of the contract.
 * @param[out] paid set to the payment status of the contract
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order_status_by_serial (void *cls,
                                      const char *instance_id,
                                      uint64_t order_serial,
                                      char **order_id,
                                      struct TALER_PrivateContractHashP *
                                      h_contract_terms,
                                      bool *paid);

#endif
