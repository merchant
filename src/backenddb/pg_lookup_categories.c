/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_categories.c
 * @brief Implementation of the lookup_categories function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_categories.h"
#include "pg_helper.h"


/**
 * Context used for TMH_PG_lookup_categories().
 */
struct LookupCategoryContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_CategoriesCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about otp_device.
 *
 * @param[in,out] cls of type `struct LookupCategoryContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_categories_cb (void *cls,
                      PGresult *result,
                      unsigned int num_results)
{
  struct LookupCategoryContext *tlc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t category_id;
    char *category_name;
    json_t *category_name_i18n;
    uint64_t product_count;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("category_serial",
                                    &category_id),
      GNUNET_PQ_result_spec_string ("category_name",
                                    &category_name),
      TALER_PQ_result_spec_json ("category_name_i18n",
                                 &category_name_i18n),
      GNUNET_PQ_result_spec_uint64 ("product_count",
                                    &product_count),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      tlc->extract_failed = true;
      return;
    }
    tlc->cb (tlc->cb_cls,
             category_id,
             category_name,
             category_name_i18n,
             product_count);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_categories (void *cls,
                          const char *instance_id,
                          TALER_MERCHANTDB_CategoriesCallback cb,
                          void *cb_cls)
{

  struct PostgresClosure *pg = cls;
  struct LookupCategoryContext tlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    /* Can be overwritten by the lookup_categories_cb */
    .extract_failed = false,
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_categories",
           "SELECT"
           " mc.category_serial"
           ",mc.category_name"
           ",mc.category_name_i18n"
           ",COALESCE(COUNT(mpc.product_serial),0)"
           "   AS product_count"
           " FROM merchant_categories mc"
           " LEFT JOIN merchant_product_categories mpc"
           "   USING (category_serial)"
           " JOIN merchant_instances mi"
           "   USING (merchant_serial)"
           " WHERE mi.merchant_id=$1"
           " GROUP BY mc.category_serial"
           " ORDER BY mc.category_serial;");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_categories",
                                             params,
                                             &lookup_categories_cb,
                                             &tlc);
  /* If there was an error inside lookup_categories_cb, return a hard error. */
  if (tlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
