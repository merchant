/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfer_details_by_order.c
 * @brief Implementation of the lookup_transfer_details_by_order function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_transfer_details_by_order.h"
#include "pg_helper.h"

/**
 * Closure for lookup_transfer_details_by_order_cb().
 */
struct LookupTransferDetailsByOrderContext
{

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Function to call with all results.
   */
  TALER_MERCHANTDB_OrderTransferDetailsCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Set to the query result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupTransferDetailsByOrderContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_transfer_details_by_order_cb (void *cls,
                                     PGresult *result,
                                     unsigned int num_results)
{
  struct LookupTransferDetailsByOrderContext *ltdo = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    struct TALER_WireTransferIdentifierRawP wtid;
    char *exchange_url;
    uint64_t deposit_serial;
    struct GNUNET_TIME_Timestamp execution_time;
    struct TALER_Amount deposit_value;
    struct TALER_Amount deposit_fee;
    uint8_t transfer_confirmed;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("deposit_serial",
                                    &deposit_serial),
      GNUNET_PQ_result_spec_timestamp ("deposit_timestamp",
                                       &execution_time),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_auto_from_type ("wtid",
                                            &wtid),
      TALER_PQ_result_spec_amount_with_currency ("exchange_deposit_value",
                                                 &deposit_value),
      TALER_PQ_result_spec_amount_with_currency ("exchange_deposit_fee",
                                                 &deposit_fee),
      GNUNET_PQ_result_spec_auto_from_type ("transfer_confirmed",
                                            &transfer_confirmed),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ltdo->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    ltdo->cb (ltdo->cb_cls,
              &wtid,
              exchange_url,
              execution_time,
              &deposit_value,
              &deposit_fee,
              (0 != transfer_confirmed));
    GNUNET_PQ_cleanup_result (rs); /* technically useless here */
  }
  ltdo->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfer_details_by_order (void *cls,
                                         uint64_t order_serial,
                                         TALER_MERCHANTDB_OrderTransferDetailsCallback cb,
                                         void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupTransferDetailsByOrderContext ltdo = {
    .pg = pg,
    .cb = cb,
    .cb_cls = cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&order_serial),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_transfer_details_by_order",
           "SELECT"
           " md.deposit_serial"
           ",mcon.exchange_url"
           ",mt.wtid"
           ",mtc.exchange_deposit_value"
           ",mtc.exchange_deposit_fee"
           ",mcon.deposit_timestamp"
           ",mt.confirmed AS transfer_confirmed"
           " FROM merchant_transfer_to_coin mtc"
           " JOIN merchant_deposits md"
           "   USING (deposit_serial)"
           " JOIN merchant_deposit_confirmations mcon"
           "   USING (deposit_confirmation_serial)"
           " JOIN merchant_transfers mt"
           "   USING (credit_serial)"
           " JOIN merchant_accounts acc"
           "   ON (acc.account_serial = mt.account_serial)"
           /* Check that all this is for the same instance */
           " JOIN merchant_contract_terms contracts"
           "   USING (merchant_serial, order_serial)"
           " WHERE mcon.order_serial=$1");

  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "lookup_transfer_details_by_order",
    params,
    &lookup_transfer_details_by_order_cb,
    &ltdo);
  if (qs < 0)
    return qs;
  return ltdo.qs;
}
