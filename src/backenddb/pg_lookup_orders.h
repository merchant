/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_orders.h
 * @brief implementation of the lookup_orders function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_ORDERS_H
#define PG_LOOKUP_ORDERS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve orders given the @a instance_id.
 *
 * @param cls closure
 * @param instance_id instance to obtain order of
 * @param of filter to apply when looking up orders
 * @param cb callback to pass all the orders that are found
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_orders (void *cls,
                      const char *instance_id,
                      const struct TALER_MERCHANTDB_OrderFilter *of,
                      TALER_MERCHANTDB_OrdersCallback cb,
                      void *cb_cls);

#endif
