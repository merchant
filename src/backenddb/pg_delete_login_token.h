/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_login_token.h
 * @brief implementation of the delete_login_token function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_DELETE_LOGIN_TOKEN_H
#define PG_DELETE_LOGIN_TOKEN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Delete login token from database.
 *
 * @param cls closure
 * @param id identifier of the instance
 * @param token value of the token
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_delete_login_token (
  void *cls,
  const char *id,
  const struct TALER_MERCHANTDB_LoginTokenP *token);


#endif
