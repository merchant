/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_instance_auth.h
 * @brief implementation of the lookup_instance_auth function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_INSTANCE_AUTH_H
#define PG_LOOKUP_INSTANCE_AUTH_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup authentication data of an instance.
 *
 * @param cls closure
 * @param instance_id instance to query
 * @param[out] ias where to store the auth data
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_instance_auth (void *cls,
                             const char *instance_id,
                             struct TALER_MERCHANTDB_InstanceAuthSettings *ias);

#endif
