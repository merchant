/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_webhook.c
 * @brief Implementation of the delete_webhook function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_webhook.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_delete_webhook (void *cls,
                       const char *instance_id,
                       const char *webhook_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (webhook_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "delete_webhook",
           "DELETE"
           " FROM merchant_webhook"
           " WHERE merchant_webhook.merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND merchant_webhook.webhook_id=$2");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_webhook",
                                             params);
}
