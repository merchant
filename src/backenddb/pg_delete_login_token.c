/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_login_token.c
 * @brief Implementation of the delete_login_token function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_login_token.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_delete_login_token (
  void *cls,
  const char *id,
  const struct TALER_MERCHANTDB_LoginTokenP *token)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (id),
    GNUNET_PQ_query_param_auto_from_type (token),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "delete_login_token",
           "DELETE FROM merchant_login_tokens"
           " WHERE token=$2"
           "   AND merchant_serial="
           "    (SELECT merchant_serial"
           "       FROM merchant_instances"
           "      WHERE merchant_id=$1)");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_login_token",
                                             params);
}

