/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order_by_fulfillment.c
 * @brief Implementation of the lookup_order_by_fulfillment function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_order_by_fulfillment.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order_by_fulfillment (
  void *cls,
  const char *instance_id,
  const char *fulfillment_url,
  const char *session_id,
  bool allow_refunded_for_repurchase,
  char **order_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (fulfillment_url),
    GNUNET_PQ_query_param_string (session_id),
    GNUNET_PQ_query_param_bool (allow_refunded_for_repurchase),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_string ("order_id",
                                  order_id),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_order_by_fulfillment",
           "SELECT"
           "  mct.order_id"
           " FROM merchant_contract_terms mct"
           " LEFT JOIN merchant_refunds mref"
           "   USING (order_serial)"
           " WHERE fulfillment_url=$2"
           "   AND session_id=$3"
           "   AND merchant_serial="
           "        (SELECT merchant_serial"
           "           FROM merchant_instances"
           "          WHERE merchant_id=$1)"
           "   AND ((CAST($4 AS BOOL)) OR"
           "        mref.refund_serial IS NULL)"
           /* Theoretically, multiple paid orders
              for the same fulfillment URL could
              exist for this session_id -- if a
              wallet was broken and did multiple
              payments without repurchase detection.
              So we need to limit to 1 when returning! */
           " ORDER BY order_id DESC"
           " LIMIT 1;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_order_by_fulfillment",
                                                   params,
                                                   rs);
}
