/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_open_transfers.h
 * @brief implementation of the select_open_transfers function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_OPEN_TRANSFERS_H
#define PG_SELECT_OPEN_TRANSFERS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Retrieve wire transfer details of wire details
 * that taler-merchant-exchange still needs to
 * investigate.
 *
 * @param cls closure, typically a connection to the db
 * @param limit maximum number of results to return
 * @param cb function called with the wire transfer data
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_open_transfers (void *cls,
                              uint64_t limit,
                              TALER_MERCHANTDB_OpenTransferCallback cb,
                              void *cb_cls);


#endif
