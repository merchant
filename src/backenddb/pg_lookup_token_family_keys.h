/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_token_family_keys.h
 * @brief implementation of the lookup_token_family_keys function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_LOOKUP_TOKEN_FAMILY_KEYS_H
#define PG_LOOKUP_TOKEN_FAMILY_KEYS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Lookup token family keys that may be used for a payment.
 *
 * @param cls closure
 * @param instance_id instance to lookup token family key for
 * @param token_family_slug slug of token family to lookup
 * @param start_time signature validity start the keys must fall into
 * @param end_time signature validity end the keys must fall into
 * @param cb function to call with each matching key
 * @param cb_cls closure for @a cb
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_token_family_keys (
  void *cls,
  const char *instance_id,
  const char *token_family_slug,
  struct GNUNET_TIME_Timestamp start_time,
  struct GNUNET_TIME_Timestamp end_time,
  TALER_MERCHANTDB_TokenKeyCallback cb,
  void *cb_cls);


#endif
