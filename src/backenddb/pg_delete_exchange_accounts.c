/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_exchange_accounts.c
 * @brief Implementation of the delete_exchange_accounts function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_exchange_accounts.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_delete_exchange_accounts (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (master_pub),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "delete_exchange_accounts",
           "DELETE FROM merchant_exchange_accounts"
           " WHERE master_pub=$1;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_exchange_accounts",
                                             params);
}
