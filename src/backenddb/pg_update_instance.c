/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_instance.c
 * @brief Implementation of the update_instance function for Postgres
 * @author Christian Grothoff
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_instance.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_instance (void *cls,
                        const struct TALER_MERCHANTDB_InstanceSettings *is)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (is->id),
    GNUNET_PQ_query_param_string (is->name),
    TALER_PQ_query_param_json (is->address),
    TALER_PQ_query_param_json (is->jurisdiction),
    GNUNET_PQ_query_param_bool (is->use_stefan),
    GNUNET_PQ_query_param_relative_time (
      &is->default_wire_transfer_delay),
    GNUNET_PQ_query_param_relative_time (
      &is->default_pay_delay),
    (NULL == is->website)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->website),
    (NULL == is->email)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->email),
    (NULL == is->logo)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (is->logo),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_instance",
           "UPDATE merchant_instances SET"
           " merchant_name=$2"
           ",address=$3"
           ",jurisdiction=$4"
           ",use_stefan=$5"
           ",default_wire_transfer_delay=$6"
           ",default_pay_delay=$7"
           ",website=$8"
           ",email=$9"
           ",logo=$10"
           " WHERE merchant_id=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_instance",
                                             params);
}
