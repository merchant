/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_unlock_inventory.h
 * @brief implementation of the unlock_inventory function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_UNLOCK_INVENTORY_H
#define PG_UNLOCK_INVENTORY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Release an inventory lock by UUID. Releases ALL stocks locked under
 * the given UUID.
 *
 * @param cls closure
 * @param uuid the UUID to release locks for
 * @return transaction status,
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS means there are no locks under @a uuid
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT indicates success
 */
enum GNUNET_DB_QueryStatus
TMH_PG_unlock_inventory (void *cls,
                         const struct GNUNET_Uuid *uuid);

#endif
