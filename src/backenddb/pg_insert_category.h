/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_category.h
 * @brief implementation of the insert_category function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_CATEGORY_H
#define PG_INSERT_CATEGORY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert new product category.
 *
 * @param cls closure
 * @param instance_id instance to insert OTP device for
 * @param category_name name of the category
 * @param category_name_i18n translations of the category name
 * @param[out] category_id set to the category id on success
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_category (void *cls,
                        const char *instance_id,
                        const char *category_name,
                        const json_t *category_name_i18n,
                        uint64_t *category_id);


#endif
