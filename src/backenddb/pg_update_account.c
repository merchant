/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_account.c
 * @brief Implementation of the update_account function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_account.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_account (
  void *cls,
  const char *id,
  const struct TALER_MerchantWireHashP *h_wire,
  const char *credit_facade_url,
  const json_t *credit_facade_credentials)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (id),
    GNUNET_PQ_query_param_auto_from_type (h_wire),
    NULL == credit_facade_url
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (credit_facade_url),
    NULL == credit_facade_credentials
    ? GNUNET_PQ_query_param_null ()
    : TALER_PQ_query_param_json (credit_facade_credentials),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_account",
           "UPDATE merchant_accounts SET"
           " credit_facade_url=$3"
           ",credit_facade_credentials=COALESCE($4,credit_facade_credentials)"
           " WHERE h_wire=$2"
           "   AND merchant_serial="
           "   (SELECT merchant_serial"
           "      FROM merchant_instances"
           "      WHERE merchant_id=$1);");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_account",
                                             params);
}
