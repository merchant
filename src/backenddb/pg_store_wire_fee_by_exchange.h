/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_store_wire_fee_by_exchange.h
 * @brief implementation of the postgres_store_wire_fee_by_exchange function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_STORE_WIRE_FEE_BY_EXCHANGE_H
#define PG_STORE_WIRE_FEE_BY_EXCHANGE_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Store information about wire fees charged by an exchange,
 * including signature (so we have proof).
 *
 * @param cls closure
 * @param master_pub public key of the exchange
 * @param h_wire_method hash of wire method
 * @param fees the fee charged
 * @param start_date start of fee being used
 * @param end_date end of fee being used
 * @param master_sig signature of exchange over fee structure
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_store_wire_fee_by_exchange (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub,
  const struct GNUNET_HashCode *h_wire_method,
  const struct TALER_WireFeeSet *fees,
  struct GNUNET_TIME_Timestamp start_date,
  struct GNUNET_TIME_Timestamp end_date,
  const struct TALER_MasterSignatureP *master_sig);

#endif
