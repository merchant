/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_contract_terms.h
 * @brief implementation of the insert_contract_terms function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_CONTRACT_TERMS_H
#define PG_INSERT_CONTRACT_TERMS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Store contract terms given its @a order_id. Note that some attributes are
 * expected to be calculated inside of the function, like the hash of the
 * contract terms (to be hashed), the creation_time and pay_deadline (to be
 * obtained from the merchant_orders table). The "session_id" should be
 * initially set to the empty string.  The "fulfillment_url" and "refund_deadline"
 * must be extracted from @a contract_terms.
 *
 * @param cls closure
 * @param instance_id instance's identifier
 * @param order_id order_id used to store
 * @param contract_terms contract terms to store
 * @param[out] order_serial set to the serial of the order
 * @return transaction status, #GNUNET_DB_STATUS_HARD_ERROR if @a contract_terms
 *          is malformed
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_contract_terms (
  void *cls,
  const char *instance_id,
  const char *order_id,
  json_t *contract_terms,
  uint64_t *order_serial);

#endif
