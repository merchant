/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_transfer_status.c
 * @brief Implementation of the update_transfer_status function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_transfer_status.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_transfer_status (
  void *cls,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  struct GNUNET_TIME_Absolute next_attempt,
  enum TALER_ErrorCode ec,
  bool failed,
  bool verified)
{
  struct PostgresClosure *pg = cls;
  uint32_t ec32 = (uint32_t) ec;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (wtid),
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_uint32 (&ec32),
    GNUNET_PQ_query_param_bool (failed),
    GNUNET_PQ_query_param_bool (verified),
    GNUNET_PQ_query_param_absolute_time (&next_attempt),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_transfer_status",
           "UPDATE merchant_transfers SET"
           " validation_status=$3"
           ",failed=$4"
           ",verified=$5"
           ",ready_time=$6"
           " WHERE wtid=$1"
           "   AND exchange_url=$2");
  return GNUNET_PQ_eval_prepared_non_select (
    pg->conn,
    "update_transfer_status",
    params);
}
