/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lock_product.h
 * @brief implementation of the lock_product function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOCK_PRODUCT_H
#define PG_LOCK_PRODUCT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lock stocks of a particular product. Note that the transaction must
 * enforce that the "stocked-sold-lost >= locked" constraint holds.
 *
 * @param cls closure
 * @param instance_id instance to lookup products for
 * @param product_id product to lookup
 * @param uuid the UUID that holds the lock
 * @param quantity how many units should be locked
 * @param expiration_time when should the lock expire
 * @return database result code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the
 *         product is unknown OR if there insufficient stocks remaining
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lock_product (void *cls,
                     const char *instance_id,
                     const char *product_id,
                     const struct GNUNET_Uuid *uuid,
                     uint64_t quantity,
                     struct GNUNET_TIME_Timestamp expiration_time);

#endif
