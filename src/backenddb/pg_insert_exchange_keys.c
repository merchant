/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_exchange_keys.c
 * @brief Implementation of the insert_exchange_keys function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_exchange_keys.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_exchange_keys (void *cls,
                             const struct TALER_EXCHANGE_Keys *keys)
{
  struct PostgresClosure *pg = cls;
  json_t *jkeys = TALER_EXCHANGE_keys_to_json (keys);
  struct GNUNET_PQ_QueryParam params[] = {
    TALER_PQ_query_param_json (jkeys),
    GNUNET_PQ_query_param_timestamp (&keys->last_denom_issue_date),
    GNUNET_PQ_query_param_string (keys->exchange_url),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "insert_exchange_keys",
           "INSERT INTO merchant_exchange_keys"
           "(keys_json"
           ",expiration_time"
           ",exchange_url"
           ") VALUES ($1, $2, $3);");
  PREPARE (pg,
           "update_exchange_keys",
           "UPDATE merchant_exchange_keys SET"
           " keys_json=$1"
           ",expiration_time=$2"
           " WHERE"
           " exchange_url=$3;");
  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "update_exchange_keys",
                                           params);
  if (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS == qs)
    qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_exchange_keys",
                                             params);
  json_decref (jkeys);
  return qs;
}
