/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_pending_webhooks.h
 * @brief implementation of the lookup_pending_webhooks function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_PENDING_WEBHOOKS_H
#define PG_LOOKUP_PENDING_WEBHOOKS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup the webhook that need to be send in priority.
 * send.
 *
 * @param cls closure
 * @param cb pending webhook callback
 * @param cb_cls callback closure
 */
// WHERE next_attempt <= now ORDER BY next_attempt ASC
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_pending_webhooks (void *cls,
                                TALER_MERCHANTDB_PendingWebhooksCallback cb,
                                void *cb_cls);


/**
 * Lookup future webhook in the pending webhook that need to be send.
 * With that we can know how long the system can 'sleep'.
 *
 * @param cls closure
 * @param cb pending webhook callback
 * @param cb_cls callback closure
 */
// ORDER BY next_attempt ASC LIMIT 1
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_future_webhook (void *cls,
                              TALER_MERCHANTDB_PendingWebhooksCallback cb,
                              void *cb_cls);


/**
   * Lookup all the webhooks in the pending webhook.
   * Use by the administrator
   *
   * @param cls closure
   * @param instance_id to lookup webhooks for this instance particularly
   * @param min_row to see the list of the pending webhook that it is started with this minimum row.
   * @param max_results to see the list of the pending webhook that it is end with this max results.
   * @param cb pending webhook callback
   * @param cb_cls callback closure
   */
// WHERE webhook_pending_serial > min_row ORDER BY webhook_pending_serial ASC LIMIT max_results
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_all_webhooks (void *cls,
                            const char *instance_id,
                            uint64_t min_row,
                            uint32_t max_results,
                            TALER_MERCHANTDB_PendingWebhooksCallback cb,
                            void *cb_cls);

#endif
