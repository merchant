/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_product.c
 * @brief Implementation of the lookup_product function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_product.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_product (void *cls,
                       const char *instance_id,
                       const char *product_id,
                       struct TALER_MERCHANTDB_ProductDetails *pd,
                       size_t *num_categories,
                       uint64_t **categories)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (product_id),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "lookup_product",
           "SELECT"
           " mi.description"
           ",mi.description_i18n"
           ",mi.unit"
           ",mi.price"
           ",mi.taxes"
           ",mi.total_stock"
           ",mi.total_sold"
           ",mi.total_lost"
           ",mi.image"
           ",mi.address"
           ",mi.next_restock"
           ",mi.minimum_age"
           ",t.category_array AS categories"
           " FROM merchant_inventory mi"
           " JOIN merchant_instances inst"
           "   USING (merchant_serial)"
           ",LATERAL ("
           "   SELECT ARRAY ("
           "     SELECT mpc.category_serial"
           "       FROM merchant_product_categories mpc"
           "      WHERE mpc.product_serial = mi.product_serial"
           "   ) AS category_array"
           " ) t"
           " WHERE inst.merchant_id=$1"
           "   AND mi.product_id=$2"
           );
  if (NULL == pd)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_product",
                                                     params,
                                                     rs_null);
  }
  else
  {
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("description",
                                    &pd->description),
      TALER_PQ_result_spec_json ("description_i18n",
                                 &pd->description_i18n),
      GNUNET_PQ_result_spec_string ("unit",
                                    &pd->unit),
      TALER_PQ_result_spec_amount_with_currency ("price",
                                                 &pd->price),
      TALER_PQ_result_spec_json ("taxes",
                                 &pd->taxes),
      GNUNET_PQ_result_spec_uint64 ("total_stock",
                                    &pd->total_stock),
      GNUNET_PQ_result_spec_uint64 ("total_sold",
                                    &pd->total_sold),
      GNUNET_PQ_result_spec_uint64 ("total_lost",
                                    &pd->total_lost),
      GNUNET_PQ_result_spec_string ("image",
                                    &pd->image),
      TALER_PQ_result_spec_json ("address",
                                 &pd->address),
      GNUNET_PQ_result_spec_timestamp ("next_restock",
                                       &pd->next_restock),
      GNUNET_PQ_result_spec_uint32 ("minimum_age",
                                    &pd->minimum_age),
      GNUNET_PQ_result_spec_array_uint64 (pg->conn,
                                          "categories",
                                          num_categories,
                                          categories),
      GNUNET_PQ_result_spec_end
    };

    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_product",
                                                     params,
                                                     rs);
  }
}
