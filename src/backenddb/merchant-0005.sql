--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0005', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_template
  ADD COLUMN required_currency VARCHAR(12) DEFAULT NULL,
  ADD COLUMN editable_defaults TEXT DEFAULT NULL;

COMMENT ON COLUMN merchant_template.required_currency
  IS 'currency that the amount to be paid entered by the user must be in; if not given and the amount is not fixed in the template contract, the user may edit the currency';
COMMENT ON COLUMN merchant_template.editable_defaults
  IS 'JSON object with fields matching the template contract, just with default values that are editable by the user';


-- Complete transaction
COMMIT;
