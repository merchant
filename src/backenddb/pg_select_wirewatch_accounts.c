/*
   This file is part of TALER
   Copyright (C) 2022, 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_wirewatch_accounts.c
 * @brief Implementation of the select_wirewatch_accounts function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_wirewatch_accounts.h"
#include "pg_helper.h"


/**
 * Closure for #handle_results().
 */
struct Context
{
  /**
   * Function to call with results.
   */
  TALER_MERCHANTDB_WirewatchWorkCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Set to true if the parsing failed.
   */
  bool failure;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about accounts.
 *
 * @param cls of type `struct Context *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
handle_results (void *cls,
                PGresult *result,
                unsigned int num_results)
{
  struct Context *ctx = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    char *instance;
    struct TALER_FullPayto payto_uri;
    char *facade_url;
    json_t *credential;
    uint64_t last_serial;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("merchant_id",
                                    &instance),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto_uri.full_payto),
      GNUNET_PQ_result_spec_string ("credit_facade_url",
                                    &facade_url),
      GNUNET_PQ_result_spec_allow_null (
        TALER_PQ_result_spec_json ("credit_facade_credentials",
                                   &credential),
        NULL),
      GNUNET_PQ_result_spec_uint64 ("last_bank_serial",
                                    &last_serial),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ctx->failure = true;
      return;
    }
    ctx->cb (ctx->cb_cls,
             instance,
             payto_uri,
             facade_url,
             credential,
             last_serial);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_select_wirewatch_accounts (
  void *cls,
  TALER_MERCHANTDB_WirewatchWorkCallback cb,
  void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct Context ctx = {
    .cb = cb,
    .cb_cls = cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pg,
           "select_wirewatch_progress",
           "SELECT"
           " last_bank_serial"
           ",merchant_id"
           ",payto_uri"
           ",credit_facade_url"
           ",credit_facade_credentials"
           " FROM merchant_accounts"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE active"
           "   AND credit_facade_url IS NOT NULL");
  check_connection (pg);
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "select_wirewatch_progress",
                                             params,
                                             &handle_results,
                                             &ctx);
  if (ctx.failure)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
