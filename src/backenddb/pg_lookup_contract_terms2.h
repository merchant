/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_contract_terms2.h
 * @brief implementation of the lookup_contract_terms2 function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_CONTRACT_TERMS2_H
#define PG_LOOKUP_CONTRACT_TERMS2_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve contract terms given its @a order_id
 *
 * @param cls closure
 * @param instance_id instance's identifier
 * @param order_id order_id used to lookup.
 * @param[out] contract_terms where to store the result, NULL to only check for existence
 * @param[out] order_serial set to the order's serial number
 * @param[out] paid set to true if the order is fully paid
 * @param[out] claim_token set to the claim token, NULL to only check for existence
 * @param[out] pos_key encoded key for payment verification
 * @param[out] pos_algorithm algorithm to compute the payment verification
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_contract_terms2 (
  void *cls,
  const char *instance_id,
  const char *order_id,
  json_t **contract_terms,
  uint64_t *order_serial,
  bool *paid,
  struct TALER_ClaimTokenP *claim_token,
  char **pos_key,
  enum TALER_MerchantConfirmationAlgorithm *pos_algorithm);

#endif
