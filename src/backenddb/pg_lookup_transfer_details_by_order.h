/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfer_details_by_order.h
 * @brief implementation of the lookup_transfer_details_by_order function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_TRANSFER_DETAILS_BY_ORDER_H
#define PG_LOOKUP_TRANSFER_DETAILS_BY_ORDER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve wire transfer details for all deposits associated with
 * a given @a order_serial.
 *
 * @param cls closure
 * @param order_serial identifies the order
 * @param cb function called with the wire transfer details
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfer_details_by_order (void *cls,
                                         uint64_t order_serial,
                                         TALER_MERCHANTDB_OrderTransferDetailsCallback cb,
                                         void *cb_cls);

#endif
