/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_account.h
 * @brief implementation of the update_account function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_ACCOUNT_H
#define PG_UPDATE_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Update information about an instance's account in our database.
 *
 * @param cls closure
 * @param id identifier of the instance
 * @param h_wire which account to update
 * @param credit_facade_url new facade URL, can be NULL
 * @param credit_facade_credentials new credentials, can be NULL to keep previous credentials
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_account (
  void *cls,
  const char *id,
  const struct TALER_MerchantWireHashP *h_wire,
  const char *credit_facade_url,
  const json_t *credit_facade_credentials);


#endif
