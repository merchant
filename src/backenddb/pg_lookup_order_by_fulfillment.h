/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order_by_fulfillment.h
 * @brief implementation of the lookup_order_by_fulfillment function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_ORDER_BY_FULFILLMENT_H
#define PG_LOOKUP_ORDER_BY_FULFILLMENT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve the order ID that was used to pay for a resource within a session.
 *
 * @param cls closure
 * @param instance_id identifying the instance
 * @param fulfillment_url URL that canonically identifies the resource
 *        being paid for
 * @param session_id session id
 * @param allow_refunded_for_repurchase true to include refunded orders in repurchase detection
 * @param[out] order_id where to store the order ID that was used when
 *             paying for the resource URL
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order_by_fulfillment (void *cls,
                                    const char *instance_id,
                                    const char *fulfillment_url,
                                    const char *session_id,
                                    bool allow_refunded_for_repurchase,
                                    char **order_id);

#endif
