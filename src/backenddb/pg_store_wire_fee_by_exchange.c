/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_store_wire_fee_by_exchange.c
 * @brief Implementation of the store_wire_fee_by_exchange function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_store_wire_fee_by_exchange.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_store_wire_fee_by_exchange (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub,
  const struct GNUNET_HashCode *h_wire_method,
  const struct TALER_WireFeeSet *fees,
  struct GNUNET_TIME_Timestamp start_date,
  struct GNUNET_TIME_Timestamp end_date,
  const struct TALER_MasterSignatureP *master_sig)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (master_pub),
    GNUNET_PQ_query_param_auto_from_type (h_wire_method),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                 &fees->wire),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                 &fees->closing),
    GNUNET_PQ_query_param_timestamp (&start_date),
    GNUNET_PQ_query_param_timestamp (&end_date),
    GNUNET_PQ_query_param_auto_from_type (master_sig),
    GNUNET_PQ_query_param_end
  };

  /* no preflight check here, run in its own transaction by the caller */
  PREPARE (pg,
           "insert_wire_fee",
           "INSERT INTO merchant_exchange_wire_fees"
           "(master_pub"
           ",h_wire_method"
           ",wire_fee"
           ",closing_fee"
           ",start_date"
           ",end_date"
           ",master_sig)"
           " VALUES "
           "($1, $2, $3, $4, $5, $6, $7)"
           " ON CONFLICT DO NOTHING");
  check_connection (pg);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Storing wire fee for %s starting at %s of %s\n",
              TALER_B2S (master_pub),
              GNUNET_TIME_timestamp2s (start_date),
              TALER_amount2s (&fees->wire));
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_wire_fee",
                                             params);
}
