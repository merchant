/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_inactivate_account.h
 * @brief implementation of the inactivate_account function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INACTIVATE_ACCOUNT_H
#define PG_INACTIVATE_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Set an instance's account in our database to "inactive".
 *
 * @param cls closure
 * @param merchant_id merchant backend instance ID
 * @param h_wire hash of the wire account to set to inactive
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_inactivate_account (void *cls,
                           const char *merchant_id,
                           const struct TALER_MerchantWireHashP *h_wire);


#endif
