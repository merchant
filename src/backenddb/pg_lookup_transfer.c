/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfer.c
 * @brief Implementation of the lookup_transfer function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_transfer.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfer (void *cls,
                        const char *instance_id,
                        const char *exchange_url,
                        const struct TALER_WireTransferIdentifierRawP *wtid,
                        struct TALER_Amount *total_amount,
                        struct TALER_Amount *wire_fee,
                        struct TALER_Amount *exchange_amount,
                        struct GNUNET_TIME_Timestamp *execution_time,
                        bool *have_exchange_sig,
                        bool *verified)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_auto_from_type (wtid),
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_end
  };
  uint8_t verified8;
  /** Amount we got actually credited, _excludes_ the wire fee */
  bool no_sig;
  struct TALER_Amount credit_amount;
  struct GNUNET_PQ_ResultSpec rs[] = {
    TALER_PQ_result_spec_amount_with_currency ("credit_amount",
                                               &credit_amount),
    GNUNET_PQ_result_spec_allow_null (
      TALER_PQ_result_spec_amount_with_currency ("wire_fee",
                                                 wire_fee),
      &no_sig),
    GNUNET_PQ_result_spec_allow_null (
      TALER_PQ_result_spec_amount_with_currency ("exchange_amount",
                                                 exchange_amount),
      NULL),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_timestamp ("execution_time",
                                       execution_time),
      NULL),
    GNUNET_PQ_result_spec_auto_from_type ("verified",
                                          &verified8),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  *execution_time = GNUNET_TIME_UNIT_ZERO_TS;

  PREPARE (pg,
           "lookup_transfer",
           "SELECT"
           " mt.credit_amount AS credit_amount"
           ",mts.credit_amount AS exchange_amount"
           ",wire_fee"
           ",execution_time"
           ",verified"
           " FROM merchant_transfers mt"
           "  JOIN merchant_accounts USING (account_serial)"
           "  JOIN merchant_instances USING (merchant_serial)"
           "  LEFT JOIN merchant_transfer_signatures mts USING (credit_serial)"
           " WHERE wtid=$2"
           "   AND exchange_url=$1"
           "   AND merchant_id=$3;");

  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "lookup_transfer",
                                                 params,
                                                 rs);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Lookup transfer returned %d\n",
              qs);
  if (qs > 0)
  {
    *have_exchange_sig = ! no_sig;
    *verified = (0 != verified8);
    if (GNUNET_OK !=
        TALER_amount_cmp_currency (&credit_amount,
                                   wire_fee))
    {
      GNUNET_break (0);
      return GNUNET_DB_STATUS_HARD_ERROR;
    }
    if ( (! no_sig) &&
         (0 >
          TALER_amount_add (total_amount,
                            &credit_amount,
                            wire_fee)) )
    {
      GNUNET_break (0);
      return GNUNET_DB_STATUS_HARD_ERROR;
    }
  }
  else
  {
    *verified = false;
    *have_exchange_sig = false;
  }
  return qs;
}
