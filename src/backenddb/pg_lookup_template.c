/*
   This file is part of TALER
   Copyright (C) 2022, 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_template.c
 * @brief Implementation of the lookup_template function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_template.h"
#include "pg_helper.h"


/**
 * Lookup details about a particular template.
 *
 * @param cls closure
 * @param instance_id instance to lookup template for
 * @param template_id template to lookup
 * @param[out] td set to the template details on success, can be NULL
 *             (in that case we only want to check if the template exists)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_template (void *cls,
                        const char *instance_id,
                        const char *template_id,
                        struct TALER_MERCHANTDB_TemplateDetails *td)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (template_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_template",
           "SELECT"
           " mt.template_description"
           ",mod.otp_id"
           ",mt.template_contract"
           ",mt.editable_defaults"
           " FROM merchant_template mt"
           " JOIN merchant_instances mi"
           "   ON (mi.merchant_serial = mt.merchant_serial)"
           " LEFT JOIN merchant_otp_devices mod"
           "   ON (mod.otp_serial = mt.otp_device_id)"
           " WHERE mi.merchant_id=$1"
           "   AND mt.template_id=$2");
  if (NULL == td)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_template",
                                                     params,
                                                     rs_null);
  }
  else
  {
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("template_description",
                                    &td->template_description),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("otp_id",
                                      &td->otp_id),
        NULL),
      TALER_PQ_result_spec_json ("template_contract",
                                 &td->template_contract),
      GNUNET_PQ_result_spec_allow_null (
        TALER_PQ_result_spec_json ("editable_defaults",
                                   &td->editable_defaults),
        NULL),
      GNUNET_PQ_result_spec_end
    };

    memset (td,
            0,
            sizeof (*td));
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_template",
                                                     params,
                                                     rs);
  }
}
