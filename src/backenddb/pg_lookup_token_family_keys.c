/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_token_family_keys.c
 * @brief Implementation of the lookup_token_family_keys function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_token_family_keys.h"
#include "pg_helper.h"


/**
 * Closure for lookup_token_keys_cb().
 */
struct Context
{
  /**
   * Postgres handle.
   */
  struct PostgresClosure *pg;

  /**
   * Function to call on each result.
   */
  TALER_MERCHANTDB_TokenKeyCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about products.
 *
 * @param[in,out] cls of type `struct LookupProductsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_token_keys_cb (void *cls,
                      PGresult *result,
                      unsigned int num_results)
{
  struct Context *ctx = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    struct TALER_MERCHANTDB_TokenFamilyKeyDetails details;
    char *kind;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_blind_sign_pub ("pub",
                                              &details.pub.public_key),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_blind_sign_priv ("priv",
                                               &details.priv.private_key),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_timestamp ("signature_validity_start",
                                         &details.signature_validity_start),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_timestamp ("signature_validity_end",
                                         &details.signature_validity_end),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_timestamp ("private_key_deleted_at",
                                         &details.private_key_deleted_at),
        NULL),
      GNUNET_PQ_result_spec_string ("slug",
                                    &details.token_family.slug),
      GNUNET_PQ_result_spec_string ("name",
                                    &details.token_family.name),
      GNUNET_PQ_result_spec_string ("description",
                                    &details.token_family.description),
      GNUNET_PQ_result_spec_string ("cipher_choice",
                                    &details.token_family.cipher_spec),
      TALER_PQ_result_spec_json ("description_i18n",
                                 &details.token_family.description_i18n),
      GNUNET_PQ_result_spec_timestamp ("valid_after",
                                       &details.token_family.valid_after),
      GNUNET_PQ_result_spec_timestamp ("valid_before",
                                       &details.token_family.valid_before),
      GNUNET_PQ_result_spec_relative_time ("duration",
                                           &details.token_family.duration),
      GNUNET_PQ_result_spec_relative_time ("validity_granularity",
                                           &details.token_family.
                                           validity_granularity),
      GNUNET_PQ_result_spec_relative_time ("start_offset",
                                           &details.token_family.start_offset),
      GNUNET_PQ_result_spec_string ("kind",
                                    &kind),
      GNUNET_PQ_result_spec_uint64 ("issued",
                                    &details.token_family.issued),
      GNUNET_PQ_result_spec_uint64 ("used",
                                    &details.token_family.used),
      GNUNET_PQ_result_spec_end
    };

    memset (&details,
            0,
            sizeof (details));
    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ctx->extract_failed = true;
      return;
    }
    if (0 == strcmp (kind,
                     "discount"))
      details.token_family.kind = TALER_MERCHANTDB_TFK_Discount;
    else if (0 == strcmp (kind,
                          "subscription"))
      details.token_family.kind = TALER_MERCHANTDB_TFK_Subscription;
    else
    {
      GNUNET_break (0);
      GNUNET_free (kind);
      GNUNET_PQ_cleanup_result (rs);
      return;
    }
    ctx->cb (ctx->cb_cls,
             &details);
    if (NULL != details.pub.public_key) /* guard against GNUnet 0.23 bug! */
      GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_token_family_keys (
  void *cls,
  const char *instance_id,
  const char *token_family_slug,
  struct GNUNET_TIME_Timestamp start_time,
  struct GNUNET_TIME_Timestamp end_time,
  TALER_MERCHANTDB_TokenKeyCallback cb,
  void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (token_family_slug),
    GNUNET_PQ_query_param_timestamp (&start_time),
    GNUNET_PQ_query_param_timestamp (&end_time),
    GNUNET_PQ_query_param_end
  };
  struct Context ctx = {
    .pg = pg,
    .cb = cb,
    .cb_cls = cb_cls
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_token_family_keys",
           "SELECT"
           " h_pub"
           ",mtfk.pub"
           ",mtfk.priv"
           ",cipher_choice"
           ",mtfk.signature_validity_start"
           ",mtfk.signature_validity_end"
           ",mtfk.private_key_deleted_at"
           ",slug"
           ",name"
           ",description"
           ",description_i18n"
           ",mtf.valid_after"
           ",mtf.valid_before"
           ",duration"
           ",validity_granularity"
           ",start_offset"
           ",kind"
           ",issued"
           ",used"
           " FROM merchant_token_families mtf"
           " LEFT JOIN merchant_token_family_keys mtfk"
           "   USING (token_family_serial)"
           " JOIN merchant_instances mi"
           "   USING (merchant_serial)"
           " WHERE mi.merchant_id=$1"
           "   AND slug=$2"
           "   AND COALESCE ($3 <= mtfk.signature_validity_end, TRUE)"
           "   AND COALESCE ($4 >= mtfk.signature_validity_start, TRUE);");
  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "lookup_token_family_keys",
    params,
    &lookup_token_keys_cb,
    &ctx);
  if (ctx.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
