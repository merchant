/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_otp.c
 * @brief Implementation of the insert_otp function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_otp.h"
#include "pg_helper.h"


/**
 * Insert details about a particular OTP device.
 *
 * @param cls closure
 * @param instance_id instance to insert OTP device for
 * @param otp_id otp identifier of OTP device to insert
 * @param td the OTP device details to insert
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_otp (void *cls,
                   const char *instance_id,
                   const char *otp_id,
                   const struct TALER_MERCHANTDB_OtpDeviceDetails *td)
{
  struct PostgresClosure *pg = cls;
  uint32_t pos32 = (uint32_t) td->otp_algorithm;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (otp_id),
    GNUNET_PQ_query_param_string (td->otp_description),
    GNUNET_PQ_query_param_string (td->otp_key),
    GNUNET_PQ_query_param_uint32 (&pos32),
    GNUNET_PQ_query_param_uint64 (&td->otp_ctr),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_otp",
           "INSERT INTO merchant_otp_devices"
           "(merchant_serial"
           ",otp_id"
           ",otp_description"
           ",otp_key"
           ",otp_algorithm"
           ",otp_ctr"
           ")"
           " SELECT merchant_serial,"
           " $2, $3, $4, $5, $6"
           " FROM merchant_instances"
           " WHERE merchant_id=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_otp",
                                             params);
}
