/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_account_kyc_get_status.h
 * @brief implementation of the account_kyc_get_status function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_ACCOUNT_KYC_GET_STATUS_H
#define PG_ACCOUNT_KYC_GET_STATUS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Check an instance's account's KYC status.
 *
 * @param cls closure
 * @param merchant_id merchant backend instance ID
 * @param h_wire hash of the wire account to check,
 *        NULL to check all accounts of the merchant
 * @param exchange_url base URL of the exchange to check,
 *        NULL to check all exchanges
 * @param kyc_cb KYC status callback to invoke
 * @param kyc_cb_cls closure for @a kyc_cb
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_account_kyc_get_status (void *cls,
                               const char *merchant_id,
                               const struct TALER_MerchantWireHashP *h_wire,
                               const char *exchange_url,
                               TALER_MERCHANTDB_KycCallback kyc_cb,
                               void *kyc_cb_cls);

#endif
