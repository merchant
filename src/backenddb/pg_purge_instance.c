/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_purge_instance.c
 * @brief Implementation of the purge_instance function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_purge_instance.h"
#include "pg_helper.h"

/**
 * Purge an instance and all associated information from our database.
 * Highly likely to cause undesired data loss. Use with caution.
 *
 * @param cls closure
 * @param merchant_id identifier of the instance
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_purge_instance (void *cls,
                       const char *merchant_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (merchant_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "purge_instance",
           "DELETE FROM merchant_instances"
           " WHERE merchant_instances.merchant_id = $1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "purge_instance",
                                             params);
}
