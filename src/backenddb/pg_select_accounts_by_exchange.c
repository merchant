/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_accounts_by_exchange.c
 * @brief Implementation of the select_accounts_by_exchange function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_accounts_by_exchange.h"
#include "pg_helper.h"


/**
 * Closure for #parse_accounts.
 */
struct SelectAccountContext
{
  /**
   * Function to call on each result.
   */
  TALER_MERCHANTDB_ExchangeAccountCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Set to true on failure.
   */
  bool failed;
};

/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about accounts.
 *
 * @param cls of type `struct SelectAccountContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
parse_accounts (void *cls,
                PGresult *result,
                unsigned int num_results)
{
  struct SelectAccountContext *ctx = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    struct TALER_FullPayto payto_uri;
    char *conversion_url = NULL;
    json_t *debit_restrictions;
    json_t *credit_restrictions;
    struct TALER_MasterSignatureP master_sig;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_auto_from_type ("master_sig",
                                            &master_sig),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto_uri.full_payto),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("conversion_url",
                                      &conversion_url),
        NULL),
      TALER_PQ_result_spec_json ("debit_restrictions",
                                 &debit_restrictions),
      TALER_PQ_result_spec_json ("credit_restrictions",
                                 &credit_restrictions),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ctx->failed = true;
      return;
    }
    ctx->cb (ctx->cb_cls,
             payto_uri,
             conversion_url,
             debit_restrictions,
             credit_restrictions,
             &master_sig);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_select_accounts_by_exchange (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub,
  TALER_MERCHANTDB_ExchangeAccountCallback cb,
  void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct SelectAccountContext ctx = {
    .cb = cb,
    .cb_cls = cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (master_pub),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "select_exchange_accounts",
           "SELECT"
           " payto_uri"
           ",conversion_url"
           ",debit_restrictions"
           ",credit_restrictions"
           ",master_sig"
           " FROM merchant_exchange_accounts"
           " WHERE master_pub=$1;");
  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "select_exchange_accounts",
    params,
    &parse_accounts,
    &ctx);
  if (ctx.failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
