/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_webhook.c
 * @brief Implementation of the lookup_webhook function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_webhook.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_webhook (void *cls,
                       const char *instance_id,
                       const char *webhook_id,
                       struct TALER_MERCHANTDB_WebhookDetails *wb)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (webhook_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_webhook",
           "SELECT"
           " event_type"
           ",url"
           ",http_method"
           ",header_template"
           ",body_template"
           " FROM merchant_webhook"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           "   AND merchant_webhook.webhook_id=$2");

  if (NULL == wb)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_webhook",
                                                     params,
                                                     rs_null);
  }
  else
  {
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("event_type",
                                    &wb->event_type),
      GNUNET_PQ_result_spec_string ("url",
                                    &wb->url),
      GNUNET_PQ_result_spec_string ("http_method",
                                    &wb->http_method),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("header_template",
                                      &wb->header_template),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("body_template",
                                      &wb->body_template),
        NULL),
      GNUNET_PQ_result_spec_end
    };

    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_webhook",
                                                     params,
                                                     rs);
  }
}
