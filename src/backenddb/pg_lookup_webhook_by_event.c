/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_webhook_by_event.c
 * @brief Implementation of the lookup_webhook_by_event function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_webhook_by_event.h"
#include "pg_helper.h"

/**
 * Context used for lookup_webhook_by_event_cb().
 */
struct LookupWebhookDetailContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_WebhookDetailCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};

/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about webhook.
 *
 * @param[in,out] cls of type `struct LookupPendingWebhookContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_webhook_by_event_cb (void *cls,
                            PGresult *result,
                            unsigned int num_results)
{
  struct LookupWebhookDetailContext *wlc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t webhook_serial;
    char *event_type;
    char *url;
    char *http_method;
    char *header_template;
    char *body_template;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("webhook_serial",
                                    &webhook_serial),
      GNUNET_PQ_result_spec_string ("event_type",
                                    &event_type),
      GNUNET_PQ_result_spec_string ("url",
                                    &url),
      GNUNET_PQ_result_spec_string ("http_method",
                                    &http_method),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("header_template",
                                      &header_template),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("body_template",
                                      &body_template),
        NULL),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      wlc->extract_failed = true;
      return;
    }
    wlc->cb (wlc->cb_cls,
             webhook_serial,
             event_type,
             url,
             http_method,
             header_template,
             body_template);
    GNUNET_PQ_cleanup_result (rs);
  }
}

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_webhook_by_event (void *cls,
                                const char *instance_id,
                                const char *event_type,
                                TALER_MERCHANTDB_WebhookDetailCallback cb,
                                void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupWebhookDetailContext wlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .extract_failed = false,
  };

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (event_type),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_webhook_by_event",
           "SELECT"
           " webhook_serial"
           ",event_type"
           ",url"
           ",http_method"
           ",header_template"
           ",body_template"
           " FROM merchant_webhook"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           "  AND event_type=$2");

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_webhook_by_event",
                                             params,
                                             &lookup_webhook_by_event_cb,
                                             &wlc);

  if (wlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
