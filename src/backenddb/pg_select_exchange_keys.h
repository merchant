/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_exchange_keys.h
 * @brief implementation of the select_exchange_keys function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_EXCHANGE_KEYS_H
#define PG_SELECT_EXCHANGE_KEYS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Retrieve exchange's keys from the database.
 *
 * @param cls plugin closure
 * @param exchange_url base URL of the exchange
 * @param[out] keys set to the keys of the exchange
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_exchange_keys (void *cls,
                             const char *exchange_url,
                             struct TALER_EXCHANGE_Keys **keys);


#endif
