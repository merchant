/*
   This file is part of TALER
   Copyright (C) 2022-2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order_status_by_serial.c
 * @brief Implementation of the lookup_order_status_by_serial function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_order_status_by_serial.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order_status_by_serial (
  void *cls,
  const char *instance_id,
  uint64_t order_serial,
  char **order_id,
  struct TALER_PrivateContractHashP *h_contract_terms,
  bool *paid)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&order_serial),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("h_contract_terms",
                                          h_contract_terms),
    GNUNET_PQ_result_spec_string ("order_id",
                                  order_id),
    GNUNET_PQ_result_spec_bool ("paid",
                                paid),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  *paid = false; /* just to be safe(r) */
  PREPARE (pg,
           "lookup_order_status_by_serial",
           "SELECT"
           " h_contract_terms"
           ",order_id"
           ",paid"
           " FROM merchant_contract_terms"
           " WHERE merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND order_serial=$2");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_order_status_by_serial",
                                                   params,
                                                   rs);
}
