/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_category.c
 * @brief Implementation of the insert_category function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_category.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_category (void *cls,
                        const char *instance_id,
                        const char *category_name,
                        const json_t *category_name_i18n,
                        uint64_t *category_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (category_name),
    TALER_PQ_query_param_json (category_name_i18n),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint64 ("category_serial",
                                  category_id),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_category",
           "INSERT INTO merchant_categories"
           "(merchant_serial"
           ",category_name"
           ",category_name_i18n"
           ")"
           " SELECT merchant_serial,"
           " $2, $3"
           " FROM merchant_instances"
           " WHERE merchant_id=$1"
           " RETURNING category_serial");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "insert_category",
                                                   params,
                                                   rs);
}
