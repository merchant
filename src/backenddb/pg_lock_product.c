/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lock_product.c
 * @brief Implementation of the lock_product function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lock_product.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lock_product (void *cls,
                     const char *instance_id,
                     const char *product_id,
                     const struct GNUNET_Uuid *uuid,
                     uint64_t quantity,
                     struct GNUNET_TIME_Timestamp expiration_time)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (product_id),
    GNUNET_PQ_query_param_auto_from_type (uuid),
    GNUNET_PQ_query_param_uint64 (&quantity),
    GNUNET_PQ_query_param_timestamp (&expiration_time),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lock_product",
           "WITH ps AS"
           "  (SELECT product_serial"
           "   FROM merchant_inventory"
           "   WHERE product_id=$2"
           "     AND merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1))"
           "INSERT INTO merchant_inventory_locks"
           "(product_serial"
           ",lock_uuid"
           ",total_locked"
           ",expiration)"
           " SELECT product_serial, $3, $4, $5"
           "   FROM merchant_inventory"
           "   JOIN ps USING (product_serial)"
           "   WHERE "
           "     total_stock - total_sold - total_lost - $4 >= "
           "     (SELECT COALESCE(SUM(total_locked), 0)"
           "        FROM merchant_inventory_locks"
           "        WHERE product_serial=ps.product_serial) + "
           "     (SELECT COALESCE(SUM(total_locked), 0)"
           "        FROM merchant_order_locks"
           "        WHERE product_serial=ps.product_serial)");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "lock_product",
                                             params);
}
