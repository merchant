/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_wire_fee.h
 * @brief implementation of the lookup_wire_fee function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_WIRE_FEE_H
#define PG_LOOKUP_WIRE_FEE_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Obtain information about wire fees charged by an exchange,
 * including signature (so we have proof).
 *
 * @param cls closure
 * @param master_pub public key of the exchange
 * @param wire_method the wire method
 * @param contract_date date of the contract to use for the lookup
 * @param[out] fees wire fees charged
 * @param[out] start_date start of fee being used
 * @param[out] end_date end of fee being used
 * @param[out] master_sig signature of exchange over fee structure
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_wire_fee (void *cls,
                        const struct TALER_MasterPublicKeyP *master_pub,
                        const char *wire_method,
                        struct GNUNET_TIME_Timestamp contract_date,
                        struct TALER_WireFeeSet *fees,
                        struct GNUNET_TIME_Timestamp *start_date,
                        struct GNUNET_TIME_Timestamp *end_date,
                        struct TALER_MasterSignatureP *master_sig);

#endif
