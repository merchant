/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_order.h
 * @brief implementation of the delete_order function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_DELETE_ORDER_H
#define PG_DELETE_ORDER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Delete information about an order.  Note that the transaction must
 * enforce that the order is not awaiting payment anymore.
 *
 * @param cls closure
 * @param instance_id instance to delete order of
 * @param order_id order to delete
 * @param force delete claimed but unpaid orders as well
 * @return DB status code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS
 *           if pending payment prevents deletion OR order unknown
 */
enum GNUNET_DB_QueryStatus
TMH_PG_delete_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     bool force);

#endif
