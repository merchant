/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_set_transfer_status_to_confirmed.h
 * @brief implementation of the set_transfer_status_to_confirmed function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SET_TRANSFER_STATUS_TO_CONFIRMED_H
#define PG_SET_TRANSFER_STATUS_TO_CONFIRMED_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Set transfer status to confirmed.
 *
 * @param cls closure
 * @param instance_id merchant instance with the update
 * @param exchange_url the exchange that made the transfer
 * @param wtid wire transfer subject
 * @param amount confirmed amount of the wire transfer
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_set_transfer_status_to_confirmed (
  void *cls,
  const char *instance_id,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const struct TALER_Amount *amount);


#endif
