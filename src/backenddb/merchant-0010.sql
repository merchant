--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- @file merchant-0010.sql
-- @brief Remove dead aml_decision column and add new ones
-- @author Christian Grothoff

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0010', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_kyc
  DROP COLUMN aml_decision
 ,DROP COLUMN exchange_sig
 ,DROP COLUMN exchange_pub
 ,ADD COLUMN access_token BYTEA DEFAULT NULL
 ,ADD COLUMN exchange_http_status INT4 DEFAULT(0)
 ,ADD COLUMN exchange_ec_code INT4 DEFAULT(0)
 ,ADD COLUMN aml_review BOOL DEFAULT(FALSE)
 ,ADD COLUMN deposit_thresholds taler_amount_currency[] DEFAULT NULL
 ,ADD COLUMN deposit_timeframes INT8[] DEFAULT NULL
 ,ADD COLUMN deposit_limits_are_soft BOOL[] DEFAULT NULL
 ,ADD CONSTRAINT access_token_length_check CHECK (LENGTH(access_token) = 32);

COMMENT ON COLUMN merchant_kyc.access_token
 IS 'Access token required to begin the KYC process';
COMMENT ON COLUMN merchant_kyc.exchange_http_status
 IS 'Last HTTP status returned by the exchange when inquiring about our KYC status.';
COMMENT ON COLUMN merchant_kyc.exchange_ec_code
 IS 'Last Taler error code returned by the exchange when inquiring about our KYC status.';
COMMENT ON COLUMN merchant_kyc.aml_review
 IS 'True if our account is under AML review according to the exchange.';
COMMENT ON COLUMN merchant_kyc.deposit_thresholds
 IS 'Maximum amount we are allowed to deposit in a given timeframe under current rules.';
COMMENT ON COLUMN merchant_kyc.deposit_timeframes
 IS 'Timeframe for which the deposit_threshold applies.';
COMMENT ON COLUMN merchant_kyc.deposit_limits_are_soft
 IS 'True if this is a soft limit';


-- Complete transaction
COMMIT;
