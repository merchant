/*
   This file is part of TALER
   Copyright (C) 2022, 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_order.c
 * @brief Implementation of the insert_order function for Postgres
 * @author Iván Ávalos
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_order.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     const char *session_id,
                     const struct TALER_MerchantPostDataHashP *h_post_data,
                     struct GNUNET_TIME_Timestamp pay_deadline,
                     const struct TALER_ClaimTokenP *claim_token,
                     const json_t *contract_terms,
                     const char *pos_key,
                     enum TALER_MerchantConfirmationAlgorithm pos_algorithm)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Timestamp now;
  uint32_t pos32 = (uint32_t) pos_algorithm;
  const char *fulfillment_url
    = json_string_value (json_object_get (contract_terms,
                                          "fulfillment_url"));
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_timestamp (&pay_deadline),
    GNUNET_PQ_query_param_auto_from_type (claim_token),
    GNUNET_PQ_query_param_auto_from_type (h_post_data),
    GNUNET_PQ_query_param_timestamp (&now),
    TALER_PQ_query_param_json (contract_terms),
    (NULL == pos_key)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (pos_key),
    GNUNET_PQ_query_param_uint32 (&pos32),
    (NULL == session_id)
    ? GNUNET_PQ_query_param_string ("")
    : GNUNET_PQ_query_param_string (session_id),
    (NULL == fulfillment_url)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (fulfillment_url),
    GNUNET_PQ_query_param_end
  };

  now = GNUNET_TIME_timestamp_get ();
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "inserting order: order_id: %s, instance_id: %s.\n",
              order_id,
              instance_id);
  check_connection (pg);
  PREPARE (pg,
           "insert_order",
           "INSERT INTO merchant_orders"
           "(merchant_serial"
           ",order_id"
           ",pay_deadline"
           ",claim_token"
           ",h_post_data"
           ",creation_time"
           ",contract_terms"
           ",pos_key"
           ",pos_algorithm"
           ",session_id"
           ",fulfillment_url)"
           " SELECT merchant_serial,"
           " $2, $3, $4, $5, $6, $7, $8, $9, $10, $11"
           " FROM merchant_instances"
           " WHERE merchant_id=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_order",
                                             params);
}
