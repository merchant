/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_webhook.c
 * @brief Implementation of the update_webhook function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_webhook.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_update_webhook (void *cls,
                       const char *instance_id,
                       const char *webhook_id,
                       const struct TALER_MERCHANTDB_WebhookDetails *wb)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (webhook_id),
    GNUNET_PQ_query_param_string (wb->event_type),
    GNUNET_PQ_query_param_string (wb->url),
    GNUNET_PQ_query_param_string (wb->http_method),
    (NULL == wb->header_template)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (wb->header_template),
    (NULL == wb->body_template)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (wb->body_template),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_webhook",
           "UPDATE merchant_webhook SET"
           " event_type=$3"
           ",url=$4"
           ",http_method=$5"
           ",header_template=$6"
           ",body_template=$7"
           " WHERE merchant_serial="
           "   (SELECT merchant_serial"
           "      FROM merchant_instances"
           "      WHERE merchant_id=$1)"
           "   AND webhook_id=$2");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_webhook",
                                             params);
}
