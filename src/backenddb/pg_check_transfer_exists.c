/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_check_transfer_exists.c
 * @brief Implementation of the check_transfer_exists function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_check_transfer_exists.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_check_transfer_exists (void *cls,
                              const char *instance_id,
                              uint64_t transfer_serial_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&transfer_serial_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "check_transfer_exists",
           "SELECT"
           " 1"
           " FROM merchant_transfers"
           " JOIN merchant_accounts"
           "  USING (account_serial)"
           " WHERE"
           "   credit_serial=$2"
           "  AND"
           "   merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "       WHERE merchant_id=$1)");

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "check_transfer_exists",
                                                   params,
                                                   rs);
}
