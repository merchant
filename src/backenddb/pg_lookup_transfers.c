/*
   This file is part of TALER
   Copyright (C) 2022-2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfers.c
 * @brief Implementation of the lookup_transfers function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_transfers.h"
#include "pg_helper.h"


/**
 * Closure for #lookup_transfers_cb().
 */
struct LookupTransfersContext
{
  /**
   * Function to call on results.
   */
  TALER_MERCHANTDB_TransferCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Postgres context.
   */
  struct PostgresClosure *pg;

  /**
   * Transaction status (set).
   */
  enum GNUNET_DB_QueryStatus qs;

};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupTransfersContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_transfers_cb (void *cls,
                     PGresult *result,
                     unsigned int num_results)
{
  struct LookupTransfersContext *ltc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    struct TALER_Amount credit_amount;
    struct TALER_WireTransferIdentifierRawP wtid;
    struct TALER_FullPayto payto_uri;
    char *exchange_url;
    uint64_t transfer_serial_id;
    struct GNUNET_TIME_Timestamp execution_time = GNUNET_TIME_UNIT_FOREVER_TS;
    bool verified;
    bool confirmed;
    struct GNUNET_PQ_ResultSpec rs[] = {
      TALER_PQ_result_spec_amount_with_currency ("credit_amount",
                                                 &credit_amount),
      GNUNET_PQ_result_spec_auto_from_type ("wtid",
                                            &wtid),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto_uri.full_payto),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_uint64 ("credit_serial",
                                    &transfer_serial_id),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_timestamp ("execution_time",
                                         &execution_time),
        NULL),
      GNUNET_PQ_result_spec_bool ("verified",
                                  &verified),
      GNUNET_PQ_result_spec_bool ("confirmed",
                                  &confirmed),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ltc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    ltc->cb (ltc->cb_cls,
             &credit_amount,
             &wtid,
             payto_uri,
             exchange_url,
             transfer_serial_id,
             execution_time,
             verified,
             confirmed);
    GNUNET_PQ_cleanup_result (rs);
  }
  ltc->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfers (void *cls,
                         const char *instance_id,
                         struct TALER_FullPayto payto_uri,
                         struct GNUNET_TIME_Timestamp before,
                         struct GNUNET_TIME_Timestamp after,
                         int64_t limit,
                         uint64_t offset,
                         enum TALER_EXCHANGE_YesNoAll verified,
                         TALER_MERCHANTDB_TransferCallback cb,
                         void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  uint64_t plimit = (uint64_t) ((limit < 0) ? -limit : limit);
  bool by_time = ( (! GNUNET_TIME_absolute_is_never (before.abs_time)) ||
                   (! GNUNET_TIME_absolute_is_zero (after.abs_time)) );
  struct LookupTransfersContext ltc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_timestamp (&before),
    GNUNET_PQ_query_param_timestamp (&after),
    GNUNET_PQ_query_param_uint64 (&offset),
    GNUNET_PQ_query_param_uint64 (&plimit),
    NULL == payto_uri.full_payto
    ? GNUNET_PQ_query_param_null () /* NULL: do not filter by payto URI */
    : GNUNET_PQ_query_param_string (payto_uri.full_payto),
    GNUNET_PQ_query_param_bool (! by_time),     /* $7: filter by time? */
    GNUNET_PQ_query_param_bool (TALER_EXCHANGE_YNA_ALL == verified), /* filter by verified? */
    GNUNET_PQ_query_param_bool (TALER_EXCHANGE_YNA_YES == verified),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_transfers_asc",
           "SELECT"
           " mt.credit_amount"
           ",wtid"
           ",mac.payto_uri"
           ",exchange_url"
           ",credit_serial"
           ",mts.execution_time"
           ",verified"
           ",confirmed"
           " FROM merchant_transfers mt"
           "  JOIN merchant_accounts mac"
           "    USING (account_serial)"
           "  LEFT JOIN merchant_transfer_signatures mts"
           "    USING (credit_serial)"
           " WHERE ( $7 OR "
           "         (mts.execution_time IS NOT NULL AND"
           "          mts.execution_time < $2 AND"
           "          mts.execution_time >= $3) )"
           "   AND ( $8 OR "
           "         (verified = $9) )"
           "   AND ( (CAST($6 AS TEXT) IS NULL) OR "
           "         (REGEXP_REPLACE(mac.payto_uri,'\\?.*','')"
           "         =REGEXP_REPLACE($6,'\\?.*','')) )"
           "   AND merchant_serial ="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "       WHERE merchant_id=$1)"
           "   AND (credit_serial > $4)"
           " ORDER BY credit_serial ASC"
           " LIMIT $5");
  PREPARE (pg,
           "lookup_transfers_desc",
           "SELECT"
           " mt.credit_amount"
           ",wtid"
           ",mac.payto_uri"
           ",exchange_url"
           ",credit_serial"
           ",mts.execution_time"
           ",verified"
           ",confirmed"
           " FROM merchant_transfers mt"
           "  JOIN merchant_accounts mac"
           "    USING (account_serial)"
           "  LEFT JOIN merchant_transfer_signatures mts"
           "    USING (credit_serial)"
           " WHERE ( $7 OR "
           "         (mts.execution_time IS NOT NULL AND"
           "          mts.execution_time < $2 AND"
           "          mts.execution_time >= $3) )"
           "   AND ( $8 OR "
           "         (verified = $9) )"
           "   AND ( (CAST($6 AS TEXT) IS NULL) OR "
           "         (REGEXP_REPLACE(mac.payto_uri,'\\?.*','')"
           "         =REGEXP_REPLACE($6,'\\?.*','')) )"
           "   AND merchant_serial ="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "       WHERE merchant_id=$1)"
           "   AND (credit_serial < $4)"
           " ORDER BY credit_serial DESC"
           " LIMIT $5");
  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    (limit > 0)
    ? "lookup_transfers_asc"
    : "lookup_transfers_desc",
    params,
    &lookup_transfers_cb,
    &ltc);
  if (0 >= qs)
    return qs;
  return ltc.qs;
}
