/*
   This file is part of TALER
   Copyright (C) 2023, 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_deposits_by_order.c
 * @brief Implementation of the lookup_deposits_by_order function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_deposits_by_order.h"
#include "pg_helper.h"

/**
 * Closure for lookup_deposits_by_order_cb().
 */
struct LookupDepositsByOrderContext
{

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Function to call with all results.
   */
  TALER_MERCHANTDB_DepositedCoinsCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Set to the query result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupDepositsByOrderContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_deposits_by_order_cb (void *cls,
                             PGresult *result,
                             unsigned int num_results)
{
  struct LookupDepositsByOrderContext *ldoc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    uint64_t deposit_serial;
    char *exchange_url;
    struct TALER_MerchantWireHashP h_wire;
    struct TALER_CoinSpendPublicKeyP coin_pub;
    struct GNUNET_TIME_Timestamp deposit_timestamp;
    struct TALER_Amount amount_with_fee;
    struct TALER_Amount deposit_fee;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("deposit_serial",
                                    &deposit_serial),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_timestamp ("deposit_timestamp",
                                       &deposit_timestamp),
      GNUNET_PQ_result_spec_auto_from_type ("h_wire",
                                            &h_wire),
      TALER_PQ_result_spec_amount_with_currency ("amount_with_fee",
                                                 &amount_with_fee),
      TALER_PQ_result_spec_amount_with_currency ("deposit_fee",
                                                 &deposit_fee),
      GNUNET_PQ_result_spec_auto_from_type ("coin_pub",
                                            &coin_pub),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ldoc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    ldoc->cb (ldoc->cb_cls,
              deposit_serial,
              exchange_url,
              &h_wire,
              deposit_timestamp,
              &amount_with_fee,
              &deposit_fee,
              &coin_pub);
    GNUNET_PQ_cleanup_result (rs); /* technically useless here */
  }
  ldoc->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_deposits_by_order (void *cls,
                                 uint64_t order_serial,
                                 TALER_MERCHANTDB_DepositedCoinsCallback cb,
                                 void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupDepositsByOrderContext ldoc = {
    .pg = pg,
    .cb = cb,
    .cb_cls = cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&order_serial),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_deposits_by_order",
           "SELECT"
           " dep.deposit_serial"
           ",mcon.exchange_url"
           ",acc.h_wire"
           ",mcon.deposit_timestamp"
           ",dep.amount_with_fee"
           ",dep.deposit_fee"
           ",dep.coin_pub"
           " FROM merchant_deposits dep"
           " JOIN merchant_deposit_confirmations mcon"
           "   USING(deposit_confirmation_serial)"
           " JOIN merchant_accounts acc"
           "   USING (account_serial)"
           " WHERE mcon.order_serial=$1");

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_deposits_by_order",
                                             params,
                                             &lookup_deposits_by_order_cb,
                                             &ldoc);

  if (qs < 0)
    return qs;
  return ldoc.qs;
}
