/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_increase_refund.h
 * @brief implementation of the increase_refund function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INCREASE_REFUND_H
#define PG_INCREASE_REFUND_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Function called when some backoffice staff decides to award or
 * increase the refund on an existing contract.  This function
 * MUST be called from within a transaction scope setup by the
 * caller as it executes mulrewardle SQL statements.
 *
 * @param cls closure
 * @param instance_id instance identifier
 * @param order_id the order to increase the refund for
 * @param refund maximum refund to return to the customer for this contract
 * @param olc function to call to obtain legal refund
 *    limits per exchange, NULL for no limits
 * @param olc_cls closure for @a olc
 * @param reason 0-terminated UTF-8 string giving the reason why the customer
 *               got a refund (free form, business-specific)
 * @return transaction status
 *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a refund is ABOVE the amount we
 *        were originally paid and thus the transaction failed;
 *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the request is valid,
 *        regardless of whether it actually increased the refund beyond
 *        what was already refunded (idempotency!)
 */
enum TALER_MERCHANTDB_RefundStatus
TMH_PG_increase_refund (
  void *cls,
  const char *instance_id,
  const char *order_id,
  const struct TALER_Amount *refund,
  TALER_MERCHANTDB_OperationLimitCallback olc,
  void *olc_cls,
  const char *reason);


#endif
