/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_refunds_detailed.c
 * @brief Implementation of the lookup_refunds_detailed function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_refunds_detailed.h"
#include "pg_helper.h"

/**
 * Closure for #lookup_refunds_detailed_cb().
 */
struct LookupRefundsDetailedContext
{
  /**
   * Function to call for each refund.
   */
  TALER_MERCHANTDB_RefundDetailCallback rc;

  /**
   * Closure for @e rc.
   */
  void *rc_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Transaction result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct GetRefundsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_refunds_detailed_cb (void *cls,
                            PGresult *result,
                            unsigned int num_results)
{
  struct LookupRefundsDetailedContext *lrdc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    uint64_t refund_serial;
    struct GNUNET_TIME_Timestamp timestamp;
    struct TALER_CoinSpendPublicKeyP coin_pub;
    uint64_t rtransaction_id;
    struct TALER_Amount refund_amount;
    char *reason;
    char *exchange_url;
    uint8_t pending8;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("refund_serial",
                                    &refund_serial),
      GNUNET_PQ_result_spec_timestamp ("refund_timestamp",
                                       &timestamp),
      GNUNET_PQ_result_spec_auto_from_type ("coin_pub",
                                            &coin_pub),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_uint64 ("rtransaction_id",
                                    &rtransaction_id),
      GNUNET_PQ_result_spec_string ("reason",
                                    &reason),
      TALER_PQ_result_spec_amount_with_currency ("refund_amount",
                                                 &refund_amount),
      GNUNET_PQ_result_spec_auto_from_type ("pending",
                                            &pending8),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      lrdc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    lrdc->rc (lrdc->rc_cls,
              refund_serial,
              timestamp,
              &coin_pub,
              exchange_url,
              rtransaction_id,
              reason,
              &refund_amount,
              0 != pending8);
    GNUNET_PQ_cleanup_result (rs);
  }
  lrdc->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_refunds_detailed (void *cls,
                                const char *instance_id,
                                const struct TALER_PrivateContractHashP *h_contract_terms,
                                TALER_MERCHANTDB_RefundDetailCallback rc,
                                void *rc_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_end
  };
  struct LookupRefundsDetailedContext lrdc = {
    .rc  = rc,
    .rc_cls = rc_cls,
    .pg = pg
  };
  enum GNUNET_DB_QueryStatus qs;

  /* no preflight check here, run in transaction by caller! */
  TALER_LOG_DEBUG ("Looking for refund %s + %s\n",
                   GNUNET_h2s (&h_contract_terms->hash),
                   instance_id);

  check_connection (pg);
  PREPARE (pg,
           "lookup_refunds_detailed",
           "SELECT"
           " ref.refund_serial"
           ",ref.refund_timestamp"
           ",dep.coin_pub"
           ",mcon.exchange_url"
           ",ref.rtransaction_id"
           ",ref.reason"
           ",ref.refund_amount"
           ",merchant_refund_proofs.exchange_sig IS NULL AS pending"
           " FROM merchant_deposit_confirmations mcon"
           " JOIN merchant_deposits dep"
           "   USING (deposit_confirmation_serial)"
           " JOIN merchant_refunds ref"
           "   USING (order_serial, coin_pub)"
           " LEFT JOIN merchant_refund_proofs"
           "   USING (refund_serial)"
           " WHERE mcon.order_serial="
           "  (SELECT order_serial"
           "     FROM merchant_contract_terms"
           "    WHERE h_contract_terms=$2"
           "      AND merchant_serial="
           "        (SELECT merchant_serial"
           "           FROM merchant_instances"
           "          WHERE merchant_id=$1))");

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_refunds_detailed",
                                             params,
                                             &lookup_refunds_detailed_cb,
                                             &lrdc);
  if (0 >= qs)
    return qs;
  return lrdc.qs;
}
