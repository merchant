/*
   This file is part of TALER
   Copyright (C) 2023, 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_token_family.c
 * @brief Implementation of the insert_token_family function for Postgres
 * @author Christian Blättler
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_token_family.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_token_family (
  void *cls,
  const char *instance_id,
  const char *token_family_slug,
  const struct TALER_MERCHANTDB_TokenFamilyDetails *details)
{
  struct PostgresClosure *pg = cls;
  const char *kind;

  switch (details->kind)
  {
  case TALER_MERCHANTDB_TFK_Discount:
    kind = "discount";
    break;
  case TALER_MERCHANTDB_TFK_Subscription:
    kind = "subscription";
    break;
  default:
    GNUNET_break (0);
    return GNUNET_DB_STATUS_HARD_ERROR;
  }
  check_connection (pg);
  PREPARE (pg,
           "insert_token_family",
           "INSERT INTO merchant_token_families"
           "(merchant_serial"
           ",slug"
           ",name"
           ",description"
           ",description_i18n"
           ",extra_data"
           ",valid_after"
           ",valid_before"
           ",duration"
           ",validity_granularity"
           ",start_offset"
           ",kind)"
           " SELECT merchant_serial, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12"
           " FROM merchant_instances"
           " WHERE merchant_id=$1"
           " ON CONFLICT DO NOTHING;");
  {
    struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string (instance_id),
      GNUNET_PQ_query_param_string (token_family_slug),
      GNUNET_PQ_query_param_string (details->name),
      GNUNET_PQ_query_param_string (details->description),
      TALER_PQ_query_param_json (details->description_i18n),
      NULL == details->extra_data
      ? GNUNET_PQ_query_param_null ()
      : TALER_PQ_query_param_json (details->extra_data),
      GNUNET_PQ_query_param_timestamp (&details->valid_after),
      GNUNET_PQ_query_param_timestamp (&details->valid_before),
      GNUNET_PQ_query_param_relative_time (&details->duration),
      GNUNET_PQ_query_param_relative_time (&details->validity_granularity),
      GNUNET_PQ_query_param_relative_time (&details->start_offset),
      GNUNET_PQ_query_param_string (kind),
      GNUNET_PQ_query_param_end
    };

    return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                               "insert_token_family",
                                               params);
  }
}
