/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_transfer.h
 * @brief implementation of the postgres_insert_transfer function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_TRANSFER_H
#define PG_INSERT_TRANSFER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert information about a wire transfer the merchant has received.
 *
 * @param cls closure
 * @param instance_id the instance that received the transfer
 * @param exchange_url which exchange made the transfer
 * @param wtid identifier of the wire transfer
 * @param credit_amount how much did we receive
 * @param payto_uri what is the merchant's bank account that received the transfer
 * @param confirmed whether the transfer was confirmed by the merchant or
 *                  was merely claimed by the exchange at this point
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_transfer (
  void *cls,
  const char *instance_id,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const struct TALER_Amount *credit_amount,
  struct TALER_FullPayto payto_uri,
  bool confirmed);


#endif
