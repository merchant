/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_get_kyc_limits.h
 * @brief implementation of the get_kyc_limits function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_GET_KYC_LIMITS_H
#define PG_GET_KYC_LIMITS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Check an account's KYC limits at an exchange.
 *
 * @param cls closure
 * @param merchant_account_uri  merchant account payto URI
 * @param instance_id the instance for which to check
 * @param exchange_url base URL of the exchange
 * @param[out] kyc_ok true if no urgent KYC work must be done for this account
 * @param[out] no_access_token true if we do not have a valid KYC access token (KYC auth missing)
 * @param[out] jlimits set to JSON array with AccountLimits, NULL if unknown (and likely defaults apply or KYC auth is urgently needed, see @a auth_ok)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_get_kyc_limits (
  void *cls,
  struct TALER_FullPayto merchant_account_uri,
  const char *instance_id,
  const char *exchange_url,
  bool *kyc_ok,
  bool *no_access_token,
  json_t **jlimits);

#endif
