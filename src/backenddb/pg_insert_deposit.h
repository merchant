/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_deposit.h
 * @brief implementation of the insert_deposit function for Postgres
 * @author IvánAvalos
 */
#ifndef PG_INSERT_DEPOSIT_H
#define PG_INSERT_DEPOSIT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert payment confirmation from the exchange into the database.
 *
 * @param cls closure
 * @param offset offset of the coin in the batch
 * @param deposit_confirmation_serial which deposit confirmation is this coin part of
 * @param coin_pub public key of the coin
 * @param coin_sig signature of the coin
 * @param amount_with_fee amount the exchange will deposit for this coin
 * @param deposit_fee fee the exchange will charge for this coin
 * @param refund_fee fee the exchange charges to refund this coin
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_deposit (
  void *cls,
  uint32_t offset,
  uint64_t deposit_confirmation_serial,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  const struct TALER_CoinSpendSignatureP *coin_sig,
  const struct TALER_Amount *amount_with_fee,
  const struct TALER_Amount *deposit_fee,
  const struct TALER_Amount *refund_fee);

#endif
