/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file merchantdb_helper.c
 * @brief Helper functions for the merchant database logic
 * @author Christian Grothoff
 * @author Priscilla Huang
 */
#include "platform.h"
#include <taler/taler_util.h>
#include "taler_merchantdb_lib.h"


void
TALER_MERCHANTDB_product_details_free (
  struct TALER_MERCHANTDB_ProductDetails *pd)
{
  GNUNET_free (pd->description);
  json_decref (pd->description_i18n);
  GNUNET_free (pd->unit);
  json_decref (pd->taxes);
  GNUNET_free (pd->image);
  json_decref (pd->address);
}


void
TALER_MERCHANTDB_template_details_free (
  struct TALER_MERCHANTDB_TemplateDetails *tp)
{
  GNUNET_free (tp->template_description);
  GNUNET_free (tp->otp_id);
  json_decref (tp->editable_defaults);
  json_decref (tp->template_contract);
}


void
TALER_MERCHANTDB_webhook_details_free (
  struct TALER_MERCHANTDB_WebhookDetails *wb)
{
  GNUNET_free (wb->event_type);
  GNUNET_free (wb->url);
  GNUNET_free (wb->http_method);
  GNUNET_free (wb->header_template);
  GNUNET_free (wb->body_template);
}


void
TALER_MERCHANTDB_pending_webhook_details_free (
  struct TALER_MERCHANTDB_PendingWebhookDetails *pwb)
{
  GNUNET_free (pwb->url);
  GNUNET_free (pwb->http_method);
  GNUNET_free (pwb->header);
  GNUNET_free (pwb->body);
}


void
TALER_MERCHANTDB_token_family_details_free (
  struct TALER_MERCHANTDB_TokenFamilyDetails *tf)
{
  GNUNET_free (tf->slug);
  GNUNET_free (tf->name);
  GNUNET_free (tf->description);
  json_decref (tf->description_i18n);
  json_decref (tf->extra_data);
  GNUNET_free (tf->cipher_spec);
}


void
TALER_MERCHANTDB_category_details_free (
  struct TALER_MERCHANTDB_CategoryDetails *cd)
{
  GNUNET_free (cd->category_name);
  json_decref (cd->category_name_i18n);
}


/* end of merchantdb_helper.c */
