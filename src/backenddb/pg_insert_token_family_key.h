/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_token_family_key.h
 * @brief implementation of the insert_token_family_key function for Postgres
 * @author Christian Blättler
 */
#ifndef PG_INSERT_TOKEN_FAMILY_KEY_H
#define PG_INSERT_TOKEN_FAMILY_KEY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Insert new key pair for a token family.
 *
 * @param cls closure
 * @param merchant_id instance name
 * @param token_family_slug slug of the token family to insert the key for
 * @param pub public key to insert
 * @param priv private key to insert
 * @param key_expires when does the private key expire (because
 *     the validity period of the next token family key starts)
 * @param valid_after start of validity period for signatures with this key
 * @param valid_before end of validity period for signatures with this key
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_token_family_key (
  void *cls,
  const char *merchant_id,
  const char *token_family_slug,
  const struct TALER_TokenIssuePublicKey *pub,
  const struct TALER_TokenIssuePrivateKey *priv,
  struct GNUNET_TIME_Timestamp key_expires,
  struct GNUNET_TIME_Timestamp valid_after,
  struct GNUNET_TIME_Timestamp valid_before);

#endif
