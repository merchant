/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_product.h
 * @brief implementation of the update_product function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_PRODUCT_H
#define PG_UPDATE_PRODUCT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Update details about a particular product. Note that the
 * transaction must enforce that the sold/stocked/lost counters
 * are not reduced.
 *
 * @param cls closure
 * @param instance_id instance to lookup products for
 * @param product_id product to lookup
 * @param num_cats length of @a cats array
 * @param cats number of categories the product is in
 * @param[out] no_instance the update failed as the instance is unknown
 * @param[out] no_cat set to -1 on success, otherwise the update failed and this is set
 *                to the index of a category in @a cats that is unknown
 * @param[out] no_product the @a product_id is unknown
 * @param[out] lost_reduced the update failed as the counter of units lost would have been lowered
 * @param[out] sold_reduced the update failed as the counter of units sold would have been lowered
 * @param[out] stocked_reduced the update failed as the counter of units stocked would have been lowered
 * @param[out] pd set to the product details on success, can be NULL
 *             (in that case we only want to check if the product exists)
 *             total_sold in @a pd is ignored, total_lost must not
 *             exceed total_stock minus the existing total_sold;
 *             total_sold and total_stock must be larger or equal to
 *             the existing value;
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_product (void *cls,
                       const char *instance_id,
                       const char *product_id,
                       const struct TALER_MERCHANTDB_ProductDetails *pd,
                       size_t num_cats,
                       const uint64_t *cats,
                       bool *no_instance,
                       ssize_t *no_cat,
                       bool *no_product,
                       bool *lost_reduced,
                       bool *sold_reduced,
                       bool *stocked_reduced);

#endif
