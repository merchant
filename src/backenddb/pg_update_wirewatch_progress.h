/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_wirewatch_progress.h
 * @brief implementation of the update_wirewatch_progress function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_WIREWATCH_PROGRESS_H
#define PG_UPDATE_WIREWATCH_PROGRESS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Update information about progress made by taler-merchant-wirewatch.
 *
 * @param cls closure
 * @param instance name of the instance to record progress for
 * @param payto_uri bank account URI to record progress for
 * @param last_serial latest serial of a transaction that was processed
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_wirewatch_progress (
  void *cls,
  const char *instance,
  struct TALER_FullPayto payto_uri,
  uint64_t last_serial);


#endif
