/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_otp.c
 * @brief Implementation of the delete_otp function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_otp.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_delete_otp (void *cls,
                   const char *instance_id,
                   const char *otp_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (otp_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "delete_otp",
           "DELETE"
           " FROM merchant_otp_devices"
           " WHERE merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND otp_id=$2");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_otp",
                                             params);
}
