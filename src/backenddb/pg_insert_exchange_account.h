/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_exchange_account.h
 * @brief implementation of the insert_exchange_account function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_EXCHANGE_ACCOUNT_H
#define PG_INSERT_EXCHANGE_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert information about a wire account of an exchange.
 *
 * @param cls closure
 * @param master_pub public key of the exchange
 * @param payto_uri URI of the bank account
 * @param conversion_url conversion service, NULL if there is no conversion required
 * @param debit_restrictions JSON array of debit restrictions on the account
 * @param credit_restrictions JSON array of debit restrictions on the account
 * @param master_sig signature affirming the account of the exchange
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_exchange_account (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub,
  const struct TALER_FullPayto payto_uri,
  const char *conversion_url,
  const json_t *debit_restrictions,
  const json_t *credit_restrictions,
  const struct TALER_MasterSignatureP *master_sig);


#endif
