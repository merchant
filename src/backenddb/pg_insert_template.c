/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_template.c
 * @brief Implementation of the insert_template function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_template.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_template (void *cls,
                        const char *instance_id,
                        const char *template_id,
                        uint64_t otp_serial_id,
                        const struct TALER_MERCHANTDB_TemplateDetails *td)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (template_id),
    GNUNET_PQ_query_param_string (td->template_description),
    (0 == otp_serial_id)
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_uint64 (&otp_serial_id),
    TALER_PQ_query_param_json (td->template_contract),
    (NULL == td->editable_defaults)
    ? GNUNET_PQ_query_param_null ()
    : TALER_PQ_query_param_json (td->editable_defaults),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_template",
           "INSERT INTO merchant_template"
           "(merchant_serial"
           ",template_id"
           ",template_description"
           ",otp_device_id"
           ",template_contract"
           ",editable_defaults"
           ")"
           " SELECT merchant_serial,"
           " $2, $3, $4, $5, $6"
           " FROM merchant_instances"
           " WHERE merchant_id=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_template",
                                             params);
}
