/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_account_kyc_set_status.h
 * @brief implementation of the account_kyc_set_status function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_ACCOUNT_KYC_SET_STATUS_H
#define PG_ACCOUNT_KYC_SET_STATUS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Update an instance's account's KYC status.
 *
 * @param cls closure
 * @param merchant_id merchant backend instance ID
 * @param h_wire hash of the wire account to check
 * @param exchange_url base URL of the exchange to check
 * @param timestamp timestamp to store
 * @param exchange_http_status HTTP status code returned last by the exchange
 * @param exchange_ec_code Taler error code returned last by the exchange
 * @param access_token access token for the KYC process, NULL for none
 * @param jlimits JSON array with AccountLimits returned by the exchange
 * @param in_aml_review true if the exchange says the account is under review
 * @param kyc_ok current KYC status (true for satisfied)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_account_kyc_set_status (
  void *cls,
  const char *merchant_id,
  const struct TALER_MerchantWireHashP *h_wire,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp timestamp,
  unsigned int exchange_http_status,
  enum TALER_ErrorCode exchange_ec_code,
  const struct TALER_AccountAccessTokenP *access_token,
  const json_t *jlimits,
  bool in_aml_review,
  bool kyc_ok);

#endif
