/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_product.h
 * @brief implementation of the insert_product function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_PRODUCT_H
#define PG_INSERT_PRODUCT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert details about a particular product.
 *
 * @param cls closure
 * @param instance_id instance to insert product for
 * @param product_id product identifier of product to insert
 * @param pd the product details to insert
 * @param num_cats length of @a cats array
 * @param cats array of categories the product is in
 * @param[out] no_instance set to true if @a instance_id is unknown
 * @param[out] conflict set to true if a conflicting
 *        product already exists in the database
 * @param[out] no_cat set to index of non-existing category from @a cats, or -1 if all @a cats were found
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_product (void *cls,
                       const char *instance_id,
                       const char *product_id,
                       const struct TALER_MERCHANTDB_ProductDetails *pd,
                       size_t num_cats,
                       const uint64_t *cats,
                       bool *no_instance,
                       bool *conflict,
                       ssize_t *no_cat);


#endif
