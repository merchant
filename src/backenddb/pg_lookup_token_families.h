/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_token_families.h
 * @brief implementation of the lookup_token_families function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_LOOKUP_TOKEN_FAMILIES_H
#define PG_LOOKUP_TOKEN_FAMILIES_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup all of the token families the given instance has configured.
 *
 * @param cls closure
 * @param instance_id instance to lookup token families for
 * @param cb function to call on all token families found
 * @param cb_cls closure for @a cb
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_token_families (void *cls,
                              const char *instance_id,
                              TALER_MERCHANTDB_TokenFamiliesCallback cb,
                              void *cb_cls);

#endif
