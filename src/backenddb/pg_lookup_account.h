/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_account.h
 * @brief implementation of the lookup_account function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_ACCOUNT_H
#define PG_LOOKUP_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup account serial by payto URI.
 *
 * @param cls closure
 * @param instance_id instance to lookup the account from
 * @param payto_uri what is the merchant's bank account to lookup
 * @param[out] account_serial serial number of the account
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_account (void *cls,
                       const char *instance_id,
                       struct TALER_FullPayto payto_uri,
                       uint64_t *account_serial);

#endif
