/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_deposit_confirmation_status.c
 * @brief Implementation of the update_deposit_confirmation_status function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_deposit_confirmation_status.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_deposit_confirmation_status (
  void *cls,
  uint64_t deposit_serial,
  bool wire_pending,
  struct GNUNET_TIME_Timestamp future_retry,
  struct GNUNET_TIME_Relative retry_backoff,
  const char *emsg)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&deposit_serial),
    GNUNET_PQ_query_param_timestamp (&future_retry),
    NULL == emsg
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (emsg),
    GNUNET_PQ_query_param_relative_time (&retry_backoff),
    GNUNET_PQ_query_param_bool (wire_pending),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_deposit_confirmation_status",
           "UPDATE merchant_deposit_confirmations SET"
           " wire_transfer_deadline=$2"
           ",exchange_failure=$3"
           ",retry_backoff=$4"
           ",wire_pending=$5 AND wire_pending"
           " WHERE deposit_confirmation_serial="
           "   (SELECT deposit_confirmation_serial"
           "      FROM merchant_deposits"
           "      WHERE deposit_serial=$1);");
  return GNUNET_PQ_eval_prepared_non_select (
    pg->conn,
    "update_deposit_confirmation_status",
    params);
}
