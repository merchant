/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_otp.h
 * @brief implementation of the select_otp function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_OTP_H
#define PG_SELECT_OTP_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Lookup details about an OTP device.
 *
 * @param cls closure
 * @param instance_id instance to lookup template for
 * @param otp_id OTP device to lookup
 * @param[out] td set to the OTP device details on success, can be NULL
 *             (in that case we only want to check if the template exists)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_otp (void *cls,
                   const char *instance_id,
                   const char *otp_id,
                   struct TALER_MERCHANTDB_OtpDeviceDetails *td);

#endif
