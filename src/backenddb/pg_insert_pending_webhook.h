/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_pending_webhook.h
 * @brief implementation of the insert_pending_webhook function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_PENDING_WEBHOOK_H
#define PG_INSERT_PENDING_WEBHOOK_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert webhook in the pending webhook.
 *
 * @param cls closure
 * @param instance_id instance to insert webhook for
 * @param webhook_serial webhook to insert in the pending webhook
 * @param url to make the request to
 * @param http_method for the webhook
 * @param header of the webhook
 * @param body of the webhook
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_pending_webhook (void *cls,
                               const char *instance_id,
                               uint64_t webhook_serial,
                               const char *url,
                               const char *http_method,
                               const char *header,
                               const char *body);

#endif
