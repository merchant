/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_product.h
 * @brief implementation of the lookup_product function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_PRODUCT_H
#define PG_LOOKUP_PRODUCT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup details about a particular product.
 *
 * @param cls closure
 * @param instance_id instance to lookup products for
 * @param product_id product to lookup
 * @param[out] pd set to the product details on success, can be NULL
 *             (in that case we only want to check if the product exists)
 * @param[out] num_categories set to length of @a categories array
 * @param[out] categories set to array of categories the
 *   product is in, caller must free() it.
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_product (void *cls,
                       const char *instance_id,
                       const char *product_id,
                       struct TALER_MERCHANTDB_ProductDetails *pd,
                       size_t *num_categories,
                       uint64_t **categories);

#endif
