/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_wirewatch_accounts.h
 * @brief implementation of the select_wirewatch_accounts function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_SELECT_WIREWATCH_ACCOUNTS_H
#define PG_SELECT_WIREWATCH_ACCOUNTS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Select information about progress made by taler-merchant-wirewatch.
 *
 * @param cls closure
 * @param cb function to call with results
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_select_wirewatch_accounts (
  void *cls,
  TALER_MERCHANTDB_WirewatchWorkCallback cb,
  void *cb_cls);


#endif
