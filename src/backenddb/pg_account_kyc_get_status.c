/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_account_kyc_get_status.c
 * @brief Implementation of the account_kyc_get_status function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_account_kyc_get_status.h"
#include "pg_helper.h"

/**
 * Closure for kyc_status_cb().
 */
struct KycStatusContext
{
  /**
   * Function to call with results.
   */
  TALER_MERCHANTDB_KycCallback kyc_cb;

  /**
   * Closure for @e kyc_cb.
   */
  void *kyc_cb_cls;

  /**
   * Number of results found.
   */
  unsigned int count;

  /**
   * Set to true on failure(s).
   */
  bool failure;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about accounts.
 *
 * @param[in,out] cls of type `struct KycStatusContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
kyc_status_cb (void *cls,
               PGresult *result,
               unsigned int num_results)
{
  struct KycStatusContext *ksc = cls;

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Got %u KYC records\n",
              num_results);
  for (unsigned int i = 0; i < num_results; i++)
  {
    struct TALER_MerchantWireHashP h_wire;
    char *exchange_url;
    struct TALER_FullPayto payto_uri;
    struct GNUNET_TIME_Timestamp last_check;
    bool kyc_ok;
    struct TALER_AccountAccessTokenP access_token;
    bool no_auth;
    uint32_t h32;
    uint32_t e32;
    bool in_aml_review;
    json_t *jlimits = NULL;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_auto_from_type ("h_wire",
                                            &h_wire),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto_uri.full_payto),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_timestamp ("kyc_timestamp",
                                       &last_check),
      GNUNET_PQ_result_spec_bool ("kyc_ok",
                                  &kyc_ok),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_auto_from_type ("access_token",
                                              &access_token),
        &no_auth),
      GNUNET_PQ_result_spec_uint32 ("exchange_http_status",
                                    &h32),
      GNUNET_PQ_result_spec_uint32 ("exchange_ec_code",
                                    &e32),
      GNUNET_PQ_result_spec_bool ("aml_review",
                                  &in_aml_review),
      GNUNET_PQ_result_spec_allow_null (
        TALER_PQ_result_spec_json ("jaccount_limits",
                                   &jlimits),
        NULL),
      GNUNET_PQ_result_spec_end
    };
    unsigned int last_http_status;
    enum TALER_ErrorCode last_ec;

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ksc->failure = true;
      return;
    }
    last_http_status = (unsigned int) h32;
    last_ec = (enum TALER_ErrorCode) (int) e32;
    ksc->count++;
    ksc->kyc_cb (ksc->kyc_cb_cls,
                 &h_wire,
                 payto_uri,
                 exchange_url,
                 last_check,
                 kyc_ok,
                 (no_auth)
                 ? NULL
                 : &access_token,
                 last_http_status,
                 last_ec,
                 in_aml_review,
                 jlimits);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_account_kyc_get_status (
  void *cls,
  const char *merchant_id,
  const struct TALER_MerchantWireHashP *h_wire,
  const char *exchange_url,
  TALER_MERCHANTDB_KycCallback kyc_cb,
  void *kyc_cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct KycStatusContext ksc = {
    .kyc_cb = kyc_cb,
    .kyc_cb_cls = kyc_cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (merchant_id),
    NULL == exchange_url
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (exchange_url),
    NULL == h_wire
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_auto_from_type (h_wire),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_kyc_status",
           "SELECT"
           " ma.h_wire"
           ",ma.payto_uri"
           ",mk.exchange_url"
           ",mk.kyc_timestamp"
           ",mk.kyc_ok"
           ",mk.access_token"
           ",mk.exchange_http_status"
           ",mk.exchange_ec_code"
           ",mk.aml_review"
           ",mk.jaccount_limits"
           " FROM merchant_instances mi"
           " JOIN merchant_accounts ma"
           "   USING (merchant_serial)"
           " JOIN merchant_kyc mk"
           "   USING (account_serial)"
           " WHERE (mi.merchant_id=$1)"
           "   AND ma.active"
           "   AND ( ($2::TEXT IS NULL)"
           "      OR (mk.exchange_url=$2) )"
           "   AND ( ($3::BYTEA IS NULL)"
           "      OR (ma.h_wire=$3) );");
  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "lookup_kyc_status",
    params,
    &kyc_status_cb,
    &ksc);
  if (ksc.failure)
  {
    GNUNET_break (0);
    return GNUNET_DB_STATUS_HARD_ERROR;
  }
  if (0 > qs)
    return qs;
  return ksc.count;
}
