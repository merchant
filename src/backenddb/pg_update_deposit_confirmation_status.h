/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_deposit_confirmation_status.h
 * @brief implementation of the update_deposit_confirmation_status function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_DEPOSIT_CONFIRMATION_STATUS_H
#define PG_UPDATE_DEPOSIT_CONFIRMATION_STATUS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Update the deposit confirmation status associated with
 * the given @a deposit_serial.
 *
 * @param cls closure
 * @param deposit_serial deposit to update status for
 * @param wire_pending did the exchange say that the wire is still pending?
 * @param future_retry when should we ask the exchange again
 * @param retry_backoff current value for the retry backoff
 * @param emsg error message to record
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_deposit_confirmation_status (
  void *cls,
  uint64_t deposit_serial,
  bool wire_pending,
  struct GNUNET_TIME_Timestamp future_retry,
  struct GNUNET_TIME_Relative retry_backoff,
  const char *emsg);


#endif
