/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfers.h
 * @brief implementation of the lookup_transfers function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_LOOKUP_TRANSFERS_H
#define PG_LOOKUP_TRANSFERS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup transfers. Note that filtering by @a verified status is done
 * outside of SQL, as we already have 8 prepared statements and adding
 * a filter on verified would further double the number of statements for
 * a likely rather ineffective filter. So we apply that filter in
 * #lookup_transfers_cb().
 *
 * @param cls closure
 * @param instance_id instance to lookup payments for
 * @param payto_uri account that we are interested in transfers to
 * @param before timestamp for the earliest transfer we care about
 * @param after timestamp for the last transfer we care about
 * @param limit number of entries to return, negative for descending in execution time,
 *                positive for ascending in execution time
 * @param offset transfer_serial number of the transfer we want to offset from
 * @param verified filter transfers by verification status
 * @param cb function to call with detailed transfer data
 * @param cb_cls closure for @a cb
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfers (void *cls,
                         const char *instance_id,
                         struct TALER_FullPayto payto_uri,
                         struct GNUNET_TIME_Timestamp before,
                         struct GNUNET_TIME_Timestamp after,
                         int64_t limit,
                         uint64_t offset,
                         enum TALER_EXCHANGE_YesNoAll verified,
                         TALER_MERCHANTDB_TransferCallback cb,
                         void *cb_cls);

#endif
