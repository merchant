/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_otp.c
 * @brief Implementation of the select_otp function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_otp.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_select_otp (void *cls,
                   const char *instance_id,
                   const char *otp_id,
                   struct TALER_MERCHANTDB_OtpDeviceDetails *td)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (otp_id),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "select_otp",
           "SELECT"
           " otp_description"
           ",otp_ctr"
           ",otp_key"
           ",otp_algorithm"
           " FROM merchant_otp_devices"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           "   AND merchant_otp_devices.otp_id=$2");
  if (NULL == td)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "select_otp",
                                                     params,
                                                     rs_null);
  }
  else
  {
    uint32_t pos32;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("otp_description",
                                    &td->otp_description),
      GNUNET_PQ_result_spec_uint64 ("otp_ctr",
                                    &td->otp_ctr),
      GNUNET_PQ_result_spec_string ("otp_key",
                                    &td->otp_key),
      GNUNET_PQ_result_spec_uint32 ("otp_algorithm",
                                    &pos32),
      GNUNET_PQ_result_spec_end
    };
    enum GNUNET_DB_QueryStatus qs;

    check_connection (pg);
    qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "select_otp",
                                                   params,
                                                   rs);
    td->otp_algorithm = (enum TALER_MerchantConfirmationAlgorithm) pos32;
    return qs;
  }
}

