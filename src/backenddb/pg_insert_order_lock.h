/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_order_lock.h
 * @brief implementation of the insert_order_lock function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_ORDER_LOCK_H
#define PG_INSERT_ORDER_LOCK_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lock inventory stock to a particular order.
 *
 * @param cls closure
 * @param instance_id identifies the instance responsible for the order
 * @param order_id alphanumeric string that uniquely identifies the order
 * @param product_id uniquely identifies the product to be locked
 * @param quantity how many units should be locked to the @a order_id
 * @return transaction status,
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS means there are insufficient stocks
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT indicates success
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_order_lock (void *cls,
                          const char *instance_id,
                          const char *order_id,
                          const char *product_id,
                          uint64_t quantity);

#endif
