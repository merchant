/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_category.h
 * @brief implementation of the update_category function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_UPDATE_CATEGORY_H
#define PG_UPDATE_CATEGORY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * Update descriptions of a product category.
 *
 * @param cls closure
 * @param instance_id instance to update OTP device for
 * @param category_id category to update
 * @param category_name name of the category
 * @param category_name_i18n translations of the category name
 * @return database result code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the template
 *         does not yet exist.
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_category (void *cls,
                        const char *instance_id,
                        uint64_t category_id,
                        const char *category_name,
                        const json_t *category_name_i18n);

#endif
