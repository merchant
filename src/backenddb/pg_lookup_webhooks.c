/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_webhooks.c
 * @brief Implementation of the lookup_webhooks function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_webhooks.h"
#include "pg_helper.h"

/**
 * Context used for postgres_lookup_webhooks().
 */
struct LookupWebhookContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_WebhooksCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about webhook.
 *
 * @param[in,out] cls of type `struct LookupWebhookContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_webhooks_cb (void *cls,
                    PGresult *result,
                    unsigned int num_results)
{
  struct LookupWebhookContext *wlc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    char *webhook_id;
    char *event_type;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("webhook_id",
                                    &webhook_id),
      GNUNET_PQ_result_spec_string ("event_type",
                                    &event_type),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      wlc->extract_failed = true;
      return;
    }
    wlc->cb (wlc->cb_cls,
             webhook_id,
             event_type);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_webhooks (void *cls,
                        const char *instance_id,
                        TALER_MERCHANTDB_WebhooksCallback cb,
                        void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupWebhookContext wlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    /* Can be overwritten by the lookup_webhook_cb */
    .extract_failed = false,
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_webhooks",
           "SELECT"
           " webhook_id"
           ",event_type"
           " FROM merchant_webhook"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1");

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_webhooks",
                                             params,
                                             &lookup_webhooks_cb,
                                             &wlc);
  /* If there was an error inside lookup_webhook_cb, return a hard error. */
  if (wlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
