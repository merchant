/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_order_lock.c
 * @brief Implementation of the insert_order_lock function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_order_lock.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_order_lock (void *cls,
                          const char *instance_id,
                          const char *order_id,
                          const char *product_id,
                          uint64_t quantity)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_string (product_id),
    GNUNET_PQ_query_param_uint64 (&quantity),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_order_lock",
           "WITH tmp AS"
           "  (SELECT "
           "      product_serial"
           "     ,merchant_serial"
           "     ,total_stock"
           "     ,total_sold"
           "     ,total_lost"
           "   FROM merchant_inventory"
           "   WHERE product_id=$3"
           "     AND merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1))"
           " INSERT INTO merchant_order_locks"
           " (product_serial"
           " ,total_locked"
           " ,order_serial)"
           " SELECT tmp.product_serial, $4, order_serial"
           "   FROM merchant_orders"
           "   JOIN tmp USING(merchant_serial)"
           "   WHERE order_id=$2 AND"
           "     tmp.total_stock - tmp.total_sold - tmp.total_lost - $4 >= "
           "     (SELECT COALESCE(SUM(total_locked), 0)"
           "        FROM merchant_inventory_locks"
           "        WHERE product_serial=tmp.product_serial) + "
           "     (SELECT COALESCE(SUM(total_locked), 0)"
           "        FROM merchant_order_locks"
           "        WHERE product_serial=tmp.product_serial)");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_order_lock",
                                             params);
}
