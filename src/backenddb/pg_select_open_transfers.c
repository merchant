/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_open_transfers.c
 * @brief Implementation of the select_open_transfers function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_open_transfers.h"
#include "pg_helper.h"


/**
 * Context used for open_transfers_cb().
 */
struct SelectOpenTransfersContext
{
  /**
   * Postgres context.
   */
  struct PostgresClosure *pg;

  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_OpenTransferCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Internal result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about rewards.
 *
 * @param[in,out] cls of type `struct SelectOpenTransfersContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
open_transfers_cb (void *cls,
                   PGresult *result,
                   unsigned int num_results)
{
  struct SelectOpenTransfersContext *plc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t rowid;
    char *instance_id;
    char *exchange_url;
    struct TALER_FullPayto payto_uri;
    struct TALER_WireTransferIdentifierRawP wtid;
    struct TALER_Amount total;
    struct GNUNET_TIME_Absolute next_attempt;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("credit_serial",
                                    &rowid),
      GNUNET_PQ_result_spec_string ("instance_id",
                                    &instance_id),
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto_uri.full_payto),
      GNUNET_PQ_result_spec_auto_from_type ("wtid",
                                            &wtid),
      TALER_PQ_result_spec_amount_with_currency ("credit_amount",
                                                 &total),
      GNUNET_PQ_result_spec_absolute_time ("next_attempt",
                                           &next_attempt),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      plc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    plc->cb (plc->cb_cls,
             rowid,
             instance_id,
             exchange_url,
             payto_uri,
             &wtid,
             &total,
             next_attempt);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_select_open_transfers (void *cls,
                              uint64_t limit,
                              TALER_MERCHANTDB_OpenTransferCallback cb,
                              void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct SelectOpenTransfersContext plc = {
    .pg = pg,
    .cb = cb,
    .cb_cls = cb_cls
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&limit),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pg,
           "select_open_transfers",
           "SELECT"
           " credit_serial"
           ",merchant_id AS instance_id"
           ",exchange_url"
           ",payto_uri"
           ",wtid"
           ",credit_amount"
           ",ready_time AS next_attempt"
           " FROM merchant_transfers"
           " JOIN merchant_accounts"
           "   USING (account_serial)"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE confirmed AND"
           "       NOT (failed OR verified)"
           " ORDER BY ready_time ASC"
           " LIMIT $1;");

  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "select_open_transfers",
    params,
    &open_transfers_cb,
    &plc);
  if (0 != plc.qs)
    return plc.qs;
  return qs;
}
