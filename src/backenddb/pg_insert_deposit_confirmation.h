/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_deposit_confirmation.h
 * @brief implementation of the insert_deposit_confirmation function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_DEPOSIT_CONFIRMATION_H
#define PG_INSERT_DEPOSIT_CONFIRMATION_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert deposit confirmation from the exchange into the database.
 *
 * @param cls closure
 * @param instance_id instance to lookup deposits for
 * @param deposit_timestamp time when the exchange generated the deposit confirmation
 * @param h_contract_terms proposal data's hashcode
 * @param exchange_url URL of the exchange that issued @a coin_pub
 * @param wire_transfer_deadline when do we expect the wire transfer from the exchange
 * @param total_without_fees deposited total in the batch without fees
 * @param wire_fee wire fee the exchange charges
 * @param h_wire hash of the wire details of the target account of the merchant
 * @param exchange_sig signature from exchange that coin was accepted
 * @param exchange_pub signing key that was used for @a exchange_sig
 * @param[out] deposit_confirmation_serial_id set to the table row
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_deposit_confirmation (
  void *cls,
  const char *instance_id,
  struct GNUNET_TIME_Timestamp deposit_timestamp,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp wire_transfer_deadline,
  const struct TALER_Amount *total_without_fees,
  const struct TALER_Amount *wire_fee,
  const struct TALER_MerchantWireHashP *h_wire,
  const struct TALER_ExchangeSignatureP *exchange_sig,
  const struct TALER_ExchangePublicKeyP *exchange_pub,
  uint64_t *deposit_confirmation_serial_id);

#endif
