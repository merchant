/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_unlock_inventory.c
 * @brief Implementation of the unlock_inventory function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_unlock_inventory.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_unlock_inventory (void *cls,
                         const struct GNUNET_Uuid *uuid)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (uuid),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "unlock_inventory",
           "DELETE"
           " FROM merchant_inventory_locks"
           " WHERE lock_uuid=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "unlock_inventory",
                                             params);
}
