/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_mark_order_wired.h
 * @brief implementation of the mark_order_wired function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_MARK_ORDER_WIRED_H
#define PG_MARK_ORDER_WIRED_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Set 'wired' status for an order to 'true'.
 *
 * @param cls closure
 * @param order_serial serial number of the order
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_mark_order_wired (void *cls,
                         uint64_t order_serial);

#endif
