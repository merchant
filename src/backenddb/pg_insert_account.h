/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_account.h
 * @brief implementation of the insert_account function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_ACCOUNT_H
#define PG_INSERT_ACCOUNT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert information about an instance's account into our database.
 *
 * @param cls closure
 * @param account_details details about the account
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_account (
  void *cls,
  const struct TALER_MERCHANTDB_AccountDetails *account_details);


#endif
