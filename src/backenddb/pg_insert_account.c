/*
   This file is part of TALER
   Copyright (C) 2022, 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_account.c
 * @brief Implementation of the insert_account function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_account.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_account (
  void *cls,
  const struct TALER_MERCHANTDB_AccountDetails *account_details)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (account_details->instance_id),
    GNUNET_PQ_query_param_auto_from_type (&account_details->h_wire),
    GNUNET_PQ_query_param_auto_from_type (&account_details->salt),
    GNUNET_PQ_query_param_string (account_details->payto_uri.full_payto),
    NULL ==account_details->credit_facade_url
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (account_details->credit_facade_url),
    NULL == account_details->credit_facade_credentials
    ? GNUNET_PQ_query_param_null ()
    : TALER_PQ_query_param_json (account_details->credit_facade_credentials),
    GNUNET_PQ_query_param_bool (account_details->active),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_account",
           "INSERT INTO merchant_accounts AS ma"
           "(merchant_serial"
           ",h_wire"
           ",salt"
           ",payto_uri"
           ",credit_facade_url"
           ",credit_facade_credentials"
           ",active)"
           " SELECT merchant_serial, $2, $3, $4, $5, $6, $7"
           " FROM merchant_instances"
           " WHERE merchant_id=$1"
           " ON CONFLICT(merchant_serial,payto_uri)"
           " DO UPDATE SET"
           " active = true"
           ",credit_facade_url = EXCLUDED.credit_facade_url"
           ",credit_facade_credentials = EXCLUDED.credit_facade_credentials"
           " WHERE NOT ma.active");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_account",
                                             params);
}
