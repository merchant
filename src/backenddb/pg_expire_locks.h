/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_expire_locks.h
 * @brief implementation of the expire_locks function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_EXPIRE_LOCKS_H
#define PG_EXPIRE_LOCKS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Release all expired product locks, including
 * those from expired offers -- across all
 * instances.
 *
 * @param cls closure
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_expire_locks (void *cls);

#endif
