/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_wire_fee.c
 * @brief Implementation of the lookup_wire_fee function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_wire_fee.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_wire_fee (void *cls,
                        const struct TALER_MasterPublicKeyP *master_pub,
                        const char *wire_method,
                        struct GNUNET_TIME_Timestamp contract_date,
                        struct TALER_WireFeeSet *fees,
                        struct GNUNET_TIME_Timestamp *start_date,
                        struct GNUNET_TIME_Timestamp *end_date,
                        struct TALER_MasterSignatureP *master_sig)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_HashCode h_wire_method;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (master_pub),
    GNUNET_PQ_query_param_auto_from_type (&h_wire_method),
    GNUNET_PQ_query_param_timestamp (&contract_date),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    TALER_PQ_result_spec_amount_with_currency ("wire_fee",
                                               &fees->wire),
    TALER_PQ_result_spec_amount_with_currency ("closing_fee",
                                               &fees->closing),
    GNUNET_PQ_result_spec_timestamp ("start_date",
                                     start_date),
    GNUNET_PQ_result_spec_timestamp ("end_date",
                                     end_date),
    GNUNET_PQ_result_spec_auto_from_type ("master_sig",
                                          master_sig),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  GNUNET_CRYPTO_hash (wire_method,
                      strlen (wire_method) + 1,
                      &h_wire_method);

  PREPARE (pg,
           "lookup_wire_fee",
           "SELECT"
           " wire_fee"
           ",closing_fee"
           ",start_date"
           ",end_date"
           ",master_sig"
           " FROM merchant_exchange_wire_fees"
           " WHERE master_pub=$1"
           "   AND h_wire_method=$2"
           "   AND start_date <= $3"
           "   AND end_date > $3");

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_wire_fee",
                                                   params,
                                                   rs);
}
