/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order.c
 * @brief Implementation of the lookup_order function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_order.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     struct TALER_ClaimTokenP *claim_token,
                     struct TALER_MerchantPostDataHashP *h_post_data,
                     json_t **contract_terms)
{
  struct PostgresClosure *pg = cls;
  json_t *j;
  struct TALER_ClaimTokenP ct;
  enum GNUNET_DB_QueryStatus qs;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    TALER_PQ_result_spec_json ("contract_terms",
                               &j),
    GNUNET_PQ_result_spec_auto_from_type ("claim_token",
                                          &ct),
    GNUNET_PQ_result_spec_auto_from_type ("h_post_data",
                                          h_post_data),
    GNUNET_PQ_result_spec_end
  };

  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Finding contract term, order_id: '%s', instance_id: '%s'.\n",
              order_id,
              instance_id);
  check_connection (pg);
  PREPARE (pg,
           "lookup_order",
           "SELECT"
           " contract_terms"
           ",claim_token"
           ",h_post_data"
           ",pos_key"
           " FROM merchant_orders"
           " WHERE merchant_orders.merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND merchant_orders.order_id=$2");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "lookup_order",
                                                 params,
                                                 rs);
  if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT == qs)
  {
    if (NULL != contract_terms)
      *contract_terms = j;
    else
      json_decref (j);
    if (NULL != claim_token)
      *claim_token = ct;
  }
  else
  {
    /* just to be safe: NULL it */
    if (NULL != contract_terms)
      *contract_terms = NULL;
    if (NULL != claim_token)
      *claim_token = (struct TALER_ClaimTokenP) { 0 }
    ;
  }
  return qs;
}
