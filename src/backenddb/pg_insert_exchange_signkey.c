/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_exchange_signkey.c
 * @brief Implementation of the insert_exchange_signkey function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_exchange_signkey.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_exchange_signkey (
  void *cls,
  const struct TALER_MasterPublicKeyP *master_pub,
  const struct TALER_ExchangePublicKeyP *exchange_pub,
  struct GNUNET_TIME_Timestamp start_date,
  struct GNUNET_TIME_Timestamp expire_date,
  struct GNUNET_TIME_Timestamp end_date,
  const struct TALER_MasterSignatureP *master_sig)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (master_pub),
    GNUNET_PQ_query_param_auto_from_type (exchange_pub),
    GNUNET_PQ_query_param_timestamp (&start_date),
    GNUNET_PQ_query_param_timestamp (&expire_date),
    GNUNET_PQ_query_param_timestamp (&end_date),
    GNUNET_PQ_query_param_auto_from_type (master_sig),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_exchange_signkey",
           "INSERT INTO merchant_exchange_signing_keys"
           "(master_pub"
           ",exchange_pub"
           ",start_date"
           ",expire_date"
           ",end_date"
           ",master_sig)"
           "VALUES"
           "($1, $2, $3, $4, $5, $6)"
           " ON CONFLICT DO NOTHING;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_exchange_signkey",
                                             params);

}
