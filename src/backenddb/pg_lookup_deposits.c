/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_deposits.c
 * @brief Implementation of the lookup_deposits function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_deposits.h"
#include "pg_helper.h"

/**
 * Closure for #lookup_deposits_cb().
 */
struct LookupDepositsContext
{
  /**
   * Function to call with results.
   */
  TALER_MERCHANTDB_DepositsCallback cb;

  /**
   * Closure for @e cls.
   */
  void *cb_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Transaction status (set).
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param[in,out] cls of type `struct LookupDepositsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_deposits_cb (void *cls,
                    PGresult *result,
                    unsigned int num_results)
{
  struct LookupDepositsContext *ldc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    struct TALER_CoinSpendPublicKeyP coin_pub;
    struct TALER_Amount amount_with_fee;
    struct TALER_Amount deposit_fee;
    struct TALER_Amount refund_fee;
    char *exchange_url;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      GNUNET_PQ_result_spec_auto_from_type ("coin_pub",
                                            &coin_pub),
      TALER_PQ_result_spec_amount_with_currency ("amount_with_fee",
                                                 &amount_with_fee),
      TALER_PQ_result_spec_amount_with_currency ("deposit_fee",
                                                 &deposit_fee),
      TALER_PQ_result_spec_amount_with_currency ("refund_fee",
                                                 &refund_fee),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ldc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    ldc->cb (ldc->cb_cls,
             exchange_url,
             &coin_pub,
             &amount_with_fee,
             &deposit_fee,
             &refund_fee);
    GNUNET_PQ_cleanup_result (rs);
  }
  ldc->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_deposits (
  void *cls,
  const char *instance_id,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  TALER_MERCHANTDB_DepositsCallback cb,
  void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_end
  };
  struct LookupDepositsContext ldc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  enum GNUNET_DB_QueryStatus qs;

  /* no preflight check here, run in its own transaction by the caller! */
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Finding deposits for h_contract_terms '%s'\n",
              GNUNET_h2s (&h_contract_terms->hash));
  check_connection (pg);
  PREPARE (pg,
           "lookup_deposits",
           "SELECT"
           " dcom.exchange_url"
           ",dep.coin_pub"
           ",dep.amount_with_fee"
           ",dep.deposit_fee"
           ",dep.refund_fee"
           " FROM merchant_deposits dep"
           " JOIN merchant_deposit_confirmations dcom"
           "   USING (deposit_confirmation_serial)"
           " WHERE dcom.order_serial="
           "     (SELECT order_serial"
           "        FROM merchant_contract_terms"
           "        WHERE h_contract_terms=$2"
           "          AND merchant_serial="
           "          (SELECT merchant_serial"
           "             FROM merchant_instances"
           "            WHERE merchant_id=$1))");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_deposits",
                                             params,
                                             &lookup_deposits_cb,
                                             &ldc);
  if (qs <= 0)
    return qs;
  return ldc.qs;
}
