/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_category.c
 * @brief Implementation of the select_category function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_category.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_select_category (
  void *cls,
  const char *instance_id,
  uint64_t category_id,
  struct TALER_MERCHANTDB_CategoryDetails *cd,
  size_t *num_products,
  char **products)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&category_id),
    GNUNET_PQ_query_param_end
  };

  if (NULL == cd)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (
      pg->conn,
      "select_category",
      params,
      rs_null);
  }
  else
  {
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("category_name",
                                    &cd->category_name),
      TALER_PQ_result_spec_json ("category_name_i18n",
                                 &cd->category_name_i18n),
      GNUNET_PQ_result_spec_array_string (pg->conn,
                                          "products",
                                          num_products,
                                          products),
      GNUNET_PQ_result_spec_end
    };

    PREPARE (pg,
             "select_category",
             "SELECT"
             " category_name"
             ",category_name_i18n"
             ",t.product_array AS products"
             " FROM merchant_categories mc"
             " JOIN merchant_instances inst"
             "   USING (merchant_serial)"
             ",LATERAL ("
             "   SELECT ARRAY ("
             "     SELECT "
             "         mi.product_id AS product_id"
             "       FROM merchant_product_categories mpc"
             "       JOIN merchant_inventory mi"
             "          USING (product_serial)"
             "      WHERE mpc.category_serial = mc.category_serial"
             "   ) AS product_array"
             " ) t"
             " WHERE inst.merchant_id=$1"
             "   AND mc.category_serial=$2");
    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (
      pg->conn,
      "select_category",
      params,
      rs);
  }
}
