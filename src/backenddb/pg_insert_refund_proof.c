/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_refund_proof.c
 * @brief Implementation of the insert_refund_proof function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_refund_proof.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_refund_proof (void *cls,
                            uint64_t refund_serial,
                            const struct TALER_ExchangeSignatureP *exchange_sig,
                            const struct TALER_ExchangePublicKeyP *exchange_pub)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&refund_serial),
    GNUNET_PQ_query_param_auto_from_type (exchange_sig),
    GNUNET_PQ_query_param_auto_from_type (exchange_pub),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_refund_proof",
           "INSERT INTO merchant_refund_proofs"
           "(refund_serial"
           ",exchange_sig"
           ",signkey_serial)"
           "SELECT $1, $2, signkey_serial"
           " FROM merchant_exchange_signing_keys"
           " WHERE exchange_pub=$3"
           "  ORDER BY start_date DESC"
           "  LIMIT 1");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_refund_proof",
                                             params);
}
