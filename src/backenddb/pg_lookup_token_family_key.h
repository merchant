/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_token_family_key.h
 * @brief implementation of the lookup_token_family_key function for Postgres
 * @author Christian Blättler
 */
#ifndef PG_LOOKUP_TOKEN_FAMILY_KEY_H
#define PG_LOOKUP_TOKEN_FAMILY_KEY_H

#include <gnunet/gnunet_common.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup details about a particular token family key. The valid_after field
 * of the key has to be >= @e min_valid_after and < @e max_valid_after.
 *
 * @param cls closure
 * @param instance_id instance to lookup token family key for
 * @param token_family_slug slug of token family to lookup
 * @param valid_at find a key with a validity period that includes this time
 * @param sign_until find a private key that can sign until this time
 * @param[out] details set to the token family key details on success, can be NULL
 *             (in that case we only want to check if the token family key exists)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_token_family_key (
  void *cls,
  const char *instance_id,
  const char *token_family_slug,
  struct GNUNET_TIME_Timestamp valid_at,
  struct GNUNET_TIME_Timestamp sign_until,
  struct TALER_MERCHANTDB_TokenFamilyKeyDetails *details);


#endif
