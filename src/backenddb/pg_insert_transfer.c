/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_transfer.c
 * @brief Implementation of the insert_transfer function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_transfer.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_transfer (
  void *cls,
  const char *instance_id,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const struct TALER_Amount *credit_amount,
  struct TALER_FullPayto payto_uri,
  bool confirmed)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_auto_from_type (wtid),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               credit_amount),
    GNUNET_PQ_query_param_string (payto_uri.full_payto),
    GNUNET_PQ_query_param_bool (confirmed),
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "insert_transfer",
           "INSERT INTO merchant_transfers"
           "(exchange_url"
           ",wtid"
           ",credit_amount"
           ",account_serial"
           ",confirmed)"
           "SELECT"
           " $1, $2, $3, account_serial, $5"
           " FROM merchant_accounts"
           " WHERE REGEXP_REPLACE(payto_uri,'\\?.*','')"
           "      =REGEXP_REPLACE($4,'\\?.*','')"
           "   AND merchant_serial="
           "        (SELECT merchant_serial"
           "           FROM merchant_instances"
           "          WHERE merchant_id=$6)"
           " ON CONFLICT DO NOTHING;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_transfer",
                                             params);
}
