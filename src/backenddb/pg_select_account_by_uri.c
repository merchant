/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_account_by_uri.c
 * @brief Implementation of the select_account_by_uri function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_account_by_uri.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_select_account_by_uri (void *cls,
                              const char *id,
                              struct TALER_FullPayto payto_uri,
                              struct TALER_MERCHANTDB_AccountDetails *ad)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (id),
    GNUNET_PQ_query_param_string (payto_uri.full_payto),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("salt",
                                          &ad->salt),
    GNUNET_PQ_result_spec_auto_from_type ("h_wire",
                                          &ad->h_wire),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_string ("credit_facade_url",
                                    &ad->credit_facade_url),
      NULL),
    GNUNET_PQ_result_spec_allow_null (
      TALER_PQ_result_spec_json ("credit_facade_credentials",
                                 &ad->credit_facade_credentials),
      NULL),
    GNUNET_PQ_result_spec_bool ("active",
                                &ad->active),
    GNUNET_PQ_result_spec_end
  };

  ad->credit_facade_url = NULL;
  ad->credit_facade_credentials = NULL;
  ad->payto_uri.full_payto
    = GNUNET_strdup (payto_uri.full_payto);
  ad->instance_id = id;
  check_connection (pg);
  PREPARE (pg,
           "select_account_by_uri",
           "SELECT"
           " salt"
           ",h_wire"
           ",credit_facade_url"
           ",credit_facade_credentials"
           ",active"
           " FROM merchant_accounts"
           " WHERE merchant_serial="
           "  (SELECT merchant_serial "
           "    FROM merchant_instances"
           "    WHERE merchant_id=$1)"
           "      AND payto_uri = $2");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "select_account_by_uri",
                                                   params,
                                                   rs);
}
