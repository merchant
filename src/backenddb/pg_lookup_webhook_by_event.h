/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_webhook_by_event.h
 * @brief implementation of the lookup_webhook_by_event function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_WEBHOOK_BY_EVENT_H
#define PG_LOOKUP_WEBHOOK_BY_EVENT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
   * Lookup webhook by event
   *
   * @param cls closure
   * @param instance_id instance to lookup webhook for
   * @param event_type event that we need to put in the pending webhook
   * @param[out] cb set to the webhook details on success
   * @param cb_cls callback closure
   * @return database result code
   */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_webhook_by_event (void *cls,
                                const char *instance_id,
                                const char *event_type,
                                TALER_MERCHANTDB_WebhookDetailCallback cb,
                                void *cb_cls);

#endif
