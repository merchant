/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_contract_terms3.c
 * @brief Implementation of the lookup_contract_terms3 function for Postgres
 * @author Iván Ávalos
 * @author Christian Grothoff
 */
#include "platform.h"
#include <sys/types.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_contract_terms3.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_contract_terms3 (
  void *cls,
  const char *instance_id,
  const char *order_id,
  const char *session_id,
  json_t **contract_terms,
  uint64_t *order_serial,
  bool *paid,
  bool *wired,
  bool *session_matches,
  struct TALER_ClaimTokenP *claim_token,
  int16_t *choice_index)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;
  struct TALER_ClaimTokenP ct;
  uint16_t ci = 0;
  bool choice_index_null = false;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    NULL == session_id
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (session_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    /* contract_terms must be first! */
    TALER_PQ_result_spec_json ("contract_terms",
                               contract_terms),
    GNUNET_PQ_result_spec_uint64 ("order_serial",
                                  order_serial),
    GNUNET_PQ_result_spec_bool ("paid",
                                paid),
    GNUNET_PQ_result_spec_bool ("wired",
                                wired),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_bool ("session_matches",
                                  session_matches),
      NULL),
    GNUNET_PQ_result_spec_auto_from_type ("claim_token",
                                          &ct),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_uint16 ("choice_index",
                                    &ci),
      &choice_index_null),
    GNUNET_PQ_result_spec_end
  };

  *session_matches = false;
  check_connection (pg);
  PREPARE (pg,
           "lookup_contract_terms3",
           "SELECT"
           " contract_terms"
           ",order_serial"
           ",claim_token"
           ",paid"
           ",wired"
           ",(session_id=$3) AS session_matches"
           ",choice_index"
           " FROM merchant_contract_terms"
           " WHERE order_id=$2"
           "   AND merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "lookup_contract_terms3",
                                                 params,
                                                 (NULL != contract_terms)
                                                   ? rs
                                                   : &rs[1]);
  if (NULL != claim_token)
    *claim_token = ct;
  if (! choice_index_null)
    *choice_index = (int16_t) ci;
  else
    *choice_index = -1;
  return qs;
}
