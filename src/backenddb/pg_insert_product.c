/*
   This file is part of TALER
   Copyright (C) 2022-2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_product.c
 * @brief Implementation of the insert_product function for Postgres
 * @author Christian Grothoff
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_product.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_product (void *cls,
                       const char *instance_id,
                       const char *product_id,
                       const struct TALER_MERCHANTDB_ProductDetails *pd,
                       size_t num_cats,
                       const uint64_t *cats,
                       bool *no_instance,
                       bool *conflict,
                       ssize_t *no_cat)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (product_id),
    GNUNET_PQ_query_param_string (pd->description),
    TALER_PQ_query_param_json (pd->description_i18n),
    GNUNET_PQ_query_param_string (pd->unit),
    GNUNET_PQ_query_param_string (pd->image),
    TALER_PQ_query_param_json (pd->taxes),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               &pd->price),
    GNUNET_PQ_query_param_uint64 (&pd->total_stock),
    TALER_PQ_query_param_json (pd->address),
    GNUNET_PQ_query_param_timestamp (&pd->next_restock),
    GNUNET_PQ_query_param_uint32 (&pd->minimum_age),
    GNUNET_PQ_query_param_array_uint64 (num_cats,
                                        cats,
                                        pg->conn),
    GNUNET_PQ_query_param_end
  };
  uint64_t ncat;
  bool cats_found = true;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("conflict",
                                conflict),
    GNUNET_PQ_result_spec_bool ("no_instance",
                                no_instance),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_uint64 ("no_cat",
                                    &ncat),
      &cats_found),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "insert_product",
           "SELECT"
           " out_conflict AS conflict"
           ",out_no_cat AS no_cat"
           ",out_no_instance AS no_instance"
           " FROM merchant_do_insert_product"
           "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "insert_product",
                                                 params,
                                                 rs);
  GNUNET_PQ_cleanup_query_params_closures (params);
  *no_cat = (cats_found) ? -1 : (ssize_t) ncat;
  return qs;
}
