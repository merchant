/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_products.h
 * @brief implementation of the lookup_products function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_PRODUCTS_H
#define PG_LOOKUP_PRODUCTS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup all of the products the given instance has configured.
 *
 * @param cls closure
 * @param instance_id instance to lookup products for
 * @param offset transfer_serial number of the transfer we want to offset from
 * @param limit number of entries to return, negative for descending,
 *                positive for ascending
 * @param cb function to call on all products found
 * @param cb_cls closure for @a cb
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_products (void *cls,
                        const char *instance_id,
                        uint64_t offset,
                        int64_t limit,
                        TALER_MERCHANTDB_ProductsCallback cb,
                        void *cb_cls);

#endif
