/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_pending_webhook.c
 * @brief Implementation of the insert_pending_webhook function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_pending_webhook.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_pending_webhook (void *cls,
                               const char *instance_id,
                               uint64_t webhook_serial,
                               const char *url,
                               const char *http_method,
                               const char *header,
                               const char *body)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&webhook_serial),
    GNUNET_PQ_query_param_string (url),
    GNUNET_PQ_query_param_string (http_method),
    NULL == header
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (header),
    NULL == body
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (body),
    GNUNET_PQ_query_param_end
  };
  check_connection (pg);
  PREPARE (pg,
           "insert_pending_webhook",
           "INSERT INTO merchant_pending_webhooks"
           "(merchant_serial"
           ",webhook_serial"
           ",url"
           ",http_method"
           ",header"
           ",body"
           ")"
           " SELECT mi.merchant_serial,"
           " $2, $3, $4, $5, $6"
           " FROM merchant_instances mi"
           " WHERE mi.merchant_id=$1");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_pending_webhook",
                                             params);
}
