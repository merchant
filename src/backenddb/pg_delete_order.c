/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_delete_order.c
 * @brief Implementation of the delete_order function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_delete_order.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_delete_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     bool force)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Absolute now = GNUNET_TIME_absolute_get ();
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_absolute_time (&now),
    GNUNET_PQ_query_param_bool (force),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_QueryParam params2[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;
  enum GNUNET_DB_QueryStatus qs2;

  check_connection (pg);
  PREPARE (pg,
           "delete_order",
           "WITH ms AS"
           "(SELECT merchant_serial "
           "   FROM merchant_instances"
           "  WHERE merchant_id=$1)"
           ", mc AS"
           "(SELECT paid"
           "   FROM merchant_contract_terms"
           "   JOIN ms USING (merchant_serial)"
           "  WHERE order_id=$2) "
           "DELETE"
           " FROM merchant_orders mo"
           " WHERE order_id=$2"
           "   AND merchant_serial=(SELECT merchant_serial FROM ms)"
           "   AND (  (pay_deadline < $3)"
           "       OR (NOT EXISTS (SELECT paid FROM mc))"
           "       OR ($4 AND (FALSE=(SELECT paid FROM mc))) );");
  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "delete_order",
                                           params);
  if ( (qs < 0) || (! force) )
    return qs;
  PREPARE (pg,
           "delete_contract",
           "DELETE"
           " FROM merchant_contract_terms"
           " WHERE order_id=$2 AND"
           "   merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND NOT paid;");
  qs2 = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                            "delete_contract",
                                            params2);
  if (qs2 < 0)
    return qs2;
  if (qs2 > 0)
    return qs2;
  return qs;
}
