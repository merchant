/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_instance_auth.h
 * @brief implementation of the update_instance_auth function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_UPDATE_INSTANCE_AUTH_H
#define PG_UPDATE_INSTANCE_AUTH_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Update information about an instance's authentication settings
 * into our database.
 *
 * @param cls closure
 * @param merchant_id identity of the instance
 * @param is authentication details about the instance
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_instance_auth (void *cls,
                             const char *merchant_id,
                             const struct
                             TALER_MERCHANTDB_InstanceAuthSettings *is);

#endif
