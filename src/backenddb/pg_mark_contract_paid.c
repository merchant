/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_mark_contract_paid.c
 * @brief Implementation of the mark_contract_paid function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_mark_contract_paid.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_mark_contract_paid (
  void *cls,
  const char *instance_id,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  const char *session_id,
  int16_t choice_index)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_string (session_id),
    (choice_index >= 0)
      ? GNUNET_PQ_query_param_int16 (&choice_index)
      : GNUNET_PQ_query_param_null (),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_QueryParam uparams[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  /* Session ID must always be given by the caller. */
  GNUNET_assert (NULL != session_id);

  /* no preflight check here, run in transaction by caller! */
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Marking h_contract_terms '%s' of %s as paid for session `%s'\n",
              GNUNET_h2s (&h_contract_terms->hash),
              instance_id,
              session_id);
  PREPARE (pg,
           "mark_contract_paid",
           "UPDATE merchant_contract_terms SET"
           " paid=TRUE"
           ",session_id=$3"
           ",choice_index=$4"
           " WHERE h_contract_terms=$2"
           "   AND merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "       WHERE merchant_id=$1)");
  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "mark_contract_paid",
                                           params);
  if (qs <= 0)
    return qs;
  PREPARE (pg,
           "mark_inventory_sold",
           "UPDATE merchant_inventory SET"
           " total_sold=total_sold + order_locks.total_locked"
           " FROM (SELECT total_locked,product_serial"
           "       FROM merchant_order_locks"
           "       WHERE order_serial="
           "       (SELECT order_serial"
           "          FROM merchant_contract_terms"
           "         WHERE h_contract_terms=$2"
           "           AND merchant_serial="
           "           (SELECT merchant_serial"
           "              FROM merchant_instances"
           "             WHERE merchant_id=$1))"
           "       ) AS order_locks"
           " WHERE merchant_inventory.product_serial"
           "             =order_locks.product_serial");
  qs = GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                           "mark_inventory_sold",
                                           uparams);
  if (qs < 0)
    return qs; /* 0: no inventory management, that's OK! */
  /* ON DELETE CASCADE deletes from merchant_order_locks */
  PREPARE (pg,
           "delete_completed_order",
           "WITH md AS"
           "  (SELECT merchant_serial"
           "     FROM merchant_instances"
           "    WHERE merchant_id=$1) "
           "DELETE"
           " FROM merchant_orders"
           " WHERE order_serial="
           "       (SELECT order_serial"
           "          FROM merchant_contract_terms"
           "          JOIN md USING (merchant_serial)"
           "         WHERE h_contract_terms=$2)");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "delete_completed_order",
                                             uparams);
}
