/*
   This file is part of TALER
   Copyright (C) 2022-2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_account_kyc_set_status.c
 * @brief Implementation of the account_kyc_set_status function for Postgres
 * @author Iván Ávalos
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_account_kyc_set_status.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_account_kyc_set_status (
  void *cls,
  const char *merchant_id,
  const struct TALER_MerchantWireHashP *h_wire,
  const char *exchange_url,
  struct GNUNET_TIME_Timestamp timestamp,
  unsigned int exchange_http_status,
  enum TALER_ErrorCode exchange_ec_code,
  const struct TALER_AccountAccessTokenP *access_token,
  const json_t *jlimits,
  bool in_aml_review,
  bool kyc_ok)
{
  struct PostgresClosure *pg = cls;
  struct TALER_MERCHANTDB_MerchantKycStatusChangeEventP ev = {
    .header.size = htons (sizeof (ev)),
    .header.type = htons (TALER_DBEVENT_MERCHANT_EXCHANGE_KYC_STATUS_CHANGED),
    .h_wire = *h_wire
  };
  struct GNUNET_DB_EventHeaderP hdr = {
    .size = htons (sizeof (hdr)),
    .type = htons (TALER_DBEVENT_MERCHANT_KYC_STATUS_CHANGED)
  };
  char *notify_s
    = GNUNET_PQ_get_event_notify_channel (&ev.header);
  char *notify2_s
    = GNUNET_PQ_get_event_notify_channel (&hdr);
  uint32_t http_status32 = (uint32_t) exchange_http_status;
  uint32_t ec_code32 = (uint32_t) exchange_ec_code;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (merchant_id),
    GNUNET_PQ_query_param_auto_from_type (h_wire),
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_timestamp (&timestamp),
    GNUNET_PQ_query_param_uint32 (&http_status32),
    GNUNET_PQ_query_param_uint32 (&ec_code32),
    NULL != access_token
    ? GNUNET_PQ_query_param_auto_from_type (access_token)
    : GNUNET_PQ_query_param_null (),
    NULL != jlimits
    ? TALER_PQ_query_param_json (jlimits)
    : GNUNET_PQ_query_param_null (),
    GNUNET_PQ_query_param_bool (in_aml_review),
    GNUNET_PQ_query_param_bool (kyc_ok),
    GNUNET_PQ_query_param_string (notify_s),
    GNUNET_PQ_query_param_string (notify2_s),
    GNUNET_PQ_query_param_end
  };
  bool no_instance;
  bool no_account;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("no_instance",
                                &no_instance),
    GNUNET_PQ_result_spec_bool ("no_account",
                                &no_account),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "account_kyc_set_status",
           "SELECT "
           "  out_no_instance AS no_instance"
           " ,out_no_account AS no_account"
           " FROM merchant_do_account_kyc_set_status"
           "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);");
  qs = GNUNET_PQ_eval_prepared_singleton_select (
    pg->conn,
    "account_kyc_set_status",
    params,
    rs);
  GNUNET_free (notify_s);
  GNUNET_free (notify2_s);
  if (qs <= 0)
  {
    GNUNET_break (GNUNET_DB_STATUS_SOFT_ERROR != qs);
    GNUNET_break (GNUNET_DB_STATUS_HARD_ERROR != qs);
    GNUNET_break (GNUNET_DB_STATUS_SUCCESS_NO_RESULTS != qs);
    return qs;
  }
  GNUNET_break (! no_instance);
  GNUNET_break (! no_account);
  return qs;
}
