/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_accounts.c
 * @brief Implementation of the select_accounts function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_accounts.h"
#include "pg_helper.h"


/**
 * Context for select_accounts().
 */
struct SelectAccountsContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_AccountCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Database context.
   */
  struct PostgresClosure *pg;

  /**
   * Set to the return value on errors.
   */
  enum GNUNET_DB_QueryStatus qs;

};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about accounts.
 *
 * @param cls of type `struct SelectAccountsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
select_account_cb (void *cls,
                   PGresult *result,
                   unsigned int num_results)
{
  struct SelectAccountsContext *lic = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    struct TALER_FullPayto payto;
    char *instance_id;
    char *facade_url = NULL;
    json_t *credential = NULL;
    struct TALER_MERCHANTDB_AccountDetails acc;
    struct TALER_MerchantPrivateKeyP merchant_priv;
    bool no_priv;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_auto_from_type ("h_wire",
                                            &acc.h_wire),
      GNUNET_PQ_result_spec_auto_from_type ("salt",
                                            &acc.salt),
      GNUNET_PQ_result_spec_string ("payto_uri",
                                    &payto.full_payto),
      GNUNET_PQ_result_spec_string ("merchant_id",
                                    &instance_id),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("credit_facade_url",
                                      &facade_url),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        TALER_PQ_result_spec_json ("credit_facade_credentials",
                                   &credential),
        NULL),
      GNUNET_PQ_result_spec_bool ("active",
                                  &acc.active),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_auto_from_type ("merchant_priv",
                                              &merchant_priv),
        &no_priv),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      lic->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    acc.instance_id = instance_id;
    acc.payto_uri = payto;
    acc.credit_facade_url = facade_url;
    acc.credit_facade_credentials = credential;
    lic->cb (lic->cb_cls,
             no_priv ? NULL : &merchant_priv,
             &acc);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_select_accounts (void *cls,
                        const char *id,
                        TALER_MERCHANTDB_AccountCallback cb,
                        void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct SelectAccountsContext lic = {
    .cb = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  struct GNUNET_PQ_QueryParam params[] = {
    NULL == id
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (id),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "select_accounts",
           "SELECT"
           " ma.h_wire"
           ",ma.salt"
           ",ma.payto_uri"
           ",ma.credit_facade_url"
           ",ma.credit_facade_credentials"
           ",ma.active"
           ",mk.merchant_priv"
           ",mi.merchant_id"
           " FROM merchant_accounts ma"
           " JOIN merchant_instances mi"
           "   ON (mi.merchant_serial=ma.merchant_serial)"
           " LEFT JOIN merchant_keys mk"
           "   ON (mk.merchant_serial=ma.merchant_serial)"
           " WHERE"
           "  ($1::TEXT IS NULL) OR"
           "  (ma.merchant_serial="
           "    (SELECT merchant_serial "
           "       FROM merchant_instances"
           "      WHERE merchant_id=$1));");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "select_accounts",
                                             params,
                                             &select_account_cb,
                                             &lic);
  if (0 > lic.qs)
    return lic.qs;
  return qs;
}
