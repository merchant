/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_refund_coin.h
 * @brief implementation of the refund_coin function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_REFUND_COIN_H
#define PG_REFUND_COIN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Function called during aborts to refund a coin. Marks the
 * respective coin as refunded.
 *
 * @param cls closure
 * @param instance_id instance to refund payment for
 * @param h_contract_terms hash of the contract to refund coin for
 * @param refund_timestamp timestamp of the refund
 * @param coin_pub public key of the coin to refund (fully)
 * @param reason text justifying the refund
 * @return transaction status
 *        #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if @a coin_pub is unknown to us;
 *        #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the request is valid,
 *        regardless of whether it actually increased the refund
 */
enum GNUNET_DB_QueryStatus
TMH_PG_refund_coin (void *cls,
                    const char *instance_id,
                    const struct TALER_PrivateContractHashP *h_contract_terms,
                    struct GNUNET_TIME_Timestamp refund_timestamp,
                    const struct TALER_CoinSpendPublicKeyP *coin_pub,
                    const char *reason);

#endif
