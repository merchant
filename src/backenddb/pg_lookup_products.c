/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_products.c
 * @brief Implementation of the lookup_products function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_products.h"
#include "pg_helper.h"

/**
 * Context used for TMH_PG_lookup_products().
 */
struct LookupProductsContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_ProductsCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about products.
 *
 * @param[in,out] cls of type `struct LookupProductsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_products_cb (void *cls,
                    PGresult *result,
                    unsigned int num_results)
{
  struct LookupProductsContext *plc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    char *product_id;
    uint64_t product_serial;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("product_id",
                                    &product_id),
      GNUNET_PQ_result_spec_uint64 ("product_serial",
                                    &product_serial),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      plc->extract_failed = true;
      return;
    }
    plc->cb (plc->cb_cls,
             product_serial,
             product_id);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_products (void *cls,
                        const char *instance_id,
                        uint64_t offset,
                        int64_t limit,
                        TALER_MERCHANTDB_ProductsCallback cb,
                        void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  uint64_t plimit = (uint64_t) ((limit < 0) ? -limit : limit);
  struct LookupProductsContext plc = {
    .cb = cb,
    .cb_cls = cb_cls,
    /* Can be overwritten by the lookup_products_cb */
    .extract_failed = false,
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&offset),
    GNUNET_PQ_query_param_uint64 (&plimit),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_products_asc",
           "SELECT"
           "  product_id"
           " ,product_serial"
           " FROM merchant_inventory"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           "   AND product_serial > $2"
           " ORDER BY product_serial ASC"
           " LIMIT $3");
  PREPARE (pg,
           "lookup_products_desc",
           "SELECT"
           "  product_id"
           " ,product_serial"
           " FROM merchant_inventory"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           "   AND product_serial < $2"
           " ORDER BY product_serial DESC"
           " LIMIT $3");
  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    (limit > 0)
    ? "lookup_products_asc"
    : "lookup_products_desc",
    params,
    &lookup_products_cb,
    &plc);
  /* If there was an error inside lookup_products_cb, return a hard error. */
  if (plc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
