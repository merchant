/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_contract_terms.c
 * @brief Implementation of the insert_contract_terms function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_contract_terms.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_insert_contract_terms (
  void *cls,
  const char *instance_id,
  const char *order_id,
  json_t *contract_terms,
  uint64_t *order_serial)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Timestamp pay_deadline;
  struct GNUNET_TIME_Timestamp refund_deadline;
  const char *fulfillment_url;
  struct TALER_PrivateContractHashP h_contract_terms;

  if (GNUNET_OK !=
      TALER_JSON_contract_hash (contract_terms,
                                &h_contract_terms))
  {
    GNUNET_break (0);
    return GNUNET_DB_STATUS_HARD_ERROR;
  }

  {
    struct GNUNET_JSON_Specification spec[] = {
      GNUNET_JSON_spec_timestamp ("pay_deadline",
                                  &pay_deadline),
      GNUNET_JSON_spec_timestamp ("refund_deadline",
                                  &refund_deadline),
      GNUNET_JSON_spec_end ()
    };
    enum GNUNET_GenericReturnValue res;
    const char *error_json_name;
    unsigned int error_line;

    res = GNUNET_JSON_parse (contract_terms,
                             spec,
                             &error_json_name,
                             &error_line);
    if (GNUNET_OK != res)
    {
      GNUNET_break (0);
      return GNUNET_DB_STATUS_HARD_ERROR;
    }
  }

  fulfillment_url =
    json_string_value (json_object_get (contract_terms,
                                        "fulfillment_url"));
  check_connection (pg);
  {
    struct GNUNET_PQ_QueryParam params[] = {
      GNUNET_PQ_query_param_string (instance_id),
      GNUNET_PQ_query_param_string (order_id),
      TALER_PQ_query_param_json (contract_terms),
      GNUNET_PQ_query_param_auto_from_type (&h_contract_terms),
      GNUNET_PQ_query_param_timestamp (&pay_deadline),
      GNUNET_PQ_query_param_timestamp (&refund_deadline),
      (NULL == fulfillment_url)
      ? GNUNET_PQ_query_param_null ()
      : GNUNET_PQ_query_param_string (fulfillment_url),
      GNUNET_PQ_query_param_end
    };
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("order_serial",
                                    order_serial),
      GNUNET_PQ_result_spec_end
    };
    PREPARE (pg,
             "insert_contract_terms",
             "INSERT INTO merchant_contract_terms"
             "(order_serial"
             ",merchant_serial"
             ",order_id"
             ",contract_terms"
             ",h_contract_terms"
             ",creation_time"
             ",pay_deadline"
             ",refund_deadline"
             ",fulfillment_url"
             ",claim_token"
             ",pos_key"
             ",pos_algorithm)"
             "SELECT"
             " mo.order_serial"
             ",mo.merchant_serial"
             ",mo.order_id"
             ",$3"  /* contract_terms */
             ",$4"  /* h_contract_terms */
             ",mo.creation_time"
             ",$5" /* pay_deadline */
             ",$6" /* refund_deadline */
             ",$7" /* fulfillment_url */
             ",mo.claim_token"
             ",mo.pos_key"
             ",mo.pos_algorithm"
             " FROM merchant_orders mo"
             " WHERE order_id=$2"
             "   AND merchant_serial="
             "     (SELECT merchant_serial"
             "        FROM merchant_instances"
             "        WHERE merchant_id=$1)"
             " RETURNING order_serial");
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "insert_contract_terms",
                                                     params,
                                                     rs);
  }
}
