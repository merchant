/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_webhook.h
 * @brief implementation of the update_webhook function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_UPDATE_WEBHOOK_H
#define PG_UPDATE_WEBHOOK_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Update details about a particular webhook.
 *
 * @param cls closure
 * @param instance_id instance to update template for
 * @param webhook_id webhook to update
 * @param wb update to the webhook details on success, can be NULL
 *             (in that case we only want to check if the webhook exists)
 * @return database result code, #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if the webhook
 *         does not yet exist.
 */
enum GNUNET_DB_QueryStatus
TMH_PG_update_webhook (void *cls,
                       const char *instance_id,
                       const char *webhook_id,
                       const struct TALER_MERCHANTDB_WebhookDetails *wb);

#endif
