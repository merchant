/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order_summary.c
 * @brief Implementation of the lookup_order_summary function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_order_summary.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order_summary (void *cls,
                             const char *instance_id,
                             const char *order_id,
                             struct GNUNET_TIME_Timestamp *timestamp,
                             uint64_t *order_serial)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint64 ("order_serial",
                                  order_serial),
    GNUNET_PQ_result_spec_timestamp ("creation_time",
                                     timestamp),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_order_summary",
           "(SELECT"
           " creation_time"
           ",order_serial"
           " FROM merchant_contract_terms"
           " WHERE merchant_contract_terms.merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND merchant_contract_terms.order_id=$2)"
           "UNION"
           "(SELECT"
           " creation_time"
           ",order_serial"
           " FROM merchant_orders"
           " WHERE merchant_orders.merchant_serial="
           "     (SELECT merchant_serial "
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)"
           "   AND merchant_orders.order_id=$2)");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_order_summary",
                                                   params,
                                                   rs);
}
