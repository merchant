--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- @file merchant-0009.sql
-- @brief Remove redundant required_currencies column
-- @author Florian Dold

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0009', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_template
  DROP COLUMN required_currency;

-- Add the column with IF NOT EXISTS since this
-- migration accidentally was in merchant-0006.sql
-- for some time before it was moved here.
ALTER TABLE merchant_contract_terms
  ADD COLUMN IF NOT EXISTS choice_index INT2 DEFAULT NULL;

COMMENT ON COLUMN merchant_contract_terms.choice_index
  IS 'Index of selected choice. Refers to the `choices` array in the contract terms. NULL for contracts without choices.';


-- Complete transaction
COMMIT;
