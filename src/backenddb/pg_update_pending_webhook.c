/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_pending_webhook.c
 * @brief Implementation of the update_pending_webhook function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_pending_webhook.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_update_pending_webhook (void *cls,
                               uint64_t webhook_pending_serial,
                               struct GNUNET_TIME_Absolute next_attempt)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&webhook_pending_serial),
    GNUNET_PQ_query_param_absolute_time (&next_attempt),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "update_pending_webhook",
           "UPDATE merchant_pending_webhooks SET"
           " retries=retries+1"
           ",next_attempt=$2"
           " WHERE webhook_pending_serial=$1");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_pending_webhook",
                                             params);
}
