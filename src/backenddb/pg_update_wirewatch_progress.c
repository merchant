/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_update_wirewatch_progress.c
 * @brief Implementation of the update_wirewatch_progress function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_update_wirewatch_progress.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_update_wirewatch_progress (
  void *cls,
  const char *instance,
  struct TALER_FullPayto payto_uri,
  uint64_t last_serial)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance),
    GNUNET_PQ_query_param_string (payto_uri.full_payto),
    GNUNET_PQ_query_param_uint64 (&last_serial),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "update_wirewatch_progress",
           "UPDATE merchant_accounts"
           " SET last_bank_serial=$3"
           " WHERE REGEXP_REPLACE(payto_uri,'\\?.*','')"
           "      =REGEXP_REPLACE(CAST ($2 AS TEXT),'\\?.*','')"
           "  AND merchant_serial ="
           "   (SELECT merchant_serial"
           "      FROM merchant_instances"
           "      WHERE merchant_id=$1)");
  check_connection (pg);
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "update_wirewatch_progress",
                                             params);
}
