/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_contract_terms2.c
 * @brief Implementation of the lookup_contract_terms2 function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_contract_terms2.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_contract_terms2 (
  void *cls,
  const char *instance_id,
  const char *order_id,
  json_t **contract_terms,
  uint64_t *order_serial,
  bool *paid,
  struct TALER_ClaimTokenP *claim_token,
  char **pos_key,
  enum TALER_MerchantConfirmationAlgorithm *pos_algorithm)
{
  struct PostgresClosure *pg = cls;
  enum GNUNET_DB_QueryStatus qs;
  struct TALER_ClaimTokenP ct;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (order_id),
    GNUNET_PQ_query_param_end
  };
  uint32_t pos32;
  struct GNUNET_PQ_ResultSpec rs[] = {
    /* contract_terms must be first! */
    TALER_PQ_result_spec_json ("contract_terms",
                               contract_terms),
    GNUNET_PQ_result_spec_uint64 ("order_serial",
                                  order_serial),
    GNUNET_PQ_result_spec_bool ("paid",
                                paid),
    GNUNET_PQ_result_spec_auto_from_type ("claim_token",
                                          &ct),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_string ("pos_key",
                                    pos_key),
      NULL),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_uint32 ("pos_algorithm",
                                    &pos32),
      NULL),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_contract_terms2",
           "SELECT"
           " contract_terms"
           ",order_serial"
           ",claim_token"
           ",paid"
           ",pos_key"
           ",pos_algorithm"
           " FROM merchant_contract_terms"
           " WHERE order_id=$2"
           "   AND merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "        WHERE merchant_id=$1)");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "lookup_contract_terms2",
                                                 params,
                                                 (NULL != contract_terms)
                                                   ? rs
                                                   : &rs[1]);
  *pos_algorithm = (enum TALER_MerchantConfirmationAlgorithm) pos32;
  if (NULL != claim_token)
    *claim_token = ct;
  return qs;
}
