/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_pending_webhooks.c
 * @brief Implementation of the lookup_pending_webhooks function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_pending_webhooks.h"
#include "pg_helper.h"

/**
 * Context used for lookup_pending_webhooks_cb().
 */
struct LookupPendingWebhookContext
{
  /**
   * Function to call with the results.
   */
  TALER_MERCHANTDB_PendingWebhooksCallback cb;

  /**
   * Closure for @a cb.
   */
  void *cb_cls;

  /**
   * Did database result extraction fail?
   */
  bool extract_failed;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results about webhook.
 *
 * @param[in,out] cls of type `struct LookupPendingWebhookContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_pending_webhooks_cb (void *cls,
                            PGresult *result,
                            unsigned int num_results)
{
  struct LookupPendingWebhookContext *pwlc = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t webhook_pending_serial;
    struct GNUNET_TIME_Absolute next_attempt;
    uint32_t retries;
    char *url;
    char *http_method;
    char *header = NULL;
    char *body = NULL;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("webhook_pending_serial",
                                    &webhook_pending_serial),
      GNUNET_PQ_result_spec_absolute_time ("next_attempt",
                                           &next_attempt),
      GNUNET_PQ_result_spec_uint32 ("retries",
                                    &retries),
      GNUNET_PQ_result_spec_string ("url",
                                    &url),
      GNUNET_PQ_result_spec_string ("http_method",
                                    &http_method),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("header",
                                      &header),
        NULL),
      GNUNET_PQ_result_spec_allow_null (
        GNUNET_PQ_result_spec_string ("body",
                                      &body),
        NULL),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      pwlc->extract_failed = true;
      return;
    }
    pwlc->cb (pwlc->cb_cls,
              webhook_pending_serial,
              next_attempt,
              retries,
              url,
              http_method,
              header,
              body);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_pending_webhooks (void *cls,
                                TALER_MERCHANTDB_PendingWebhooksCallback cb,
                                void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupPendingWebhookContext pwlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .extract_failed = false,
  };
  struct GNUNET_TIME_Absolute now = GNUNET_TIME_absolute_get ();
  struct GNUNET_PQ_QueryParam params_null[] = {
    GNUNET_PQ_query_param_absolute_time (&now),
    GNUNET_PQ_query_param_end
  };

  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_pending_webhooks",
           "SELECT"
           " webhook_pending_serial"
           ",next_attempt"
           ",retries"
           ",url"
           ",http_method"
           ",header"
           ",body"
           " FROM merchant_pending_webhooks"
           " WHERE next_attempt <= $1"
           "   ORDER BY next_attempt ASC"
           );

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_pending_webhooks",
                                             params_null,
                                             &lookup_pending_webhooks_cb,
                                             &pwlc);

  if (pwlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_future_webhook (void *cls,
                              TALER_MERCHANTDB_PendingWebhooksCallback cb,
                              void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupPendingWebhookContext pwlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .extract_failed = false,
  };
  struct GNUNET_PQ_QueryParam params_null[] = {
    GNUNET_PQ_query_param_end
  };

  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_future_webhook",
           "SELECT"
           " webhook_pending_serial"
           ",next_attempt"
           ",retries"
           ",url"
           ",http_method"
           ",header"
           ",body"
           " FROM merchant_pending_webhooks"
           " ORDER BY next_attempt ASC LIMIT 1"
           );

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_future_webhook",
                                             params_null,
                                             &lookup_pending_webhooks_cb,
                                             &pwlc);

  if (pwlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_all_webhooks (void *cls,
                            const char *instance_id,
                            uint64_t min_row,
                            uint32_t max_results,
                            TALER_MERCHANTDB_PendingWebhooksCallback cb,
                            void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct LookupPendingWebhookContext pwlc = {
    .cb = cb,
    .cb_cls = cb_cls,
    .extract_failed = false,
  };
  uint64_t max_results64 = max_results;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_uint64 (&min_row),
    GNUNET_PQ_query_param_uint64 (&max_results64),
    GNUNET_PQ_query_param_end
  };

  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_all_webhooks",
           " SELECT"
           " webhook_pending_serial"
           ",next_attempt"
           ",retries"
           ",url"
           ",http_method"
           ",header"
           ",body"
           " FROM merchant_pending_webhooks"
           " JOIN merchant_instances"
           "   USING (merchant_serial)"
           " WHERE merchant_instances.merchant_id=$1"
           " AND webhook_pending_serial > $2"
           "  ORDER BY webhook_pending_serial"
           "   ASC LIMIT $3");

  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_all_webhooks",
                                             params,
                                             &lookup_pending_webhooks_cb,
                                             &pwlc);

  if (pwlc.extract_failed)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
