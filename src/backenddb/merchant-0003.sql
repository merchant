--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('merchant-0003', NULL, NULL);

SET search_path TO merchant;

ALTER TABLE merchant_deposit_to_transfer
  ADD COLUMN wtid BYTEA CHECK (LENGTH(wtid)=32) DEFAULT NULL;

UPDATE merchant_deposit_to_transfer dst
  SET wtid=src.wtid
  FROM merchant_transfers src
  WHERE (src.credit_serial = dst.credit_serial);

ALTER TABLE merchant_deposit_to_transfer
  DROP COLUMN credit_serial,
  ALTER COLUMN wtid SET NOT NULL,
  ADD UNIQUE (deposit_serial,wtid);

COMMENT ON COLUMN merchant_deposit_to_transfer.wtid
  IS 'wire transfer identifier of the transfer the exchange claims to have done';


ALTER TABLE merchant_deposit_confirmations
  ADD COLUMN retry_backoff INT8 DEFAULT (0) NOT NULL;

COMMENT ON COLUMN merchant_deposit_confirmations.retry_backoff
  IS 'exponentially increasing value we add to the wire_transfer_deadline on each failure to confirm the wire transfer';


-- Complete transaction
COMMIT;
