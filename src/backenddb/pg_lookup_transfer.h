/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfer.h
 * @brief implementation of the lookup_transfer function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_TRANSFER_H
#define PG_LOOKUP_TRANSFER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup transfer status.
 *
 * @param cls closure
 * @param instance_id at which instance should we resolve the transfer
 * @param exchange_url the exchange that made the transfer
 * @param wtid wire transfer subject
 * @param[out] total_amount amount that was debited from our
 *             aggregate balance at the exchange (in total, sum of
 *             the wire transfer amount and the @a wire_fee)
 * @param[out] wire_fee the wire fee the exchange charged (only set if @a have_exchange_sig is true)
 * @param[out] exchange_amount the amount the exchange claims was transferred (only set if @a have_exchange_sig is true)
 * @param[out] execution_time when the transfer was executed by the exchange (only set if @a have_exchange_sig is true)
 * @param[out] have_exchange_sig do we have a response from the exchange about this transfer
 * @param[out] verified did we confirm the transfer was OK
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfer (void *cls,
                        const char *instance_id,
                        const char *exchange_url,
                        const struct TALER_WireTransferIdentifierRawP *wtid,
                        struct TALER_Amount *total_amount,
                        struct TALER_Amount *wire_fee,
                        struct TALER_Amount *exchange_amount,
                        struct GNUNET_TIME_Timestamp *execution_time,
                        bool *have_exchange_sig,
                        bool *verified);

#endif
