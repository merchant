/*
   This file is part of TALER
   Copyright (C) 2022, 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_order.h
 * @brief implementation of the insert_order function for Postgres
 * @author Iván Ávalos
 * @author Christian Grothoff
 */
#ifndef PG_INSERT_ORDER_H
#define PG_INSERT_ORDER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert order into the DB.
 *
 * @param cls closure
 * @param instance_id identifies the instance responsible for the order
 * @param order_id alphanumeric string that uniquely identifies the proposal
 * @param session_id session ID associated with the order, can be NULL
 * @param h_post_data hash of the POST data for idempotency checks
 * @param pay_deadline how long does the customer have to pay for the order
 * @param claim_token token to use for access control
 * @param contract_terms proposal data to store
 * @param pos_key encoded key for payment verification
 * @param pos_algorithm algorithm to compute the payment verification
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     const char *session_id,
                     const struct TALER_MerchantPostDataHashP *h_post_data,
                     struct GNUNET_TIME_Timestamp pay_deadline,
                     const struct TALER_ClaimTokenP *claim_token,
                     const json_t *contract_terms,
                     const char *pos_key,
                     enum TALER_MerchantConfirmationAlgorithm pos_algorithm);

#endif
