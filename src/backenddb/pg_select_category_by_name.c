/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_select_category_by_name.c
 * @brief Implementation of the select_category_by_name function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_select_category_by_name.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_select_category_by_name (
  void *cls,
  const char *instance_id,
  const char *category_name,
  json_t **name_i18n,
  uint64_t *category_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (category_name),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint64 ("category_serial",
                                  category_id),
    TALER_PQ_result_spec_json ("category_name_i18n",
                               name_i18n),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "select_category_by_name",
           "SELECT"
           " category_serial"
           ",category_name_i18n"
           " FROM merchant_categories mc"
           " JOIN merchant_instances mi"
           "   USING (merchant_serial)"
           " WHERE mi.merchant_id=$1"
           "   AND mc.category_name=$2");
  return GNUNET_PQ_eval_prepared_singleton_select (
    pg->conn,
    "select_category_by_name",
    params,
    rs);
}
