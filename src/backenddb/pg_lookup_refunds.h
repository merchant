/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_refunds.h
 * @brief implementation of the lookup_refunds function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_REFUNDS_H
#define PG_LOOKUP_REFUNDS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Obtain refunds associated with a contract.
 *
 * @param cls closure, typically a connection to the db
 * @param instance_id instance to lookup refunds for
 * @param h_contract_terms hash code of the contract
 * @param rc function to call for each coin on which there is a refund
 * @param rc_cls closure for @a rc
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_refunds (
  void *cls,
  const char *instance_id,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  TALER_MERCHANTDB_RefundCallback rc,
  void *rc_cls);

#endif
