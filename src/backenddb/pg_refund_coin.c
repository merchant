/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_refund_coin.c
 * @brief Implementation of the refund_coin function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_refund_coin.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_refund_coin (void *cls,
                    const char *instance_id,
                    const struct TALER_PrivateContractHashP *h_contract_terms,
                    struct GNUNET_TIME_Timestamp refund_timestamp,
                    const struct TALER_CoinSpendPublicKeyP *coin_pub,
                    const char *reason)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_timestamp (&refund_timestamp),
    GNUNET_PQ_query_param_auto_from_type (coin_pub),
    GNUNET_PQ_query_param_string (reason),
    GNUNET_PQ_query_param_end
  };
  PREPARE (pg,
           "refund_coin",
           "INSERT INTO merchant_refunds"
           "(order_serial"
           ",rtransaction_id"
           ",refund_timestamp"
           ",coin_pub"
           ",reason"
           ",refund_amount"
           ") "
           "SELECT "
           " dcon.order_serial"
           ",0" /* rtransaction_id always 0 for /abort */
           ",$3"
           ",dep.coin_pub"
           ",$5"
           ",dep.amount_with_fee"
           " FROM merchant_deposits dep"
           " JOIN merchant_deposit_confirmations dcon"
           "   USING (deposit_confirmation_serial)"
           " WHERE dep.coin_pub=$4"
           "   AND dcon.order_serial="
           "  (SELECT order_serial"
           "     FROM merchant_contract_terms"
           "    WHERE h_contract_terms=$2"
           "      AND merchant_serial="
           "        (SELECT merchant_serial"
           "           FROM merchant_instances"
           "          WHERE merchant_id=$1))");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "refund_coin",
                                             params);
}
