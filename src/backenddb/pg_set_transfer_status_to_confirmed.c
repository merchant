/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_set_transfer_status_to_confirmed.c
 * @brief Implementation of the set_transfer_status_to_confirmed function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_set_transfer_status_to_confirmed.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_set_transfer_status_to_confirmed (
  void *cls,
  const char *instance_id,
  const char *exchange_url,
  const struct TALER_WireTransferIdentifierRawP *wtid,
  const struct TALER_Amount *amount)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (wtid),
    GNUNET_PQ_query_param_string (exchange_url),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               amount),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "set_transfer_status_to_confirmed",
           "UPDATE merchant_transfers SET"
           " confirmed=TRUE"
           " WHERE wtid=$2"
           "   AND credit_amount=cast($4 AS taler_amount_currency)"
           "   AND exchange_url=$3"
           "   AND account_serial IN"
           "   (SELECT account_serial"
           "     FROM merchant_accounts"
           "    WHERE merchant_serial ="
           "      (SELECT merchant_serial"
           "         FROM merchant_instances"
           "        WHERE merchant_id=$1));");
  return GNUNET_PQ_eval_prepared_non_select (
    pg->conn,
    "set_transfer_status_to_confirmed",
    params);
}
