/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_deposits_by_contract_and_coin.c
 * @brief Implementation of the lookup_deposits_by_contract_and_coin function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_deposits_by_contract_and_coin.h"
#include "pg_helper.h"

/**
 * Closure for #lookup_deposits_by_contract_and_coin_cb().
 */
struct LookupDepositsByCnCContext
{
  /**
   * Function to call for each deposit.
   */
  TALER_MERCHANTDB_CoinDepositCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Total amount refunded on this coin and contract.
   */
  struct TALER_Amount refund_total;

  /**
   * Transaction result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupDepositsByCnCContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_refunds_cb (void *cls,
                   PGresult *result,
                   unsigned int num_results)
{
  struct LookupDepositsByCnCContext *ldcc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    struct TALER_Amount refund_amount;
    struct GNUNET_PQ_ResultSpec rs[] = {
      TALER_PQ_result_spec_amount_with_currency ("refund_amount",
                                                 &refund_amount),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ldcc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Coin had refund of %s\n",
                TALER_amount2s (&refund_amount));
    if (0 == i)
      ldcc->refund_total = refund_amount;
    else
      GNUNET_assert (0 <=
                     TALER_amount_add (&ldcc->refund_total,
                                       &ldcc->refund_total,
                                       &refund_amount));
    GNUNET_PQ_cleanup_result (rs); /* technically useless here */
  }
}


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupDepositsByCnCContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_deposits_by_contract_and_coin_cb (void *cls,
                                         PGresult *result,
                                         unsigned int num_results)
{
  struct LookupDepositsByCnCContext *ldcc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    char *exchange_url;
    struct TALER_Amount amount_with_fee;
    struct TALER_Amount deposit_fee;
    struct TALER_Amount refund_fee;
    struct TALER_Amount wire_fee;
    struct TALER_MerchantWireHashP h_wire;
    struct GNUNET_TIME_Timestamp deposit_timestamp;
    struct GNUNET_TIME_Timestamp refund_deadline;
    struct TALER_ExchangeSignatureP exchange_sig;
    struct TALER_ExchangePublicKeyP exchange_pub;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("exchange_url",
                                    &exchange_url),
      TALER_PQ_result_spec_amount_with_currency ("amount_with_fee",
                                                 &amount_with_fee),
      TALER_PQ_result_spec_amount_with_currency ("deposit_fee",
                                                 &deposit_fee),
      TALER_PQ_result_spec_amount_with_currency ("refund_fee",
                                                 &refund_fee),
      TALER_PQ_result_spec_amount_with_currency ("wire_fee",
                                                 &wire_fee),
      GNUNET_PQ_result_spec_auto_from_type ("h_wire",
                                            &h_wire),
      GNUNET_PQ_result_spec_timestamp ("deposit_timestamp",
                                       &deposit_timestamp),
      GNUNET_PQ_result_spec_timestamp ("refund_deadline",
                                       &refund_deadline),
      GNUNET_PQ_result_spec_auto_from_type ("exchange_sig",
                                            &exchange_sig),
      GNUNET_PQ_result_spec_auto_from_type ("exchange_pub",
                                            &exchange_pub),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ldcc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Coin original deposit value is %s\n",
                TALER_amount2s (&amount_with_fee));
    if (TALER_amount_is_valid (&ldcc->refund_total))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                  "Coin had total refunds of %s\n",
                  TALER_amount2s (&ldcc->refund_total));
      if (1 ==
          TALER_amount_cmp (&ldcc->refund_total,
                            &amount_with_fee))
      {
        /* Refunds exceeded total deposit? not OK! */
        GNUNET_break (0);
        ldcc->qs = GNUNET_DB_STATUS_HARD_ERROR;
        return;
      }
      if (0 ==
          TALER_amount_cmp (&ldcc->refund_total,
                            &amount_with_fee))
      {
        /* refund_total == amount_with_fee;
           in this case, the total contributed to the
           wire transfer is zero (as are fees) */
        GNUNET_assert (GNUNET_OK ==
                       TALER_amount_set_zero (ldcc->refund_total.currency,
                                              &amount_with_fee));
        GNUNET_assert (GNUNET_OK ==
                       TALER_amount_set_zero (ldcc->refund_total.currency,
                                              &deposit_fee));

      }
      else
      {
        /* Compute deposit value by subtracting refunds */
        GNUNET_assert (0 <
                       TALER_amount_subtract (&amount_with_fee,
                                              &amount_with_fee,
                                              &ldcc->refund_total));
        if (-1 ==
            TALER_amount_cmp (&amount_with_fee,
                              &deposit_fee))
        {
          /* amount_with_fee < deposit_fee, so after refunds less than
             the deposit fee remains; reduce deposit fee to
             the remaining value of the coin */
          deposit_fee = amount_with_fee;
        }
      }
    }
    ldcc->cb (ldcc->cb_cls,
              exchange_url,
              &amount_with_fee,
              &deposit_fee,
              &refund_fee,
              &wire_fee,
              &h_wire,
              deposit_timestamp,
              refund_deadline,
              &exchange_sig,
              &exchange_pub);
    GNUNET_PQ_cleanup_result (rs);
  }
  ldcc->qs = num_results;
}


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_deposits_by_contract_and_coin (
  void *cls,
  const char *instance_id,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  const struct TALER_CoinSpendPublicKeyP *coin_pub,
  TALER_MERCHANTDB_CoinDepositCallback cb,
  void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_auto_from_type (h_contract_terms),
    GNUNET_PQ_query_param_auto_from_type (coin_pub),
    GNUNET_PQ_query_param_end
  };
  struct LookupDepositsByCnCContext ldcc = {
    .cb  = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  /* no preflight check here, run in transaction by caller! */
  TALER_LOG_DEBUG ("Looking for refund of h_contract_terms %s at `%s'\n",
                   GNUNET_h2s (&h_contract_terms->hash),
                   instance_id);
  check_connection (pg);
  PREPARE (pg,
           "lookup_refunds_by_coin_and_contract",
           "SELECT"
           " refund_amount"
           " FROM merchant_refunds"
           /* Join to filter by refunds that actually
              did work, not only those we approved */
           " JOIN merchant_refund_proofs"
           "   USING (refund_serial)"
           " WHERE coin_pub=$3"
           "  AND order_serial="
           "  (SELECT order_serial"
           "     FROM merchant_contract_terms"
           "    WHERE h_contract_terms=$2"
           "      AND merchant_serial="
           "        (SELECT merchant_serial"
           "           FROM merchant_instances"
           "          WHERE merchant_id=$1))");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "lookup_refunds_by_coin_and_contract",
                                             params,
                                             &lookup_refunds_cb,
                                             &ldcc);
  if (0 > qs)
    return qs;

  PREPARE (pg,
           "lookup_deposits_by_contract_and_coin",
           "SELECT"
           " mcon.exchange_url"
           ",dep.amount_with_fee"
           ",dep.deposit_fee"
           ",dep.refund_fee"
           ",mcon.wire_fee"
           ",acc.h_wire"
           ",mcon.deposit_timestamp"
           ",mct.refund_deadline"
           ",mcon.exchange_sig"
           ",msig.exchange_pub"
           " FROM merchant_contract_terms mct"
           "  JOIN merchant_deposit_confirmations mcon"
           "    USING (order_serial)"
           "  JOIN merchant_deposits dep"
           "    USING (deposit_confirmation_serial)"
           "  JOIN merchant_exchange_signing_keys msig"
           "    USING (signkey_serial)"
           "  JOIN merchant_accounts acc"
           "    USING (account_serial)"
           " WHERE h_contract_terms=$2"
           "   AND dep.coin_pub=$3"
           "   AND mct.merchant_serial="
           "     (SELECT merchant_serial"
           "        FROM merchant_instances"
           "       WHERE merchant_id=$1)");

  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "lookup_deposits_by_contract_and_coin",
    params,
    &lookup_deposits_by_contract_and_coin_cb,
    &ldcc);
  if (0 >= qs)
    return qs;
  return ldcc.qs;
}
