/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_refund_proof.c
 * @brief Implementation of the lookup_refund_proof function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_refund_proof.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_refund_proof (void *cls,
                            uint64_t refund_serial,
                            struct TALER_ExchangeSignatureP *exchange_sig,
                            struct TALER_ExchangePublicKeyP *exchange_pub)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&refund_serial),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("exchange_sig",
                                          exchange_sig),
    GNUNET_PQ_result_spec_auto_from_type ("exchange_pub",
                                          exchange_pub),
    GNUNET_PQ_result_spec_end
  };

  check_connection (pg);
  PREPARE (pg,
           "lookup_refund_proof",
           "SELECT"
           " merchant_exchange_signing_keys.exchange_pub"
           ",exchange_sig"
           " FROM merchant_refund_proofs"
           "  JOIN merchant_exchange_signing_keys"
           "    USING (signkey_serial)"
           " WHERE"
           "   refund_serial=$1");

  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_refund_proof",
                                                   params,
                                                   rs);
}
