/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_transfer_details.c
 * @brief Implementation of the lookup_transfer_details function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_transfer_details.h"
#include "pg_helper.h"

/**
 * Closure for #lookup_transfer_details_cb().
 */
struct LookupTransferDetailsContext
{
  /**
   * Function to call for each order that was aggregated.
   */
  TALER_MERCHANTDB_TransferDetailsCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Transaction result.
   */
  enum GNUNET_DB_QueryStatus qs;
};


/**
 * Function to be called with the results of a SELECT statement
 * that has returned @a num_results results.
 *
 * @param cls of type `struct LookupTransferDetailsContext *`
 * @param result the postgres result
 * @param num_results the number of results in @a result
 */
static void
lookup_transfer_details_cb (void *cls,
                            PGresult *result,
                            unsigned int num_results)
{
  struct LookupTransferDetailsContext *ltdc = cls;

  for (unsigned int i = 0; i<num_results; i++)
  {
    uint64_t current_offset;
    struct TALER_TrackTransferDetails ttd;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("offset_in_exchange_list",
                                    &current_offset),
      GNUNET_PQ_result_spec_auto_from_type ("h_contract_terms",
                                            &ttd.h_contract_terms),
      GNUNET_PQ_result_spec_auto_from_type ("coin_pub",
                                            &ttd.coin_pub),
      TALER_PQ_result_spec_amount_with_currency ("exchange_deposit_value",
                                                 &ttd.coin_value),
      TALER_PQ_result_spec_amount_with_currency ("exchange_deposit_fee",
                                                 &ttd.coin_fee),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ltdc->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }
    ltdc->cb (ltdc->cb_cls,
              (unsigned int) current_offset,
              &ttd);
    GNUNET_PQ_cleanup_result (rs);
  }
  ltdc->qs = num_results;
}

enum GNUNET_DB_QueryStatus
TMH_PG_lookup_transfer_details (void *cls,
                                const char *exchange_url,
                                const struct TALER_WireTransferIdentifierRawP *wtid,
                                TALER_MERCHANTDB_TransferDetailsCallback cb,
                                void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (exchange_url),
    GNUNET_PQ_query_param_auto_from_type (wtid),
    GNUNET_PQ_query_param_end
  };
  struct LookupTransferDetailsContext ltdc = {
    .cb  = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  enum GNUNET_DB_QueryStatus qs;

  check_connection (pg);
  PREPARE (pg,
           "lookup_transfer_details",
           "SELECT"
           " mterm.h_contract_terms"
           ",mtcoin.offset_in_exchange_list"
           ",dep.coin_pub"
           ",mtcoin.exchange_deposit_value"
           ",mtcoin.exchange_deposit_fee"
           " FROM merchant_transfer_to_coin mtcoin"
           " JOIN merchant_deposits dep"
           "   USING (deposit_serial)"
           " JOIN merchant_deposit_confirmations mcon"
           "   USING (deposit_confirmation_serial)"
           " JOIN merchant_contract_terms mterm"
           "   USING (order_serial)"
           " JOIN merchant_transfers mtr"
           "   USING (credit_serial)"
           " WHERE mtr.wtid=$2"
           "   AND mtr.exchange_url=$1");

  qs = GNUNET_PQ_eval_prepared_multi_select (
    pg->conn,
    "lookup_transfer_details",
    params,
    &lookup_transfer_details_cb,
    &ltdc);
  if (0 >= qs)
    return qs;
  return ltdc.qs;
}
