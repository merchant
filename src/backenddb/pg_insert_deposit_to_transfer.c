/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_deposit_to_transfer.c
 * @brief Implementation of the insert_deposit_to_transfer function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_deposit_to_transfer.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_insert_deposit_to_transfer (
  void *cls,
  uint64_t deposit_serial,
  const struct TALER_EXCHANGE_DepositData *dd,
  bool *wpc)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&deposit_serial),
    TALER_PQ_query_param_amount_with_currency (pg->conn,
                                               &dd->coin_contribution),
    GNUNET_PQ_query_param_timestamp (&dd->execution_time),
    GNUNET_PQ_query_param_auto_from_type (&dd->exchange_sig),
    GNUNET_PQ_query_param_auto_from_type (&dd->exchange_pub),
    GNUNET_PQ_query_param_auto_from_type (&dd->wtid),
    GNUNET_PQ_query_param_end
  };
  bool conflict;
  bool no_exchange_pub;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("out_wire_pending_cleared",
                                wpc),
    GNUNET_PQ_result_spec_bool ("out_conflict",
                                &conflict),
    GNUNET_PQ_result_spec_bool ("out_no_exchange_pub",
                                &no_exchange_pub),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  *wpc = false;
  PREPARE (pg,
           "insert_deposit_to_transfer",
           "SELECT"
           "  out_wire_pending_cleared"
           " ,out_conflict"
           " ,out_no_exchange_pub"
           " FROM merchant_insert_deposit_to_transfer"
           " ($1,$2,$3,$4,$5,$6);");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "insert_deposit_to_transfer",
                                                 params,
                                                 rs);
  if (qs <= 0)
    return qs;
  if (no_exchange_pub)
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Exchange public key unknown (bug!?)\n");
  if (*wpc)
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Wire pending flag cleared (good!)\n");
  if (conflict)
    return GNUNET_DB_STATUS_SUCCESS_NO_RESULTS;
  return qs;
}
