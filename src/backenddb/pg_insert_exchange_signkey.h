/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_exchange_signkey.h
 * @brief implementation of the insert_exchange_signkey function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_INSERT_EXCHANGE_SIGNKEY_H
#define PG_INSERT_EXCHANGE_SIGNKEY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Insert an exchange signing key into our database.
 *
 * @param cls closure
 * @param master_pub exchange master public key used for @a master_sig
 * @param exchange_pub exchange signing key to insert
 * @param start_date when does the signing key become valid
 * @param expire_date when does the signing key stop being used
 * @param end_date when does the signing key become void as proof
 * @param master_sig signature of @a master_pub over the @a exchange_pub and the dates
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_exchange_signkey (void *cls,
                                const struct TALER_MasterPublicKeyP *master_pub,
                                const struct
                                TALER_ExchangePublicKeyP *exchange_pub,
                                struct GNUNET_TIME_Timestamp start_date,
                                struct GNUNET_TIME_Timestamp expire_date,
                                struct GNUNET_TIME_Timestamp end_date,
                                const struct
                                TALER_MasterSignatureP *master_sig);

#endif
