/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_order.h
 * @brief implementation of the lookup_order function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_ORDER_H
#define PG_LOOKUP_ORDER_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Retrieve order given its @a order_id and the @a instance_id.
 *
 * @param cls closure
 * @param instance_id instance to obtain order of
 * @param order_id order id used to perform the lookup
 * @param[out] claim_token the claim token generated for the order,
 *             NULL to only test if the order exists
 * @param[out] h_post_data set to the hash of the POST data that created the order
 * @param[out] contract_terms where to store the retrieved contract terms,
 *             NULL to only test if the order exists
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_order (void *cls,
                     const char *instance_id,
                     const char *order_id,
                     struct TALER_ClaimTokenP *claim_token,
                     struct TALER_MerchantPostDataHashP *h_post_data,
                     json_t **contract_terms);

#endif
