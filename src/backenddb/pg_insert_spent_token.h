/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_insert_spent_token.h
 * @brief implementation of the insert_spent_token function for Postgres
 * @author Christian Blättler
 */
#ifndef PG_INSERT_SPENT_TOKEN_H
#define PG_INSERT_SPENT_TOKEN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"


/**
 * @param cls closure
 * @param h_contract_terms hash of the contract the token was used for
 * @param h_issue_pub hash of the token issue public key
 * @param use_pub token use public key
 * @param use_sig token use signature
 * @param issue_sig token issue signature
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_insert_spent_token (
  void *cls,
  const struct TALER_PrivateContractHashP *h_contract_terms,
  const struct TALER_TokenIssuePublicKeyHashP *h_issue_pub,
  const struct TALER_TokenUsePublicKeyP *use_pub,
  const struct TALER_TokenUseSignatureP *use_sig,
  const struct TALER_TokenIssueSignature *issue_sig);

#endif
