/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_webhook.h
 * @brief implementation of the lookup_webhook function for Postgres
 * @author Iván Ávalos
 */
#ifndef PG_LOOKUP_WEBHOOK_H
#define PG_LOOKUP_WEBHOOK_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "taler_merchantdb_plugin.h"

/**
 * Lookup details about a particular webhook.
 *
 * @param cls closure
 * @param instance_id instance to lookup webhook for
 * @param webhook_id webhook to lookup
 * @param[out] wb set to the webhook details on success, can be NULL
 *             (in that case we only want to check if the webhook exists)
 * @return database result code
 */
enum GNUNET_DB_QueryStatus
TMH_PG_lookup_webhook (void *cls,
                       const char *instance_id,
                       const char *webhook_id,
                       struct TALER_MERCHANTDB_WebhookDetails *wb);

#endif
