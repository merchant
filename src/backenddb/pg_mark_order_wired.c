/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_mark_order_wired.c
 * @brief Implementation of the mark_order_wired function for Postgres
 * @author Iván Ávalos
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_mark_order_wired.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
TMH_PG_mark_order_wired (void *cls,
                         uint64_t order_serial)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&order_serial),
    GNUNET_PQ_query_param_end
  };

  check_connection (pg);
  PREPARE (pg,
           "mark_order_wired",
           "UPDATE merchant_contract_terms SET"
           " wired=TRUE"
           " WHERE order_serial=$1");

  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "mark_order_wired",
                                             params);
}
