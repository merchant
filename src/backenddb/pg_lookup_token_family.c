/*
   This file is part of TALER
   Copyright (C) 2023, 2024, 2025 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file backenddb/pg_lookup_token_family.c
 * @brief Implementation of the lookup_token_family function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_token_family.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
TMH_PG_lookup_token_family (
  void *cls,
  const char *instance_id,
  const char *token_family_slug,
  struct TALER_MERCHANTDB_TokenFamilyDetails *details)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (instance_id),
    GNUNET_PQ_query_param_string (token_family_slug),
    GNUNET_PQ_query_param_end
  };

  if (NULL == details)
  {
    struct GNUNET_PQ_ResultSpec rs_null[] = {
      GNUNET_PQ_result_spec_end
    };

    check_connection (pg);
    return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                     "lookup_token_family",
                                                     params,
                                                     rs_null);
  }
  else
  {
    char *kind;
    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_string ("slug",
                                    &details->slug),
      GNUNET_PQ_result_spec_string ("name",
                                    &details->name),
      GNUNET_PQ_result_spec_string ("cipher_choice",
                                    &details->cipher_spec),
      GNUNET_PQ_result_spec_string ("description",
                                    &details->description),
      TALER_PQ_result_spec_json ("description_i18n",
                                 &details->description_i18n),
      GNUNET_PQ_result_spec_allow_null (
        TALER_PQ_result_spec_json ("extra_data",
                                   &details->extra_data),
        NULL),
      GNUNET_PQ_result_spec_timestamp ("valid_after",
                                       &details->valid_after),
      GNUNET_PQ_result_spec_timestamp ("valid_before",
                                       &details->valid_before),
      GNUNET_PQ_result_spec_relative_time ("duration",
                                           &details->duration),
      GNUNET_PQ_result_spec_relative_time ("validity_granularity",
                                           &details->validity_granularity),
      GNUNET_PQ_result_spec_relative_time ("start_offset",
                                           &details->start_offset),
      GNUNET_PQ_result_spec_string ("kind",
                                    &kind),
      GNUNET_PQ_result_spec_uint64 ("issued",
                                    &details->issued),
      GNUNET_PQ_result_spec_uint64 ("used",
                                    &details->used),
      GNUNET_PQ_result_spec_end
    };
    enum GNUNET_DB_QueryStatus qs;

    check_connection (pg);
    PREPARE (pg,
             "lookup_token_family",
             "SELECT"
             " slug"
             ",name"
             ",cipher_choice"
             ",description"
             ",description_i18n"
             ",extra_data"
             ",valid_after"
             ",valid_before"
             ",duration"
             ",validity_granularity"
             ",start_offset"
             ",kind"
             ",issued"
             ",used"
             " FROM merchant_token_families"
             " JOIN merchant_instances"
             "   USING (merchant_serial)"
             " WHERE merchant_instances.merchant_id=$1"
             "   AND merchant_token_families.slug=$2");
    memset (details,
            0,
            sizeof (*details));
    qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_token_family",
                                                   params,
                                                   rs);
    if (GNUNET_DB_STATUS_SUCCESS_ONE_RESULT == qs)
    {
      if (0 == strcmp (kind, "discount"))
        details->kind = TALER_MERCHANTDB_TFK_Discount;
      else if (0 == strcmp (kind, "subscription"))
        details->kind = TALER_MERCHANTDB_TFK_Subscription;
      else
      {
        GNUNET_break (0);
        return GNUNET_DB_STATUS_HARD_ERROR;
      }
    }
    return qs;
  }
}
