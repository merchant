/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_instance.c
 * @brief command to test GET /instance/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET instance" CMD.
 */
struct GetInstanceState
{

  /**
   * Handle for a "GET instance" request.
   */
  struct TALER_MERCHANT_InstanceGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Public key of the merchant instance.
   */
  union TALER_AccountPublicKeyP account_pub;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the instance to run GET for.
   */
  const char *instance_id;

  /**
   * Reference for a POST or PATCH /instances CMD (optional).
   */
  const char *instance_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /get/instance/$ID operation.
 *
 * @param cls closure for this function
 * @param igr response
 */
static void
get_instance_cb (void *cls,
                 const struct TALER_MERCHANT_InstanceGetResponse *igr)
{
  struct GetInstanceState *gis = cls;
  const struct TALER_TESTING_Command *instance_cmd;

  instance_cmd = TALER_TESTING_interpreter_lookup_command (
    gis->is,
    gis->instance_reference);

  gis->igh = NULL;
  if (gis->http_status != igr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                igr->hr.http_status,
                (int) igr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (igr->hr.http_status)
  {
  case MHD_HTTP_OK:
    {
      const struct TALER_MERCHANT_InstanceDetails *details =
        &igr->details.ok.details;

      gis->account_pub.merchant_pub
        = details->merchant_pub;
      {
        const char *name;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_instance_name (instance_cmd,
                                                   &name))
          TALER_TESTING_interpreter_fail (gis->is);
        if (0 != strcmp (details->name,
                         name))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance name does not match: Got `%s', wanted `%s'\n",
                      details->name,
                      name);
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
      {
        const json_t *address;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_address (instance_cmd,
                                             &address))
          TALER_TESTING_interpreter_fail (gis->is);
        if (1 != json_equal (details->address,
                             address))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance address does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
      {
        const json_t *jurisdiction;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_jurisdiction (instance_cmd,
                                                  &jurisdiction))
          TALER_TESTING_interpreter_fail (gis->is);
        if (1 != json_equal (details->jurisdiction,
                             jurisdiction))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance jurisdiction does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
      {
        const bool *use_stefan;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_use_stefan (instance_cmd,
                                                &use_stefan))
          TALER_TESTING_interpreter_fail (gis->is);
        if (*use_stefan != details->use_stefan)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance use_stefan value does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
      {
        const struct GNUNET_TIME_Relative *default_wire_transfer_delay;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_wire_delay (instance_cmd,
                                                &default_wire_transfer_delay))
          TALER_TESTING_interpreter_fail (gis->is);
        if (details->default_wire_transfer_delay.rel_value_us !=
            default_wire_transfer_delay->rel_value_us)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance default wire transfer delay does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
      {
        const struct GNUNET_TIME_Relative *default_pay_delay;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_pay_delay (instance_cmd,
                                               &default_pay_delay))
          TALER_TESTING_interpreter_fail (gis->is);
        if (details->default_pay_delay.rel_value_us !=
            default_pay_delay->rel_value_us)
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Instance default pay delay does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for GET instance ID.\n",
                igr->hr.http_status);
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET instance" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_instance_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetInstanceState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_instance_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    gis->instance_id,
    &get_instance_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET instance" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_instance_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetInstanceState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /instances/$ID operation did not complete\n");
    TALER_MERCHANT_instance_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


/**
 * Offers information from the GET /instance/$ID CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
get_instance_traits (void *cls,
                     const void **ret,
                     const char *trait,
                     unsigned int index)
{
  struct GetInstanceState *pps = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_merchant_base_url (
      pps->merchant_url),
    TALER_TESTING_make_trait_merchant_pub (
      &pps->account_pub.merchant_pub),
    TALER_TESTING_make_trait_account_pub (
      &pps->account_pub),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instance (
  const char *label,
  const char *merchant_url,
  const char *instance_id,
  unsigned int http_status,
  const char *instance_reference)
{
  struct GetInstanceState *gis;

  gis = GNUNET_new (struct GetInstanceState);
  gis->merchant_url = merchant_url;
  gis->instance_id = instance_id;
  gis->http_status = http_status;
  gis->instance_reference = instance_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_instance_run,
      .cleanup = &get_instance_cleanup,
      .traits = &get_instance_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_instance.c */
