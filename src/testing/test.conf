# This file is in the public domain.
#
[PATHS]
# Persistent data storage for the testcase
TALER_TEST_HOME = test_merchant_api_home/
TALER_RUNTIME_DIR = ${TMPDIR:-${TMP:-/tmp}}/${USER:-}/taler-system-runtime/

# Persistent data storage
TALER_DATA_HOME = $TALER_HOME/.local/share/taler/

# Configuration files
TALER_CONFIG_HOME = $TALER_HOME/.config/taler/

# Cached data, no big deal if lost
TALER_CACHE_HOME = $TALER_HOME/.cache/taler/

[taler]
# What currency do we use?
CURRENCY = EUR
CURRENCY_ROUND_UNIT = EUR:0.01

[taler-helper-crypto-rsa]
# Reduce from 1 year to speed up test
LOOKAHEAD_SIGN = 24 days

[taler-helper-crypto-eddsa]
# Reduce from 1 year to speed up test
LOOKAHEAD_SIGN = 24 days
# Reduce from 12 weeks to ensure we have multiple
DURATION = 14 days

[bank]
HTTP_PORT = 8082

##########################################
# Configuration for the merchant backend #
##########################################

[merchant]

# Which port do we run the backend on? (HTTP server)
PORT = 8080
SERVE = tcp

# Which plugin (backend) do we use for the DB.
DB = postgres

# This specifies which database the postgres backend uses.
[merchantdb-postgres]
CONFIG = postgres:///talercheck
SQL_DIR = $DATADIR/sql/merchant/

# Sections starting with "merchant-exchange-" specify trusted exchanges
# (by the merchant)
[merchant-exchange-test]
MASTER_KEY = NKX42KSCQHDQK7CF1PC6X9DMQPXW6KHXKGD3DPQJMP32FKXSWYK0
EXCHANGE_BASE_URL = http://localhost:8081/
CURRENCY = EUR


#######################################################
# Configuration for the auditor for the testcase
#######################################################
[auditor]
BASE_URL = http://the.auditor/


#######################################################
# Configuration for ??? Is this used?
#######################################################

# Auditors must be in sections "auditor-", the rest of the section
# name could be anything.
[auditor-ezb]
# Informal name of the auditor. Just for the user.
NAME = European Central Bank

# URL of the auditor (especially for in the future, when the
# auditor offers an automated issue reporting system).
# Not really used today.
URL = http://taler.ezb.eu/

# This is the important bit: the signing key of the auditor.
PUBLIC_KEY = 9QXF7XY7E9VPV47B5Z806NDFSX2VJ79SVHHD29QEQ3BG31ANHZ60

# Which currency is this auditor trusted for?
CURRENCY = EUR


###################################################
# Configuration for the exchange for the testcase #
###################################################

[exchange]
AML_THRESHOLD = EUR:1000000


# How to access our database
DB = postgres

# HTTP port the exchange listens to
PORT = 8081

# Our public key
MASTER_PUBLIC_KEY = NKX42KSCQHDQK7CF1PC6X9DMQPXW6KHXKGD3DPQJMP32FKXSWYK0

# Base URL of the exchange.
BASE_URL = "http://localhost:8081/"


[exchangedb-postgres]
CONFIG = "postgres:///talercheck"


[auditordb-postgres]
CONFIG = postgres:///talercheck


# Account of the EXCHANGE
[exchange-account-exchange]
# What is the exchange's bank account (with the "Taler Bank" demo system)?
PAYTO_URI = "payto://x-taler-bank/localhost/2?receiver-name=2"
ENABLE_DEBIT = YES
ENABLE_CREDIT = YES

[exchange-accountcredentials-exchange]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/2/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = NONE

[admin-accountcredentials-exchange]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/2/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = NONE


[coin_eur_ct_1]
value = EUR:0.01
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.00
fee_deposit = EUR:0.00
fee_refresh = EUR:0.01
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = CS

[coin_eur_ct_10]
value = EUR:0.10
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = CS

[coin_eur_1]
value = EUR:1
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = CS

[coin_eur_5]
value = EUR:5
duration_withdraw = 7 days
duration_spend = 2 years
duration_legal = 3 years
fee_withdraw = EUR:0.01
fee_deposit = EUR:0.01
fee_refresh = EUR:0.03
fee_refund = EUR:0.01
rsa_keysize = 1024
CIPHER = CS
