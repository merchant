/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_templates.c
 * @brief command to test GET /templates
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET templates" CMD.
 */
struct GetTemplatesState
{

  /**
   * Handle for a "GET template" request.
   */
  struct TALER_MERCHANT_TemplatesGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * The list of template references.
   */
  const char **templates;

  /**
   * Length of @e templates.
   */
  unsigned int templates_length;

};


/**
 * Callback for a GET /templates operation.
 *
 * @param cls closure for this function
 * @param tgr response details
 */
static void
get_templates_cb (void *cls,
                  const struct TALER_MERCHANT_TemplatesGetResponse *tgr)
{
  struct GetTemplatesState *gis = cls;

  gis->igh = NULL;
  if (gis->http_status != tgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                tgr->hr.http_status,
                (int) tgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (tgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    if (tgr->details.ok.templates_length != gis->templates_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Length of templates found does not match\n");
      TALER_TESTING_interpreter_fail (gis->is);
      return;
    }
    for (unsigned int i = 0; i < gis->templates_length; ++i)
    {
      const struct TALER_TESTING_Command *template_cmd;

      template_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->templates[i]);

      {
        const char *template_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_template_id (template_cmd,
                                                 &template_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch template id\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
        if (0 != strcmp (tgr->details.ok.templates[i].template_id,
                         template_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Template id does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    /* instance does not exist */
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u (%d).\n",
                tgr->hr.http_status,
                tgr->hr.ec);
    break;
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /templates" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_templates_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct GetTemplatesState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_templates_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    &get_templates_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET template" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_templates_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct GetTemplatesState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /templates operation did not complete\n");
    TALER_MERCHANT_templates_get_cancel (gis->igh);
  }
  GNUNET_array_grow (gis->templates,
                     gis->templates_length,
                     0);
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_templates (const char *label,
                                          const char *merchant_url,
                                          unsigned int http_status,
                                          ...)
{
  struct GetTemplatesState *gis;

  gis = GNUNET_new (struct GetTemplatesState);
  gis->merchant_url = merchant_url;
  gis->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gis->templates,
                           gis->templates_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_templates_run,
      .cleanup = &get_templates_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_templates.c */
