[PATHS]
TALER_HOME = ${PWD}/test_rotation_home/
TALER_DATA_HOME = $TALER_HOME/.local/share/taler/
TALER_CONFIG_HOME = $TALER_HOME/.config/taler/
TALER_CACHE_HOME = $TALER_HOME/.cache/taler/
TALER_RUNTIME_DIR = ${TMPDIR:-${TMP:-/tmp}}/taler-system-runtime/

[taler]
CURRENCY = TESTKUDOS
CURRENCY_ROUND_UNIT = TESTKUDOS:0.01

[exchange]
MAX_KEYS_CACHING = forever
DB = postgres
SERVE = tcp
UNIXPATH = ${TALER_RUNTIME_DIR}/exchange.http
UNIXPATH_MODE = 660
PORT = 8081
BASE_URL = http://localhost:8081/
SIGNKEY_DURATION = 30 s
SIGNKEY_LEGAL_DURATION = 240 s
KEYDIR = ${TALER_DATA_HOME}/exchange/live-keys/
REVOCATION_DIR = ${TALER_DATA_HOME}/exchange/revocations/
TERMS_ETAG = 0
PRIVACY_ETAG = 0

[taler-exchange-secmod-eddsa]
OVERLAP_DURATION = 2 s
KEY_DIR = ${TALER_DATA_HOME}/crypto-eddsa/
UNIXPATH = $TALER_RUNTIME_DIR/taler-exchange-secmod-eddsa.sock
SM_PRIV_KEY = ${TALER_DATA_HOME}/taler-exchange-secmod-eddsa/.private-key
LOOKAHEAD_SIGN = 60 s
DURATION = 30 s

[taler-exchange-secmod-rsa]
OVERLAP_DURATION = 2 s
KEY_DIR = ${TALER_DATA_HOME}/crypto-rsa/
UNIXPATH = $TALER_RUNTIME_DIR/taler-exchange-secmod-rsa.sock
SM_PRIV_KEY = ${TALER_DATA_HOME}/taler-exchange-secmod-rsa/.private-key
LOOKAHEAD_SIGN = 60 s


[merchant]
SERVE = tcp
PORT = 9966
UNIXPATH = ${TALER_RUNTIME_DIR}/merchant.http
UNIXPATH_MODE = 660
DEFAULT_WIRE_FEE_AMORTIZATION = 1
DB = postgres
WIREFORMAT = default
# Set very low, so we can be sure that the database generated
# will contain wire transfers "ready" for the aggregator.
WIRE_TRANSFER_DELAY = 1 minute
DEFAULT_PAY_DEADLINE = 1 day
DEFAULT_MAX_DEPOSIT_FEE = TESTKUDOS:0.1
KEYFILE = ${TALER_DATA_HOME}/merchant/merchant.priv
DEFAULT_MAX_WIRE_FEE = TESTKUDOS:0.10

# Ensure that merchant reports EVERY deposit confirmation to auditor
FORCE_AUDIT = YES

[auditor]
DB = postgres
AUDITOR_PRIV_FILE = ${TALER_DATA_HOME}/auditor/offline-keys/auditor.priv
SERVE = tcp
UNIXPATH = ${TALER_RUNTIME_DIR}/exchange.http
UNIXPATH_MODE = 660
PORT = 8083
AUDITOR_URL = http://localhost:8083/
TINY_AMOUNT = TESTKUDOS:0.01
BASE_URL = "http://localhost:8083/"

[bank]
DATABASE = postgres:///taler-auditor-basedb
MAX_DEBT = TESTKUDOS:50.0
MAX_DEBT_BANK = TESTKUDOS:100000.0
HTTP_PORT = 8082
SUGGESTED_EXCHANGE = http://localhost:8081/
SUGGESTED_EXCHANGE_PAYTO = payto://x-taler-bank/localhost/2?receiver-name=Exchange
ALLOW_REGISTRATIONS = YES
SERVE = tcp

[exchangedb]
IDLE_RESERVE_EXPIRATION_TIME = 4 weeks
LEGAL_RESERVE_EXPIRATION_TIME = 7 years

[exchange-account-1]
PAYTO_URI = payto://x-taler-bank/localhost/Exchange?receiver-name=Exchange
enable_debit = yes
enable_credit = yes

[exchange-accountcredentials-1]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/Exchange/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = basic
USERNAME = Exchange
PASSWORD = password

[admin-accountcredentials-1]
WIRE_GATEWAY_URL = "http://localhost:8082/accounts/Exchange/taler-wire-gateway/"
WIRE_GATEWAY_AUTH_METHOD = basic
USERNAME = Exchange
PASSWORD = password

[merchant-exchange-default]
EXCHANGE_BASE_URL = http://localhost:8081/
CURRENCY = TESTKUDOS

[payments-generator]
currency = TESTKUDOS
instance = default
bank = http://localhost:8082/
merchant = http://localhost:9966/
exchange_admin = http://localhost:18080/
exchange-admin = http://localhost:18080/
exchange = http://localhost:8081/

[coin_kudos_ct_1]
value = TESTKUDOS:0.01
duration_withdraw = 10 s
duration_spend = 60 s
duration_legal = 2 m
fee_withdraw = TESTKUDOS:0.01
fee_deposit = TESTKUDOS:0.01
fee_refresh = TESTKUDOS:0.01
fee_refund = TESTKUDOS:0.01
rsa_keysize = 1024
CIPHER = RSA

[coin_kudos_ct_10]
value = TESTKUDOS:0.10
duration_withdraw = 10 s
duration_spend = 60 s
duration_legal = 2 m
fee_withdraw = TESTKUDOS:0.01
fee_deposit = TESTKUDOS:0.01
fee_refresh = TESTKUDOS:0.03
fee_refund = TESTKUDOS:0.01
rsa_keysize = 1024
CIPHER = RSA

[coin_kudos_50]
value = TESTKUDOS:1
duration_withdraw = 10 s
duration_spend = 60 s
duration_legal = 2 m
fee_withdraw = TESTKUDOS:0.02
fee_deposit = TESTKUDOS:0.02
fee_refresh = TESTKUDOS:0.03
fee_refund = TESTKUDOS:0.01
rsa_keysize = 1024
CIPHER = RSA
