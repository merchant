#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. setup.sh

# Launch only the merchant.
setup -c test_template.conf -m


STATUS=$(curl -H "Content-Type: application/json" -X OPTIONS \
    http://localhost:9966/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 when default instance does not exist yet. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "404" ]
then
    exit_fail "Expected 404 when the default instance is not yet created. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:other_secret"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    http://localhost:9966/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    exit_fail "Expected 401 without the token for the list of product when the default instance was created. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:other_secret' \
    http://localhost:9966/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 for the list of product when the default instance was created. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:other_secret' \
    http://localhost:9966/private/auth \
    -d '{"method":"token","token":"secret-token:zxc"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance auth token changed. got: $STATUS"
fi


STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/private" \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "401" ]
then
    exit_fail "Expected 401 without the token, when purging the instance. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:other_secret' \
    "http://localhost:9966/private" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    exit_fail "Expected 401 using old token, when purging the instance. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:zxc' \
    "http://localhost:9966/private" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 when purging the instance. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:zxc' \
    http://localhost:9966/private/products \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "404" ]
then
    exit_fail "Expected 404 when trying to list the product and the default instance was deleted. got: $STATUS"
fi

echo "Test PASSED"

exit 0
