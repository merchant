/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_webhooks.c
 * @brief command to test GET /webhooks
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET webhooks" CMD.
 */
struct GetWebhooksState
{

  /**
   * Handle for a "GET webhook" request.
   */
  struct TALER_MERCHANT_WebhooksGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * The list of webhook references.
   */
  const char **webhooks;

  /**
   * Length of @e webhooks.
   */
  unsigned int webhooks_length;

};


/**
 * Callback for a GET /webhooks operation.
 *
 * @param cls closure for this function
 * @param wgr response details
 */
static void
get_webhooks_cb (void *cls,
                 const struct TALER_MERCHANT_WebhooksGetResponse *wgr)
{
  struct GetWebhooksState *gis = cls;

  gis->igh = NULL;
  if (gis->http_status != wgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                wgr->hr.http_status,
                (int) wgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (wgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    if (wgr->details.ok.webhooks_length != gis->webhooks_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Length of webhooks found does not match\n");
      TALER_TESTING_interpreter_fail (gis->is);
      return;
    }
    for (unsigned int i = 0; i < gis->webhooks_length; ++i)
    {
      const struct TALER_TESTING_Command *webhook_cmd;

      webhook_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->webhooks[i]);

      {
        const char *webhook_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_webhook_id (webhook_cmd,
                                                &webhook_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch webhook id\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
        if (0 != strcmp (wgr->details.ok.webhooks[i].webhook_id,
                         webhook_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Webhook id does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    /* instance does not exist */
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u (%d).\n",
                wgr->hr.http_status,
                wgr->hr.ec);
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /webhooks" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_webhooks_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetWebhooksState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_webhooks_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    &get_webhooks_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET webhook" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_webhooks_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetWebhooksState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /webhooks operation did not complete\n");
    TALER_MERCHANT_webhooks_get_cancel (gis->igh);
  }
  GNUNET_array_grow (gis->webhooks,
                     gis->webhooks_length,
                     0);
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_webhooks (const char *label,
                                         const char *merchant_url,
                                         unsigned int http_status,
                                         ...)
{
  struct GetWebhooksState *gis;

  gis = GNUNET_new (struct GetWebhooksState);
  gis->merchant_url = merchant_url;
  gis->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gis->webhooks,
                           gis->webhooks_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_webhooks_run,
      .cleanup = &get_webhooks_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_webhooks.c */
