/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_delete_account.c
 * @brief command to test DELETE /account/$H_WIRE
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "DELETE /accounts/$H_WIRE" CMD.
 */
struct DeleteAccountState
{

  /**
   * Handle for a "DELETE account" request.
   */
  struct TALER_MERCHANT_AccountDeleteHandle *adh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the command to get account details from.
   */
  const char *create_account_ref;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a DELETE /account/$H_WIRE operation.
 *
 * @param cls closure for this function
 * @param adr response being processed
 */
static void
delete_account_cb (void *cls,
                   const struct TALER_MERCHANT_AccountDeleteResponse *adr)
{
  struct DeleteAccountState *das = cls;

  das->adh = NULL;
  if (das->http_status != adr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                adr->hr.http_status,
                (int) adr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (das->is));
    TALER_TESTING_interpreter_fail (das->is);
    return;
  }
  switch (adr->hr.http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for DELETE account.\n",
                adr->hr.http_status);
  }
  TALER_TESTING_interpreter_next (das->is);
}


/**
 * Run the "DELETE account" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
delete_account_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct DeleteAccountState *das = cls;
  const struct TALER_TESTING_Command *ref;
  const struct TALER_MerchantWireHashP *h_wire;
  const char *merchant_url;

  das->is = is;
  ref = TALER_TESTING_interpreter_lookup_command (is,
                                                  das->create_account_ref);
  if (NULL == ref)
  {
    GNUNET_break (0);
    TALER_TESTING_FAIL (is);
    return;
  }
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_merchant_base_url (ref,
                                                 &merchant_url))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Command %s lacked merchant base URL\n",
                das->create_account_ref);
    GNUNET_break (0);
    TALER_TESTING_FAIL (is);
    return;
  }
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_h_wires (ref,
                                       0,
                                       &h_wire))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Command %s did not return H_WIRE\n",
                das->create_account_ref);
      GNUNET_break (0);
      TALER_TESTING_FAIL (is);
      return;
  }
  GNUNET_assert (NULL != h_wire);
  das->adh = TALER_MERCHANT_account_delete (
    TALER_TESTING_interpreter_get_context (is),
    merchant_url,
    h_wire,
    &delete_account_cb,
    das);
  GNUNET_assert (NULL != das->adh);
}


/**
 * Free the state of a "DELETE account" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
delete_account_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct DeleteAccountState *das = cls;

  if (NULL != das->adh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "DELETE /accounts/$ID operation did not complete\n");
    TALER_MERCHANT_account_delete_cancel (das->adh);
  }
  GNUNET_free (das);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_account (const char *label,
                                           const char *create_account_ref,
                                           unsigned int http_status)
{
  struct DeleteAccountState *das;

  das = GNUNET_new (struct DeleteAccountState);
  das->create_account_ref = create_account_ref;
  das->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = das,
      .label = label,
      .run = &delete_account_run,
      .cleanup = &delete_account_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_delete_account.c */
