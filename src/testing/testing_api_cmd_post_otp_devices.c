/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_post_otp_devices.c
 * @brief command to test POST /otp-devices
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /otp-devices" CMD.
 */
struct PostOtpDevicesState
{

  /**
   * Handle for a "GET otp_device" request.
   */
  struct TALER_MERCHANT_OtpDevicesPostHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the otp_device to run POST for.
   */
  const char *otp_device_id;

  /**
   * description of the otp_device
   */
  const char *otp_device_description;

  /**
   * base64-encoded key
   */
  char *otp_key;

  /**
   * Option that add amount of the order
   */
  enum TALER_MerchantConfirmationAlgorithm otp_alg;

  /**
   * Counter at the OTP device.
   */
  uint64_t otp_ctr;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /otp-devices operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
post_otp_devices_cb (void *cls,
                   const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PostOtpDevicesState *tis = cls;

  tis->iph = NULL;
  if (tis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (tis->is));
    TALER_TESTING_interpreter_fail (tis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST /otp-devices.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (tis->is);
}


/**
 * Run the "POST /otp-devices" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_otp_devices_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct PostOtpDevicesState *tis = cls;

  tis->is = is;
  tis->iph = TALER_MERCHANT_otp_devices_post (
    TALER_TESTING_interpreter_get_context (is),
    tis->merchant_url,
    tis->otp_device_id,
    tis->otp_device_description,
    tis->otp_key,
    tis->otp_alg,
    tis->otp_ctr,
    &post_otp_devices_cb,
    tis);
  if (NULL == tis->iph)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (tis->is);
    return;
  }
}


/**
 * Offers information from the POST /otp-devices CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
post_otp_devices_traits (void *cls,
                       const void **ret,
                       const char *trait,
                       unsigned int index)
{
  struct PostOtpDevicesState *pts = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_otp_device_description (pts->otp_device_description),
    TALER_TESTING_make_trait_otp_key (pts->otp_key),
    TALER_TESTING_make_trait_otp_alg (&pts->otp_alg),
    TALER_TESTING_make_trait_otp_id (pts->otp_device_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST otp_device" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_otp_devices_cleanup (void *cls,
                          const struct TALER_TESTING_Command *cmd)
{
  struct PostOtpDevicesState *tis = cls;

  if (NULL != tis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /otp-devices operation did not complete\n");
    TALER_MERCHANT_otp_devices_post_cancel (tis->iph);
  }
  GNUNET_free (tis->otp_key);
  GNUNET_free (tis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_otp_devices (
  const char *label,
  const char *merchant_url,
  const char *otp_device_id,
  const char *otp_device_description,
  const char *otp_key,
  const enum TALER_MerchantConfirmationAlgorithm otp_alg,
  uint64_t otp_ctr,
  unsigned int http_status)
{
  struct PostOtpDevicesState *tis;

  tis = GNUNET_new (struct PostOtpDevicesState);
  tis->merchant_url = merchant_url;
  tis->otp_device_id = otp_device_id;
  tis->http_status = http_status;
  tis->otp_device_description = otp_device_description;
  tis->otp_key = GNUNET_strdup (otp_key);
  tis->otp_alg = otp_alg;
  tis->otp_ctr = otp_ctr;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tis,
      .label = label,
      .run = &post_otp_devices_run,
      .cleanup = &post_otp_devices_cleanup,
      .traits = &post_otp_devices_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_post_otp_devices.c */
