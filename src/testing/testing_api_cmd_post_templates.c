/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_post_templates.c
 * @brief command to test POST /templates
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /templates" CMD.
 */
struct PostTemplatesState
{

  /**
   * Handle for a "GET template" request.
   */
  struct TALER_MERCHANT_TemplatesPostHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the template to run POST for.
   */
  const char *template_id;

  /**
   * description of the template
   */
  const char *template_description;

  /**
   * OTP device ID.
   */
  char *otp_id;

  /**
   * Contract of the company
   */
  json_t *template_contract;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /templates operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
post_templates_cb (void *cls,
                   const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PostTemplatesState *tis = cls;

  tis->iph = NULL;
  if (tis->http_status != hr->http_status)
  {
    TALER_TESTING_unexpected_status_with_body (tis->is,
                                               hr->http_status,
                                               tis->http_status,
                                               hr->reply);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST /templates.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (tis->is);
}


/**
 * Run the "POST /templates" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_templates_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct PostTemplatesState *tis = cls;

  tis->is = is;
  tis->iph = TALER_MERCHANT_templates_post (
    TALER_TESTING_interpreter_get_context (is),
    tis->merchant_url,
    tis->template_id,
    tis->template_description,
    tis->otp_id,
    tis->template_contract,
    &post_templates_cb,
    tis);
  if (NULL == tis->iph)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (tis->is);
    return;
  }
}


/**
 * Offers information from the POST /templates CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
post_templates_traits (void *cls,
                       const void **ret,
                       const char *trait,
                       unsigned int index)
{
  struct PostTemplatesState *pts = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_template_description (pts->template_description),
    TALER_TESTING_make_trait_otp_id (pts->otp_id),
    TALER_TESTING_make_trait_template_contract (pts->template_contract),
    TALER_TESTING_make_trait_template_id (pts->template_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST template" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_templates_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct PostTemplatesState *tis = cls;

  if (NULL != tis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /templates operation did not complete\n");
    TALER_MERCHANT_templates_post_cancel (tis->iph);
  }
  GNUNET_free (tis->otp_id);
  json_decref (tis->template_contract);
  GNUNET_free (tis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_templates2 (
  const char *label,
  const char *merchant_url,
  const char *template_id,
  const char *template_description,
  const char *otp_id,
  json_t *template_contract,
  unsigned int http_status)
{
  struct PostTemplatesState *tis;

  GNUNET_assert ((NULL == template_contract) ||
                 json_is_object (template_contract));

  tis = GNUNET_new (struct PostTemplatesState);
  tis->merchant_url = merchant_url;
  tis->template_id = template_id;
  tis->http_status = http_status;
  tis->template_description = template_description;
  tis->otp_id = (NULL == otp_id) ? NULL : GNUNET_strdup (otp_id);
  tis->template_contract = template_contract;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = tis,
      .label = label,
      .run = &post_templates_run,
      .cleanup = &post_templates_cleanup,
      .traits = &post_templates_traits
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_templates (const char *label,
                                           const char *merchant_url,
                                           const char *template_id,
                                           const char *template_description,
                                           unsigned int http_status)
{
  return TALER_TESTING_cmd_merchant_post_templates2 (
    label,
    merchant_url,
    template_id,
    template_description,
    NULL,
    GNUNET_JSON_PACK (
      GNUNET_JSON_pack_uint64 ("minimum_age", 0),
      GNUNET_JSON_pack_time_rel ("pay_duration",
                                 GNUNET_TIME_UNIT_MINUTES)),
    http_status);
}


/* end of testing_api_cmd_post_templates.c */
