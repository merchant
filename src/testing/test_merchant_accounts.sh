#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

# Cleanup to run whenever we exit
function my_cleanup()
{
    for n in $(jobs -p)
    do
        kill "$n" 2> /dev/null || true
    done
    wait
    if [ -n "${LAST_RESPONSE+x}" ]
    then
        rm -f "${LAST_RESPONSE}"
    fi
}

. setup.sh

setup -c test_template.conf -m
CONF="test_template.conf.edited"
LAST_RESPONSE=$(mktemp -p "${TMPDIR:-/tmp}" test_response.conf-XXXXXX)

echo -n "Configuring 'default' instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:new_value"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS" >&2
fi

echo "OK" >&2

##
# Test deleting and creating the account again.
# it should bring the account active again
##

echo -n "creating first account ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=user43"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

echo "OK" >&2


ACCOUNT_ID=$(jq -r .h_wire $LAST_RESPONSE)

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi

echo -n "deleting account ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s )

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 OK. Got: $STATUS"
fi

echo "OK" >&2

echo -n "creating same account again to make it active ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=user43"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi

echo "OK" >&2

##
# Using different name should not conflict with previous account.
##

echo -n "creating same account with different name ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=not-user-43"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

ACCOUNT_ID=$(jq -r .h_wire $LAST_RESPONSE)


STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi

echo "OK" >&2

echo -n "deleting the account ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s )

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 OK. Got: $STATUS"
fi

echo "OK" >&2

echo -n "now make it active again ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=not-user-43"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi


echo " OK" >&2

##
# Activating the account again with different values should not break.
##

echo -n "creating second account ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/12?receiver-name=user12"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

echo "OK" >&2


ACCOUNT_ID=$(jq -r .h_wire $LAST_RESPONSE)

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi

echo -n "deleting second account ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s )

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 OK. Got: $STATUS"
fi

echo "OK" >&2

echo -n "make it active with different facade ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/12?receiver-name=user12", "credit_facade_credentials":{"type":"none"},"credit_facade_url":"http://asd.com/"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts/$ACCOUNT_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

ACTIVE=$(jq -r .active $LAST_RESPONSE)

if [ "$ACTIVE" != "true" ]
then
    exit_fail "Expected account active."
fi

FACADE=$(jq -r .credit_facade_url $LAST_RESPONSE)

if [ "$FACADE" != "http://asd.com/" ]
then
    exit_fail "Expected account with facade http://asd.com/."
fi

echo "OK" >&2

##
# Still, the previous activation should only work if the account is deactivated, the same as if the account was deleted.
# Trying to create when there is already an active account but with different values should return Conflict 409.
##

echo -n "should validate conflict ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/12?receiver-name=user12", "credit_facade_credentials":{"type":"none"},"credit_facade_url":"http://invalid.com/"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "409" ]
then
    exit_fail "Expected 409 Conflict. Got: $STATUS"
fi

echo "OK" >&2
