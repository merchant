/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_delete_order.c
 * @brief command to test DELETE /orders/$ORDER_ID
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "DELETE /order/$ORDER_ID" CMD.
 */
struct DeleteOrderState
{

  /**
   * Handle for a "DELETE order" request.
   */
  struct TALER_MERCHANT_OrderDeleteHandle *odh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the order to run DELETE for.
   */
  const char *order_id;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a DELETE /orders/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
delete_order_cb (void *cls,
                 const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct DeleteOrderState *dos = cls;

  dos->odh = NULL;
  if (dos->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (dos->is));
    TALER_TESTING_interpreter_fail (dos->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for DELETE order.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (dos->is);
}


/**
 * Run the "DELETE order" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
delete_order_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct DeleteOrderState *dos = cls;

  dos->is = is;
  dos->odh = TALER_MERCHANT_order_delete (
    TALER_TESTING_interpreter_get_context (is),
    dos->merchant_url,
    dos->order_id,
    false,                                       /* FIXME: support testing force... */
    &delete_order_cb,
    dos);
  GNUNET_assert (NULL != dos->odh);
}


/**
 * Free the state of a "DELETE order" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
delete_order_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct DeleteOrderState *dos = cls;

  if (NULL != dos->odh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "DELETE /orders/$ORDER_ID operation did not complete\n");
    TALER_MERCHANT_order_delete_cancel (dos->odh);
  }
  GNUNET_free (dos);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_order (const char *label,
                                         const char *merchant_url,
                                         const char *order_id,
                                         unsigned int http_status)
{
  struct DeleteOrderState *dos;

  dos = GNUNET_new (struct DeleteOrderState);
  dos->merchant_url = merchant_url;
  dos->order_id = order_id;
  dos->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = dos,
      .label = label,
      .run = &delete_order_run,
      .cleanup = &delete_order_cleanup
    };

    return cmd;
  }
}
