/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_products.c
 * @brief command to test GET /products
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET products" CMD.
 */
struct GetProductsState
{

  /**
   * Handle for a "GET product" request.
   */
  struct TALER_MERCHANT_ProductsGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * The list of product references.
   */
  const char **products;

  /**
   * Length of @e products.
   */
  unsigned int products_length;

};


/**
 * Callback for a GET /products operation.
 *
 * @param cls closure for this function
 * @param gpr response details
 */
static void
get_products_cb (void *cls,
                 const struct TALER_MERCHANT_GetProductsResponse *gpr)
{
  struct GetProductsState *gis = cls;
  const struct TALER_MERCHANT_HttpResponse *hr = &gpr->hr;

  gis->igh = NULL;
  if (gis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    {
      unsigned int products_length
        = gpr->details.ok.products_length;
      const struct TALER_MERCHANT_InventoryEntry *products
        = gpr->details.ok.products;

      if (products_length != gis->products_length)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Length of products found does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
      for (unsigned int i = 0; i < gis->products_length; ++i)
      {
        const struct TALER_TESTING_Command *product_cmd;

        product_cmd = TALER_TESTING_interpreter_lookup_command (
          gis->is,
          gis->products[i]);

        {
          const char *product_id;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_product_id (product_cmd,
                                                  &product_id))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Could not fetch product id\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
          if (0 != strcmp (products[i].product_id,
                           product_id))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Product id does not match\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    /* instance does not exist */
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u (%d).\n",
                hr->http_status,
                hr->ec);
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /products" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_products_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetProductsState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_products_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    &get_products_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET product" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_products_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetProductsState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /products operation did not complete\n");
    TALER_MERCHANT_products_get_cancel (gis->igh);
  }
  GNUNET_array_grow (gis->products,
                     gis->products_length,
                     0);
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_products (const char *label,
                                         const char *merchant_url,
                                         unsigned int http_status,
                                         ...)
{
  struct GetProductsState *gis;

  gis = GNUNET_new (struct GetProductsState);
  gis->merchant_url = merchant_url;
  gis->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gis->products,
                           gis->products_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_products_run,
      .cleanup = &get_products_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_products.c */
