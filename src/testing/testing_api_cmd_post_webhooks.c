/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_post_webhooks.c
 * @brief command to test POST /webhooks
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /webhooks" CMD.
 */
struct PostWebhooksState
{

  /**
   * Handle for a "GET webhook" request.
   */
  struct TALER_MERCHANT_WebhooksPostHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the webhook to run POST for.
   */
  const char *webhook_id;

  /**
   * event of the webhook
   */
  const char *event_type;

  /**
   * url use by the customer
   */
  const char *url;

  /**
  * http_method use by the merchant
  */
  const char *http_method;

  /**
  * header of the webhook
  */
  const char *header_template;

  /**
  * body of the webhook
  */
  const char *body_template;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /webhooks operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
post_webhooks_cb (void *cls,
                  const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PostWebhooksState *wis = cls;

  wis->iph = NULL;
  if (wis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (wis->is));
    TALER_TESTING_interpreter_fail (wis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST /templates.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (wis->is);
}


/**
 * Run the "POST /webhooks" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_webhooks_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct PostWebhooksState *wis = cls;

  wis->is = is;
  wis->iph = TALER_MERCHANT_webhooks_post (
    TALER_TESTING_interpreter_get_context (is),
    wis->merchant_url,
    wis->webhook_id,
    wis->event_type,
    wis->url,
    wis->http_method,
    wis->header_template,
    wis->body_template,
    &post_webhooks_cb,
    wis);
  GNUNET_assert (NULL != wis->iph);
}


/**
 * Offers information from the POST /webhooks CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static int
post_webhooks_traits (void *cls,
                      const void **ret,
                      const char *trait,
                      unsigned int index)
{
  struct PostWebhooksState *pws = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_event_type (pws->event_type),
    TALER_TESTING_make_trait_url (pws->url),
    TALER_TESTING_make_trait_http_method (pws->http_method),
    TALER_TESTING_make_trait_header_template (pws->header_template),
    TALER_TESTING_make_trait_body_template (pws->body_template),
    TALER_TESTING_make_trait_webhook_id (pws->webhook_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST webhook" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_webhooks_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct PostWebhooksState *wis = cls;

  if (NULL != wis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /webhooks operation did not complete\n");
    TALER_MERCHANT_webhooks_post_cancel (wis->iph);
  }
  GNUNET_free (wis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_webhooks2 (
  const char *label,
  const char *merchant_url,
  const char *webhook_id,
  const char *event_type,
  const char *url,
  const char *http_method,
  const char *header_template,
  const char *body_template,
  unsigned int http_status)
{
  struct PostWebhooksState *wis;

  wis = GNUNET_new (struct PostWebhooksState);
  wis->merchant_url = merchant_url;
  wis->webhook_id = webhook_id;
  wis->http_status = http_status;
  wis->event_type = event_type;
  wis->url = url;
  wis->http_method = http_method;
  wis->header_template = (NULL==header_template) ? NULL : header_template;
  wis->body_template = (NULL==body_template) ? NULL : body_template;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = wis,
      .label = label,
      .run = &post_webhooks_run,
      .cleanup = &post_webhooks_cleanup,
      .traits = &post_webhooks_traits
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_webhooks (const char *label,
                                          const char *merchant_url,
                                          const char *webhook_id,
                                          const char *event_type,
                                          unsigned int http_status)
{
  return TALER_TESTING_cmd_merchant_post_webhooks2 (
    label,
    merchant_url,
    webhook_id,
    event_type,
    "http://localhost:12345/",
    "POST",
    "Taler-test-header: EFEHYJS-Bakery",
    "5.0 EUR",
    http_status);
}


/* end of testing_api_cmd_post_webhooks.c */
