/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_instances.c
 * @brief command to test GET /instances
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET instances" CMD.
 */
struct GetInstancesState
{

  /**
   * Handle for a "GET instance" request.
   */
  struct TALER_MERCHANT_InstancesGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * The list of instance references to compare to.
   */
  const char **instances;

  /**
   * The length of @e instances.
   */
  unsigned int instances_length;

};


/**
 * Callback for a GET /instances operation.
 *
 * @param cls closure for this function
 * @param igr response
 */
static void
get_instances_cb (void *cls,
                  const struct TALER_MERCHANT_InstancesGetResponse *igr)
{
  const struct TALER_MERCHANT_HttpResponse *hr = &igr->hr;
  struct GetInstancesState *gis = cls;

  gis->igh = NULL;
  if (gis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    {
      unsigned int iis_length
        = igr->details.ok.iis_length;
      const struct TALER_MERCHANT_InstanceInformation *iis
        = igr->details.ok.iis;

      if (iis_length != gis->instances_length)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Length of instances found does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
      for (unsigned int i = 0; i < iis_length; ++i)
      {
        const struct TALER_TESTING_Command *instance_cmd;

        instance_cmd = TALER_TESTING_interpreter_lookup_command (
          gis->is,
          gis->instances[i]);

        {
          const char *name;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_instance_name (instance_cmd,
                                                     &name))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Could not fetch instance name\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
          if (0 != strcmp (iis[i].name,
                           name))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Instance name does not match\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
        }

        {
          const char *id;

          if (GNUNET_OK !=
              TALER_TESTING_get_trait_instance_id (instance_cmd,
                                                   &id))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Could not fetch instance id\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
          if (0 != strcmp (iis[i].id,
                           id))
          {
            GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                        "Instance id does not match\n");
            TALER_TESTING_interpreter_fail (gis->is);
            return;
          }
        }
      }

      // FIXME: compare payment_targets
      break;
    }
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for GET /instances.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /instances" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_instances_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct GetInstancesState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_instances_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    &get_instances_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET instance" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_instances_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct GetInstancesState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /instances operation did not complete\n");
    TALER_MERCHANT_instances_get_cancel (gis->igh);
  }
  GNUNET_array_grow (gis->instances,
                     gis->instances_length,
                     0);
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_instances (const char *label,
                                          const char *merchant_url,
                                          unsigned int http_status,
                                          ...)
{
  struct GetInstancesState *gis;

  gis = GNUNET_new (struct GetInstancesState);
  gis->merchant_url = merchant_url;
  gis->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gis->instances,
                           gis->instances_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_instances_run,
      .cleanup = &get_instances_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_instances.c */
