/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_template.c
 * @brief command to test GET /templates/$ID
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET template" CMD.
 */
struct GetTemplateState
{

  /**
   * Handle for a "GET template" request.
   */
  struct TALER_MERCHANT_TemplateGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the template to run GET for.
   */
  const char *template_id;

  /**
   * Reference for a POST or PATCH /templates CMD (optional).
   */
  const char *template_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /get/templates/$ID operation.
 *
 * @param cls closure for this function
 * @param tgr HTTP response details
 */
static void
get_template_cb (void *cls,
                 const struct TALER_MERCHANT_TemplateGetResponse *tgr)
{
  struct GetTemplateState *gis = cls;
  const struct TALER_TESTING_Command *template_cmd;

  gis->igh = NULL;
  if (gis->http_status != tgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                tgr->hr.http_status,
                (int) tgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (tgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    {
      const char *expected_description;

      template_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->template_reference);
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_template_description (template_cmd,
                                                        &expected_description))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (tgr->details.ok.template_description,
                       expected_description))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Template description does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_otp_id;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_otp_id (template_cmd,
                                          &expected_otp_id))
        TALER_TESTING_interpreter_fail (gis->is);
      if ( ( (NULL == tgr->details.ok.otp_id) && (NULL != expected_otp_id)) ||
           ( (NULL != tgr->details.ok.otp_id) && (NULL == expected_otp_id)) ||
           ( (NULL != tgr->details.ok.otp_id) &&
             (0 != strcmp (tgr->details.ok.otp_id,
                           expected_otp_id)) ) )
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Template pos_key `%s' does not match `%s'\n",
                    tgr->details.ok.otp_id,
                    expected_otp_id);
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const json_t *expected_template_contract;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_template_contract (template_cmd,
                                                     &expected_template_contract))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (tgr->details.ok.template_contract,
                           expected_template_contract))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Template contract does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET template" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_template_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct GetTemplateState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_template_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    gis->template_id,
    &get_template_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET template" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_template_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct GetTemplateState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /templates/$ID operation did not complete\n");
    TALER_MERCHANT_template_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_template (const char *label,
                                         const char *merchant_url,
                                         const char *template_id,
                                         unsigned int http_status,
                                         const char *template_reference)
{
  struct GetTemplateState *gis;

  gis = GNUNET_new (struct GetTemplateState);
  gis->merchant_url = merchant_url;
  gis->template_id = template_id;
  gis->http_status = http_status;
  gis->template_reference = template_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_template_run,
      .cleanup = &get_template_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_template.c */
