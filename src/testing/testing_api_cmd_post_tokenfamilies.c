/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file testing_api_cmd_post_tokenfamilies.c
 * @brief command to run POST /tokenfamilies
 * @author Christian Blättler
 */
#include "platform.h"
#include <gnunet/gnunet_time_lib.h>
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /tokenfamilies" CMD.
 */
struct PostTokenFamiliesState
{

  /**
   * Expected status code.
   */
  unsigned int http_status;

  /**
   * Handle for a "POST /tokenfamilies" request.
   */
  struct TALER_MERCHANT_TokenFamiliesPostHandle *handle;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Slug of the token family.
   */
  const char *slug;

  /**
   * Name of the token family.
   */
  const char *name;

  /**
   * Description of the token family.
   */
  const char *description;

  /**
   * Map from IETF BCP 47 language tags to localized descriptions.
   */
  json_t *description_i18n;

  /**
   * Start of the validity period.
   */
  struct GNUNET_TIME_Timestamp valid_after;

  /**
   * End of the validity period.
   */
  struct GNUNET_TIME_Timestamp valid_before;

  /**
   * Validity duation of issued tokens of this family.
   */
  struct GNUNET_TIME_Relative duration;

  /**
   * Rounding duation of token family.
   */
  struct GNUNET_TIME_Relative rounding;

  /**
   * Kind of the token family. "subscription" or "discount".
   */
  const char *kind;
};


/**
 * Callback for a POST /tokenfamilies operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
post_tokenfamilies_cb (void *cls,
                       const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PostTokenFamiliesState *state = cls;

  state->handle = NULL;
  if (state->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (state->is));
    TALER_TESTING_interpreter_fail (state->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST /tokenfamilies.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (state->is);
}


/**
 * Run the "POST /tokenfamilies" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_tokenfamilies_run (void *cls,
                        const struct TALER_TESTING_Command *cmd,
                        struct TALER_TESTING_Interpreter *is)
{
  struct PostTokenFamiliesState *state = cls;

  state->is = is;
  state->handle = TALER_MERCHANT_token_families_post (
    TALER_TESTING_interpreter_get_context (is),
    state->merchant_url,
    state->slug,
    state->name,
    state->description,
    state->description_i18n,
    NULL, /* extra data */
    state->valid_after,
    state->valid_before,
    state->duration,
    state->rounding,
    GNUNET_TIME_UNIT_ZERO, /* start_offset */
    state->kind,
    &post_tokenfamilies_cb,
    state);
  GNUNET_assert (NULL != state->handle);
}


/**
 * Offers information from the "POST /tokenfamilies" CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
post_tokenfamilies_traits (void *cls,
                           const void **ret,
                           const char *trait,
                           unsigned int index)
{
  struct PostTokenFamiliesState *state = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_token_family_slug (state->slug),
    TALER_TESTING_make_trait_timestamp (0,
                                        &state->valid_after),
    TALER_TESTING_make_trait_timestamp (1,
                                        &state->valid_before),
    TALER_TESTING_make_trait_token_family_duration (&state->duration),
    TALER_TESTING_make_trait_token_family_kind (state->kind),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST /tokenfamilies" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_tokenfamilies_cleanup (void *cls,
                            const struct TALER_TESTING_Command *cmd)
{
  struct PostTokenFamiliesState *state = cls;

  if (NULL != state->handle)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /tokenfamilies operation did not complete\n");
    TALER_MERCHANT_token_families_post_cancel (state->handle);
  }
  json_decref (state->description_i18n);
  GNUNET_free (state);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_tokenfamilies (
  const char *label,
  const char *merchant_url,
  unsigned int http_status,
  const char *slug,
  const char *name,
  const char *description,
  json_t *description_i18n,
  struct GNUNET_TIME_Timestamp valid_after,
  struct GNUNET_TIME_Timestamp valid_before,
  struct GNUNET_TIME_Relative duration,
  struct GNUNET_TIME_Relative rounding,
  const char *kind) /* "subscription" or "discount" */
{
  struct PostTokenFamiliesState *state;

  GNUNET_assert ((NULL == description_i18n) ||
                 json_is_object (description_i18n));
  state = GNUNET_new (struct PostTokenFamiliesState);
  state->merchant_url = merchant_url;
  state->http_status = http_status;
  state->slug = slug;
  state->name = name;
  state->description = description;
  state->description_i18n = description_i18n; /* ownership taken */
  state->valid_after = valid_after;
  state->valid_before = valid_before;
  state->duration = duration;
  state->rounding = rounding;
  state->kind = kind;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = state,
      .label = label,
      .run = &post_tokenfamilies_run,
      .cleanup = &post_tokenfamilies_cleanup,
      .traits = &post_tokenfamilies_traits
    };

    return cmd;
  }
}
