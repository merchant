/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_webhook.c
 * @brief command to test GET /webhooks/$ID
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET webhook" CMD.
 */
struct GetWebhookState
{

  /**
   * Handle for a "GET webhook" request.
   */
  struct TALER_MERCHANT_WebhookGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the webhook to run GET for.
   */
  const char *webhook_id;

  /**
   * Reference for a POST or PATCH /webhooks CMD (optional).
   */
  const char *webhook_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /get/webhooks/$ID operation.
 *
 * @param cls closure for this function
 * @param hr HTTP response details
 * @param event_type event of the webhook
 * @param url use by the customer
 * @param http_method method use by the merchant
 * @param header_template of the webhook
 * @param body_template of the webhook
 */
static void
get_webhook_cb (void *cls,
                const struct TALER_MERCHANT_HttpResponse *hr,
                const char *event_type,
                const char *url,
                const char *http_method,
                const char *header_template,
                const char *body_template)
{
  struct GetWebhookState *gis = cls;
  const struct TALER_TESTING_Command *webhook_cmd;

  gis->igh = NULL;
  if (gis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    {
      const char *expected_event_type;

      webhook_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->webhook_reference);
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_event_type (webhook_cmd,
                                              &expected_event_type))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (event_type,
                       expected_event_type))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Event type does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_url;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_url (webhook_cmd,
                                       &expected_url))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (url,
                       expected_url))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "URL does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_http_method;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_http_method (webhook_cmd,
                                               &expected_http_method))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (http_method,
                       expected_http_method))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "http_method does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_header_template;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_header_template (webhook_cmd,
                                                   &expected_header_template))
        TALER_TESTING_interpreter_fail (gis->is);
      if ( ( (NULL == header_template) && (NULL != expected_header_template)) ||
           ( (NULL != header_template) && (NULL == expected_header_template)) ||
           ( (NULL != header_template) &&
             (0 != strcmp (header_template,
                           expected_header_template)) ) )
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "header template does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_body_template;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_body_template (webhook_cmd,
                                                 &expected_body_template))
        TALER_TESTING_interpreter_fail (gis->is);
      if ( ( (NULL == body_template) && (NULL != expected_body_template)) ||
           ( (NULL != body_template) && (NULL == expected_body_template)) ||
           ( (NULL != body_template) &&
             (0 != strcmp (body_template,
                           expected_body_template)) ) )
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "body template does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET webhook" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_webhook_run (void *cls,
                 const struct TALER_TESTING_Command *cmd,
                 struct TALER_TESTING_Interpreter *is)
{
  struct GetWebhookState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_webhook_get (TALER_TESTING_interpreter_get_context (
                                           is),
                                         gis->merchant_url,
                                         gis->webhook_id,
                                         &get_webhook_cb,
                                         gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET webhook" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_webhook_cleanup (void *cls,
                     const struct TALER_TESTING_Command *cmd)
{
  struct GetWebhookState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /webhooks/$ID operation did not complete\n");
    TALER_MERCHANT_webhook_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_webhook (const char *label,
                                        const char *merchant_url,
                                        const char *webhook_id,
                                        unsigned int http_status,
                                        const char *webhook_reference)
{
  struct GetWebhookState *gis;

  gis = GNUNET_new (struct GetWebhookState);
  gis->merchant_url = merchant_url;
  gis->webhook_id = webhook_id;
  gis->http_status = http_status;
  gis->webhook_reference = webhook_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_webhook_run,
      .cleanup = &get_webhook_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_webhook.c */
