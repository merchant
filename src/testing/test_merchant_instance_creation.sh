#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. setup.sh

# Launch only the merchant.
setup -c test_template.conf -m


echo -n "Configuring a merchant instance before configuring the default instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:other_secret"},"id":"first","name":"test","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    echo "Expected 204, instance created. got: $STATUS"
    exit 1
fi

echo " OK"

echo -n "Configuring default instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    echo "Expected 401, permission denied. got: $STATUS"
    exit 1
fi

echo " OK"

echo -n "Configuring a second merchant instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:other_secret"},"id":"second","name":"test","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "401" ]
then
    echo "Expected 401, permission denied. got: $STATUS"
    exit 1
fi

echo " OK"

exit 0
