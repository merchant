/*
  This file is part of TALER
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_transfers.c
 * @brief command to test GET /transfers.
 * @author Marcello Stanisci
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a GET transfers CMD.
 */
struct GetTransfersState
{

  /**
   * Handle for a "get transfer" request.
   */
  struct TALER_MERCHANT_GetTransfersHandle *gth;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * payto URI of the merchant to filter by.
   */
  struct TALER_FullPayto payto_uri;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * Reference for a "check bank" CMD.  It offers the
   * WTID to get.
   */
  const char *check_bank_reference;

  /**
   * Array of POST /transfer command labels we expect to see listed.
   */
  const char **transfers;

  /**
   * Length of @e transfers.
   */
  unsigned int transfers_length;

};


/**
 * Check the result of our GET /transfers request to a merchant
 *
 * @param cls closure
 * @param gtr response details
 */
static void
get_transfers_cb (
  void *cls,
  const struct TALER_MERCHANT_GetTransfersResponse *gtr)
{
  struct GetTransfersState *gts = cls;

  gts->gth = NULL;
  if (gts->http_status != gtr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                gtr->hr.http_status,
                (int) gtr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gts->is));
    TALER_TESTING_interpreter_fail (gts->is);
    return;
  }
  switch (gtr->hr.http_status)
  {
  case MHD_HTTP_OK:
    if (gtr->details.ok.transfers_length != gts->transfers_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Transfers length does not match (got %u/want %u)\n",
                  gtr->details.ok.transfers_length,
                  gts->transfers_length);
      TALER_TESTING_interpreter_fail (gts->is);
      return;
    }
    for (unsigned int i = 0; i < gtr->details.ok.transfers_length; ++i)
    {
      const struct TALER_MERCHANT_TransferData *transfer
        = &gtr->details.ok.transfers[i];
      const struct TALER_TESTING_Command *transfer_cmd;

      transfer_cmd = TALER_TESTING_interpreter_lookup_command (
        gts->is,
        gts->transfers[i]);
      if (NULL == transfer_cmd)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Command `%s' not found!\n",
                    gts->transfers[i]);
        TALER_TESTING_interpreter_fail (gts->is);
        return;
      }
      {
        const struct TALER_WireTransferIdentifierRawP *wtid;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_wtid (transfer_cmd,
                                          &wtid))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch wire transfer id\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if (0 != GNUNET_memcmp (wtid,
                                &transfer->wtid))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Wire transfer id does not match\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        TALER_TESTING_cmd_merchant_post_transfer_set_serial (
          (struct TALER_TESTING_Command *) transfer_cmd,
          transfer->credit_serial);
      }
      {
        const struct TALER_FullPayto *payto_uri;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_credit_payto_uri (transfer_cmd,
                                                      &payto_uri))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch wire transfer payto uri\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if (0 !=
            TALER_full_payto_cmp (*payto_uri,
                                  transfer->payto_uri))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Wire transfer payto uri does not match: %s != %s\n",
                      payto_uri->full_payto,
                      transfer->payto_uri.full_payto);
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
      }
      {
        const struct TALER_Amount *credit_amount;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_amount (transfer_cmd,
                                            &credit_amount))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch wire transfer credit amount\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if ( (GNUNET_OK !=
              TALER_amount_cmp_currency (credit_amount,
                                         &transfer->credit_amount)) ||
             (0 != TALER_amount_cmp (credit_amount,
                                     &transfer->credit_amount)))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Wire transfer credit amount does not match\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
      }
      {
        const char *exchange_url;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_exchange_url (transfer_cmd,
                                                  &exchange_url))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch wire transfer exchange url\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
        if (0 != strcmp (exchange_url,
                         transfer->exchange_url))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Wire transfer exchange url does not match\n");
          TALER_TESTING_interpreter_fail (gts->is);
          return;
        }
      }
    }
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u.\n",
                gtr->hr.http_status);
    break;
  }
  TALER_TESTING_interpreter_next (gts->is);
}


/**
 * Run the "get transfer" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_transfers_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct GetTransfersState *gts = cls;

  gts->is = is;
  gts->gth = TALER_MERCHANT_transfers_get (
    TALER_TESTING_interpreter_get_context (is),
    gts->merchant_url,
    gts->payto_uri,
    GNUNET_TIME_UNIT_FOREVER_TS,
    GNUNET_TIME_UNIT_ZERO_TS,
    INT64_MAX,
    0,
    TALER_EXCHANGE_YNA_ALL,
    &get_transfers_cb,
    gts);
  GNUNET_assert (NULL != gts->gth);
}


/**
 * Free the state of a "get transfer" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_transfers_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct GetTransfersState *gts = cls;

  if (NULL != gts->gth)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /transfer operation did not complete\n");
    TALER_MERCHANT_transfers_get_cancel (gts->gth);
  }
  GNUNET_array_grow (gts->transfers,
                     gts->transfers_length,
                     0);
  GNUNET_free (gts);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_transfers (
  const char *label,
  const char *merchant_url,
  struct TALER_FullPayto payto_uri,
  unsigned int http_code,
  ...)
{
  struct GetTransfersState *gts;

  gts = GNUNET_new (struct GetTransfersState);
  gts->merchant_url = merchant_url;
  gts->payto_uri = payto_uri;
  gts->http_status = http_code;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_code);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gts->transfers,
                           gts->transfers_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gts,
      .label = label,
      .run = &get_transfers_run,
      .cleanup = &get_transfers_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_transfers.c */
