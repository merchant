/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_orders.c
 * @brief command to test GET /orders
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET orders" CMD.
 */
struct GetOrdersState
{

  /**
   * Handle for a "GET orders" request.
   */
  struct TALER_MERCHANT_OrdersGetHandle *ogh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * A NULL-terminated array of CMD labels that created orders.
   */
  const char **orders;

  /**
   * The length of @e orders.
   */
  unsigned int orders_length;

};


/**
 * Callback for a GET /orders operation.
 *
 * @param cls closure for this function
 * @param ogr response
 */
static void
get_orders_cb (void *cls,
               const struct TALER_MERCHANT_OrdersGetResponse *ogr)
{
  struct GetOrdersState *gos = cls;

  gos->ogh = NULL;
  if (gos->http_status != ogr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                ogr->hr.http_status,
                (int) ogr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gos->is));
    TALER_TESTING_interpreter_fail (gos->is);
    return;
  }
  switch (ogr->hr.http_status)
  {
  case MHD_HTTP_OK:
    if (ogr->details.ok.orders_length != gos->orders_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Number of orders found does not match\n");
      TALER_TESTING_interpreter_fail (gos->is);
      return;
    }
    for (unsigned int i = 0; i < ogr->details.ok.orders_length; ++i)
    {
      const struct TALER_MERCHANT_OrderEntry *order =
        &ogr->details.ok.orders[i];
      const struct TALER_TESTING_Command *order_cmd;

      order_cmd = TALER_TESTING_interpreter_lookup_command (
        gos->is,
        gos->orders[i]);

      {
        const char *order_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_order_id (order_cmd,
                                              &order_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch order id\n");
          TALER_TESTING_interpreter_fail (gos->is);
          return;
        }
        if (0 != strcmp (order->order_id,
                         order_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Order id does not match\n");
          TALER_TESTING_interpreter_fail (gos->is);
          return;
        }
      }
      {
        const json_t *contract_terms;
        struct TALER_Amount amount;
        const char *summary;
        struct GNUNET_JSON_Specification spec[] = {
          GNUNET_JSON_spec_string ("summary",
                                   &summary),
          TALER_JSON_spec_amount_any ("amount",
                                      &amount),
          GNUNET_JSON_spec_end ()
        };

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_contract_terms (order_cmd,
                                                    &contract_terms))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch order contract terms\n");
          TALER_TESTING_interpreter_fail (gos->is);
          return;
        }
        if (GNUNET_OK !=
            GNUNET_JSON_parse (contract_terms,
                               spec,
                               NULL, NULL))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not parse order contract terms\n");
          TALER_TESTING_interpreter_fail (gos->is);
          return;
        }
        if ((0 != strcmp (summary,
                          order->summary)) ||
            (GNUNET_OK != TALER_amount_cmp_currency (&amount,
                                                     &order->amount)) ||
            (0 != TALER_amount_cmp (&amount,
                                    &order->amount)))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Order summary and/or amount does not match\n");
          TALER_TESTING_interpreter_fail (gos->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_ACCEPTED:
    /* FIXME: do more checks here (new KYC logic!) */
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gos->is);
}


/**
 * Run the "GET /orders" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_orders_run (void *cls,
                const struct TALER_TESTING_Command *cmd,
                struct TALER_TESTING_Interpreter *is)
{
  struct GetOrdersState *gos = cls;

  gos->is = is;
  gos->ogh = TALER_MERCHANT_orders_get (TALER_TESTING_interpreter_get_context (
                                          is),
                                        gos->merchant_url,
                                        &get_orders_cb,
                                        gos);
  GNUNET_assert (NULL != gos->ogh);
}


/**
 * Free the state of a "GET orders" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_orders_cleanup (void *cls,
                    const struct TALER_TESTING_Command *cmd)
{
  struct GetOrdersState *gos = cls;

  if (NULL != gos->ogh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /orders operation did not complete\n");
    TALER_MERCHANT_orders_get_cancel (gos->ogh);
  }
  GNUNET_array_grow (gos->orders,
                     gos->orders_length,
                     0);
  GNUNET_free (gos);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_orders (const char *label,
                                       const char *merchant_url,
                                       unsigned int http_status,
                                       ...)
{
  struct GetOrdersState *gos;

  gos = GNUNET_new (struct GetOrdersState);
  gos->merchant_url = merchant_url;
  gos->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gos->orders,
                           gos->orders_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gos,
      .label = label,
      .run = &get_orders_run,
      .cleanup = &get_orders_cleanup
    };

    return cmd;
  }
}


struct MerchantPollOrdersConcludeState
{
  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Reference to a command that can provide a poll orders start command.
   */
  const char *start_reference;

  /**
   * Task to wait for the deadline.
   */
  struct GNUNET_SCHEDULER_Task *task;

  /**
   * Expected HTTP response status code.
   */
  unsigned int expected_http_status;
};


struct MerchantPollOrdersStartState
{
  /**
   * The merchant base URL.
   */
  const char *merchant_url;

  /**
   * The handle to the current GET /private/orders request.
   */
  struct TALER_MERCHANT_OrdersGetHandle *ogh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * How long to wait for server to return a response.
   */
  struct GNUNET_TIME_Relative timeout;

  /**
   * Conclude state waiting for completion (if any).
   */
  struct MerchantPollOrdersConcludeState *cs;

  /**
   * The HTTP status code returned by the backend.
   */
  unsigned int http_status;

  /**
   * When the request should be completed by.
   */
  struct GNUNET_TIME_Absolute deadline;
};


/**
 * Task called when either the timeout for the get orders
 * command expired or we got a response.  Checks if the
 * result is what we expected.
 *
 * @param cls a `struct MerchantPollOrdersConcludeState`
 */
static void
conclude_task (void *cls)
{
  struct MerchantPollOrdersConcludeState *poc = cls;
  const struct TALER_TESTING_Command *poll_cmd;
  struct MerchantPollOrdersStartState *pos;
  struct GNUNET_TIME_Absolute now;

  poc->task = NULL;
  poll_cmd =
    TALER_TESTING_interpreter_lookup_command (poc->is,
                                              poc->start_reference);
  if (NULL == poll_cmd)
    TALER_TESTING_FAIL (poc->is);
  pos = poll_cmd->cls;
  if (NULL != pos->ogh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Expected poll GET /private/orders to have completed, but it did not!\n");
    TALER_TESTING_FAIL (poc->is);
  }
  if (pos->http_status != poc->expected_http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Expected HTTP status %u, got %u\n",
                poc->expected_http_status,
                pos->http_status);
    TALER_TESTING_FAIL (poc->is);
  }
  now = GNUNET_TIME_absolute_get ();
  if (GNUNET_TIME_absolute_cmp (GNUNET_TIME_absolute_add (
                                  pos->deadline,
                                  GNUNET_TIME_UNIT_SECONDS),
                                <,
                                now))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Expected answer to be delayed until %llu, but got response at %llu\n",
                (unsigned long long) pos->deadline.abs_value_us,
                (unsigned long long) now.abs_value_us);
    TALER_TESTING_FAIL (poc->is);
  }
  TALER_TESTING_interpreter_next (poc->is);
}


/**
 * Callback to process a GET /orders request
 *
 * @param cls closure
 * @param ogr response details
 */
static void
merchant_poll_orders_cb (
  void *cls,
  const struct TALER_MERCHANT_OrdersGetResponse *ogr)
{
  struct MerchantPollOrdersStartState *pos = cls;

  pos->ogh = NULL;
  if (MHD_HTTP_OK != ogr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                ogr->hr.http_status,
                (int) ogr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (pos->is));
    TALER_TESTING_interpreter_fail (pos->is);
    return;
  }
  switch (ogr->hr.http_status)
  {
  case MHD_HTTP_OK:
    // FIXME: use order references to check if the data returned matches that from the POST / PATCH
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  pos->http_status = ogr->hr.http_status;
  if (NULL != pos->cs)
  {
    GNUNET_SCHEDULER_cancel (pos->cs->task);
    pos->cs->task = GNUNET_SCHEDULER_add_now (&conclude_task,
                                              pos->cs);
  }
}


/**
 * Run the "GET orders" CMD.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
merchant_poll_orders_start_run (void *cls,
                                const struct TALER_TESTING_Command *cmd,
                                struct TALER_TESTING_Interpreter *is)
{
  struct MerchantPollOrdersStartState *pos = cls;

  /* add 1s grace time to timeout */
  pos->deadline
    = GNUNET_TIME_relative_to_absolute (
        GNUNET_TIME_relative_add (pos->timeout,
                                  GNUNET_TIME_UNIT_SECONDS));
  pos->is = is;
  pos->ogh = TALER_MERCHANT_orders_get2 (TALER_TESTING_interpreter_get_context (
                                           is),
                                         pos->merchant_url,
                                         TALER_EXCHANGE_YNA_ALL,
                                         TALER_EXCHANGE_YNA_ALL,
                                         TALER_EXCHANGE_YNA_ALL,
                                         GNUNET_TIME_UNIT_ZERO_TS,
                                         1,
                                         2,
                                         pos->timeout,
                                         &merchant_poll_orders_cb,
                                         pos);
  GNUNET_assert (NULL != pos->ogh);
  /* We CONTINUE to run the interpreter while the long-polled command
     completes asynchronously! */
  TALER_TESTING_interpreter_next (pos->is);
}


/**
 * Free the state of a "GET orders" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
merchant_poll_orders_start_cleanup (void *cls,
                                    const struct TALER_TESTING_Command *cmd)
{
  struct MerchantPollOrdersStartState *pos = cls;

  if (NULL != pos->ogh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Command `%s' was not terminated\n",
                TALER_TESTING_interpreter_get_current_label (
                  pos->is));
    TALER_MERCHANT_orders_get_cancel (pos->ogh);
  }
  GNUNET_free (pos);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_orders_start (const char *label,
                                     const char *merchant_url,
                                     struct GNUNET_TIME_Relative timeout)
{
  struct MerchantPollOrdersStartState *pos;

  pos = GNUNET_new (struct MerchantPollOrdersStartState);
  pos->merchant_url = merchant_url;
  pos->timeout = timeout;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pos,
      .label = label,
      .run = &merchant_poll_orders_start_run,
      .cleanup = &merchant_poll_orders_start_cleanup
    };

    return cmd;
  }
}


/**
 * Wait for the "GET orders" CMD to complete.
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
merchant_poll_orders_conclude_run (void *cls,
                                   const struct TALER_TESTING_Command *cmd,
                                   struct TALER_TESTING_Interpreter *is)
{
  struct MerchantPollOrdersConcludeState *poc = cls;
  const struct TALER_TESTING_Command *poll_cmd;
  struct MerchantPollOrdersStartState *pos;

  poc->is = is;
  poll_cmd =
    TALER_TESTING_interpreter_lookup_command (is,
                                              poc->start_reference);
  if (NULL == poll_cmd)
    TALER_TESTING_FAIL (poc->is);
  GNUNET_assert (poll_cmd->run == &merchant_poll_orders_start_run);
  pos = poll_cmd->cls;
  pos->cs = poc;
  if (NULL == pos->ogh)
    poc->task = GNUNET_SCHEDULER_add_now (&conclude_task,
                                          poc);
  else
    poc->task = GNUNET_SCHEDULER_add_at (pos->deadline,
                                         &conclude_task,
                                         poc);
}


/**
 * Free the state of a "GET orders" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
merchant_poll_orders_conclude_cleanup (void *cls,
                                       const struct TALER_TESTING_Command *cmd)
{
  struct MerchantPollOrdersConcludeState *poc = cls;

  if (NULL != poc->task)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Command `%s' was not terminated\n",
                TALER_TESTING_interpreter_get_current_label (
                  poc->is));
    GNUNET_SCHEDULER_cancel (poc->task);
    poc->task = NULL;
  }
  GNUNET_free (poc);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_poll_orders_conclude (const char *label,
                                        unsigned int http_status,
                                        const char *poll_start_reference)
{
  struct MerchantPollOrdersConcludeState *poc;

  poc = GNUNET_new (struct MerchantPollOrdersConcludeState);
  poc->start_reference = poll_start_reference;
  poc->expected_http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = poc,
      .label = label,
      .run = &merchant_poll_orders_conclude_run,
      .cleanup = &merchant_poll_orders_conclude_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_orders.c */
