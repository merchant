#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

# Cleanup to run whenever we exit
function my_cleanup()
{
    for n in $(jobs -p)
    do
        kill "$n" 2> /dev/null || true
    done
    wait
    if [ -n "${LAST_RESPONSE+x}" ]
    then
        rm -f "${LAST_RESPONSE}"
    fi
}

. setup.sh

setup -c test_template.conf -m
CONF="test_template.conf.edited"
LAST_RESPONSE=$(mktemp -p "${TMPDIR:-/tmp}" test_response.conf-XXXXXX)

echo -n "Configuring 'default' instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:new_value"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS" >&2
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:new_value' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=user43"}' \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

echo " OK" >&2

# Kill merchant
kill -TERM "$SETUP_PID"
wait
unset SETUP_PID

setup -c test_template.conf \
      -ef \
      -u "exchange-account-2" \
      -r "merchant-exchange-default"

NEW_SECRET=secret-token:different_value

taler-merchant-exchangekeyupdate \
    -c "${CONF}" \
    -L DEBUG \
    2> taler-merchant-exchangekeyupdate2.log &
taler-merchant-httpd \
    -a "${NEW_SECRET}" \
    -c "${CONF}" \
    -L DEBUG \
    2> taler-merchant-httpd2.log &
# Install cleanup handler (except for kill -9)
trap my_cleanup EXIT

echo -n "Waiting for the merchant..." >&2
# Wait for merchant to be available (usually the slowest)
for n in $(seq 1 50)
do
    echo -n "." >&2
    sleep 0.1
    OK=0
    # merchant
    wget --waitretry=0 \
         --timeout=1 \
         http://localhost:9966/ \
         -o /dev/null \
         -O /dev/null \
         >/dev/null || continue
    OK=1
    break
done

if [ "x$OK" != "x1" ]
then
    exit_fail "Failed to (re)start merchant backend"
fi


echo -n "Creating order to test auth is ok..." >&2
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    'http://localhost:9966/private/orders' \
    -H 'Authorization: Bearer '"$NEW_SECRET" \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -e -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -e -r .token < "$LAST_RESPONSE")

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
     -H 'Authorization: Bearer '"$NEW_SECRET" \
     -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, getting order info before claming it. got: $STATUS"
fi

PAY_URL=$(jq -e -r .taler_pay_uri < "$LAST_RESPONSE")

echo "OK order ${ORDER_ID} with ${TOKEN} and ${PAY_URL}" >&2

echo -n "Configuring 'second' instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer '"$NEW_SECRET" \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:second"},"id":"second","name":"second","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS"
fi

echo "OK" >&2

echo -n "Updating 'second' instance token using the 'default' auth token..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer '"$NEW_SECRET" \
    http://localhost:9966/management/instances/second/auth \
    -d '{"method":"token","token":"secret-token:new_one"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance auth token changed. got: $STATUS"
fi
NEW_SECRET="secret-token:new_one"
echo " OK" >&2


echo -n "Requesting login token..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer '"$NEW_SECRET" \
    http://localhost:9966/instances/second/private/token \
    -d '{"scope":"readonly","refreshable":true}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq < "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, login token created. got: $STATUS"
fi

TOKEN=$(jq -e -r .token < "$LAST_RESPONSE")

echo " OK" >&2

echo -n "Using login token..." >&2

STATUS=$(curl "http://localhost:9966/instances/second/private/orders" \
     -H 'Authorization: Bearer '"$TOKEN" \
     -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq < "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, getting orders. got: $STATUS"
fi

echo " OK" >&2

echo -n "Refreshing login token..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer '"$TOKEN" \
    http://localhost:9966/instances/second/private/token \
    -d '{"scope":"write","refreshable":true}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "403" ]
then
    jq < "$LAST_RESPONSE" >&2
    exit_fail "Expected 403, refused to upgrade login token. got: $STATUS"
fi

echo " OK" >&2


echo -n "Deleting login token..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    -H 'Authorization: Bearer '"$TOKEN" \
    http://localhost:9966/instances/second/private/token \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "204" ]
then
    jq < "$LAST_RESPONSE" >&2
    exit_fail "Expected 204, login token deleted. got: $STATUS"
fi
echo " OK" >&2

echo -n "Using deleted login token..." >&2

STATUS=$(curl "http://localhost:9966/instances/second/private/orders" \
     -H 'Authorization: Bearer '"$TOKEN" \
     -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "401" ]
then
    jq < "$LAST_RESPONSE" >&2
    exit_fail "Expected 401, token was deleted. got: $STATUS"
fi

echo " OK" >&2


echo "Test PASSED"

exit 0
