/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_cmd_depositcheck.c
 * @brief run the taler-merchant-depositcheck command
 * @author Priscilla HUANG
 * @author Christian Grothoff
 */
#include "platform.h"
#include "taler/taler_json_lib.h"
#include <gnunet/gnunet_curl_lib.h>
#include "taler/taler_signatures.h"
#include "taler/taler_testing_lib.h"
#include "taler_merchant_testing_lib.h"


/**
 * State for a "depositcheck" CMD.
 */
struct DepositcheckState
{

  /**
   * Process for the depositcheck.
   */
  struct GNUNET_OS_Process *depositcheck_proc;

  /**
   * Configuration file used by the depositcheck.
   */
  const char *config_filename;
};


/**
 * Run the command; use the `taler-merchant-depositcheck' program.
 *
 * @param cls closure.
 * @param cmd command currently being executed.
 * @param is interpreter state.
 */
static void
depositcheck_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct DepositcheckState *ws = cls;

  (void) cmd;
  ws->depositcheck_proc
    = GNUNET_OS_start_process (GNUNET_OS_INHERIT_STD_ALL,
                               NULL, NULL, NULL,
                               "taler-merchant-depositcheck",
                               "taler-merchant-depositcheck",
                               "-c", ws->config_filename,
                               "-t", /* exit when done */
                               "-L", "DEBUG",
                               NULL);
  if (NULL == ws->depositcheck_proc)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (is);
    return;
  }
  TALER_TESTING_wait_for_sigchld (is);
}


/**
 * Free the state of a "depositcheck" CMD, and possibly
 * kills its process if it did not terminate regularly.
 *
 * @param cls closure.
 * @param cmd the command being freed.
 */
static void
depositcheck_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct DepositcheckState *ws = cls;

  (void) cmd;
  if (NULL != ws->depositcheck_proc)
  {
    GNUNET_break (0 ==
                  GNUNET_OS_process_kill (ws->depositcheck_proc,
                                          SIGKILL));
    GNUNET_OS_process_wait (ws->depositcheck_proc);
    GNUNET_OS_process_destroy (ws->depositcheck_proc);
    ws->depositcheck_proc = NULL;
  }
  GNUNET_free (ws);
}


/**
 * Offer "depositcheck" CMD internal data to other commands.
 *
 * @param cls closure.
 * @param[out] ret result.
 * @param trait name of the trait.
 * @param index index number of the object to offer.
 * @return #GNUNET_OK on success.
 */
static enum GNUNET_GenericReturnValue
depositcheck_traits (void *cls,
                     const void **ret,
                     const char *trait,
                     unsigned int index)
{
  struct DepositcheckState *ws = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_process (&ws->depositcheck_proc),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_depositcheck (const char *label,
                                const char *config_filename)
{
  struct DepositcheckState *ws;

  ws = GNUNET_new (struct DepositcheckState);
  ws->config_filename = config_filename;

  {
    struct TALER_TESTING_Command cmd = {
      .cls = ws,
      .label = label,
      .run = &depositcheck_run,
      .cleanup = &depositcheck_cleanup,
      .traits = &depositcheck_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_depositcheck.c */
