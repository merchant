/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_post_instances.c
 * @brief command to test POST /instances
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /instances" CMD.
 */
struct PostInstancesState
{

  /**
   * Handle for a "POST instance" request.
   */
  struct TALER_MERCHANT_InstancesPostHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the instance to run POST for.
   */
  const char *instance_id;

  /**
   * Name of the instance.
   */
  const char *name;

  /**
   * Address to use.
   */
  json_t *address;

  /**
   * Jurisdiction to use.
   */
  json_t *jurisdiction;

  /**
   * Authentication token to require for this instance.
   */
  const char *auth_token;

  /**
   * Use STEFAN curves?
   */
  bool use_stefan;

  /**
   * Wire transfer delay to use.
   */
  struct GNUNET_TIME_Relative default_wire_transfer_delay;

  /**
   * Order validity default duration to use.
   */
  struct GNUNET_TIME_Relative default_pay_delay;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /instances operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
post_instances_cb (
  void *cls,
  const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PostInstancesState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    TALER_TESTING_unexpected_status_with_body (
      pis->is,
      hr->http_status,
      pis->http_status,
      hr->reply);
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST instances.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "POST /instances" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_instances_run (
  void *cls,
  const struct TALER_TESTING_Command *cmd,
  struct TALER_TESTING_Interpreter *is)
{
  struct PostInstancesState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_instances_post (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->instance_id,
    pis->name,
    pis->address,
    pis->jurisdiction,
    pis->use_stefan,
    pis->default_wire_transfer_delay,
    pis->default_pay_delay,
    pis->auth_token,
    &post_instances_cb,
    pis);
  if (NULL == pis->iph)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
}


/**
 * Offers information from the POST /instances CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
post_instances_traits (void *cls,
                       const void **ret,
                       const char *trait,
                       unsigned int index)
{
  struct PostInstancesState *pis = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_instance_name (pis->name),
    TALER_TESTING_make_trait_instance_id (pis->instance_id),
    TALER_TESTING_make_trait_address (pis->address),
    TALER_TESTING_make_trait_jurisdiction (pis->jurisdiction),
    TALER_TESTING_make_trait_use_stefan (&pis->use_stefan),
    TALER_TESTING_make_trait_wire_delay (&pis->default_wire_transfer_delay),
    TALER_TESTING_make_trait_pay_delay (&pis->default_pay_delay),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST /instances" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_instances_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct PostInstancesState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /instances operation did not complete\n");
    TALER_MERCHANT_instances_post_cancel (pis->iph);
  }
  json_decref (pis->address);
  json_decref (pis->jurisdiction);
  GNUNET_free (pis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instances2 (
  const char *label,
  const char *merchant_url,
  const char *instance_id,
  const char *name,
  json_t *address,
  json_t *jurisdiction,
  bool use_stefan,
  struct GNUNET_TIME_Relative default_wire_transfer_delay,
  struct GNUNET_TIME_Relative default_pay_delay,
  const char *auth_token,
  unsigned int http_status)
{
  struct PostInstancesState *pis;

  pis = GNUNET_new (struct PostInstancesState);
  pis->merchant_url = merchant_url;
  pis->instance_id = instance_id;
  pis->http_status = http_status;
  pis->name = name;
  pis->address = address; /* ownership transfer! */
  pis->jurisdiction = jurisdiction; /* ownership transfer! */
  pis->use_stefan = use_stefan;
  pis->default_wire_transfer_delay = default_wire_transfer_delay;
  pis->default_pay_delay = default_pay_delay;
  pis->auth_token = auth_token;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &post_instances_run,
      .cleanup = &post_instances_cleanup,
      .traits = &post_instances_traits
    };

    return cmd;
  }
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instances (
  const char *label,
  const char *merchant_url,
  const char *instance_id,
  unsigned int http_status)
{
  return TALER_TESTING_cmd_merchant_post_instances2 (
    label,
    merchant_url,
    instance_id,
    instance_id,
    json_pack ("{s:s}", "city", "shopcity"),
    json_pack ("{s:s}", "city", "lawyercity"),
    true,
    GNUNET_TIME_UNIT_ZERO, /* no wire transfer delay */
    GNUNET_TIME_UNIT_MINUTES,
    NULL,
    http_status);
}


/* end of testing_api_cmd_post_instance.c */
