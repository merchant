/*
  This file is part of TALER
  Copyright (C) 2020-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_product.c
 * @brief command to test GET /product/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET product" CMD.
 */
struct GetProductState
{

  /**
   * Handle for a "GET product" request.
   */
  struct TALER_MERCHANT_ProductGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the product to run GET for.
   */
  const char *product_id;

  /**
   * Reference for a POST or PATCH /products CMD (optional).
   */
  const char *product_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /get/product/$ID operation.
 *
 * @param cls closure for this function
 * @param pgr response details
 */
static void
get_product_cb (void *cls,
                const struct TALER_MERCHANT_ProductGetResponse *pgr)
{
  struct GetProductState *gis = cls;
  const struct TALER_TESTING_Command *product_cmd;

  gis->igh = NULL;
  if (gis->http_status != pgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                pgr->hr.http_status,
                (int) pgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (pgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    {
      const char *expected_description;

      product_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->product_reference);
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_product_description (product_cmd,
                                                       &expected_description))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (pgr->details.ok.description,
                       expected_description))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product description does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const json_t *expected_description_i18n;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_i18n_description (product_cmd,
                                                    &expected_description_i18n))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (pgr->details.ok.description_i18n,
                           expected_description_i18n))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product description i18n does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct TALER_Amount *expected_price;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_amount (product_cmd,
                                          &expected_price))
        TALER_TESTING_interpreter_fail (gis->is);
      if ((GNUNET_OK !=
           TALER_amount_cmp_currency (&pgr->details.ok.price,
                                      expected_price)) ||
          (0 != TALER_amount_cmp (&pgr->details.ok.price,
                                  expected_price)))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product price does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_image;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_product_image (product_cmd,
                                                 &expected_image))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (pgr->details.ok.image,
                       expected_image))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product image does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const json_t *expected_taxes;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_taxes (product_cmd,
                                         &expected_taxes))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (pgr->details.ok.taxes,
                           expected_taxes))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product taxes do not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const char *expected_unit;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_product_unit (product_cmd,
                                                &expected_unit))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (pgr->details.ok.unit,
                       expected_unit))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product unit does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const json_t *expected_location;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_address (product_cmd,
                                           &expected_location))
        TALER_TESTING_interpreter_fail (gis->is);
      if (1 != json_equal (pgr->details.ok.location,
                           expected_location))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product location does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const int64_t *expected_total_stock;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_product_stock (product_cmd,
                                                 &expected_total_stock))
        TALER_TESTING_interpreter_fail (gis->is);
      if (pgr->details.ok.total_stock != *expected_total_stock)
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product total stock does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    {
      const struct GNUNET_TIME_Timestamp *expected_next_restock;

      if (GNUNET_OK !=
          TALER_TESTING_get_trait_timestamp (product_cmd,
                                             0,
                                             &expected_next_restock))
        TALER_TESTING_interpreter_fail (gis->is);
      if (GNUNET_TIME_timestamp_cmp (pgr->details.ok.next_restock,
                                     !=,
                                     *expected_next_restock))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "Product next restock does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET product" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_product_run (void *cls,
                 const struct TALER_TESTING_Command *cmd,
                 struct TALER_TESTING_Interpreter *is)
{
  struct GetProductState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_product_get (TALER_TESTING_interpreter_get_context (
                                           is),
                                         gis->merchant_url,
                                         gis->product_id,
                                         &get_product_cb,
                                         gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET product" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_product_cleanup (void *cls,
                     const struct TALER_TESTING_Command *cmd)
{
  struct GetProductState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /products/$ID operation did not complete\n");
    TALER_MERCHANT_product_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_product (const char *label,
                                        const char *merchant_url,
                                        const char *product_id,
                                        unsigned int http_status,
                                        const char *product_reference)
{
  struct GetProductState *gis;

  gis = GNUNET_new (struct GetProductState);
  gis->merchant_url = merchant_url;
  gis->product_id = product_id;
  gis->http_status = http_status;
  gis->product_reference = product_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_product_run,
      .cleanup = &get_product_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_product.c */
