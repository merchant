/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_instance_auth.c
 * @brief command to test /private/auth POSTing
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /instances/$ID/private/auth" CMD.
 */
struct AuthInstanceState
{

  /**
   * Handle for a "POST auth" request.
   */
  struct TALER_MERCHANT_InstanceAuthPostHandle *iaph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the instance to run GET for.
   */
  const char *instance_id;

  /**
   * Desired token. Can be NULL
   */
  const char *auth_token;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /instances/$ID/private/auth operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
auth_instance_cb (void *cls,
                  const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct AuthInstanceState *ais = cls;

  ais->iaph = NULL;
  if (ais->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (ais->is));
    TALER_TESTING_interpreter_fail (ais->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_BAD_REQUEST:
    /* likely invalid auth_token value, we do not check client-side */
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u (%d) returned from /private/auth operation.\n",
                hr->http_status,
                hr->ec);
  }
  TALER_TESTING_interpreter_next (ais->is);
}


/**
 * Run the "AUTH /instances/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
auth_instance_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct AuthInstanceState *ais = cls;

  ais->is = is;
  ais->iaph = TALER_MERCHANT_instance_auth_post (
    TALER_TESTING_interpreter_get_context (is),
    ais->merchant_url,
    ais->instance_id,
    ais->auth_token,
    &auth_instance_cb,
    ais);
  GNUNET_assert (NULL != ais->iaph);
}


/**
 * Free the state of a "POST instance auth" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
auth_instance_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct AuthInstanceState *ais = cls;

  if (NULL != ais->iaph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /instance/$ID/auth operation did not complete\n");
    TALER_MERCHANT_instance_auth_post_cancel (ais->iaph);
  }
  GNUNET_free (ais);
}


/**
 * Offer internal data to other commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
auth_instance_traits (void *cls,
                      const void **ret,
                      const char *trait,
                      unsigned int index)
{
  struct AuthInstanceState *ais = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_auth_token (ais->auth_token),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_instance_auth (const char *label,
                                               const char *merchant_url,
                                               const char *instance_id,
                                               const char *auth_token,
                                               unsigned int http_status)
{
  struct AuthInstanceState *ais;

  ais = GNUNET_new (struct AuthInstanceState);
  ais->merchant_url = merchant_url;
  ais->instance_id = instance_id;
  ais->auth_token = auth_token;
  ais->http_status = http_status;

  {
    struct TALER_TESTING_Command cmd = {
      .cls = ais,
      .label = label,
      .run = &auth_instance_run,
      .cleanup = &auth_instance_cleanup,
      .traits = &auth_instance_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_auth_instance.c */
