#!/usr/bin/env bash
# This file is part of TALER
# Copyright (C) 2014-2021 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
# This script runs test from the wallet's integration test harness.
# If the wallet is not installed, the tests are skipped.
# Only tests from the "merchant" test suite are run.

set -eu

unset XDG_DATA_HOME
unset XDG_CONFIG_HOME

. setup.sh

echo -n "Testing for libeufin-bank"
libeufin-bank --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"

echo -n "Testing for taler-harness"
taler-harness --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"


export WITH_LIBEUFIN=1
res=0
taler-harness run-integrationtests --dry --suites merchant 2&>/dev/null || res=$?

if [[ $res -ne 0 ]]; then
  echo "skipping wallet test harness"
  exit 77
fi


exec taler-harness run-integrationtests --suites merchant
