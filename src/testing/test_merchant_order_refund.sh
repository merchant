#!/bin/bash
#!/bin/bash
# This file is in the public domain.

set -eu

function clean_wallet() {
    echo rm -f "${WALLET_DB}"
    exit_cleanup
}


# Replace with 0 for nexus...
USE_FAKEBANK=1
if [ 1 = "$USE_FAKEBANK" ]
then
    ACCOUNT="exchange-account-2"
    BANK_FLAGS="-f -d x-taler-bank -u $ACCOUNT"
    BANK_URL="http://localhost:8082/"
else
    ACCOUNT="exchange-account-1"
    BANK_FLAGS="-ns -d iban -u $ACCOUNT"
    BANK_URL="http://localhost:18082/"
    echo -n "Testing for libeufin-bank"
    libeufin-bank --help >/dev/null </dev/null || exit_skip " MISSING"
    echo " FOUND"

fi

. setup.sh

echo -n "Testing for taler-harness"
taler-harness --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"

# Launch exchange, merchant and bank.
setup -c "test_template.conf" \
      -em \
      -r "merchant-exchange-default" \
      $BANK_FLAGS
LAST_RESPONSE=$(mktemp -p "${TMPDIR:-/tmp}" test_response.conf-XXXXXX)
CONF="test_template.conf.edited"
WALLET_DB=$(mktemp -p "${TMPDIR:-/tmp}" test_wallet.json-XXXXXX)
EXCHANGE_URL="http://localhost:8081/"

# Install cleanup handler (except for kill -9)
trap clean_wallet EXIT

echo -n "First prepare wallet with coins ..."
rm -f "$WALLET_DB"
taler-wallet-cli \
    --no-throttle \
    --wallet-db="$WALLET_DB" \
    api \
    --expect-success 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:99",
        corebankApiBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "${BANK_URL}" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
echo -n "."
# FIXME-MS: add logic to have nexus check immediately here.
# sleep 10
echo -n "."
# NOTE: once libeufin can do long-polling, we should
# be able to reduce the delay here and run wirewatch
# always in the background via setup
taler-exchange-wirewatch \
    -a "$ACCOUNT" \
    -L "INFO" \
    -c "$CONF" \
    -t &> taler-exchange-wirewatch.out
echo -n "."
taler-wallet-cli \
    --wallet-db="$WALLET_DB" \
    run-until-done \
    2>wallet-withdraw-finish-1.err \
    >wallet-withdraw-finish-1.out
echo " OK"

CURRENCY_COUNT=$(taler-wallet-cli --wallet-db="$WALLET_DB" balance | jq '.balances|length')
if [ "$CURRENCY_COUNT" = "0" ]
then
    exit_fail "Expected least one currency, withdrawal failed. check log."
fi

#
# CREATE INSTANCE FOR TESTING
#

echo -n "Configuring merchant instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 50000000000},"default_pay_delay":{"d_us": 60000000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected '204 No content' response. Got instead $STATUS"
fi
echo "Ok"

echo -n "Configuring merchant account ..."

if [ 1 = "$USE_FAKEBANK" ]
then
    FORTYTHREE="payto://x-taler-bank/localhost/fortythree?receiver-name=fortythree"
else
    FORTYTHREE=$(get_payto_uri fortythree x)
fi
# create with 2 bank account addresses
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"'"$FORTYTHREE"'"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected '200 OK' response. Got instead $STATUS"
fi

echo "Ok"


#
# CREATE ORDER AND SELL IT
#

echo -n "Creating order to be paid..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:5","summary":"payme","auto_refund":{"d_us":180000000}}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -e -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -e -r .token < "$LAST_RESPONSE")

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, getting order info before claming it. got: $STATUS"
fi

PAY_URL=$(jq -e -r .taler_pay_uri < "$LAST_RESPONSE")

echo "OK"

#
# PAY THE ORDER
#
# set -x
PAYMENT_START=$(date +%s)
echo "Pay first order ${PAY_URL} ..."
taler-wallet-cli --no-throttle -V --wallet-db="$WALLET_DB" handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log
PAYMENT_END=$(date +%s)
echo " OK (payment took $(( PAYMENT_END - PAYMENT_START )) secs )"

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, after pay. got: $STATUS"
fi

ORDER_STATUS=$(jq -r .order_status < "$LAST_RESPONSE")

if [ "$ORDER_STATUS" != "paid" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Order status should be 'paid'. got: $ORDER_STATUS"
fi


echo Sending refund for TESTKUDOS:1

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}/refund" \
    -d '{"refund":"TESTKUDOS:1","reason":"duplicated"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, after refund. got: $STATUS"
fi

REFUND_URI=$(jq -e -r .taler_refund_uri < "$LAST_RESPONSE")

REFUND_START=$(date +%s)
echo "First refund order ${REFUND_URI} ..."
set -x
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" handle-uri "${REFUND_URI}" -y 2> wallet-refund1.err > wallet-refund1.log
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" run-pending -y 2> wallet-pending-refund1.err > wallet-pending-refund1.log
REFUND_END=$(date +%s)
echo " OK (refund1 took $(( REFUND_END - REFUND_START )) secs )"

echo Increasing refund for TESTKUDOS:3

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}/refund" \
    -d '{"refund":"TESTKUDOS:5","reason":"duplicated"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, after refund. got: $STATUS"
fi

REFUND_URI=$(jq -e -r .taler_refund_uri < "$LAST_RESPONSE")

REFUND2_START=$(date +%s)
echo "Second refund order ${REFUND_URI} ..."
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" handle-uri "${REFUND_URI}" -y 2> wallet-refund2.err > wallet-refund2.log
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" run-pending -y 2> wallet-pending-refund2.err > wallet-pending-refund2.log
REFUND2_END=$(date +%s)
echo " OK (refund2 took $(( REFUND2_END - REFUND_START )) secs )"

exit 0
