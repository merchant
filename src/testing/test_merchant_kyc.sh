#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
set -eu

. setup.sh


# Launch system.
setup \
    -c "test_template.conf" \
    -mef \
    -r "merchant-exchange-default" \
    -u "exchange-account-2"
LAST_RESPONSE=$(mktemp -p "${TMPDIR:-/tmp}" test_response.conf-XXXXXX)

echo -n "Configuring a merchant default instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 50000000},"default_pay_delay":{"d_us": 60000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 ok, instance created. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/43?receiver-name=user43"}' \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://x-taler-bank/localhost:8082/44?receiver-name=user44"}' \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 OK. Got: $STATUS"
fi

echo " OK"

echo -n "Check the instance exists ..."

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    http://localhost:9966/private/ \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200 ok, instance exists. got: $STATUS"
fi

echo " OK"

RANDOM_IMG='data:image/png;base64,abcdefg'

#
# CREATE AN ORDER WITHOUT TOKEN
#

echo -n "Creating order without TOKEN..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"create_token":false,"order":{"amount":"TESTKUDOS:7","summary":"3","products":[{"description":"desct","image":"'"$RANDOM_IMG"'","price":"TESTKUDOS:1","taxes":[],"unit":"u","quantity":1}]}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    echo "Should respond 200 OK, order created. got: $STATUS"
    jq < "$LAST_RESPONSE"
    exit 1
fi

ORDER_ID=$(jq -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -r .token < "$LAST_RESPONSE")

if [ "$TOKEN" != "null" ]
then
    exit_fail "Token should be null, got: $TOKEN"
fi

echo "OK"

echo -n "Checking created order without TOKEN..."

STATUS=$(curl http://localhost:9966/orders/$ORDER_ID \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

PAY_URI=$(jq -r .taler_pay_uri < "$LAST_RESPONSE")

if [ "$PAY_URI" == "null" ]
then
    cat "$LAST_RESPONSE"
    exit_fail "Expected a taler_pay_uri. Got: $PAY_URI"
fi
echo "OK"


echo -n "Getting information about KYC ..."

STATUS=$(curl -H "Content-Type: application/json" -X GET \
    http://localhost:9966/private/kyc \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200. got: $STATUS"
fi

echo " OK"

exit 0
