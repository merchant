/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_post_account.c
 * @brief command to test POST /account
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /account" CMD.
 */
struct PostAccountState
{

  /**
   * Handle for a "GET product" request.
   */
  struct TALER_MERCHANT_AccountsPostHandle *aph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Wire hash of the created account, set on success.
   */
  struct TALER_MerchantWireHashP h_wire;

  /**
   * RFC 8905 URI for the account to create.
   */
  struct TALER_FullPayto payto_uri;

  /**
   * Credit facade URL for the account to create.
   */
  char *credit_facade_url;

  /**
   * Credit facade credentials for the account to create.
   */
  json_t *credit_facade_credentials;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /account operation.
 *
 * @param cls closure for this function
 * @param apr response being processed
 */
static void
post_account_cb (void *cls,
                 const struct TALER_MERCHANT_AccountsPostResponse *apr)
{
  struct PostAccountState *pas = cls;

  pas->aph = NULL;
  if (pas->http_status != apr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                apr->hr.http_status,
                (int) apr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (pas->is));
    TALER_TESTING_interpreter_fail (pas->is);
    return;
  }
  switch (apr->hr.http_status)
  {
  case MHD_HTTP_OK:
    pas->h_wire = apr->details.ok.h_wire;
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for POST /account.\n",
                apr->hr.http_status);
  }
  TALER_TESTING_interpreter_next (pas->is);
}


/**
 * Run the "POST /account" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
post_account_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct PostAccountState *pas = cls;

  pas->is = is;
  pas->aph = TALER_MERCHANT_accounts_post (
    TALER_TESTING_interpreter_get_context (is),
    pas->merchant_url,
    pas->payto_uri,
    pas->credit_facade_url,
    pas->credit_facade_credentials,
    &post_account_cb,
    pas);
  GNUNET_assert (NULL != pas->aph);
}


/**
 * Offers information from the POST /account CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
post_account_traits (void *cls,
                     const void **ret,
                     const char *trait,
                     unsigned int index)
{
  struct PostAccountState *pps = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_h_wires (
      0,
      &pps->h_wire),
    TALER_TESTING_make_trait_h_wire (
      &pps->h_wire),
    TALER_TESTING_make_trait_payto_uris (
      0,
      &pps->payto_uri),
    TALER_TESTING_make_trait_merchant_base_url (
      pps->merchant_url),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "POST product" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
post_account_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct PostAccountState *pas = cls;

  if (NULL != pas->aph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /account operation did not complete\n");
    TALER_MERCHANT_accounts_post_cancel (pas->aph);
  }
  GNUNET_free (pas->payto_uri.full_payto);
  GNUNET_free (pas->credit_facade_url);
  json_decref (pas->credit_facade_credentials);
  GNUNET_free (pas);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_post_account (
  const char *label,
  const char *merchant_url,
  struct TALER_FullPayto payto_uri,
  const char *credit_facade_url,
  const json_t *credit_facade_credentials,
  unsigned int http_status)
{
  struct PostAccountState *pas;

  pas = GNUNET_new (struct PostAccountState);
  pas->merchant_url = merchant_url;
  pas->payto_uri.full_payto
    = GNUNET_strdup (payto_uri.full_payto);
  if (NULL != credit_facade_url)
    pas->credit_facade_url = GNUNET_strdup (credit_facade_url);
  if (NULL != credit_facade_credentials)
    pas->credit_facade_credentials
      = json_incref ((json_t *) credit_facade_credentials);
  pas->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pas,
      .label = label,
      .run = &post_account_run,
      .cleanup = &post_account_cleanup,
      .traits = &post_account_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_post_account.c */
