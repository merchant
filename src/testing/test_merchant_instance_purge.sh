#!/bin/bash
# This file is part of TALER
# Copyright (C) 2014-2023 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#

. setup.sh

# Launch only the merchant.
setup -c test_template.conf -m

echo -n "Configuring default instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS"
fi

echo " OK" >&2

echo -n "Configuring merchant instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"token","token":"secret-token:other_secret"},"id":"test","name":"test","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 3600000000},"default_pay_delay":{"d_us": 3600000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance created. got: $STATUS"
fi
echo " OK" >&2

echo -n "Deleting instance ..." >&2
STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/management/instances/test" \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance deleted. got: $STATUS"
fi

echo " OK" >&2
echo -n "Purging instance ..." >&2

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/management/instances/test?purge=yes" \
    -w "%{http_code}" -s -o /dev/null)


if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, instance deleted. got: $STATUS"
fi

echo " OK" >&2
echo "Test PASSED"

exit 0
