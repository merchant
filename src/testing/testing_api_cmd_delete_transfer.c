/*
  This file is part of TALER
  Copyright (C) 2021 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_delete_transfer.c
 * @brief command to test DELETE /transfers/$TRANSFER_ID
 * @author Jonathan Buchanan
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "DELETE /transfer/$TRANSFER_ID" CMD.
 */
struct DeleteTransferState
{

  /**
   * Handle for a "DELETE transfer" request.
   */
  struct TALER_MERCHANT_TransferDeleteHandle *tdh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Ref to cmd with ID of the transfer to run DELETE for.
   */
  const char *transfer_ref;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a DELETE /transfers/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
delete_transfer_cb (void *cls,
                    const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct DeleteTransferState *dts = cls;

  dts->tdh = NULL;
  if (dts->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (dts->is));
    TALER_TESTING_interpreter_fail (dts->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for DELETE transfer.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (dts->is);
}


/**
 * Run the "DELETE transfer" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
delete_transfer_run (void *cls,
                     const struct TALER_TESTING_Command *cmd,
                     struct TALER_TESTING_Interpreter *is)
{
  struct DeleteTransferState *dts = cls;
  const struct TALER_TESTING_Command *ref;
  const uint64_t *tid;

  dts->is = is;
  ref = TALER_TESTING_interpreter_lookup_command (is,
                                                  dts->transfer_ref);
  if (NULL == ref)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (dts->is);
    return;
  }
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_bank_row (ref,
                                        &tid))
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (dts->is);
    return;
  }
  if (0 == tid)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (dts->is);
    return;
  }
  dts->tdh = TALER_MERCHANT_transfer_delete (
    TALER_TESTING_interpreter_get_context (is),
    dts->merchant_url,
    *tid,
    &delete_transfer_cb,
    dts);
  GNUNET_assert (NULL != dts->tdh);
}


/**
 * Free the state of a "DELETE transfer" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
delete_transfer_cleanup (void *cls,
                         const struct TALER_TESTING_Command *cmd)
{
  struct DeleteTransferState *dts = cls;

  if (NULL != dts->tdh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "DELETE /transfers/$TRANSFER_ID operation did not complete\n");
    TALER_MERCHANT_transfer_delete_cancel (dts->tdh);
  }
  GNUNET_free (dts);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_transfer (const char *label,
                                            const char *merchant_url,
                                            const char *transfer_ref,
                                            unsigned int http_status)
{
  struct DeleteTransferState *dts;

  dts = GNUNET_new (struct DeleteTransferState);
  dts->merchant_url = merchant_url;
  dts->transfer_ref = transfer_ref;
  dts->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = dts,
      .label = label,
      .run = &delete_transfer_run,
      .cleanup = &delete_transfer_cleanup
    };

    return cmd;
  }
}
