/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_otp_device.c
 * @brief command to test GET /otp-devices/$ID
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET OTP device" CMD.
 */
struct GetOtpDeviceState
{

  /**
   * Handle for a "GET /otp-device/$ID" request.
   */
  struct TALER_MERCHANT_OtpDeviceGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the otp_device to run GET for.
   */
  const char *otp_device_id;

  /**
   * Reference for a POST or PATCH /otp-devices CMD (optional).
   */
  const char *otp_device_reference;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a GET /otp-devices/$ID operation.
 *
 * @param cls closure for this function
 * @param tgr HTTP response details
 */
static void
get_otp_device_cb (void *cls,
                   const struct TALER_MERCHANT_OtpDeviceGetResponse *tgr)
{
  struct GetOtpDeviceState *gis = cls;
  const struct TALER_TESTING_Command *otp_device_cmd;

  gis->igh = NULL;
  if (gis->http_status != tgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                tgr->hr.http_status,
                (int) tgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (tgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    {
      const char *expected_description;

      otp_device_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->otp_device_reference);
      if (GNUNET_OK !=
          TALER_TESTING_get_trait_otp_device_description (otp_device_cmd,
                                                          &expected_description))
        TALER_TESTING_interpreter_fail (gis->is);
      if (0 != strcmp (tgr->details.ok.otp_device_description,
                       expected_description))
      {
        GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                    "OtpDevice description does not match\n");
        TALER_TESTING_interpreter_fail (gis->is);
        return;
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status.\n");
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /otp-device/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_otp_device_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct GetOtpDeviceState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_otp_device_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    gis->otp_device_id,
    &get_otp_device_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET /otp-device/$ID" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_otp_device_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct GetOtpDeviceState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /otp-devices/$ID operation did not complete\n");
    TALER_MERCHANT_otp_device_get_cancel (gis->igh);
  }
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_otp_device (
  const char *label,
  const char *merchant_url,
  const char *otp_device_id,
  unsigned int http_status,
  const char *otp_device_reference)
{
  struct GetOtpDeviceState *gis;

  gis = GNUNET_new (struct GetOtpDeviceState);
  gis->merchant_url = merchant_url;
  gis->otp_device_id = otp_device_id;
  gis->http_status = http_status;
  gis->otp_device_reference = otp_device_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_otp_device_run,
      .cleanup = &get_otp_device_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_otp_device.c */
