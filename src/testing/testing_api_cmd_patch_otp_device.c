/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_patch_otp_device.c
 * @brief command to test PATCH /otp-device
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "PATCH /otp-device" CMD.
 */
struct PatchOtpDeviceState
{

  /**
   * Handle for a "GET otp_device" request.
   */
  struct TALER_MERCHANT_OtpDevicePatchHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the otp_device to run GET for.
   */
  const char *otp_device_id;

  /**
   * description of the otp_device
   */
  const char *otp_device_description;

  /**
   * base64-encoded key
   */
  char *otp_key;

  /**
   * Algorithm used by the OTP device
   */
  enum TALER_MerchantConfirmationAlgorithm otp_alg;

  /**
   * Counter of the device (if in counter mode).
   */
  uint64_t otp_ctr;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a PATCH /otp-devices/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
patch_otp_device_cb (void *cls,
                     const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PatchOtpDeviceState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (pis->is));
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for PATCH /otp-devices/ID.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "PATCH /otp-devices/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
patch_otp_device_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct PatchOtpDeviceState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_otp_device_patch (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->otp_device_id,
    pis->otp_device_description,
    pis->otp_key,
    pis->otp_alg,
    pis->otp_ctr,
    &patch_otp_device_cb,
    pis);
  GNUNET_assert (NULL != pis->iph);
}


/**
 * Offers information from the PATCH /otp-devices CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
patch_otp_device_traits (void *cls,
                         const void **ret,
                         const char *trait,
                         unsigned int index)
{
  struct PatchOtpDeviceState *pts = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_otp_device_description (pts->otp_device_description),
    TALER_TESTING_make_trait_otp_key (pts->otp_key),
    TALER_TESTING_make_trait_otp_alg (&pts->otp_alg),
    TALER_TESTING_make_trait_otp_id (pts->otp_device_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "GET otp_device" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
patch_otp_device_cleanup (void *cls,
                          const struct TALER_TESTING_Command *cmd)
{
  struct PatchOtpDeviceState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "PATCH /otp-devices/$ID operation did not complete\n");
    TALER_MERCHANT_otp_device_patch_cancel (pis->iph);
  }
  GNUNET_free (pis->otp_key);
  GNUNET_free (pis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_otp_device (
  const char *label,
  const char *merchant_url,
  const char *otp_device_id,
  const char *otp_device_description,
  const char *otp_key,
  const enum TALER_MerchantConfirmationAlgorithm otp_alg,
  uint64_t otp_ctr,
  unsigned int http_status)
{
  struct PatchOtpDeviceState *pis;

  pis = GNUNET_new (struct PatchOtpDeviceState);
  pis->merchant_url = merchant_url;
  pis->otp_device_id = otp_device_id;
  pis->http_status = http_status;
  pis->otp_device_description = otp_device_description;
  pis->otp_key = GNUNET_strdup (otp_key);
  pis->otp_alg = otp_alg;
  pis->otp_ctr = otp_ctr;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &patch_otp_device_run,
      .cleanup = &patch_otp_device_cleanup,
      .traits = &patch_otp_device_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_patch_otp_device.c */
