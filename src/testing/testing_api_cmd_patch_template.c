/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_patch_template.c
 * @brief command to test PATCH /template
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "PATCH /template" CMD.
 */
struct PatchTemplateState
{

  /**
   * Handle for a "GET template" request.
   */
  struct TALER_MERCHANT_TemplatePatchHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the template to run GET for.
   */
  const char *template_id;

  /**
   * description of the template
   */
  const char *template_description;

  /**
   * OTP device ID
   */
  char *otp_id;

  /**
   * Contract of the company
   */
  json_t *template_contract;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a PATCH /templates/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
patch_template_cb (void *cls,
                   const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PatchTemplateState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (pis->is));
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for PATCH /templates/ID.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "PATCH /templates/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
patch_template_run (void *cls,
                    const struct TALER_TESTING_Command *cmd,
                    struct TALER_TESTING_Interpreter *is)
{
  struct PatchTemplateState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_template_patch (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->template_id,
    pis->template_description,
    pis->otp_id,
    pis->template_contract,
    &patch_template_cb,
    pis);
  GNUNET_assert (NULL != pis->iph);
}


/**
 * Offers information from the PATCH /templates CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
patch_template_traits (void *cls,
                       const void **ret,
                       const char *trait,
                       unsigned int index)
{
  struct PatchTemplateState *pts = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_template_description (pts->template_description),
    TALER_TESTING_make_trait_otp_id (pts->otp_id),
    TALER_TESTING_make_trait_template_contract (pts->template_contract),
    TALER_TESTING_make_trait_template_id (pts->template_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "GET template" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
patch_template_cleanup (void *cls,
                        const struct TALER_TESTING_Command *cmd)
{
  struct PatchTemplateState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "PATCH /templates/$ID operation did not complete\n");
    TALER_MERCHANT_template_patch_cancel (pis->iph);
  }
  GNUNET_free (pis->otp_id);
  json_decref (pis->template_contract);
  GNUNET_free (pis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_template (
  const char *label,
  const char *merchant_url,
  const char *template_id,
  const char *template_description,
  const char *otp_id,
  json_t *template_contract,
  unsigned int http_status)
{
  struct PatchTemplateState *pis;

  pis = GNUNET_new (struct PatchTemplateState);
  pis->merchant_url = merchant_url;
  pis->template_id = template_id;
  pis->http_status = http_status;
  pis->template_description = template_description;
  pis->otp_id = (NULL == otp_id) ? NULL : GNUNET_strdup (otp_id);
  pis->template_contract = template_contract; /* ownership taken */
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &patch_template_run,
      .cleanup = &patch_template_cleanup,
      .traits = &patch_template_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_patch_template.c */
