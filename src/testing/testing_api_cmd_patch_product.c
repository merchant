/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_patch_product.c
 * @brief command to test PATCH /product
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "PATCH /product" CMD.
 */
struct PatchProductState
{

  /**
   * Handle for a "GET product" request.
   */
  struct TALER_MERCHANT_ProductPatchHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the product to run GET for.
   */
  const char *product_id;

  /**
   * description of the product
   */
  const char *description;

  /**
   * Map from IETF BCP 47 language tags to localized descriptions
   */
  json_t *description_i18n;

  /**
   * unit in which the product is measured (liters, kilograms, packages, etc.)
   */
  const char *unit;

  /**
   * the price for one @a unit of the product
   */
  struct TALER_Amount price;

  /**
   * base64-encoded product image
   */
  char *image;

  /**
   * list of taxes paid by the merchant
   */
  json_t *taxes;

  /**
   * in @e units, -1 to indicate "infinite" (i.e. electronic books)
   */
  int64_t total_stock;

  /**
   * in @e units.
   */
  int64_t total_lost;

  /**
   * where the product is in stock
   */
  json_t *address;

  /**
   * when the next restocking is expected to happen, 0 for unknown,
   */
  struct GNUNET_TIME_Timestamp next_restock;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a PATCH /products/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
patch_product_cb (void *cls,
                  const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PatchProductState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (pis->is));
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for PATCH /products/ID.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "PATCH /products/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
patch_product_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct PatchProductState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_product_patch (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->product_id,
    pis->description,
    pis->description_i18n,
    pis->unit,
    &pis->price,
    pis->image,
    pis->taxes,
    pis->total_stock,
    pis->total_lost,
    pis->address,
    pis->next_restock,
    &patch_product_cb,
    pis);
  GNUNET_assert (NULL != pis->iph);
}


/**
 * Offers information from the PATCH /products CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
patch_product_traits (void *cls,
                      const void **ret,
                      const char *trait,
                      unsigned int index)
{
  struct PatchProductState *pps = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_product_description (pps->description),
    TALER_TESTING_make_trait_i18n_description (pps->description_i18n),
    TALER_TESTING_make_trait_product_unit (pps->unit),
    TALER_TESTING_make_trait_amount (&pps->price),
    TALER_TESTING_make_trait_product_image (pps->image),
    TALER_TESTING_make_trait_taxes (pps->taxes),
    TALER_TESTING_make_trait_product_stock (&pps->total_stock),
    TALER_TESTING_make_trait_address (pps->address),
    TALER_TESTING_make_trait_timestamp (0,
                                        &pps->next_restock),
    TALER_TESTING_make_trait_product_id (pps->product_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "GET product" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
patch_product_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct PatchProductState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "PATCH /products/$ID operation did not complete\n");
    TALER_MERCHANT_product_patch_cancel (pis->iph);
  }
  json_decref (pis->description_i18n);
  GNUNET_free (pis->image);
  json_decref (pis->taxes);
  json_decref (pis->address);
  GNUNET_free (pis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_product (
  const char *label,
  const char *merchant_url,
  const char *product_id,
  const char *description,
  json_t *description_i18n,
  const char *unit,
  const char *price,
  const char *image,
  json_t *taxes,
  int64_t total_stock,
  uint64_t total_lost,
  json_t *address,
  struct GNUNET_TIME_Timestamp next_restock,
  unsigned int http_status)
{
  struct PatchProductState *pis;

  GNUNET_assert ( (NULL == taxes) ||
                  json_is_array (taxes));
  pis = GNUNET_new (struct PatchProductState);
  pis->merchant_url = merchant_url;
  pis->product_id = product_id;
  pis->http_status = http_status;
  pis->description = description;
  pis->description_i18n = description_i18n; /* ownership taken */
  pis->unit = unit;
  GNUNET_assert (GNUNET_OK ==
                 TALER_string_to_amount (price,
                                         &pis->price));
  pis->image = GNUNET_strdup (image);
  pis->taxes = taxes; /* ownership taken */
  pis->total_stock = total_stock;
  pis->total_lost = total_lost;
  pis->address = address; /* ownership taken */
  pis->next_restock = next_restock;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &patch_product_run,
      .cleanup = &patch_product_cleanup,
      .traits = &patch_product_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_patch_product.c */
