/*
  This file is part of TALER
  Copyright (C) 2020 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_lock_product.c
 * @brief command to test LOCK /product
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "POST /products/$ID" CMD.
 */
struct LockProductState
{

  /**
   * Handle for a "GET product" request.
   */
  struct TALER_MERCHANT_ProductLockHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the product to run GET for.
   */
  const char *product_id;

  /**
   * UUID that identifies the client holding the lock
   */
  char *uuid;

  /**
   * duration how long should the lock be held
   */
  struct GNUNET_TIME_Relative duration;

  /**
   * how much product should be locked
   */
  uint32_t quantity;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a POST /products/$ID/lock operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
lock_product_cb (void *cls,
                 const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct LockProductState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (pis->is));
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_GONE:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for lock product.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "LOCK /products/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
lock_product_run (void *cls,
                  const struct TALER_TESTING_Command *cmd,
                  struct TALER_TESTING_Interpreter *is)
{
  struct LockProductState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_product_lock (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->product_id,
    pis->uuid,
    pis->duration,
    pis->quantity,
    &lock_product_cb,
    pis);
  GNUNET_assert (NULL != pis->iph);
}


/**
 * Free the state of a "GET product" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
lock_product_cleanup (void *cls,
                      const struct TALER_TESTING_Command *cmd)
{
  struct LockProductState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "POST /product/$ID/lock operation did not complete\n");
    TALER_MERCHANT_product_lock_cancel (pis->iph);
  }
  GNUNET_free (pis->uuid);
  GNUNET_free (pis);
}


/**
 * Offer internal data to other commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
lock_product_traits (void *cls,
                     const void **ret,
                     const char *trait,
                     unsigned int index)
{
  struct LockProductState *lps = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_lock_uuid (lps->uuid),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_lock_product (
  const char *label,
  const char *merchant_url,
  const char *product_id,
  struct GNUNET_TIME_Relative duration,
  uint32_t quantity,
  unsigned int http_status)
{
  struct LockProductState *pis;
  struct GNUNET_Uuid uuid;

  pis = GNUNET_new (struct LockProductState);
  pis->merchant_url = merchant_url;
  pis->product_id = product_id;
  pis->http_status = http_status;
  GNUNET_CRYPTO_random_block (GNUNET_CRYPTO_QUALITY_WEAK,
                              &uuid,
                              sizeof (struct GNUNET_Uuid));
  pis->uuid = GNUNET_STRINGS_data_to_string_alloc (&uuid,
                                                   sizeof (uuid));
  pis->duration = duration;
  pis->quantity = quantity;

  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &lock_product_run,
      .cleanup = &lock_product_cleanup,
      .traits = &lock_product_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_lock_product.c */
