/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_delete_template.c
 * @brief command to test DELETE /templates/$ID
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "DELETE /templates/$ID" CMD.
 */
struct DeleteTemplateState
{

  /**
   * Handle for a "DELETE template" request.
   */
  struct TALER_MERCHANT_TemplateDeleteHandle *tdh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the template to run DELETE for.
   */
  const char *template_id;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a /delete/templates/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
delete_template_cb (void *cls,
                    const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct DeleteTemplateState *dis = cls;

  dis->tdh = NULL;
  if (dis->http_status != hr->http_status)
  {
    TALER_TESTING_unexpected_status_with_body (dis->is,
                                               hr->http_status,
                                               dis->http_status,
                                               hr->reply);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for DELETE template.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (dis->is);
}


/**
 * Run the "DELETE template" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
delete_template_run (void *cls,
                     const struct TALER_TESTING_Command *cmd,
                     struct TALER_TESTING_Interpreter *is)
{
  struct DeleteTemplateState *dis = cls;

  dis->is = is;
  dis->tdh = TALER_MERCHANT_template_delete (
    TALER_TESTING_interpreter_get_context (is),
    dis->merchant_url,
    dis->template_id,
    &delete_template_cb,
    dis);
  GNUNET_assert (NULL != dis->tdh);
}


/**
 * Free the state of a "DELETE template" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
delete_template_cleanup (void *cls,
                         const struct TALER_TESTING_Command *cmd)
{
  struct DeleteTemplateState *dis = cls;

  if (NULL != dis->tdh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "DELETE /templates/$ID operation did not complete\n");
    TALER_MERCHANT_template_delete_cancel (dis->tdh);
  }
  GNUNET_free (dis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_delete_template (const char *label,
                                            const char *merchant_url,
                                            const char *template_id,
                                            unsigned int http_status)
{
  struct DeleteTemplateState *dis;

  dis = GNUNET_new (struct DeleteTemplateState);
  dis->merchant_url = merchant_url;
  dis->template_id = template_id;
  dis->http_status = http_status;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = dis,
      .label = label,
      .run = &delete_template_run,
      .cleanup = &delete_template_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_delete_template.c */
