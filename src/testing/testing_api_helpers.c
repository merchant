/*
  This file is part of TALER
  Copyright (C) 2014-2018 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file testing_api_helpers.c
 * @brief helper functions for test library.
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_testing_lib.h"


char *
TALER_MERCHANT_TESTING_extract_host (const char *merchant_url)
{
  const char *hosts = strchr (merchant_url, '/');
  const char *hend;
  const char *pstr;
  const char *pend;
  char *host;

  if (NULL == hosts)
  {
    GNUNET_break (0);
    return NULL;
  }
  if (hosts[1] != '/')
  {
    GNUNET_break (0);
    return NULL;
  }
  hosts += 2;
  pstr = strchr (hosts, ':');
  if (NULL == pstr)
  {
    hend = &hosts[strlen (hosts)];
    pstr = "80";
    pend = &pstr[2];
  }
  else
  {
    hend = pstr;
    pstr++;
    pend = strchr (pstr, '/');
  }
  GNUNET_asprintf (&host,
                   "%.*s:%.*s",
                   (int) (hend - hosts),
                   hosts,
                   (int) (pend - pstr),
                   pstr);
  return host;
}
