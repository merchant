/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_get_otp_devices.c
 * @brief command to test GET /otp-devices
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "GET /otp-devices" CMD.
 */
struct GetOtpDevicesState
{

  /**
   * Handle for a "GET /otp-devices" request.
   */
  struct TALER_MERCHANT_OtpDevicesGetHandle *igh;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

  /**
   * The list of otp_device references.
   */
  const char **otp_devices;

  /**
   * Length of @e otp_devices.
   */
  unsigned int otp_devices_length;

};


/**
 * Callback for a GET /otp-devices operation.
 *
 * @param cls closure for this function
 * @param tgr response details
 */
static void
get_otp_devices_cb (void *cls,
                  const struct TALER_MERCHANT_OtpDevicesGetResponse *tgr)
{
  struct GetOtpDevicesState *gis = cls;

  gis->igh = NULL;
  if (gis->http_status != tgr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                tgr->hr.http_status,
                (int) tgr->hr.ec,
                TALER_TESTING_interpreter_get_current_label (gis->is));
    TALER_TESTING_interpreter_fail (gis->is);
    return;
  }
  switch (tgr->hr.http_status)
  {
  case MHD_HTTP_OK:
    if (tgr->details.ok.otp_devices_length != gis->otp_devices_length)
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Length of otp_devices found does not match\n");
      TALER_TESTING_interpreter_fail (gis->is);
      return;
    }
    for (unsigned int i = 0; i < gis->otp_devices_length; ++i)
    {
      const struct TALER_TESTING_Command *otp_device_cmd;

      otp_device_cmd = TALER_TESTING_interpreter_lookup_command (
        gis->is,
        gis->otp_devices[i]);

      {
        const char *otp_device_id;

        if (GNUNET_OK !=
            TALER_TESTING_get_trait_otp_id (otp_device_cmd,
                                            &otp_device_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "Could not fetch otp_device id\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
        if (0 != strcmp (tgr->details.ok.otp_devices[i].otp_device_id,
                         otp_device_id))
        {
          GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                      "OtpDevice id does not match\n");
          TALER_TESTING_interpreter_fail (gis->is);
          return;
        }
      }
    }
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_NOT_FOUND:
    /* instance does not exist */
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u (%d).\n",
                tgr->hr.http_status,
                tgr->hr.ec);
    break;
  }
  TALER_TESTING_interpreter_next (gis->is);
}


/**
 * Run the "GET /otp-devices" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
get_otp_devices_run (void *cls,
                     const struct TALER_TESTING_Command *cmd,
                     struct TALER_TESTING_Interpreter *is)
{
  struct GetOtpDevicesState *gis = cls;

  gis->is = is;
  gis->igh = TALER_MERCHANT_otp_devices_get (
    TALER_TESTING_interpreter_get_context (is),
    gis->merchant_url,
    &get_otp_devices_cb,
    gis);
  GNUNET_assert (NULL != gis->igh);
}


/**
 * Free the state of a "GET otp_device" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
get_otp_devices_cleanup (void *cls,
                         const struct TALER_TESTING_Command *cmd)
{
  struct GetOtpDevicesState *gis = cls;

  if (NULL != gis->igh)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "GET /otp-devices operation did not complete\n");
    TALER_MERCHANT_otp_devices_get_cancel (gis->igh);
  }
  GNUNET_array_grow (gis->otp_devices,
                     gis->otp_devices_length,
                     0);
  GNUNET_free (gis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_get_otp_devices (const char *label,
                                            const char *merchant_url,
                                            unsigned int http_status,
                                            ...)
{
  struct GetOtpDevicesState *gis;

  gis = GNUNET_new (struct GetOtpDevicesState);
  gis->merchant_url = merchant_url;
  gis->http_status = http_status;
  {
    const char *clabel;
    va_list ap;

    va_start (ap, http_status);
    while (NULL != (clabel = va_arg (ap, const char *)))
    {
      GNUNET_array_append (gis->otp_devices,
                           gis->otp_devices_length,
                           clabel);
    }
    va_end (ap);
  }
  {
    struct TALER_TESTING_Command cmd = {
      .cls = gis,
      .label = label,
      .run = &get_otp_devices_run,
      .cleanup = &get_otp_devices_cleanup
    };

    return cmd;
  }
}


/* end of testing_api_cmd_get_otp_devices.c */
