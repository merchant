/*
  This file is part of TALER
  Copyright (C) 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing_api_cmd_patch_webhook.c
 * @brief command to test PATCH /webhook
 * @author Priscilla HUANG
 */
#include "platform.h"
#include <taler/taler_exchange_service.h>
#include <taler/taler_testing_lib.h>
#include "taler_merchant_service.h"
#include "taler_merchant_testing_lib.h"


/**
 * State of a "PATCH /webhook" CMD.
 */
struct PatchWebhookState
{

  /**
   * Handle for a "GET webhook" request.
   */
  struct TALER_MERCHANT_WebhookPatchHandle *iph;

  /**
   * The interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Base URL of the merchant serving the request.
   */
  const char *merchant_url;

  /**
   * ID of the webhook to run GET for.
   */
  const char *webhook_id;

  /**
   * event of the webhook
   */
  const char *event_type;

  /**
   * url use by the customer
   */
  const char *url;

  /**
   * http_method use by the merchant
   */
  const char *http_method;

  /**
   * header of the webhook
   */
  const char *header_template;

  /**
   * body_template of the webhook
   */
  const char *body_template;

  /**
   * Expected HTTP response code.
   */
  unsigned int http_status;

};


/**
 * Callback for a PATCH /webhooks/$ID operation.
 *
 * @param cls closure for this function
 * @param hr response being processed
 */
static void
patch_webhook_cb (void *cls,
                  const struct TALER_MERCHANT_HttpResponse *hr)
{
  struct PatchWebhookState *pis = cls;

  pis->iph = NULL;
  if (pis->http_status != hr->http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u (%d) to command %s\n",
                hr->http_status,
                (int) hr->ec,
                TALER_TESTING_interpreter_get_current_label (pis->is));
    TALER_TESTING_interpreter_fail (pis->is);
    return;
  }
  switch (hr->http_status)
  {
  case MHD_HTTP_NO_CONTENT:
    break;
  case MHD_HTTP_UNAUTHORIZED:
    break;
  case MHD_HTTP_FORBIDDEN:
    break;
  case MHD_HTTP_NOT_FOUND:
    break;
  case MHD_HTTP_CONFLICT:
    break;
  default:
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "Unhandled HTTP status %u for PATCH /webhooks/ID.\n",
                hr->http_status);
  }
  TALER_TESTING_interpreter_next (pis->is);
}


/**
 * Run the "PATCH /webhooks/$ID" CMD.
 *
 *
 * @param cls closure.
 * @param cmd command being run now.
 * @param is interpreter state.
 */
static void
patch_webhook_run (void *cls,
                   const struct TALER_TESTING_Command *cmd,
                   struct TALER_TESTING_Interpreter *is)
{
  struct PatchWebhookState *pis = cls;

  pis->is = is;
  pis->iph = TALER_MERCHANT_webhook_patch (
    TALER_TESTING_interpreter_get_context (is),
    pis->merchant_url,
    pis->webhook_id,
    pis->event_type,
    pis->url,
    pis->http_method,
    pis->header_template,
    pis->body_template,
    &patch_webhook_cb,
    pis);
  GNUNET_assert (NULL != pis->iph);
}


/**
 * Offers information from the PATCH /webhooks CMD state to other
 * commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to extract.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
patch_webhook_traits (void *cls,
                      const void **ret,
                      const char *trait,
                      unsigned int index)
{
  struct PatchWebhookState *pws = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_event_type (pws->event_type),
    TALER_TESTING_make_trait_url (pws->url),
    TALER_TESTING_make_trait_http_method (pws->http_method),
    TALER_TESTING_make_trait_header_template (pws->header_template),
    TALER_TESTING_make_trait_body_template (pws->body_template),
    TALER_TESTING_make_trait_webhook_id (pws->webhook_id),
    TALER_TESTING_trait_end (),
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


/**
 * Free the state of a "GET webhook" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd command being run.
 */
static void
patch_webhook_cleanup (void *cls,
                       const struct TALER_TESTING_Command *cmd)
{
  struct PatchWebhookState *pis = cls;

  if (NULL != pis->iph)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "PATCH /webhooks/$ID operation did not complete\n");
    TALER_MERCHANT_webhook_patch_cancel (pis->iph);
  }
  GNUNET_free (pis);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_merchant_patch_webhook (
  const char *label,
  const char *merchant_url,
  const char *webhook_id,
  const char *event_type,
  const char *url,
  const char *http_method,
  const char *header_template,
  const char *body_template,
  unsigned int http_status)
{
  struct PatchWebhookState *pis;

  pis = GNUNET_new (struct PatchWebhookState);
  pis->merchant_url = merchant_url;
  pis->webhook_id = webhook_id;
  pis->http_status = http_status;
  pis->event_type = event_type;
  pis->url = url;
  pis->http_method = http_method;
  pis->header_template = (NULL == header_template) ? NULL : header_template;
  pis->body_template = (NULL == body_template) ? NULL : body_template;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = pis,
      .label = label,
      .run = &patch_webhook_run,
      .cleanup = &patch_webhook_cleanup,
      .traits = &patch_webhook_traits
    };

    return cmd;
  }
}


/* end of testing_api_cmd_patch_webhook.c */
