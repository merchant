#!/bin/bash
# This file is in the public domain.

set -eu

function clean_wallet() {
    rm -f "${WALLET_DB}"
    exit_cleanup
}


# Replace with 0 for nexus...
USE_FAKEBANK=1
if [ 1 = "$USE_FAKEBANK" ]
then
    ACCOUNT="exchange-account-2"
    BANK_FLAGS="-f -d x-taler-bank -u $ACCOUNT"
    BANK_URL="http://localhost:8082/"
else
    ACCOUNT="exchange-account-1"
    BANK_FLAGS="-ns -d iban -u $ACCOUNT"
    BANK_URL="http://localhost:18082/"
    echo -n "Testing for libeufin-bank"
    libeufin-bank --help >/dev/null </dev/null || exit_skip " MISSING"
    echo " FOUND"

fi

. setup.sh

echo -n "Testing for taler-harness"
taler-harness --help >/dev/null </dev/null || exit_skip " MISSING"
echo " FOUND"

# Launch exchange, merchant and bank.
setup -c "test_template.conf" \
      -r "merchant-exchange-default" \
      -em \
      $BANK_FLAGS
LAST_RESPONSE=$(mktemp -p "${TMPDIR:-/tmp}" test_response.conf-XXXXXX)
CONF="test_template.conf.edited"
WALLET_DB=$(mktemp -p "${TMPDIR:-/tmp}" test_wallet.json-XXXXXX)
EXCHANGE_URL="http://localhost:8081/"

# Install cleanup handler (except for kill -9)
trap clean_wallet EXIT

echo -n "First prepare wallet with coins ..."
rm -f "$WALLET_DB"
taler-wallet-cli \
    --no-throttle \
    --wallet-db="$WALLET_DB" \
    api \
    --expect-success 'withdrawTestBalance' \
  "$(jq -n '
    {
        amount: "TESTKUDOS:99",
        corebankApiBaseUrl: $BANK_URL,
        exchangeBaseUrl: $EXCHANGE_URL
    }' \
    --arg BANK_URL "${BANK_URL}" \
    --arg EXCHANGE_URL "$EXCHANGE_URL"
  )" 2>wallet-withdraw-1.err >wallet-withdraw-1.out
echo -n "."
# FIXME-MS: add logic to have nexus check immediately here.
# sleep 10
echo -n "."
# NOTE: once libeufin can do long-polling, we should
# be able to reduce the delay here and run wirewatch
# always in the background via setup
taler-exchange-wirewatch \
    -a "$ACCOUNT" \
    -L "INFO" \
    -c "$CONF" \
    -t &> taler-exchange-wirewatch.out
echo -n "."
taler-wallet-cli \
    --wallet-db="$WALLET_DB" \
    run-until-done \
    2>wallet-withdraw-finish-1.err \
    >wallet-withdraw-finish-1.out
echo " OK"

CURRENCY_COUNT=$(taler-wallet-cli --wallet-db="$WALLET_DB" balance | jq '.balances|length')
if [ "$CURRENCY_COUNT" = "0" ]
then
    exit_fail "Expected least one currency, withdrawal failed. check log."
fi

#
# CREATE INSTANCE FOR TESTING
#


echo -n "Configuring merchant instance ..."

STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/management/instances \
    -d '{"auth":{"method":"external"},"id":"default","name":"default","user_type":"business","address":{},"jurisdiction":{},"use_stefan":true,"default_wire_transfer_delay":{"d_us" : 50000000000},"default_pay_delay":{"d_us": 60000000000}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected '204 No content' response. Got instead $STATUS"
fi
echo "Ok"

echo -n "Configuring merchant account ..."

if [ 1 = "$USE_FAKEBANK" ]
then
    FORTYTHREE="payto://x-taler-bank/localhost/fortythree?receiver-name=fortythree"
else
    FORTYTHREE=$(get_payto_uri fortythree x)
fi
# create with 2 bank account addresses
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"'"$FORTYTHREE"'"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected '200 OK' response. Got instead $STATUS"
fi
STATUS=$(curl -H "Content-Type: application/json" -X POST \
    -H 'Authorization: Bearer secret-token:super_secret' \
    http://localhost:9966/private/accounts \
    -d '{"payto_uri":"payto://iban/SANDBOXX/DE270744?receiver-name=Forty+Four"}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "200" ]
then
    exit_fail "Expected '200 OK' response. Got instead $STATUS"
fi

echo "Ok"


echo -n "Get accounts..."
STATUS=$(curl http://localhost:9966/private/accounts \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")
PAY_URI=$(jq -r .accounts[1].payto_uri < "$LAST_RESPONSE")
H_WIRE=$(jq -r .accounts[1].h_wire < "$LAST_RESPONSE")
if [ "$PAY_URI" != "payto://iban/SANDBOXX/DE270744?receiver-name=Forty+Four" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected second payto URI. Got $PAY_URI"
fi
echo "OK"

# remove one account address
echo -n "Deleting one account ..."
STATUS=$(curl -H "Content-Type: application/json" -X PATCH \
    -H 'Authorization: Bearer secret-token:super_secret' \
    "http://localhost:9966/private/accounts/${H_WIRE}" \
    -X DELETE \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected '204 No content' for deletion of ${H_WIRE}. Got instead: $STATUS"
fi
echo "OK"

RANDOM_IMG='data:image/png;base64,abcdefg'

#
# CREATE AN ORDER WITHOUT TOKEN
#

echo -n "Creating order without TOKEN..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"create_token":false,"order":{"amount":"TESTKUDOS:7","summary":"3","products":[{"description":"desct","image":"'"$RANDOM_IMG"'","price":"TESTKUDOS:1","taxes":[],"unit":"u","quantity":1}]}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -r .token < "$LAST_RESPONSE")

if [ "$TOKEN" != "null" ]
then
    exit_fail "token should be null, got: $TOKEN"
fi

echo "OK"

echo -n "Checking created order without TOKEN..."
STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID" \
              -w "%{http_code}" -s -o "$LAST_RESPONSE")
PAY_URI=$(jq -r .taler_pay_uri < "$LAST_RESPONSE")
if [ "$PAY_URI" == "null" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected non-NULL payuri. got $PAY_URI"
fi
echo "OK"

#
# CREATE AN ORDER WITHOUT TOKEN WITH FULLFILMENT URL
#

echo -n "Creating order without TOKEN and fullfilment URL..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"create_token":false,"order":{"fulfillment_url":"go_here_please", "amount":"TESTKUDOS:7","summary":"3","products":[{"description":"desct","image":"'"$RANDOM_IMG"'","price":"TESTKUDOS:1","taxes":[],"unit":"u","quantity":1}]}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -r .token < "$LAST_RESPONSE")

if [ "$TOKEN" != "null" ]
then
    exit_fail "Token should be null, got: $TOKEN"
fi

STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

PAY_URI=$(jq -r .taler_pay_uri < "$LAST_RESPONSE")
FULLFILMENT_URL=$(jq -r .fulfillment_url < "$LAST_RESPONSE")

if [ "$FULLFILMENT_URL" != "go_here_please" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected a pay URI. got: $PAY_URI"
fi

if [ "$PAY_URI" == "null" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected non-NULL pay URI. Got: $PAY_URI"
fi
echo "OK"

#
# CREATE A DISCOUNT TOKEN FAMILY
#
echo -n "Creating discount token family..."
VALID_AFTER="{\"t_s\": $(date +%s)}" # now
VALID_BEFORE="{\"t_s\": $(date +%s -d "+30 days")}" # 30 days from now
DURATION="{\"d_us\": $(expr 30 \* 24 \* 60 \* 60 \* 1000000)}" # 30 days
STATUS=$(curl 'http://localhost:9966/private/tokenfamilies' \
              -d "{\"kind\": \"discount\", \"slug\":\"test-discount\", \"name\": \"Test discount\", \"description\": \"Less money $$\", \"description_i18n\": {\"en\": \"Less money $$\", \"es\": \"Menos dinero $$\"}, \"valid_after\": $VALID_AFTER, \"valid_before\": $VALID_BEFORE, \"duration\": $DURATION, \"validity_granularity\": $DURATION}" \
              -w "%{http_code}" -s -o /dev/null)
if [ "$STATUS" != "204" ]
then
    exit_fail "Expected '204 OK' response. Got instead $STATUS"
fi
echo "Ok"

#
# CREATE A SUBSCRIPTION TOKEN FAMILY
#
echo -n "Creating subscription token family..."
VALID_AFTER="{\"t_s\": $(date +%s)}" # now
VALID_BEFORE="{\"t_s\": $(date +%s -d "+30 days")}" # 30 days from now
DURATION="{\"d_us\": $(expr 30 \* 24 \* 60 \* 60 \* 1000000)}" # 30 days
STATUS=$(curl 'http://localhost:9966/private/tokenfamilies' \
              -d "{\"kind\": \"subscription\", \"slug\":\"test-subscription\", \"name\": \"Test subscription\", \"description\": \"Money per month\", \"description_i18n\": {\"en\": \"Money $$$ per month\", \"es\": \"Dinero $$$ al mes\"}, \"valid_after\": $VALID_AFTER, \"valid_before\": $VALID_BEFORE, \"duration\": $DURATION, \"validity_granularity\": $DURATION}" \
              -w "%{http_code}" -s -o /dev/null)
if [ "$STATUS" != "204" ]
then
    exit_fail "Expected '204 OK' response. Got instead $STATUS"
fi
echo "Ok"

#
# CREATE AN DISCOUNTABLE ORDER WITHOUT TOKEN
#
RANDOM_IMG='data:image/png;base64,abcdefg'

echo -n "Creating discountable order..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"create_token":true,"order":{"version":1,"summary":"Expensive purchase","products":[{"description":"Expensive steak","quantity":2,"unit":"pieces","price":"TESTKUDOS:100"}],"choices":[{"amount":"TESTKUDOS:100"},{"amount":"TESTKUDOS:10","inputs":[{"type":"token","token_family_slug":"test-discount","count":1}],"outputs":[]}]}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order created. got: $STATUS"
fi
echo "OK"

ORDER_ID=$(jq -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -r .token < "$LAST_RESPONSE")

echo -n "Checking created order..."
STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID?token=$TOKEN" \
              -w "%{http_code}" -s -o "$LAST_RESPONSE")
PAY_URI=$(jq -r .taler_pay_uri < "$LAST_RESPONSE")
if [ "$PAY_URI" == "null" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected non-NULL payuri. got $PAY_URI"
fi
echo "OK"

echo -n "Claming order with token family ..."
STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID"/claim \
    -d '{"nonce":"","token":"'"$TOKEN"'"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order claimed. got: $STATUS"
fi

echo " OK"

# echo -n "Fetching pay URL for order ..."
# STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
#     -w "%{http_code}" -s -o "$LAST_RESPONSE")

# if [ "$STATUS" != "200" ]
# then
#     jq . < "$LAST_RESPONSE"
#     exit_fail "Expected 200, getting order info before claming it. got: $STATUS"
# fi

# PAY_URL=$(jq -e -r .taler_pay_uri < "$LAST_RESPONSE")

# echo " OK"

# NOW=$(date +%s)

# echo -n "Pay for order ${PAY_URL} ..."
# taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log
# taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" run-until-done 2> wallet-finish-pay1.err > wallet-finish-pay1.log
# NOW2=$(date +%s)
# echo " OK (took $(( NOW2 - NOW )) secs )"

#
# CREATE ORDER WITH NON-INVENTORY AND CHECK
#

echo -n "Creating order with non-inventory products..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:7","summary":"3","products":[{"description":"desct","image":"'"$RANDOM_IMG"'","price":"TESTKUDOS:1","taxes":[],"unit":"u","quantity":1}]}}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -r .token < "$LAST_RESPONSE")

STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID"/claim \
    -d '{"nonce":"","token":"'"$TOKEN"'"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    cat "$LAST_RESPONSE" >&2
    exit_fail "Expected 200, order claimed. got: $STATUS"
fi

QUANTITY=$(jq -r .contract_terms.products[0].quantity < "$LAST_RESPONSE")
if [ "$QUANTITY" != "1" ]
then
    exit_fail "Expected quantity 1. got: $QUANTITY"
fi

IMAGE=$(jq -r .contract_terms.products[0].image < "$LAST_RESPONSE")
if [ "$IMAGE" != "$RANDOM_IMG" ]
then
    exit_fail "Expected $RANDOM_IMG but got something else: $IMAGE"
fi
echo "OK"


#
# CREATE INVENTORY PRODUCT AND CLAIM IT
#

echo -n "Creating product..."
STATUS=$(curl 'http://localhost:9966/private/products' \
    -d '{"product_id":"2","description":"product with id 2 and price :15","price":"TESTKUDOS:15","total_stock":2,"description_i18n":{},"unit":"","image":"'$RANDOM_IMG'","taxes":[],"address":{},"next_restock":{"t_s":"never"}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204, product created. got: $STATUS"
fi
echo "OK"

echo -n "Creating order with inventory products..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:7","summary":"3"},"inventory_products":[{"product_id":"2","quantity":1}]}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")


if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200 OK, order created response. Got: $STATUS"
fi

ORDER_ID=$(jq -e -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -e -r .token < "$LAST_RESPONSE")

STATUS=$(curl http://localhost:9966/orders/"$ORDER_ID"/claim \
    -d '{"nonce":"","token":"'"$TOKEN"'"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, order claimed. got: $STATUS"
fi

QUANTITY=$(jq -r .contract_terms.products[0].quantity < "$LAST_RESPONSE")

if [ "$QUANTITY" != "1" ]
then
    exit_fail "Expected quantity 1. got: $QUANTITY"
fi

echo "OK"

#
# Create product in another currency
#


STATUS=$(curl 'http://localhost:9966/private/products' \
    -d '{"product_id":"1","description":"product with id 1 and price :15","price":"USD:15","total_stock":1,"description_i18n":{},"unit":"","image":"","taxes":[],"address":{},"next_restock":{"t_s":"never"}}' \
    -w "%{http_code}" -s -o /dev/null)

if [ "$STATUS" != "204" ]
then
    exit_fail "Expected 204 no content. got: $STATUS"
fi

#
# CREATE ORDER AND SELL IT
#

echo -n "Creating order to be paid..."
STATUS=$(curl 'http://localhost:9966/private/orders' \
    -d '{"order":{"amount":"TESTKUDOS:1","summary":"payme"},"inventory_products":[{"product_id":"2","quantity":1}]}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, order created. got: $STATUS"
fi

ORDER_ID=$(jq -e -r .order_id < "$LAST_RESPONSE")
TOKEN=$(jq -e -r .token < "$LAST_RESPONSE")

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, getting order info before claming it. got: $STATUS"
fi

PAY_URL=$(jq -e -r .taler_pay_uri < "$LAST_RESPONSE")

echo "OK"

NOW=$(date +%s)

echo -n "Pay first order ${PAY_URL} ..."
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" handle-uri "${PAY_URL}" -y 2> wallet-pay1.err > wallet-pay1.log
taler-wallet-cli --no-throttle --wallet-db="$WALLET_DB" run-until-done 2> wallet-finish-pay1.err > wallet-finish-pay1.log
NOW2=$(date +%s)
echo " OK (took $(( NOW2 - NOW )) secs )"

STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, after pay. got: $STATUS"
fi

ORDER_STATUS=$(jq -r .order_status < "$LAST_RESPONSE")

if [ "$ORDER_STATUS" != "paid" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Order status should be 'paid'. got: $ORDER_STATUS"
fi

#
# WIRE TRANSFER TO MERCHANT AND NOTIFY BACKEND
#

# PAY_DEADLINE=$(jq -r .contract_terms.pay_deadline.t_s < "$LAST_RESPONSE")
WIRE_DEADLINE=$(jq -r .contract_terms.wire_transfer_deadline.t_s < "$LAST_RESPONSE")

NOW=$(date +%s)

TO_SLEEP=$(( WIRE_DEADLINE - NOW ))
echo "Waiting $TO_SLEEP secs for wire transfer"

echo -n "Perform wire transfers ..."
taler-exchange-aggregator -y -c "$CONF" -T "${TO_SLEEP}"000000 -t -L INFO &> aggregator.log
taler-exchange-transfer -c "$CONF" -t -L INFO &> transfer.log
echo " DONE"
echo -n "Give time to Nexus to route the payment to Sandbox..."
# FIXME-MS: trigger immediate update at nexus
# NOTE: once libeufin can do long-polling, we should
# be able to reduce the delay here and run aggregator/transfer
# always in the background via setup
sleep 3
echo " DONE"

echo -n "Obtaining wire transfer details from bank ($USE_FAKEBANK)..."

BANKDATA="$(curl 'http://localhost:8082/accounts/exchange/taler-wire-gateway/history/outgoing?delta=1' -s)"
WTID=$(echo "$BANKDATA" | jq -r .outgoing_transactions[0].wtid)
WURL=$(echo "$BANKDATA" | jq -r .outgoing_transactions[0].exchange_base_url)
CREDIT_AMOUNT=$(echo "$BANKDATA" | jq -r .outgoing_transactions[0].amount)
TARGET_PAYTO=$(echo "$BANKDATA" | jq -r .outgoing_transactions[0].credit_account)

if [ "$EXCHANGE_URL" != "$WURL" ]
then
    exit_fail "Wrong exchange URL in '$BANKDATA' response, expected '$EXCHANGE_URL'"
fi

echo " OK"

set +e

echo -n "Notifying merchant of bogus wire transfer ..."

STATUS=$(curl 'http://localhost:9966/private/transfers' \
    -d '{"credit_amount":"'"$CREDIT_AMOUNT"'1","wtid":"'"$WTID"'","payto_uri":"'"$TARGET_PAYTO"'","exchange_url":"'"$WURL"'"}' \
    -m 3 \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "204" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected to fail since the amount is not valid. got: $STATUS"
fi

echo "OK"

echo -n "Notifying merchant of correct wire transfer (conflicting with old data)..."

STATUS=$(curl 'http://localhost:9966/private/transfers' \
    -d '{"credit_amount":"'"$CREDIT_AMOUNT"'","wtid":"'"$WTID"'","payto_uri":"'"$TARGET_PAYTO"'","exchange_url":"'"$WURL"'"}' \
    -m 3 \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "409" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response conflict, after providing conflicting transfer data. got: $STATUS"
fi

echo " OK"

echo -n "Deleting bogus wire transfer ..."

TID=$(curl -s http://localhost:9966/private/transfers | jq -r .transfers[0].transfer_serial_id)
STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/private/transfers/$TID" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "204" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response 204 No Content, after deleting valid TID. got: $STATUS"
fi

STATUS=$(curl -H "Content-Type: application/json" -X DELETE \
    "http://localhost:9966/private/transfers/$TID" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")
if [ "$STATUS" != "404" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response 404 Not found, after deleting TID again. got: $STATUS"
fi

echo " OK"

echo -n "Notifying merchant of correct wire transfer (now working)..."

STATUS=$(curl 'http://localhost:9966/private/transfers' \
    -d '{"credit_amount":"'"$CREDIT_AMOUNT"'","wtid":"'"$WTID"'","payto_uri":"'"$TARGET_PAYTO"'","exchange_url":"'"$WURL"'"}' \
    -m 3 \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "204" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response 204 No content, after providing transfer data. got: $STATUS"
fi

echo " OK"

echo -n "Testing idempotence ..."
set -e


# Test idempotence: do it again!

STATUS=$(curl 'http://localhost:9966/private/transfers' \
    -d '{"credit_amount":"'"$CREDIT_AMOUNT"'","wtid":"'"$WTID"'","payto_uri":"'"$TARGET_PAYTO"'","exchange_url":"'"$WURL"'"}' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "204" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response No Content, after providing transfer data. got: $STATUS"
fi

echo " OK"
echo -n "Testing taler-merchant-reconciliation ..."
set -e
taler-merchant-reconciliation -L INFO -c "$CONF" -t &> taler-merchant-reconciliation.log
echo " OK"


echo -n "Fetching wire transfers ..."

STATUS=$(curl 'http://localhost:9966/private/transfers' \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")

if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected response 200 Ok. got: $STATUS"
fi

TRANSFERS_LIST_SIZE=$(jq -r '.transfers | length' < "$LAST_RESPONSE")

if [ "$TRANSFERS_LIST_SIZE" != "1" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 1 entry in transfer list. Got: $TRANSFERS_LIST_SIZE"
fi

echo "OK"

echo -n "Checking order status ..."
STATUS=$(curl "http://localhost:9966/private/orders/${ORDER_ID}?transfer=YES" \
    -w "%{http_code}" -s -o "$LAST_RESPONSE")
if [ "$STATUS" != "200" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected 200, after order inquiry. got: $STATUS"
fi
DEPOSIT_TOTAL=$(jq -r .deposit_total < "$LAST_RESPONSE")
if [ "$DEPOSIT_TOTAL" == "TESTKUDOS:0" ]
then
    jq . < "$LAST_RESPONSE"
    exit_fail "Expected non-zero deposit total. got: $DEPOSIT_TOTAL"
fi
echo " OK"

echo -n "Checking bank account status ..."
if [ 1 = "$USE_FAKEBANK" ]
then
    STATUS=$(curl "http://localhost:8082/accounts/fortythree" \
                  -w "%{http_code}" \
                  -s \
                  -o "$LAST_RESPONSE")
    if [ "$STATUS" != "200" ]
    then
        jq . < "$LAST_RESPONSE"
        exit_fail "Expected response 200 Ok, getting account status. Got: $STATUS"
    fi
    BALANCE=$(jq -r .balance.amount < "$LAST_RESPONSE")
    if [ "$BALANCE" == "TESTKUDOS:0" ]
    then
        jq . < "$LAST_RESPONSE"
        exit_fail "Wire transfer did not happen. Got: $BALANCE"
    fi
else
    ACCOUNT_PASSWORD="fortythree:x"
    BANK_HOST="localhost:18082"
    STATUS=$(curl "http://$ACCOUNT_PASSWORD@$BANK_HOST/accounts/fortythree" \
                  -w "%{http_code}" -s -o "$LAST_RESPONSE")
    if [ "$STATUS" != "200" ]
    then
        jq . < "$LAST_RESPONSE"
        exit_fail "Expected response 200 Ok, getting account status. Got: $STATUS"
    fi
    BALANCE=$(jq -r .balance.amount < "$LAST_RESPONSE")
    if [ "$BALANCE" == "TESTKUDOS:0" ]
    then
        jq . < "$LAST_RESPONSE"
        exit_fail "Wire transfer did not happen. Got: $BALANCE"
    fi
fi
echo " OK"

echo -n "Getting information about kyc ..."
STATUS=$(curl -H "Content-Type: application/json" -X GET \
    http://localhost:9966/private/kyc \
    -w "%{http_code}" -s -o /dev/null)
if [ "$STATUS" != "200" ]
then
    exit_fail "Expected 200. Got: $STATUS"
fi
echo " OK"

exit 0
