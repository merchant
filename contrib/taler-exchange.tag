<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>taler_exchange_service.h</name>
    <path></path>
    <filename>taler_exchange_service.h</filename>
    <member kind="function">
      <type>struct TALER_EXCHANGE_Keys *</type>
      <name>TALER_EXCHANGE_get_keys</name>
      <anchorfile>taler_exchange_service.h</anchorfile>
      <arglist>(struct TALER_EXCHANGE_Handle *exchange)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_mhd_lib.h</name>
    <path></path>
    <filename>taler_mhd_lib.h</filename>
    <member kind="function">
      <type>enum GNUNET_GenericReturnValue</type>
      <name>TALER_MHD_parse_post_json</name>
      <anchorfile>taler_mhd_lib.h</anchorfile>
      <arglist>
        (struct MHD_Connection *connection,
         void **con_cls,
         const char *upload_data,
         size_t *upload_data_size,
         json_t **json)
      </arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_NONE</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_INVALID</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_INVALID_RESPONSE</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_MERCHANT_PRIVATE_POST_REWARD_AUTHORIZE_RESERVE_EXPIRED</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_MERCHANT_PRIVATE_POST_REWARD_AUTHORIZE_RESERVE_NOT_FOUND</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_MERCHANT_PRIVATE_POST_REWARD_AUTHORIZE_INSUFFICIENT_FUNDS</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_DB_START_FAILED</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_DB_FETCH_FAILED</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_DB_STORE_FAILED</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_DB_INVARIANT_FAILURE</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_error_codes.h</name>
    <path></path>
    <filename>taler_error_codes.h</filename>
    <member kind="function">
      <type>enum TALER_ErrorCode</type>
      <name>TALER_EC_GENERIC_DB_SOFT_FAILURE</name>
      <anchorfile>taler_error_codes.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_INSTANCE_SETTINGS</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_REWARD_PICKUP</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_ORDER_PAID</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_REFUND_OBTAINED</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_ORDER_REFUND</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_ORDERS_CHANGE</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
  <compound kind="file">
    <name>taler_dbevents.h</name>
    <path></path>
    <filename>taler_dbevents.h</filename>
    <member kind="function">
      <type>#define</type>
      <name>TALER_DBEVENT_MERCHANT_SESSION_CAPTURED</name>
      <anchorfile>taler_dbevents.h</anchorfile>
    </member>
  </compound>
</tagfile>
