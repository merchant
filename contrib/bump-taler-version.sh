#!/usr/bin/env bash
set -eu

if [ $# != 1 ]; then
    >&2 echo "Illegal number of arguments"
    >&2 echo "Usage: $0 <version>"
    exit -1
fi

VERSION="$1"
DATE="$(date -R)"
GIT_USER="$(git config user.name)"
GIT_EMAIL="$(git config user.email)"

function updated {
    local FILE=$1
    if [[ $(grep "${VERSION}" "${FILE}") ]]; then
        echo "${FILE} already in ${VERSION}"
        return -1
    fi
}

# update configure.ac
function configure_ac {
    updated configure.ac || return 0

    sed -i "/AC_INIT/s/,\\[\\(.*\\)\\],/,[${VERSION}],/" configure.ac
    echo "configure.ac ${VERSION}"
}

# update debian/changelog
function debian_changelog {
    updated debian/changelog || return 0

    cat <<EOF > ./debian/changelog.tmp
taler-merchant (${VERSION}) unstable; urgency=low

  * Release ${VERSION}.

 -- ${GIT_USER} <${GIT_EMAIL}>  ${DATE}

EOF
    cat ./debian/changelog >> ./debian/changelog.tmp
    mv ./debian/changelog.tmp ./debian/changelog
    echo "debian/changelog ${VERSION}"
}

function doc_doxygen_taler_doxy {
    updated doc/doxygen/taler.doxy || return 0

    sed -i "/PROJECT_NUMBER/s/= \(.*\)/= ${VERSION}/" doc/doxygen/taler.doxy
    echo "doc/doxygen/taler.doxy ${VERSION}"
}

configure_ac
debian_changelog
doc_doxygen_taler_doxy
