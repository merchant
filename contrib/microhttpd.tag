<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>microhttpd.h</name>
    <path></path>
    <filename>microhttpd.h</filename>
    <member kind="function">
      <type>enum MHD_Result</type>
      <name>MHD_run</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist>(struct MHD_Daemon *daemon)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>MHD_get_connection_values</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist>(struct MHD_Connection *connection,
                enum MHD_ValueKind kind,
                MHD_KeyValueIterator iterator,
                void *iterator_cls)
      </arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_YES</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_NO</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_OK</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_ACCEPTED</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_BAD_GATEWAY</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_GATEWAY_TIMEOUT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_GONE</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_CONFLICT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_FAILED_DEPENDENCY</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_SERVICE_UNAVAILABLE</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_NOT_FOUND</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_NO_CONTENT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_GET</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_PATCH</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_PUT</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_DELETE</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_METHOD_POST</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_HTTP_VERSION_1_1</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MHD_OPTION_NOTIFY_COMPLETED</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>int</type>
      <name>MHD_AccessHandlerCallback</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist>)(void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls)</arglist>
    </member>
    <member kind="typedef">
      <type>void</type>
      <name>MHD_RequestCompletedCallback</name>
      <anchorfile>microhttpd.h</anchorfile>
      <arglist>)(void *cls, struct MHD_Connection *connection, void **con_cls, enum MHD_RequestTerminationCode toe)</arglist>
    </member>
  </compound>
</tagfile>
