FROM docker.io/library/debian:bookworm

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -yqq && \
    apt-get install -yqq \
                   git \
		   autoconf \
		   libjansson-dev \
		   libgcrypt-dev \
		   libqrencode-dev \
		   libpq-dev \
		   pkg-config \
                   libtool \
                   recutils \
		   make \
		   postgresql \
                   python3-pip \
		   python3-sphinx \
		   python3-sphinx-rtd-theme \
                   texinfo \
		   autopoint \
		   curl \
                   libcurl4-gnutls-dev \
                   libsodium-dev \
		   libidn11-dev \
		   zlib1g-dev \
		   libunistring-dev

# Debian packaging tools
RUN apt-get install -yqq \
                   po-debconf \
                   build-essential \
                   debhelper-compat \
                   devscripts \
		   git-buildpackage

RUN pip3 install --break-system-packages requests click poetry uwsgi htmlark

# Install docs generation utils
RUN apt-get update -yqq && \
    apt-get install -yqq \
                   graphviz \
		   doxygen \
   && rm -rf /var/lib/apt/lists/*

# Install Taler (and friends) packages
RUN curl -sS https://deb.taler.net/apt-nightly/taler-bookworm-ci.sources \
    | tee /etc/apt/sources.list.d/taler-bookworm-ci.sources

RUN echo '\
Package: * \n\
Pin: origin "deb.taler.net" \n\
Pin-Priority: 999' > /etc/apt/preferences.d/taler

RUN cat /etc/apt/preferences.d/taler && \
    apt-get update -y && \
    apt-get install -y \
                   libgnunet-dev \
                   libgnunet \
		   libtalerexchange-dev \
		   libtalerexchange \
		   taler-exchange \
		   taler-exchange-database \
		   taler-exchange-offline \
		   taler-auditor \
		   taler-wallet-cli \
		   taler-harness \
&& rm -rf /var/lib/apt/lists/*

RUN apt-get update -yqq && \
    apt-get install -yqq \
                   jq

WORKDIR /workdir
